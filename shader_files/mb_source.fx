///////////////////////////////////////////////////////////////////////////////////
//
// Mount&Blade Warband Shaders
// You can add edit main shaders and lighting system with this file.
// You cannot change fx_configuration.h file since it holds application dependent
// configration parameters. Sorry its not well documented.
// Please send your feedbacks to our forums.
//
// All rights reserved.
// www.taleworlds.com
//
///////////////////////////////////////////////////////////////////////////////////
// compile_fx.bat:
// ------------------------------
// @echo off
// fxc /D PS_2_X=ps_2_a /T fx_2_0 /Fo mb_2a.fxo mb.fx
// pause>nul
///////////////////////////////////////////////////////////////////////////////////
#if !defined (PS_2_X)
	#error "define high quality shader profile: PS_2_X ( ps_2_b or ps_2_a )"
#endif
#if !defined (VS_X)
	#error "define high quality shader profile: VS_X ( vs_2_a )"
#endif
#include "fx_configuration.h"	// source code dependent configration definitions..

////////////////////////////////////////////////////////////////////////////////
//definitions:
#define NUM_LIGHTS					10
#define NUM_SIMUL_LIGHTS			4
#define NUM_WORLD_MATRICES			32

#define PCF_NONE					0
#define PCF_DEFAULT					1
#define PCF_NVIDIA					2


#define INCLUDE_VERTEX_LIGHTING
#define VERTEX_LIGHTING_SCALER   1.0f	//used for diffuse calculation
#define VERTEX_LIGHTING_SPECULAR_SCALER   1.0f

// #define USE_PRECOMPILED_SHADER_LISTS


//put this to un-reachable code blocks..
#define GIVE_ERROR_HERE {for(int i = 0; i < 1000; i++)		{Output.RGBColor *= Output.RGBColor;}}
#define GIVE_ERROR_HERE_VS {for(int i = 0; i < 1000; i++)		{Out.Pos *= Out.Pos;}}

#ifdef DONT_INIT_OUTPUTS
	#pragma warning( disable : 4000)
	#define INITIALIZE_OUTPUT(structure, var)	structure var;
#else
	#define INITIALIZE_OUTPUT(structure, var)	structure var = (structure)0;
#endif

#pragma warning( disable : 3571)	//pow(f,e)


//Categories..
#define OUTPUT_STRUCTURES
#define FUNCTIONS

//Constant categories
#define PER_MESH_CONSTANTS
#define PER_FRAME_CONSTANTS
#define PER_SCENE_CONSTANTS
#define APPLICATION_CONSTANTS

//Shader categories
#define MISC_SHADERS
#define UI_SHADERS
#define SHADOW_RELATED_SHADERS
#define WATER_SHADERS
#define SKYBOX_SHADERS
#define HAIR_SHADERS
#define FACE_SHADERS
#define FLORA_SHADERS
#define MAP_SHADERS
#define SOFT_PARTICLE_SHADERS
#define STANDART_SHADERS
#define STANDART_RELATED_SHADER
#define OCEAN_SHADERS
#ifdef USE_NEW_TREE_SYSTEM
#define NEWTREE_SHADERS
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef PER_MESH_CONSTANTS
	float4x4 matWorldViewProj;
	float4x4 matWorldView;
	float4x4 matWorld;

	float4x4 matWaterWorldViewProj;
	float4x4 matWorldArray[NUM_WORLD_MATRICES] : WORLDMATRIXARRAY;
	//float4   matBoneOriginArray[NUM_WORLD_MATRICES];

	float4 vMaterialColor = float4(255.f/255.f, 230.f/255.f, 200.f/255.f, 1.0f);
	float4 vMaterialColor2;
	float fMaterialPower = 16.f;
	float4 vSpecularColor = float4(5, 5, 5, 5);
	float4 texture_offset = {0,0,0,0};

	int iLightPointCount;
	int	   iLightIndices[NUM_SIMUL_LIGHTS] = { 0, 1, 2, 3 };

	bool bUseMotionBlur = false;
	float4x4 matMotionBlur;
#endif

////////////////////////////////////////
#ifdef PER_FRAME_CONSTANTS
	float time_var = 0.0f;
	float4x4 matWaterViewProj;
	float morale = 1.0f;
#endif

////////////////////////////////////////
#ifdef PER_SCENE_CONSTANTS
	float vCloudAmount = 0.0f;
	float3 vSkyDir = float3(0.0f, 0.0f, -1.0f);
	float4 vLightning = float4(0, 0, 0, 0);
	
	float fFogDensity = 0.05f;

	float3 vSkyLightDir;
	float4 vSkyLightColor;
	float3 vSunDir;
	float4 vSunColor;
	float4 vWindVector = float4(0.0f,0.0f,0.0f,0.0f);// usage wind_x, wind_y, wind_z, total_wind

	float4 vAmbientColor = float4(64.f/255.f, 64.f/255.f, 64.f/255.f, 1.0f);
	float4 vGroundAmbientColor = float4(84.f/255.f, 44.f/255.f, 54.f/255.f, 1.0f);

	float4 vCameraPos;
	float4x4 matSunViewProj;
	float4x4 matView;
	float4x4 matViewProj;

	float3 vLightPosDir[NUM_LIGHTS];
	float4 vLightDiffuse[NUM_LIGHTS];
	float4 vPointLightColor;	//agerage color of lights

	float reflection_factor;
#endif

////////////////////////////////////////
#ifdef APPLICATION_CONSTANTS
	bool use_depth_effects = false;
	float far_clip_Inv;
	float4 vDepthRT_HalfPixel_ViewportSizeInv;
	
	static const float lightning_radius = 250;

	static const float sun_power = 64.0;
	static const float sky_power = 4.0;

	float fShadowMapNextPixel = 1.0f / 4096;
	float fShadowMapSize = 4096;

	static const float input_gamma = 2.2f;
	float4 output_gamma = float4(2.2f, 2.2f, 2.2f, 2.2f);			//STR: float4 yapyldy
	float4 output_gamma_inv = float4(1.0f / 2.2f, 1.0f / 2.2f, 1.0f / 2.2f, 1.0f / 2.2f);

	float4 debug_vector = {0,0,0,1};

	// float spec_coef = 0.1f;	//valid value after module_data!
	float spec_coef = 1.0f;	//valid value after module_data!


	static const float map_normal_detail_factor = 1.4f;
	static const float uv_2_scale = 1.237;
	static const float fShadowBias = 0.00002f;//-0.000002f;

	#ifdef USE_NEW_TREE_SYSTEM
		float flora_detail = 40.0f;
		#define flora_detail_fade 		(flora_detail*FLORA_DETAIL_FADE_MUL)
		#define flora_detail_fade_inv 	(flora_detail-flora_detail_fade)
		// #define flora_detail_clip 		(max(0,flora_detail_fade + 20.0f))
		#define flora_detail_clip (100.0f * FLORA_DETAIL_FADE_MUL)
	#endif

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Texture&Samplers
texture postFX_texture1;

#if defined(USE_SHARED_DIFFUSE_MAP) || !defined(USE_DEVICE_TEXTURE_ASSIGN)
	texture diffuse_texture;
#endif

#ifndef USE_DEVICE_TEXTURE_ASSIGN
	texture diffuse_texture_2;
	texture specular_texture;
	texture normal_texture;
	texture env_texture;
	texture shadowmap_texture;

	texture cubic_texture;

	texture depth_texture;
	texture screen_texture;

	#ifdef USE_REGISTERED_SAMPLERS
	sampler ReflectionTextureSampler 	: register(fx_ReflectionTextureSampler_RegisterS 		) = sampler_state	{  Texture = env_texture;		};
	sampler PostFXTextureSampler			: register(fx_EnvTextureSampler_RegisterS				) = sampler_state	{  Texture = postFX_texture1;		};
	sampler Diffuse2Sampler 			: register(fx_Diffuse2Sampler_RegisterS 				) = sampler_state	{  Texture = diffuse_texture_2;	};
	sampler NormalTextureSampler		: register(fx_NormalTextureSampler_RegisterS			) = sampler_state	{  Texture = normal_texture;	};
	sampler SpecularTextureSampler 		: register(fx_SpecularTextureSampler_RegisterS 			) = sampler_state	{  Texture = specular_texture;	};
	sampler DepthTextureSampler 		: register(fx_DepthTextureSampler_RegisterS 			) = sampler_state	{  Texture = depth_texture;	    };
	sampler CubicTextureSampler 		: register(fx_CubicTextureSampler_RegisterS 			) = sampler_state	{  Texture = cubic_texture;	    };
	sampler ShadowmapTextureSampler 	: register(fx_ShadowmapTextureSampler_RegisterS 		) = sampler_state	{  Texture = shadowmap_texture;	};
	sampler ScreenTextureSampler 		: register(fx_ScreenTextureSampler_RegisterS			) = sampler_state	{  Texture = screen_texture;	};
	sampler MeshTextureSampler 			: register(fx_MeshTextureSampler_RegisterS 				) = sampler_state	{  Texture = diffuse_texture;	};
	sampler ClampedTextureSampler 		: register(fx_ClampedTextureSampler_RegisterS 			) = sampler_state	{  Texture = diffuse_texture;	};
	sampler FontTextureSampler 			: register(fx_FontTextureSampler_RegisterS 				) = sampler_state	{  Texture = diffuse_texture;	};
	sampler CharacterShadowTextureSampler:register(fx_CharacterShadowTextureSampler_RegisterS	) = sampler_state	{  Texture = diffuse_texture;	};
	sampler MeshTextureSamplerNoFilter 	: register(fx_MeshTextureSamplerNoFilter_RegisterS 		) = sampler_state	{  Texture = diffuse_texture;	};
	sampler DiffuseTextureSamplerNoWrap : register(fx_DiffuseTextureSamplerNoWrap_RegisterS 	) = sampler_state	{  Texture = diffuse_texture;	};
	sampler GrassTextureSampler 		: register(fx_GrassTextureSampler_RegisterS 			) = sampler_state	{  Texture = diffuse_texture;	};
	#else


	sampler ReflectionTextureSampler 	= sampler_state	{  Texture = env_texture;		AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler PostFXTextureSampler			= sampler_state	{  Texture = postFX_texture1;		AddressU = WRAP;  AddressV = WRAP;  MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler Diffuse2Sampler 			= sampler_state	{  Texture = diffuse_texture_2;	AddressU = WRAP; AddressV = WRAP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler NormalTextureSampler		= sampler_state	{  Texture = normal_texture;	AddressU = WRAP; AddressV = WRAP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler SpecularTextureSampler 		= sampler_state	{  Texture = specular_texture;	AddressU = WRAP; AddressV = WRAP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler DepthTextureSampler 		= sampler_state	{  Texture = depth_texture;		AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;    };
	sampler CubicTextureSampler 		= sampler_state	{  Texture = cubic_texture;	 	AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;   };
	sampler ShadowmapTextureSampler 	= sampler_state	{  Texture = shadowmap_texture;	AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler ScreenTextureSampler 		= sampler_state	{  Texture = screen_texture;	AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler MeshTextureSampler 			= sampler_state	{  Texture = diffuse_texture;	AddressU = WRAP; AddressV = WRAP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler ClampedTextureSampler 		= sampler_state	{  Texture = diffuse_texture;	AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler FontTextureSampler 			= sampler_state	{  Texture = diffuse_texture;	AddressU = WRAP; AddressV = WRAP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler CharacterShadowTextureSampler= sampler_state	{  Texture = diffuse_texture;	AddressU = BORDER; AddressV = BORDER; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler MeshTextureSamplerNoFilter 	= sampler_state	{  Texture = diffuse_texture;	AddressU = WRAP; AddressV = WRAP; MinFilter = NONE; MagFilter = NONE;	};
	sampler DiffuseTextureSamplerNoWrap = sampler_state	{  Texture = diffuse_texture;	AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	};
	sampler GrassTextureSampler 		= sampler_state	{  Texture = diffuse_texture;	AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	};

	#endif

#else
	sampler ReflectionTextureSampler 	: register(fx_ReflectionTextureSampler_RegisterS 		);
	sampler PostFXTextureSampler			: register(fx_EnvTextureSampler_RegisterS				);
	sampler Diffuse2Sampler 			: register(fx_Diffuse2Sampler_RegisterS 				);
	sampler NormalTextureSampler		: register(fx_NormalTextureSampler_RegisterS			);
	sampler SpecularTextureSampler 		: register(fx_SpecularTextureSampler_RegisterS 			);
	sampler DepthTextureSampler 		: register(fx_DepthTextureSampler_RegisterS 			);
	sampler CubicTextureSampler 		: register(fx_CubicTextureSampler_RegisterS 			);
	sampler ShadowmapTextureSampler 	: register(fx_ShadowmapTextureSampler_RegisterS 		);
	sampler ScreenTextureSampler 		: register(fx_ScreenTextureSampler_RegisterS			);

	#ifdef USE_SHARED_DIFFUSE_MAP
		sampler MeshTextureSampler 			: register(fx_MeshTextureSampler_RegisterS 				) = sampler_state	{  Texture = diffuse_texture;	};
		sampler ClampedTextureSampler 		: register(fx_ClampedTextureSampler_RegisterS 			) = sampler_state	{  Texture = diffuse_texture;	};
		sampler FontTextureSampler 			: register(fx_FontTextureSampler_RegisterS 				) = sampler_state	{  Texture = diffuse_texture;	};
		sampler CharacterShadowTextureSampler:register(fx_CharacterShadowTextureSampler_RegisterS	) = sampler_state	{  Texture = diffuse_texture;	};
		sampler MeshTextureSamplerNoFilter 	: register(fx_MeshTextureSamplerNoFilter_RegisterS 		) = sampler_state	{  Texture = diffuse_texture;	};
		sampler DiffuseTextureSamplerNoWrap : register(fx_DiffuseTextureSamplerNoWrap_RegisterS 	) = sampler_state	{  Texture = diffuse_texture;	};
		sampler GrassTextureSampler 		: register(fx_GrassTextureSampler_RegisterS 			) = sampler_state	{  Texture = diffuse_texture;	};
	#else
		sampler MeshTextureSampler 			: register(fx_MeshTextureSampler_RegisterS 				);
		sampler ClampedTextureSampler 		: register(fx_ClampedTextureSampler_RegisterS 			);
		sampler FontTextureSampler 			: register(fx_FontTextureSampler_RegisterS 				);
		sampler CharacterShadowTextureSampler:register(fx_CharacterShadowTextureSampler_RegisterS	);
		sampler MeshTextureSamplerNoFilter 	: register(fx_MeshTextureSamplerNoFilter_RegisterS 		);
		sampler DiffuseTextureSamplerNoWrap : register(fx_DiffuseTextureSamplerNoWrap_RegisterS 	);
		sampler GrassTextureSampler 		: register(fx_GrassTextureSampler_RegisterS 			);
	#endif
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef OUTPUT_STRUCTURES

struct PS_OUTPUT
{
	float4 RGBColor : COLOR;
};

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef FUNCTIONS
float GetSunAmount(uniform const int PcfMode, float4 ShadowTexCoord)
{
	float sun_amount;
	if(PcfMode == PCF_NVIDIA)
		sun_amount = tex2Dproj(ShadowmapTextureSampler, ShadowTexCoord).r;
	else if(PcfMode == PCF_DEFAULT)
	{
		float2 lerps = frac(ShadowTexCoord * fShadowMapSize);
		//read in bilerp stamp, doing the shadow checks
		float sourcevals[4];
		sourcevals[0] = (tex2D(ShadowmapTextureSampler, ShadowTexCoord).r < ShadowTexCoord.z)? 0.0f: 1.0f;
		sourcevals[1] = (tex2D(ShadowmapTextureSampler, ShadowTexCoord + float2(fShadowMapNextPixel, 0)).r < ShadowTexCoord.z)? 0.0f: 1.0f;
		sourcevals[2] = (tex2D(ShadowmapTextureSampler, ShadowTexCoord + float2(0, fShadowMapNextPixel)).r < ShadowTexCoord.z)? 0.0f: 1.0f;
		sourcevals[3] = (tex2D(ShadowmapTextureSampler, ShadowTexCoord + float2(fShadowMapNextPixel, fShadowMapNextPixel)).r < ShadowTexCoord.z)? 0.0f: 1.0f;

		// lerp between the shadow values to calculate our light amount
		sun_amount = lerp(lerp(sourcevals[0], sourcevals[1], lerps.x), lerp(sourcevals[2], sourcevals[3], lerps.x), lerps.y);
	}
	else
		sun_amount = 1.0f;
	sun_amount *= 1.0f - pow(vCloudAmount, 2.0f) * 0.875f;
	return sun_amount;
}

float GetSunAmountNoShadow()
{
	return 1.0f - pow(vCloudAmount, 2.0f) * 0.875f;
}

////////////////////////////////////////
float get_fog_amount(float dist, float height)
{// 1 = no fog
	float max_height = 2000.0f;
	float height_density = fFogDensity * pow(1.0f - clamp(height, 0.0f, max_height) / max_height, 2.0f);
	float dist_factor = 1.0f / exp2(dist * height_density);
	return dist_factor;
}


float3 get_sky_color()
{
	float gray_sky = vAmbientColor;
	return(lerp(vAmbientColor, gray_sky, vCloudAmount * 0.5f));
}

float get_wet_value(float pos_z)
{
	return max(0.5f, saturate(pos_z + 1.0f));
}

//0=grass 1=leaves 2=trunk
float3 apply_flora_wind(int type, float3 local_pos, float3 global_pos)
{
	float3 wind_move = 0.0f;
	float wind_push = vWindVector.w / 10.0f;
	float interval = sin(time_var * (1.0f + vWindVector.w / 40.0f) + global_pos / vWindVector.w / 4.0f);
	float bending = 0.005f;
	//if (type == 0)//fix, generated grass(and flora) works different ... local pos = global pos
	//	bending = 0.005f;
	//else
	//	bending = pow(local_pos.z, 1.25f) * 0.003f;
	wind_move.xy += (interval * vWindVector.xy * bending) + (vWindVector.xy * bending);
	wind_move.z -= pow(abs((interval + wind_push) * wind_push * 0.15f), 2.0f) * vWindVector.w * bending;
	//if (type < 2)
		wind_move += cos(time_var / (0.25f - vWindVector.w / 115.0f) + local_pos * 100.0f) * vWindVector.w / 300.0f;
	return wind_move/*  * saturate((flora_detail_clip - length(vCameraPos.xyz - global_pos.xyz)) * 0.1f) */;
}

float2 parallax_offset(float3 viewdir, float amplitude)
{
	return viewdir.xy / (viewdir.z + 0.42f) * (amplitude - 0.5f) * 0.025f;
}

////////////////////////////////////////
// static const float2 specularExp = float2(256.0,7.1)*0.7;


float4x4 build_instance_frame_matrix(float3 vInstanceData0, float3 vInstanceData1, float3 vInstanceData2, float3 vInstanceData3)
{
	const float3 position = vInstanceData0.xyz;

	float3 frame_s = vInstanceData1;
	float3 frame_f = vInstanceData2;
	float3 frame_u = vInstanceData3;//cross(frame_s, frame_f);;

	float4x4 matWorldOfInstance  = {float4(frame_s.x, frame_f.x, frame_u.x, position.x ),
									float4(frame_s.y, frame_f.y, frame_u.y, position.y ),
									float4(frame_s.z, frame_f.z, frame_u.z, position.z ),
									float4(0.0f, 0.0f, 0.0f, 1.0f )  };

	return matWorldOfInstance;
}

float4 skinning_deform(float4 vPosition, float4 vBlendWeights, float4 vBlendIndices )
{
	return 	  mul(matWorldArray[vBlendIndices.x], vPosition /*- matBoneOriginArray[vBlendIndices.x]*/) * vBlendWeights.x
			+ mul(matWorldArray[vBlendIndices.y], vPosition /*- matBoneOriginArray[vBlendIndices.y]*/) * vBlendWeights.y
			+ mul(matWorldArray[vBlendIndices.z], vPosition /*- matBoneOriginArray[vBlendIndices.z]*/) * vBlendWeights.z
			+ mul(matWorldArray[vBlendIndices.w], vPosition /*- matBoneOriginArray[vBlendIndices.w]*/) * vBlendWeights.w;
}

#define DEFINE_TECHNIQUES(tech_name, vs_name, ps_name)	\
				technique tech_name	\
				{ pass P0 { VertexShader = compile VS_X vs_name(PCF_NONE); \
							PixelShader = compile PS_2_X ps_name(PCF_NONE);} } \
				technique tech_name##_SHDW	\
				{ pass P0 { VertexShader = compile VS_X vs_name(PCF_DEFAULT); \
							PixelShader = compile PS_2_X ps_name(PCF_DEFAULT);} } \
				technique tech_name##_SHDWNVIDIA	\
				{ pass P0 { VertexShader = compile VS_X vs_name(PCF_NVIDIA); \
							PixelShader = compile PS_2_X ps_name(PCF_NVIDIA);} }
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef MISC_SHADERS	//notexture, clear_floating_point_buffer, diffuse_no_shadow, simple_shading, simple_shading_no_filter, no_shading, no_shading_no_alpha

//shared vs_font
struct VS_OUTPUT_FONT
{
	float4 Pos					: POSITION;
	float  Fog				    : FOG;
	float4 Color				: COLOR0;
	float2 Tex0					: TEXCOORD0;
};
VS_OUTPUT_FONT vs_font(float4 vPosition : POSITION, float4 vColor : COLOR, float2 tc : TEXCOORD0)
{
	VS_OUTPUT_FONT Out;

	Out.Pos = mul(matWorldViewProj, vPosition);
	float3 P = mul(matWorldView, vPosition); //position in view space
	Out.Tex0 = tc;
	Out.Color = vColor * vMaterialColor;
	//apply fog
	float d = length(P);
	float4 vWorldPos = (float4)mul(matWorld,vPosition);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	return Out;
}
VertexShader vs_font_compiled_2_0 = compile VS_X vs_font();

//---
struct VS_OUTPUT_NOTEXTURE
{
	float4 Pos           : POSITION;
	float4 Color         : COLOR0;
	float  Fog           : FOG;
};
VS_OUTPUT_NOTEXTURE vs_main_notexture(float4 vPosition : POSITION, float4 vColor : COLOR)
{
	VS_OUTPUT_NOTEXTURE Out;

	Out.Pos = mul(matWorldViewProj, vPosition);
	Out.Color = vColor * vMaterialColor;
	float3 P = mul(matWorldView, vPosition); //position in view space
	//apply fog
	float d = length(P);
	float4 vWorldPos = (float4)mul(matWorld,vPosition);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	return Out;
}
PS_OUTPUT ps_main_notexture( VS_OUTPUT_NOTEXTURE In )
{
	PS_OUTPUT Output;
	Output.RGBColor = In.Color;
	return Output;
}

technique notexture{pass P0{
	VertexShader = compile VS_X vs_main_notexture();
	PixelShader = compile PS_2_X ps_main_notexture();}}

//---
struct VS_OUTPUT_CLEAR_FLOATING_POINT_BUFFER
{
	float4 Pos			: POSITION;
};
VS_OUTPUT_CLEAR_FLOATING_POINT_BUFFER vs_clear_floating_point_buffer(float4 vPosition : POSITION)
{
	VS_OUTPUT_CLEAR_FLOATING_POINT_BUFFER Out;
	Out.Pos = mul(matWorldViewProj, vPosition);
	return Out;
}
PS_OUTPUT ps_clear_floating_point_buffer()
{
	PS_OUTPUT Out;
	Out.RGBColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
	return Out;
}

technique clear_floating_point_buffer{pass P0{
	VertexShader = compile VS_X vs_clear_floating_point_buffer();
	PixelShader = compile PS_2_X ps_clear_floating_point_buffer();}}

//---
struct VS_OUTPUT_FONT_X
{
	float4 Pos					: POSITION;
	float4 Color					: COLOR0;
	float2 Tex0					: TEXCOORD0;
	float  Fog				    : FOG;
};

PS_OUTPUT ps_simple_shading(VS_OUTPUT_FONT_X In) 
{ 
	PS_OUTPUT Output;
	float4 tex_col = tex2D(MeshTextureSampler, In.Tex0);
	Output.RGBColor =  In.Color * tex_col;
	return Output;
}
PS_OUTPUT ps_simple_no_filtering(VS_OUTPUT_FONT_X In)
{
	PS_OUTPUT Output;
	float4 tex_col = tex2D(MeshTextureSamplerNoFilter, In.Tex0);
	Output.RGBColor =  In.Color * tex_col;
	return Output;
}
PS_OUTPUT ps_no_shading(VS_OUTPUT_FONT In)
{
	PS_OUTPUT Output;
	Output.RGBColor =  In.Color;
	Output.RGBColor *= tex2D(MeshTextureSampler, In.Tex0);
	return Output;
}
PS_OUTPUT ps_no_shading_no_alpha(VS_OUTPUT_FONT In)
{
	PS_OUTPUT Output;
	Output.RGBColor =  In.Color;
	Output.RGBColor *= tex2D(MeshTextureSamplerNoFilter, In.Tex0);
	Output.RGBColor.a = 1.0f;
	return Output;
}

technique simple_shading{pass P0{
	VertexShader = vs_font_compiled_2_0;
	PixelShader = compile PS_2_X ps_simple_shading();}}
technique simple_shading_no_filter{pass P0{
	VertexShader = vs_font_compiled_2_0;
	PixelShader = compile PS_2_X ps_simple_no_filtering();}}
technique no_shading{pass P0{
	VertexShader = vs_font_compiled_2_0;
	PixelShader = compile PS_2_X ps_no_shading();}}
technique no_shading_no_alpha{pass P0{
	VertexShader = vs_font_compiled_2_0;
	PixelShader = compile PS_2_X ps_no_shading_no_alpha();}}

#endif

///////////////////////////////////////////////
#ifdef UI_SHADERS
PS_OUTPUT ps_font_uniform_color(VS_OUTPUT_FONT In)
{
	PS_OUTPUT Output;
	Output.RGBColor =  In.Color;
	Output.RGBColor.a *= tex2D(FontTextureSampler, In.Tex0).a;
	return Output;
}
PS_OUTPUT ps_font_background(VS_OUTPUT_FONT In)
{
	PS_OUTPUT Output;
	Output.RGBColor.a = 1.0f;
	Output.RGBColor.rgb = tex2D(FontTextureSampler, In.Tex0).rgb + In.Color.rgb;
	return Output;
}
PS_OUTPUT ps_font_outline(VS_OUTPUT_FONT In)
{
	float4 sample = tex2D(FontTextureSampler, In.Tex0);
	PS_OUTPUT Output;
	Output.RGBColor =  In.Color;
	Output.RGBColor.a = (1.0f - sample.r) + sample.a;
	Output.RGBColor.rgb *= sample.a + 0.05f;
	Output.RGBColor	= saturate(Output.RGBColor);
	return Output;
}

technique font_uniform_color{pass P0{
	VertexShader = vs_font_compiled_2_0;
	PixelShader = compile PS_2_X ps_font_uniform_color();}}
technique font_background{pass P0{
	VertexShader = vs_font_compiled_2_0;
	PixelShader = compile PS_2_X ps_font_background();}}
technique font_outline{pass P0{
	VertexShader = vs_font_compiled_2_0;
	PixelShader = compile PS_2_X ps_font_outline();}}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef SHADOW_RELATED_SHADERS

struct VS_OUTPUT_SHADOWMAP
{
	float4 Pos          : POSITION;
	float2 Tex0			: TEXCOORD0;
};
VS_OUTPUT_SHADOWMAP vs_main_shadowmap_skin (float4 vPosition : POSITION, float2 tc : TEXCOORD0, float4 vBlendWeights : BLENDWEIGHT, float4 vBlendIndices : BLENDINDICES)
{
	VS_OUTPUT_SHADOWMAP Out;
	float4 vObjectPos = skinning_deform(vPosition, vBlendWeights, vBlendIndices);
	Out.Pos = mul(matWorldViewProj, vObjectPos);
	Out.Tex0 = tc;
	return Out;
}
VS_OUTPUT_SHADOWMAP vs_main_shadowmap (float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0)
{
	VS_OUTPUT_SHADOWMAP Out;
	Out.Pos = mul(matWorldViewProj, vPosition);
	Out.Tex0 = tc;
	return Out;
}
VS_OUTPUT_SHADOWMAP vs_main_shadowmap_biased (float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0)
{
	VS_OUTPUT_SHADOWMAP Out;
	Out.Pos = mul(matWorldViewProj, vPosition);
	Out.Tex0 = tc;
	return Out;
}

PS_OUTPUT ps_main_shadowmap(VS_OUTPUT_SHADOWMAP In)
{
	PS_OUTPUT Output;
	Output.RGBColor.a = tex2D(MeshTextureSampler, In.Tex0).a;
	clip(Output.RGBColor.a-0.2);
	Output.RGBColor.rgb = 1.0f;
	return Output;
}
VS_OUTPUT_SHADOWMAP vs_main_shadowmap_light(uniform const bool skinning, float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0,
											float4 vBlendWeights : BLENDWEIGHT, float4 vBlendIndices : BLENDINDICES)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_SHADOWMAP, Out);
	return Out;
}
PS_OUTPUT ps_main_shadowmap_light(VS_OUTPUT_SHADOWMAP In)
{
	PS_OUTPUT Output;
	Output.RGBColor = float4(1,0,0,1);
	return Output;
}
PS_OUTPUT ps_render_character_shadow(VS_OUTPUT_SHADOWMAP In)
{
	PS_OUTPUT Output;
	Output.RGBColor = 1.0f;
	return Output;
}

VertexShader vs_main_shadowmap_compiled = compile VS_X vs_main_shadowmap();
VertexShader vs_main_shadowmap_skin_compiled = compile VS_X vs_main_shadowmap_skin();

PixelShader ps_main_shadowmap_compiled = compile PS_2_X ps_main_shadowmap();
PixelShader ps_main_shadowmap_light_compiled = compile PS_2_X ps_main_shadowmap_light();
PixelShader ps_render_character_shadow_compiled = compile PS_2_X ps_render_character_shadow();

technique renderdepth{pass P0{
	VertexShader = vs_main_shadowmap_compiled;
	PixelShader = ps_main_shadowmap_compiled;}}
technique renderdepth_biased{pass P0{
	VertexShader = compile VS_X vs_main_shadowmap_biased();
	PixelShader = ps_main_shadowmap_compiled;}}
technique renderdepthwithskin{pass P0{
	VertexShader = vs_main_shadowmap_skin_compiled;
	PixelShader = ps_main_shadowmap_compiled;}}
technique renderdepth_light{pass P0{
	VertexShader = compile VS_X vs_main_shadowmap_light(false);
	PixelShader = ps_main_shadowmap_light_compiled;}}
technique renderdepthwithskin_light{pass P0{
	VertexShader = compile VS_X vs_main_shadowmap_light(true);
	PixelShader = ps_main_shadowmap_light_compiled;}}
technique render_character_shadow{pass P0{
	VertexShader = vs_main_shadowmap_compiled;
	PixelShader = ps_render_character_shadow_compiled;}}
technique render_character_shadow_with_skin{pass P0{
	VertexShader = vs_main_shadowmap_skin_compiled;
	PixelShader = ps_render_character_shadow_compiled;}}

//--
float blurred_read_alpha(float2 texCoord)
{
	float sample_start = tex2D(CharacterShadowTextureSampler, texCoord).a;
	static const int SAMPLE_COUNT = 3;
	static const float2 offsets[SAMPLE_COUNT] = {-1, 1, 1, 1, 0, 2,};
	float blur_amount = saturate(1.0f - texCoord.y);
	blur_amount*=blur_amount;
	float sampleDist = (6.0f / 256.0f) * blur_amount;
	float sample = sample_start;
	for (int i = 0; i < SAMPLE_COUNT; i++) {
		float2 sample_pos = texCoord + sampleDist * offsets[i];
		float sample_here = tex2D(CharacterShadowTextureSampler, sample_pos).a;
		sample += sample_here;
	}
	sample /= SAMPLE_COUNT+1;
	return sample;
}
struct VS_OUTPUT_CHARACTER_SHADOW
{
	float4 Pos				    : POSITION;
	float  Fog                  : FOG;
	float2 Tex0					: TEXCOORD0;
	float  Color			    : COLOR0;
	float4 ShadowTexCoord		: TEXCOORD1;
};
VS_OUTPUT_CHARACTER_SHADOW vs_character_shadow (uniform const int PcfMode, float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0, float4 vColor : COLOR)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_CHARACTER_SHADOW, Out);

	float4 vWorldPos = (float4)mul(matWorld,vPosition);
	if (PcfMode != PCF_NONE)
	{
		//shadow mapping variables
		float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal));
		float4 ShadowPos = mul(matSunViewProj, vWorldPos);
		Out.ShadowTexCoord = ShadowPos;
		Out.ShadowTexCoord.z /= ShadowPos.w;
		Out.ShadowTexCoord.w = 1.0f;
		//shadow mapping variables end
	}
	Out.Pos = mul(matWorldViewProj, vPosition);
	Out.Tex0 = tc;
	Out.Color = vColor.a;
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	return Out;
}
PS_OUTPUT ps_character_shadow(uniform const int PcfMode, VS_OUTPUT_CHARACTER_SHADOW In)
{
	PS_OUTPUT Output;
	float sun_amount = GetSunAmount(PcfMode, In.ShadowTexCoord);
	Output.RGBColor.a = saturate(blurred_read_alpha(In.Tex0) * In.Color * sun_amount);
	Output.RGBColor.rgb = 0.0;
	return Output;
}

DEFINE_TECHNIQUES(character_shadow, vs_character_shadow, ps_character_shadow)

PS_OUTPUT ps_character_shadow_new(uniform const int PcfMode, VS_OUTPUT_CHARACTER_SHADOW In)
{
	PS_OUTPUT Output;
	float sun_amount = GetSunAmount(PcfMode, In.ShadowTexCoord);
	Output.RGBColor.a = saturate(tex2D(CharacterShadowTextureSampler, In.Tex0).r * In.Color * sun_amount);
	Output.RGBColor.rgb = 0.0;
	return Output;
}

DEFINE_TECHNIQUES(character_shadow_new, vs_character_shadow, ps_character_shadow_new)

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef WATER_SHADERS
struct VS_OUTPUT_WATER
{
	float4 Pos          : POSITION;
	float  Fog          : FOG;
	float3 Tex0         : TEXCOORD0;
	float4 ViewDir	: TEXCOORD1;
	float4 ProjReflect 	: TEXCOORD2;
	float4 ProjRefract		: TEXCOORD3;
	float3 SunLightDir			: TEXCOORD4;
	float4 LightningDir		: TEXCOORD5;
};
VS_OUTPUT_WATER vs_main_water(
	uniform const int PcfMode, 
	uniform const bool surface_movement, 
	float4 vPosition : POSITION, 
	float3 vNormal : NORMAL, 
	float4 vColor : COLOR,
	float2 tc : TEXCOORD0, 
	float3 vTangent : TANGENT, 
	float3 vBinormal : BINORMAL)
{
	VS_OUTPUT_WATER Out = (VS_OUTPUT_WATER) 0;
	
	float4 vWorldPos = mul(matWorld, vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal));

	float random = frac(sin(vWorldPos.x * vWorldPos.y) * 10000.0f);
	if(surface_movement && vWindVector.w > 0)
	{
		float wave_height = vWindVector.w / 40.0f;
		float limit = saturate(vColor.a -0.5f);
		float offset = (-0.5f + limit) + (0.5f + limit) * sin(time_var + /*movement.x + movement.y*/random * 6.0f);
		vWorldPos.z += offset /* * ((0.5f + 0.5f * cos(unique_movement.x)) + (0.5f + 0.5f * cos(unique_movement.y)))*/ * wave_height;
	}
	Out.Pos = mul(matViewProj, vWorldPos);
	Out.Tex0.xy = tc;
	Out.Tex0.z = random;
	
	float3 vWorld_binormal = normalize(mul((float3x3)matWorld, vBinormal)); //normal in world space
	float3 vWorld_tangent  = normalize(mul((float3x3)matWorld, vTangent)); //normal in world space
	float3x3 TBNMatrix = float3x3(vWorld_tangent, vWorld_binormal, vWorldN);
	
	float3 viewdir = vCameraPos.xyz - vWorldPos.xyz;
	Out.ViewDir.xyz = mul(TBNMatrix, viewdir);
	
	float4 water_pos = mul(matWaterViewProj, vWorldPos);
	Out.ProjReflect.xy = (float2(water_pos.x, -water_pos.y)+water_pos.w)/2;
	Out.ProjReflect.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * water_pos.w);
	Out.ProjReflect.zw = water_pos.zw;
	if(use_depth_effects)
	{
		Out.ProjRefract.xy = (float2(Out.Pos.x, -Out.Pos.y)+Out.Pos.w)/2.0f;
		Out.ProjRefract.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
		Out.ProjRefract.zw = Out.Pos.zw;
		Out.ViewDir.w = Out.Pos.z * far_clip_Inv; //distance camera to surface / far_clip_distance
	}
	
	Out.SunLightDir = mul(TBNMatrix, -vSunDir);
	if (vLightning.w > 0.0f)
	{
		Out.LightningDir.xyz = mul(TBNMatrix, normalize(vLightning.xyz - vWorldPos.xyz));
		Out.LightningDir.a = saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));
	}

	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	
	return Out;
}
PS_OUTPUT ps_main_water(VS_OUTPUT_WATER In, 
	uniform const int PcfMode, 
	uniform const bool high_quality,
	uniform const bool mud_factor)
{ 
	PS_OUTPUT Output;
	
	float3 viewdir = normalize(In.ViewDir.xyz);
	
	if(vWindVector.w > 0)
		In.Tex0.xy += float2(time_var * vWindVector.x * -0.005f, time_var * vWindVector.y * 0.005f);
	
	float random = In.Tex0.z;
	float unique_movement = random * 6.0f;
	float2 unique_offset = float2(random, 1.0f - random) * float2(sin(time_var - unique_movement), cos(time_var + unique_movement));
	unique_offset *= 0.004f;
	
	float2 offset_1 = In.Tex0.xy + unique_offset; 
	float2 offset_2 = In.Tex0.xy + 0.5f + unique_offset;
	if (high_quality)//parallax
	{
		offset_1 += parallax_offset(viewdir, tex2D(NormalTextureSampler, offset_1).a);
		offset_2 += parallax_offset(viewdir, tex2D(NormalTextureSampler, offset_2).a);
	}
	float3 water_color =  get_sky_color() * 0.25f;
	float3 normal = 2.0f * lerp(tex2D(NormalTextureSampler, offset_1).rgb, tex2D(NormalTextureSampler, offset_2).rgb, 0.5f + 0.5f * sin(time_var * 2.0f + unique_movement)) - 1.0f;
	
	float4 reflection_pos = In.ProjReflect;
	reflection_pos.xyz += normal;
	
	float3 reflection_color = tex2Dproj(ReflectionTextureSampler, reflection_pos);//reflection turns into fog color at 100m height
	reflection_color += 2.0f * vSunColor * GetSunAmountNoShadow() * pow(saturate(dot(normalize(viewdir + In.SunLightDir), normal)), sun_power);
	if (vLightning.w > 0.0f)
		reflection_color += pow(saturate(dot(normalize(viewdir + In.LightningDir.xyz), normal)), fMaterialPower) * In.LightningDir.a;
	
	float fresnel_factor = pow(1.0f - saturate(dot(viewdir, normal)), 2.0f);
	if(use_depth_effects)
	{
		float edge_dist_linear = tex2Dproj(DepthTextureSampler, In.ProjRefract).r - In.ViewDir.w;//far_clip_distance = 1.0
		
		float3 refraction_color;
		if(high_quality)
		{
			float4 depth_pos = In.ProjRefract;
			depth_pos.xyz += normal / 8.0f;
			float edge_dist_refracted = tex2Dproj(DepthTextureSampler, depth_pos).r - In.ViewDir.w;//far_clip_distance = 1.0
			refraction_color = lerp(tex2Dproj(ScreenTextureSampler, depth_pos), water_color, saturate(abs(edge_dist_refracted * 1000.0f)));
			Output.RGBColor.a = 1.0f;
		}
		else
		{
			refraction_color = water_color;
			Output.RGBColor.a = saturate(abs(edge_dist_linear * 1000.0f));
		}
		Output.RGBColor.rgb = lerp(refraction_color, reflection_color, fresnel_factor);
		Output.RGBColor.a *= saturate(abs(edge_dist_linear * 10000.0f));//soft edges
	}
	else
	{
		Output.RGBColor.rgb = lerp(reflection_color, water_color, fresnel_factor);
		Output.RGBColor.a = fresnel_factor;
	}
	if(mud_factor)
		Output.RGBColor.rgb = Output.RGBColor.rgb / 4.0f + float3(0.070, 0.060, 0.030);

	return Output;
}

technique watermap{pass P0{
	VertexShader = compile VS_X vs_main_water(PCF_NONE,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_NONE,false, false);}}
	
technique watermap_for_objects{pass P0{
	VertexShader = compile VS_X vs_main_water(PCF_NONE,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_NONE,false, false);}}

technique watermap_high{pass P0{
	VertexShader = compile VS_X vs_main_water(PCF_NONE,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_NONE,true, false);}}
technique watermap_high_SHDW{pass P0{
	VertexShader = compile VS_X vs_main_water(PCF_DEFAULT,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_DEFAULT,true, false);}}
technique watermap_high_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_main_water(PCF_NVIDIA,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_NVIDIA,true, false);}}
	
technique watermap_mud{pass P0{
	VertexShader = compile VS_X vs_main_water(PCF_NONE,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_NONE,false, true);}}

technique watermap_mud_high{pass P0{	
	VertexShader = compile VS_X vs_main_water(PCF_NONE,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_NONE,true, true);}}
technique watermap_mud_high_SHDW{pass P0{	
	VertexShader = compile VS_X vs_main_water(PCF_DEFAULT,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_DEFAULT,true, true);}}
technique watermap_mud_high_SHDWNVIDIA{pass P0{	
	VertexShader = compile VS_X vs_main_water(PCF_NVIDIA,true);
	PixelShader = compile PS_2_X ps_main_water(PCF_NVIDIA,true, true);}}

technique watermap_no_move{pass P0{
	VertexShader = compile VS_X vs_main_water(PCF_NONE,false);
	PixelShader = compile PS_2_X ps_main_water(PCF_NONE,false, false);}}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef SKYBOX_SHADERS
struct VS_OUTPUT_CLOUDS
{
	float4 Pos          : POSITION;
	float4 Color				: COLOR0;
	float2 Tex0         : TEXCOORD0;
	float3 SunLightDir	: TEXCOORD1;
	float3 SkyLightDir	: TEXCOORD2;
	float4 ViewDir 			: TEXCOORD3;
	float4 LightningDir : TEXCOORD4;
	float  Fog          : FOG;
	float4 projCoord			: TEXCOORD5;
};

VS_OUTPUT_CLOUDS vs_clouds(
	float4 vPosition : POSITION, 
	float4 vColor : COLOR, 
	float3 vNormal : NORMAL, 
	float2 tc : TEXCOORD0,
	float3 vTangent : TANGENT, 
	float3 vBinormal : BINORMAL)
{
	VS_OUTPUT_CLOUDS Out;
	
	float4 vWorldPos = mul(matWorld, vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal));
	
	float3 vWorld_binormal = normalize(mul((float3x3)matWorld, vBinormal)); //normal in world space
	float3 vWorld_tangent  = normalize(mul((float3x3)matWorld, vTangent)); //normal in world space
	float3x3 TBNMatrix = float3x3(vWorld_tangent, vWorld_binormal, vWorldN);
	Out.SunLightDir = mul(TBNMatrix, -vSunDir);
	Out.SkyLightDir = mul(TBNMatrix, -vSkyDir);
	
	Out.ViewDir.xyz = mul(TBNMatrix, vCameraPos.xyz - vWorldPos.xyz);
	Out.Pos = mul(matWorldViewProj, vPosition);
	// Out.Pos.z = Out.Pos.w; //hack to avoid distance clipping
	Out.Tex0 = tc;
	Out.Color = vColor * vMaterialColor;
	Out.Color.a = saturate(vPosition.z /250.0f);

	if (vLightning.w > 0.0f)
	{
		Out.LightningDir.xyz = mul(TBNMatrix, normalize(vLightning.xyz - vWorldPos.xyz));
		Out.LightningDir.a = saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));
	}
	
	if(use_depth_effects) 
	{
		Out.projCoord.xy = (float2(Out.Pos.x, -Out.Pos.y)+Out.Pos.w)/2.0f;
		Out.projCoord.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
		Out.projCoord.zw = Out.Pos.zw;
		Out.ViewDir.w = Out.Pos.z * far_clip_Inv;
	}
	
	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);

	return Out;
}

PS_OUTPUT ps_clouds(VS_OUTPUT_CLOUDS In, uniform const bool high_quality)
{
	PS_OUTPUT Output;
	
	float3 viewdir = normalize(In.ViewDir.xyz);
	
	float2 offset_1 = In.Tex0;
	float2 offset_2 = In.Tex0 * 2.0f;
	
	if (vWindVector.w > 0.0f)
	{
		offset_1 += time_var * vWindVector.xy * 0.00025f;
		offset_2 += time_var * vWindVector.xy * 0.00100f;
	}
	if (high_quality)
	{
		offset_1 += parallax_offset(viewdir, 1.0f - tex2D(MeshTextureSampler, offset_1).r);
		offset_2 += parallax_offset(viewdir, 1.0f - tex2D(MeshTextureSampler, offset_2).r);
	}
	
	float3 tex_col = 0.5f * (tex2D(MeshTextureSampler, offset_1) + tex2D(MeshTextureSampler, offset_2));
	float3 normal = tex2D(NormalTextureSampler, offset_1).rgb + tex2D(NormalTextureSampler, offset_2).rgb - 1.0f;
	
	float alpha_threshold = saturate((vCloudAmount - tex_col.rgb) * 8.0f);
	float color_multiplier = 0.5f + 0.5f * saturate(tex_col.rgb + 1.0f - vCloudAmount);
	
	float3 sky_light = (1.5f + 0.5f * dot(In.SkyLightDir, normal)) * get_sky_color();

	float sun_dir = mul(viewdir, -In.SunLightDir);
	
	float sun_amount = dot(In.SunLightDir, normal);
	float indirect_sun = (1.0f - saturate((vCloudAmount - tex_col.rgb) * 2.0f));
	indirect_sun *= (0.5f + 0.5f * sun_dir);
	indirect_sun *= (1.0f + 0.5f * sun_amount);
	
	float3 sun_light = max(saturate(sun_amount), indirect_sun) * (0.75f + 0.25f * sun_dir) * GetSunAmount(PCF_NONE, 0) * vSunColor;
	
	Output.RGBColor.rgb = sky_light + sun_light;
	if (vLightning.w > 0.0f)
		Output.RGBColor.rgb += saturate(dot(In.LightningDir.xyz, normal)) * In.LightningDir.a;
	Output.RGBColor.rgb *= color_multiplier;

	Output.RGBColor.a = alpha_threshold * In.Color.a;
	
	//if(use_depth_effects)
	//	Output.RGBColor.a *= saturate(abs(tex2Dproj(DepthTextureSampler, In.projCoord).r - In.ViewDir.w) * 20.0f);//soft edges
	
	return Output;
}

technique clouds{pass P0{
	VertexShader = compile VS_X vs_clouds();
	PixelShader = compile PS_2_X ps_clouds(false);}}
technique clouds_high{pass P0{
	VertexShader = compile VS_X vs_clouds();
	PixelShader = compile PS_2_X ps_clouds(true);}}

struct VS_OUTPUT_SKY
{
	float4 Pos          : POSITION;
	// float4 Color				: COLOR0;
	float2 Tex0         : TEXCOORD0;
	float3 Normal			: TEXCOORD1;
	float  Fog          : FOG;
};

VS_OUTPUT_SKY vs_skybox(
	float4 vPosition : POSITION,
	float4 vColor : COLOR,
	float2 tc : TEXCOORD0)
{
	VS_OUTPUT_SKY Out;
	
	float3 vWorldN = normalize(mul((float3x3)matWorld, vPosition));
	Out.Normal = vWorldN;
	
	Out.Pos = mul(matWorldViewProj, vPosition);
	Out.Pos.z = Out.Pos.w;
	Out.Tex0 = tc;

	// Out.Color.rgb = density * vAmbientColor * 2.0f;
	// Out.Color.rgb += pow(light, 64.0f) * vSunColor;
	// Out.Color.rgb += pow(density, 8.0f) * vSunColor;
	// Out.Color.a = 1.0f;

	//apply fog
	float factor = saturate(dot(-vSkyDir, vWorldN));
	Out.Fog = get_fog_amount(8000.0f, factor * 8000.0f);

	return Out;
}
PS_OUTPUT ps_skybox_shading_new(VS_OUTPUT_SKY In)
{
	PS_OUTPUT Output;
	
	float3 diffuse = tex2D(MeshTextureSampler, In.Tex0).rgb;
	float3 normal = In.Normal + diffuse / 512.0f;
	
	float sky_amount = 1.0f - saturate(dot(-vSkyDir, normal));
	float sun_amount = saturate(dot(-vSunDir, normal));
	
	float sky_light = 0.5f + sky_amount;
	
	float sun_light = pow(sky_amount, 8.0f) * (0.75f + 0.25f * sun_amount) /4;
	sun_light += pow(sun_amount, 2.0f) / 2;
	sun_light += pow(sun_amount, 64.0f);
	
	// float light = 0.75f + 0.25f * dot(-vSunDir, normal);
	
	float3 sky = sky_light * vAmbientColor + sun_light * vSunColor;
	// sky += pow(light, 256.0f) * vSunColor;
	// sky += pow(density, 8.0f) * vSunColor;
	
	float stars = pow(1.0f - diffuse, 16.0f) / 4.0f;
	
	Output.RGBColor.rgb = max(sky, stars);
	Output.RGBColor.a = 1.0f;
	
	// float3 normal = tex2D(MeshTextureSampler, In.Tex0).rgb;
	// Output.RGBColor.rgb = max(In.Color.rgb, pow(stars, 16)/4);
	// Output.RGBColor.a = 1.0f;

	return Output;
}

technique skybox{}
technique skybox_new{pass P0{
	VertexShader = compile VS_X vs_skybox();
	PixelShader = compile PS_2_X ps_skybox_shading_new();}}
technique skybox_new_HDR{pass P0{
	VertexShader = compile VS_X vs_skybox();
	PixelShader = compile PS_2_X ps_skybox_shading_new();}}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef STANDART_RELATED_SHADER //these are going to be same with standart!

struct VS_OUTPUT
{
	float4 Pos					: POSITION;
	float  Fog				    : FOG;
	float4 VertexColor				: COLOR0;
	float2 Tex0					: TEXCOORD0;
};

VS_OUTPUT vs_main(
	uniform const bool use_skinning,
	float4 vPosition : POSITION, 
	float3 vNormal : NORMAL, 
	float2 tc : TEXCOORD0, 
	
	float4 vVertexColor : COLOR0, 
	float4 vBlendWeights : BLENDWEIGHT, 
	float4 vBlendIndices : BLENDINDICES)
{
	INITIALIZE_OUTPUT(VS_OUTPUT, Out);

	float4 vObjectPos;
	float3 vObjectN;
	
	if (use_skinning)
	{
		vObjectPos = skinning_deform(vPosition, vBlendWeights, vBlendIndices);
		vObjectN = normalize( mul((float3x3)matWorldArray[vBlendIndices.x], vNormal) * vBlendWeights.x
								+ mul((float3x3)matWorldArray[vBlendIndices.y], vNormal) * vBlendWeights.y
								+ mul((float3x3)matWorldArray[vBlendIndices.z], vNormal) * vBlendWeights.z
								+ mul((float3x3)matWorldArray[vBlendIndices.w], vNormal) * vBlendWeights.w);
	}
	else
	{
		vObjectPos = vPosition;
		vObjectN = vNormal;
	}
	
	float4 vWorldPos = mul(matWorld,vObjectPos);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vObjectN));
	Out.Pos = mul(matWorldViewProj, vObjectPos);
	Out.Tex0 = tc;
	
	Out.VertexColor.rgb = (0.75f + 0.25f * dot(-vSkyDir, vWorldN)) * get_sky_color();
	Out.VertexColor.rgb += saturate(dot(-vSunDir, vWorldN)) * GetSunAmountNoShadow() * vSunColor;
	if (vLightning.w > 0.0f)
		Out.VertexColor.rgb += saturate(dot(normalize(vLightning.xyz - vWorldPos.xyz), vWorldN)) * saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));
	Out.VertexColor.a = 1.0f;
	Out.VertexColor *= vMaterialColor * vVertexColor;
	
	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	
	return Out;
}

PS_OUTPUT ps_main(VS_OUTPUT In, uniform const bool advanced_tiling)
{
	PS_OUTPUT Output;
	if(advanced_tiling)
		Output.RGBColor = 0.5f * (tex2D(MeshTextureSampler, In.Tex0) + tex2D(MeshTextureSampler, In.Tex0 * 10.0f));
	else
		Output.RGBColor = In.VertexColor * tex2D(MeshTextureSampler, In.Tex0);
	Output.RGBColor *= In.VertexColor;
	return Output;
}

technique diffuse{pass P0{
	VertexShader = compile VS_X vs_main(false);
	PixelShader = compile PS_2_X ps_main(false);}}

technique diffuse_dynamic{pass P0{
	VertexShader = compile VS_X vs_main(false);
	PixelShader = compile PS_2_X ps_main(false);}}
	
technique diffuse_dynamic_Instanced{pass P0{
	VertexShader = compile VS_X vs_main(false);
	PixelShader = compile PS_2_X ps_main(false);}}

technique envmap_metal{pass P0{
	VertexShader = compile VS_X vs_main(false);
	PixelShader = compile PS_2_X ps_main(false);}}
	
technique skin_diffuse{pass P0{
	VertexShader = compile VS_X vs_main(true);
	PixelShader = compile PS_2_X ps_main(false);}}

technique diffuse_no_shadow{pass P0{
	VertexShader = compile VS_X vs_main(false);
	PixelShader = compile PS_2_X ps_main(true);}}

technique bumpmap{}
technique dot3{}
technique dot3_multitex{}
technique bumpmap_interior{}
technique bumpmap_interior_new{}
technique bumpmap_interior_new_specmap{}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef STANDART_SHADERS

struct VS_OUTPUT_STANDART
{
	float4 Pos					: POSITION;
	float  Fog					: FOG;
	float4 VertexColor			: COLOR0;
	float2 Tex0					: TEXCOORD0;
	float3 SunLightDir			: TEXCOORD1;
	float3 SkyLightDir			: TEXCOORD2;
	float4 LightningDir		: TEXCOORD3;
	float4 ShadowTexCoord		: TEXCOORD4;
	float3 ViewDir				: TEXCOORD5;
	float4 projCoord 	: TEXCOORD6;
};

VS_OUTPUT_STANDART vs_main_standart (
	uniform const int PcfMode, 
	uniform const bool use_bumpmap,
	uniform const bool use_skinning,
	uniform const bool is_windy_trunk,
	float4 vPosition : POSITION, 
	float3 vNormal : NORMAL, 
	float2 tc : TEXCOORD0,  
	float3 vTangent : TANGENT, 
	float3 vBinormal : BINORMAL,
	float4 vVertexColor : COLOR0, 
	float4 vBlendWeights : BLENDWEIGHT, 
	float4 vBlendIndices : BLENDINDICES)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_STANDART, Out);

	float4 vObjectPos;
	float3 vObjectN, vObjectT, vObjectB;

	if(use_skinning) 
	{
		vObjectPos = skinning_deform(vPosition, vBlendWeights, vBlendIndices);
		vObjectN = normalize(  mul((float3x3)matWorldArray[vBlendIndices.x], vNormal) * vBlendWeights.x
									+ mul((float3x3)matWorldArray[vBlendIndices.y], vNormal) * vBlendWeights.y
									+ mul((float3x3)matWorldArray[vBlendIndices.z], vNormal) * vBlendWeights.z
									+ mul((float3x3)matWorldArray[vBlendIndices.w], vNormal) * vBlendWeights.w);
		if(use_bumpmap)
		{
			vObjectT = normalize(  mul((float3x3)matWorldArray[vBlendIndices.x], vTangent) * vBlendWeights.x
										+ mul((float3x3)matWorldArray[vBlendIndices.y], vTangent) * vBlendWeights.y
										+ mul((float3x3)matWorldArray[vBlendIndices.z], vTangent) * vBlendWeights.z
										+ mul((float3x3)matWorldArray[vBlendIndices.w], vTangent) * vBlendWeights.w);
			vObjectB = /*normalize*/( cross( vObjectN, vObjectT ));
			bool left_handed = (dot(cross(vNormal,vTangent),vBinormal) < 0.0f);
			if(left_handed)
				vObjectB = -vObjectB;
		}
	}
	else
	{
		vObjectPos = vPosition;
		vObjectN = vNormal;
		if(use_bumpmap)
		{
			vObjectT = vTangent;
			vObjectB = vBinormal;
		}
	}

	float4 vWorldPos = mul(matWorld, vObjectPos);
	float4 vWorldPosStatic = vWorldPos;
	float3 vWorldN = normalize(mul((float3x3)matWorld, vObjectN));
	
	//if (/* (length(vCameraPos.xyz - vWorldPos.xyz) < flora_detail_clip) && */ is_windy_trunk && vWindVector.w > 0 && vObjectPos.z > 0.0)
	//	vWorldPos.xyz += apply_flora_wind(2, vObjectPos.xyz, vWorldPos.xyz);
	
	Out.Pos = mul(matViewProj, vWorldPos);
	Out.Tex0 = tc;

	if (vLightning.w > 0.0f)
		Out.LightningDir.a = saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));

	if(use_bumpmap)
	{
		float3 vWorld_bitangent = normalize(mul((float3x3)matWorld, vBinormal));
		float3 vWorld_tangent  = normalize(mul((float3x3)matWorld, vTangent));
		float3x3 TBNMatrix = float3x3(vWorld_tangent, vWorld_bitangent, vWorldN);
		Out.ViewDir = mul(TBNMatrix, vCameraPos.xyz - vWorldPos.xyz);
		if (vLightning.w > 0.0f)
			Out.LightningDir.xyz = mul(TBNMatrix, normalize(vLightning.xyz - vWorldPos.xyz));
		Out.SunLightDir = mul(TBNMatrix, -vSunDir);
		Out.SkyLightDir = mul(TBNMatrix, -vSkyDir);
	}
	else
	{
		Out.ViewDir = vCameraPos.xyz - vWorldPos.xyz;
		if (vLightning.w > 0.0f)
			Out.LightningDir.xyz = normalize(vLightning.xyz - vWorldPos.xyz);
		Out.SunLightDir = vWorldN;
	}
	
	Out.VertexColor.rgb = vMaterialColor.rgb * vVertexColor.rgb * get_wet_value(vWorldPos.z);
	Out.VertexColor.a = vVertexColor.a;

	if (PcfMode != PCF_NONE)
	{
		float4 ShadowPos = mul(matSunViewProj, vWorldPosStatic);
		Out.ShadowTexCoord = ShadowPos;
		Out.ShadowTexCoord.z /= ShadowPos.w;
		Out.ShadowTexCoord.w = 1.0f;
		//shadow mapping variables end
	}
	
	//apply fog
	float3 P = mul(matWorldView, vObjectPos); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	
	if(use_depth_effects)
	{
		Out.projCoord.xy = (float2(Out.Pos.x, -Out.Pos.y)+Out.Pos.w)/2.0f;
		Out.projCoord.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
		Out.projCoord.zw = Out.Pos.zw;
	}
	
	return Out;
}

VS_OUTPUT_STANDART vs_main_standart_Instanced (
	uniform const int PcfMode, 
	uniform const bool use_bumpmap,
	uniform const bool use_skinning, 
	uniform const bool is_windy_trunk,
	float4 vPosition : POSITION, 
	float3 vNormal : NORMAL, 
	float2 tc : TEXCOORD0, 
	float3 vTangent : TANGENT, 
	float3 vBinormal : BINORMAL,
	float4 vVertexColor : COLOR0, 
	float4 vBlendWeights : BLENDWEIGHT, 
	float4 vBlendIndices : BLENDINDICES,
	//instance data:
	float3 vInstanceData0 : TEXCOORD1, 
	float3 vInstanceData1 : TEXCOORD2,
	float3 vInstanceData2 : TEXCOORD3, 
	float3 vInstanceData3 : TEXCOORD4)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_STANDART, Out);

	float4 vObjectPos;
	float3 vObjectN, vObjectT, vObjectB;

	if(use_skinning) 
	{
		//no skinned instancing support yet!
		GIVE_ERROR_HERE_VS;
	}
	else 
	{
		vObjectPos = vPosition;
		vObjectN = vNormal;
		if(use_bumpmap)
		{
			vObjectT = vTangent;
			vObjectB = vBinormal;
		}
	}
	float4x4 matWorldOfInstance = build_instance_frame_matrix(vInstanceData0, vInstanceData1, vInstanceData2, vInstanceData3);

	float4 vWorldPos = mul(matWorldOfInstance, vObjectPos);
	float4 vWorldPosStatic = vWorldPos;
	float3 vWorldN = normalize(mul((float3x3)matWorldOfInstance, vObjectN));
	
	if (/* (length(vCameraPos.xyz - vWorldPos.xyz) < flora_detail_clip) && */ is_windy_trunk && vWindVector.w > 0 && vObjectPos.z > 0.0)
		vWorldPos.xyz += apply_flora_wind(2, vObjectPos.xyz, vWorldPos.xyz);
	
  	Out.Pos = mul(matViewProj, vWorldPos);
	Out.Tex0 = tc;

	if (vLightning.w > 0.0f)
		Out.LightningDir.a = saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));

	if(use_bumpmap)
	{
		float3 vWorld_bitangent = normalize(mul((float3x3)matWorldOfInstance, vBinormal));
		float3 vWorld_tangent  = normalize(mul((float3x3)matWorldOfInstance, vTangent));
		float3x3 TBNMatrix = float3x3(vWorld_tangent, vWorld_bitangent, vWorldN);
		Out.ViewDir = mul(TBNMatrix, normalize(vCameraPos.xyz - vWorldPos.xyz));
		if (vLightning.w > 0.0f)
			Out.LightningDir.xyz = mul(TBNMatrix, normalize(vLightning.xyz - vWorldPos.xyz));
		Out.SunLightDir = mul(TBNMatrix, -vSunDir);
		Out.SkyLightDir = mul(TBNMatrix, -vSkyDir);
	}
	else
	{
		Out.ViewDir = normalize(vCameraPos.xyz - vWorldPos.xyz);
		if (vLightning.w > 0.0f)
			Out.LightningDir.xyz = normalize(vLightning.xyz - vWorldPos.xyz);
		Out.SunLightDir = vWorldN;
	}
	
	Out.VertexColor.rgb = vMaterialColor.rgb * vVertexColor.rgb * get_wet_value(vWorldPos.z);
	Out.VertexColor.a = vVertexColor.a;
	
	if (PcfMode != PCF_NONE)
	{
		float4 ShadowPos = mul(matSunViewProj, vWorldPos);
		Out.ShadowTexCoord = ShadowPos;
		Out.ShadowTexCoord.z /= ShadowPos.w;
		Out.ShadowTexCoord.w = 1.0f;
		//shadow mapping variables end
	}
	
	//apply fog
	float3 P = mul(matView, vWorldPos); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	
	if(use_depth_effects)
	{
		Out.projCoord.xy = (float2(Out.Pos.x, -Out.Pos.y)+Out.Pos.w)/2.0f;
		Out.projCoord.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
		Out.projCoord.zw = Out.Pos.zw;
	}

	return Out;
}

PS_OUTPUT ps_main_standart (VS_OUTPUT_STANDART In, 
	uniform const int PcfMode,
	uniform const bool use_bumpmap, 
	uniform const bool use_specularmap,
	uniform const bool use_ssao,
	uniform const bool use_parallax)
{
	PS_OUTPUT Output;

	// bool advanced_tiling = false;
	
	float3 viewdir = normalize(In.ViewDir);
	float3 normal;
	if (use_bumpmap)
	{
		if (use_parallax)
		{
			// if(advanced_tiling)
				// In.Tex0 += viewdir.xy * (0.5f * (tex2D(NormalTextureSampler, In.Tex0).a + tex2D(NormalTextureSampler, In.Tex0 * 0.25f).a) * parallax_scale - parallax_bias);
			// else
			//In.Tex0 += viewdir.xy * (tex2D(NormalTextureSampler, In.Tex0).a * parallax_scale - parallax_bias);
			In.Tex0 += parallax_offset(viewdir, tex2D(NormalTextureSampler, In.Tex0).a);
		}
			
		// if(advanced_tiling)
			// normal = 0.5f * (tex2D(NormalTextureSampler, In.Tex0) + tex2D(NormalTextureSampler, In.Tex0 * 0.25f));
		// else
			normal =  tex2D(NormalTextureSampler, In.Tex0).rgb;
		normal *= 2.0f;
		normal -= 1.0f;
	}
	else
		normal = In.SunLightDir.rgb;
	
	float4 tex_col;
	// if(advanced_tiling)
		// tex_col = 0.5f * (tex2D(MeshTextureSampler, In.Tex0) + tex2D(MeshTextureSampler, In.Tex0 * 0.25f));
	// else
		tex_col = tex2D(MeshTextureSampler, In.Tex0);
	
	float3 sky_dir = use_bumpmap? In.SkyLightDir : -vSkyDir;
	float3 sun_dir = use_bumpmap? In.SunLightDir : -vSunDir;
	
	// float sky_amount = saturate(dot(normal, sky_dir));
	float sun_amount = GetSunAmount(PcfMode, In.ShadowTexCoord) * saturate(dot(normal, sun_dir));
	float sky_amount = 0.25 + 0.75 * saturate(dot(normal, sky_dir));
	if(use_depth_effects && use_ssao)
	{
		float4 ssao = tex2Dproj(PostFXTextureSampler, In.projCoord);
	 	if ((ssao.r + ssao.g + ssao.b) > 0.0f)
	 		sky_amount *= 1.0f - ssao.a;
	}
	float3 sky_color = get_sky_color();

	float3 light_col = sun_amount * vSunColor + sky_amount * sky_color;

	if (vLightning.w > 0.0f)
		light_col += saturate(dot(In.LightningDir.xyz, normal)) * In.LightningDir.a;

	if(use_specularmap) 
	{
		// if(advanced_tiling)
			// spec_amount = 0.5f * (tex2D(SpecularTextureSampler, In.Tex0) + tex2D(SpecularTextureSampler, In.Tex0 * 0.25f));
		// else
			float spec_amount = tex2D(SpecularTextureSampler, In.Tex0);

		float fresnel_factor = pow(1.0 - saturate(dot(viewdir, normal)), 2.0);

		// float3 spec_col = sky_color * (0.625f + 0.375f * dot(normalize(viewdir + sky_dir), normal));
		// spec_col += vSunColor * sun_amount * pow(saturate(dot(normalize(viewdir + sun_dir), normal)), sun_power);
		// if (vLightning.w > 0.0f)
		// 	spec_col += pow(saturate(dot(normalize(viewdir + In.LightningDir.xyz), normal)), fMaterialPower) * In.LightningDir.a;
		
		float3 spec_col = vSunColor * sun_amount * pow(saturate(dot(normalize(viewdir + sun_dir), normal)), sun_power * spec_amount);
		spec_col += vSunColor * pow(saturate(dot(normalize(viewdir + sun_dir), normal)), 16 * spec_amount) /2;
		spec_col += sky_color * (0.25 + 0.75 * saturate(dot(normalize(viewdir + sky_dir), normal)));
		// spec_col += sky_color * pow(saturate(dot(normalize(viewdir + sky_dir), normal)), 16.0 * spec_amount);

		

		float3 dif_col = tex_col.rgb * light_col;
		float refelection = spec_amount * (spec_amount + (1.0 - spec_amount) * fresnel_factor);

		Output.RGBColor.rgb = lerp(dif_col, spec_col, refelection);
		// Output.RGBColor.rgb = dif_col + spec_col * refelection;
		tex_col.rgb *= In.VertexColor.rgb;
	}
	else
	{
		Output.RGBColor.rgb = tex_col.rgb * In.VertexColor.rgb * light_col;
	}

	Output.RGBColor.a = tex_col.a * In.VertexColor.a;
	
	return Output;
}

#define DEFINE_STANDART_TECHNIQUE(tech_name, use_bumpmap, use_specularmap, use_ssao, use_parallax, use_skinning, is_windy_trunk)	\
				technique tech_name	\
				{ pass P0 { VertexShader = compile VS_X vs_main_standart(PCF_NONE, use_bumpmap, use_skinning, is_windy_trunk); \
							PixelShader = compile PS_2_X ps_main_standart(PCF_NONE, use_bumpmap, use_specularmap, use_ssao, use_parallax);} } \
				technique tech_name##_SHDW	\
				{ pass P0 { VertexShader = compile VS_X vs_main_standart(PCF_DEFAULT, use_bumpmap, use_skinning, is_windy_trunk); \
							PixelShader = compile PS_2_X ps_main_standart(PCF_DEFAULT, use_bumpmap, use_specularmap, use_ssao, use_parallax);} } \
				technique tech_name##_SHDWNVIDIA	\
				{ pass P0 { VertexShader = compile VS_X vs_main_standart(PCF_NVIDIA, use_bumpmap, use_skinning, is_windy_trunk); \
							PixelShader = compile PS_2_X ps_main_standart(PCF_NVIDIA, use_bumpmap, use_specularmap, use_ssao, use_parallax);} }  \

#define DEFINE_STANDART_TECHNIQUE_INSTANCED(tech_name, use_bumpmap, use_specularmap, use_ssao, use_parallax, use_skinning, is_windy_trunk)	\
				technique tech_name	\
				{ pass P0 { VertexShader = compile VS_X vs_main_standart_Instanced(PCF_NONE, use_bumpmap, use_skinning, is_windy_trunk); \
							PixelShader = compile PS_2_X ps_main_standart(PCF_NONE, use_bumpmap, use_specularmap, use_ssao, use_parallax);} } \
				technique tech_name##_SHDW	\
				{ pass P0 { VertexShader = compile VS_X vs_main_standart_Instanced(PCF_DEFAULT, use_bumpmap, use_skinning, is_windy_trunk); \
							PixelShader = compile PS_2_X ps_main_standart(PCF_DEFAULT, use_bumpmap, use_specularmap, use_ssao, use_parallax);} } \
				technique tech_name##_SHDWNVIDIA	\
				{ pass P0 { VertexShader = compile VS_X vs_main_standart_Instanced(PCF_NVIDIA, use_bumpmap, use_skinning, is_windy_trunk); \
							PixelShader = compile PS_2_X ps_main_standart(PCF_NVIDIA, use_bumpmap, use_specularmap, use_ssao, use_parallax);} }  \

DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_nospecmap, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_specmap, true, true, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_bump_nospecmap, true, false, false, false, true, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_bump_specmap, true, true, false, false, true, false)

DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_specmap_trunk_move, true, true, false, false, false, true)

DEFINE_STANDART_TECHNIQUE(standart_skin_bump_nospecmap_high, true, false, false, false, true, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_bump_specmap_high, true, true, false, false, true, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_nospecmap_high, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_specmap_high, true, true, false, false, false, false)

DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_specmap_ssao_high, true, true, true, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_specmap_ssao_parallax_high, true, true, true, true, false, false)

DEFINE_STANDART_TECHNIQUE(standart_noskin_nobump_nospecmap, false, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_nobump_specmap, false, true, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_nobump_nospecmap, false, false, false, false, true, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_nobump_specmap, false, true, false, false, true, false)

DEFINE_STANDART_TECHNIQUE(standart_noskin_nobump_nospec, false, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_nospec, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_nospec_noterraincolor, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_nobump_nospec, false, false, false, false, true, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_bump_nospec, true, false, false, false, true, false)

DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_nospec_high, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_noskin_bump_nospec_high_noterraincolor, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE(standart_skin_bump_nospec_high, true, false, false, false, true, false)

DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_bump_nospecmap_Instanced, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_nobump_specmap_Instanced, false, true, false, false , false, false)
DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_bump_specmap_Instanced, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_nobump_nospecmap_Instanced, false, false, false, false, false, false)

DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_bump_nospec_high_Instanced, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_bump_nospec_high_noterraincolor_Instanced, true, false, false, false, false, false)
DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_bump_specmap_high_Instanced, true, true, false, false, false, false)
DEFINE_STANDART_TECHNIQUE_INSTANCED(standart_noskin_bump_nospecmap_high_Instanced, true, false, false, false, false, false)

DEFINE_STANDART_TECHNIQUE(envmap_specular_diffuse, false, true, false, false, false, false)
DEFINE_STANDART_TECHNIQUE_INSTANCED(envmap_specular_diffuse_Instanced, false, true, false, false, false, false)


technique standart_skin_bump_nospecmap_high_aniso{}
technique standart_skin_bump_specmap_high_aniso{}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef HAIR_SHADERS

struct VS_OUTPUT_SIMPLE_HAIR
{
	float4 Pos					: POSITION;
	float  Fog				    : FOG;
	
	float2 Tex0					: TEXCOORD0;
	float3 Normal			: TEXCOORD1;
	float4 ShadowTexCoord		: TEXCOORD2;
	float4 projCoord 	: TEXCOORD3;
	float4 LightningDir : TEXCOORD4;
};

VS_OUTPUT_SIMPLE_HAIR vs_hair (uniform const int PcfMode, float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0, float4 vVertexColor : COLOR0)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_SIMPLE_HAIR, Out);
	
	float4 vWorldPos = mul(matWorld,vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal)); //normal in world space

	Out.Pos = mul(matViewProj, vWorldPos);
	Out.Tex0 = tc;
	Out.Normal = vWorldN;
	
	//shadow mapping variables
	if (PcfMode != PCF_NONE)
	{
		float4 ShadowPos = mul(matSunViewProj, vWorldPos);
		Out.ShadowTexCoord = ShadowPos;
		Out.ShadowTexCoord.z /= ShadowPos.w;
		Out.ShadowTexCoord.w = 1.0f;
	}
	
	if (vLightning.w > 0.0f)
	{
		Out.LightningDir.xyz = normalize(vLightning.xyz - vWorldPos.xyz);
		Out.LightningDir.a = saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));
	}
	
	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	
	if(use_depth_effects)
	{
		Out.projCoord.xy = (float2(Out.Pos.x, -Out.Pos.y)+Out.Pos.w)/2.0f;
		Out.projCoord.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
		Out.projCoord.zw = Out.Pos.zw;
	}
	
	return Out;
}

PS_OUTPUT ps_hair(VS_OUTPUT_SIMPLE_HAIR In, uniform const int PcfMode)
{
	PS_OUTPUT Output;
	
	float4 tex1_col = tex2D(MeshTextureSampler, In.Tex0);
	float4 tex2_col = tex2D(Diffuse2Sampler, In.Tex0);
	
	float4 final_col = tex1_col * vMaterialColor;
	float alpha = saturate(((2.0f * vMaterialColor2.a) + tex2_col.a) - 1.9f);
	final_col.rgb *= (1.0f - alpha);
	final_col.rgb += tex2_col.rgb * alpha;

	float sky_amount = 0.75f + 0.25f * dot(In.Normal, -vSkyDir);
	if(use_depth_effects)
	{
		float4 ssao = tex2Dproj(PostFXTextureSampler, In.projCoord);
		if ((ssao.r + ssao.g + ssao.b) > 0.0f)
			sky_amount *= 1.0f - ssao.a;
	}
	float sun_amount = saturate(dot(In.Normal, -vSunDir));
	sun_amount *= GetSunAmount(PcfMode, In.ShadowTexCoord);
	
	Output.RGBColor.rgb = (sky_amount * get_sky_color()) + (sun_amount * vSunColor);
	if (vLightning.w > 0.0f)
		Output.RGBColor.rgb += saturate(dot(In.LightningDir.xyz, In.Normal)) * In.LightningDir.a;
	
	Output.RGBColor.rgb *= final_col.rgb;
	Output.RGBColor.a = tex1_col.a;
	
	return Output;
}

DEFINE_TECHNIQUES(hair_shader, vs_hair, ps_hair)
technique hair_shader_aniso{}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef FACE_SHADERS

struct VS_OUTPUT_SIMPLE_FACE
{
	float4 Pos					: POSITION;
	float  Fog				    : FOG;
	float4 VertexLighting		: COLOR0;
	float2 Tex0					: TEXCOORD0;
	float3 Normal			: TEXCOORD1;
	float4 ShadowTexCoord		: TEXCOORD2;
	float4 projCoord 	: TEXCOORD3;
	float4 LightningDir : TEXCOORD4;
};
VS_OUTPUT_SIMPLE_FACE vs_face (uniform const int PcfMode, float4 vPosition : POSITION, float3 vNormal : NORMAL, float2 tc : TEXCOORD0, float4 vColor : COLOR0)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_SIMPLE_FACE, Out);

	float4 vWorldPos = (float4)mul(matWorld,vPosition);
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal)); //normal in world space

	Out.Pos = mul(matViewProj, vWorldPos);
	Out.Tex0 = tc;
	Out.Normal = vWorldN;

	Out.VertexLighting = vMaterialColor * vColor;
	
	//shadow mapping variables
	if (PcfMode != PCF_NONE)
	{
		float4 ShadowPos = mul(matSunViewProj, vWorldPos);
		Out.ShadowTexCoord = ShadowPos;
		Out.ShadowTexCoord.z /= ShadowPos.w;
		Out.ShadowTexCoord.w = 1.0f;
	}

	if (vLightning.w > 0.0f)
	{
		Out.LightningDir.xyz = normalize(vLightning.xyz - vWorldPos.xyz);
		Out.LightningDir.a = saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));
	}
	
	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	
	if(use_depth_effects)
	{
		Out.projCoord.xy = (float2(Out.Pos.x, -Out.Pos.y)+Out.Pos.w)/2.0f;
		Out.projCoord.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
		Out.projCoord.zw = Out.Pos.zw;
	}
	
	return Out;
}
PS_OUTPUT ps_face(VS_OUTPUT_SIMPLE_FACE In, uniform const int PcfMode)
{
	PS_OUTPUT Output;

	float4 tex1_col = tex2D(MeshTextureSampler, In.Tex0);
	float4 tex2_col = tex2D(Diffuse2Sampler, In.Tex0);
	float4 tex_col = lerp(tex1_col, tex2_col, In.VertexLighting.a);

	float sky_amount = 0.75f + 0.25f * dot(In.Normal, -vSkyDir);
	if(use_depth_effects)
	{
		float4 ssao = tex2Dproj(PostFXTextureSampler, In.projCoord);
		if ((ssao.r + ssao.g + ssao.b) > 0.0f)
			sky_amount *= 1.0f - ssao.a;
	}
	float sun_amount = saturate(dot(In.Normal, -vSunDir));
	sun_amount *= GetSunAmount(PcfMode, In.ShadowTexCoord);
	
	Output.RGBColor = In.VertexLighting + sky_amount + sun_amount;
	if (vLightning.w > 0.0f)
		Output.RGBColor += saturate(dot(In.LightningDir.xyz, In.Normal)) * In.LightningDir.a;
	Output.RGBColor *= tex_col;
	
	Output.RGBColor.a = vMaterialColor.a;

	return Output;
}

DEFINE_TECHNIQUES(face_shader, vs_face, ps_face)

PS_OUTPUT ps_main_standart_face_mod( VS_OUTPUT_STANDART In, uniform const int PcfMode,
										uniform const bool use_bumpmap, uniform const bool use_ps2a , uniform const bool use_2_textures = true)
{
	PS_OUTPUT Output;

	float3 normal;
	if(use_bumpmap)
	{
		float3 tex1_norm, tex2_norm;
		tex1_norm = tex2D(NormalTextureSampler, In.Tex0);

		if(use_2_textures) {//add old's normal map with ps2a
			tex2_norm = tex2D(SpecularTextureSampler, In.Tex0);
			normal = lerp(tex1_norm, tex2_norm, In.VertexColor.a);	// blend normals different?
			normal = 2.0f * normal - 1.0f;
			normal = normalize(normal);
		}
		else
			normal = (2 * tex1_norm - 1);
	}
	else
		normal = In.SunLightDir.rgb;
		
	float3 sky_dir = use_bumpmap? In.SkyLightDir : -vSkyDir;
	float3 sun_dir = use_bumpmap? In.SunLightDir : -vSunDir;

	float sky_amount = 0.75f + 0.25f * dot(normal, sky_dir);
	if(use_depth_effects)
	{
		float4 ssao = tex2Dproj(PostFXTextureSampler, In.projCoord);
		if ((ssao.r + ssao.g + ssao.b) > 0.0f)
			sky_amount *= 1.0f - ssao.a;
	}
	float sun_amount = saturate(dot(normal, sun_dir));
	sun_amount *= GetSunAmount(PcfMode, In.ShadowTexCoord);
	
	Output.RGBColor.rgb = (sky_amount * get_sky_color()) + (sun_amount * vSunColor);
	if (vLightning.w > 0.0f)
		Output.RGBColor.rgb += saturate(dot(In.LightningDir.xyz, normal)) * In.LightningDir.a;

	float4 tex_col = tex2D(MeshTextureSampler, In.Tex0);
	if(use_2_textures)
	{
		float4 tex2_col = tex2D(Diffuse2Sampler, In.Tex0);
		tex_col = lerp(tex_col, tex2_col, In.VertexColor.a);
	}
	Output.RGBColor.rgb *= In.VertexColor.rgb * tex_col.rgb;

	if(use_ps2a)
	{
		//add highlight
		float3 vSunHalf = normalize(In.ViewDir + ((use_bumpmap)? In.SunLightDir : -vSunDir));
		float3 vSkyHalf = normalize(In.ViewDir + ((use_bumpmap)? In.SkyLightDir : -vSkyDir));
		float3 sun_highlight = sun_amount * pow(saturate(dot(vSunHalf, normal)), sun_power);
		float3 sky_highlight = sky_amount * pow(saturate(dot(vSkyHalf, normal)), sky_power) * 0.5f;
		Output.RGBColor.rgb += ((sun_highlight * vSunColor) + (sky_highlight * get_sky_color())) * 0.1;
	}
	Output.RGBColor.a = vMaterialColor.a;
	
	return Output;
}

technique face_shader_high{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_NONE, true, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_NONE, true, false);}}
technique face_shader_high_SHDW{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_DEFAULT, true, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_DEFAULT, true, false);}}
technique face_shader_high_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_NVIDIA, true, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_NVIDIA, true, false);}}

technique faceshader_high_specular{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_NONE, true, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_NONE, true, true);}}
technique faceshader_high_specular_SHDW{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_DEFAULT, true, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_DEFAULT, true, true);}}
technique faceshader_high_specular_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_NVIDIA, true, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_NVIDIA, true, true);}}

technique faceshader_simple{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_NONE, false, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_NONE, false, false);}}
technique faceshader_simple_SHDW{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_NVIDIA, false, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_DEFAULT, false, false);}}
technique faceshader_simple_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_main_standart(PCF_NVIDIA, false, false, false);
	PixelShader = compile PS_2_X ps_main_standart_face_mod(PCF_NVIDIA, false, false);}}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef FLORA_SHADERS

struct VS_OUTPUT_FLORA
{
	float4 Pos					: POSITION;
	float  Fog				    : FOG;
	float4 VertexColor			: COLOR0;
	float4 Ligthing				: COLOR1;
	float2 Tex0					: TEXCOORD0;
	float4 ShadowTexCoord		: TEXCOORD1;
};

VS_OUTPUT_FLORA vs_flora(
	uniform const int PcfMode, 
	uniform const int type, //0=grass 1=leaves 2=trunk
	uniform const bool surface_movement, 
	float4 vPosition : POSITION, 
	float3 vNormal : NORMAL, 
	float4 vVertexColor : COLOR0, 
	float2 tc : TEXCOORD0)
{
	INITIALIZE_OUTPUT(VS_OUTPUT_FLORA, Out);

	float4 vWorldPos = mul(matWorld, vPosition);
	float4 vWorldPosStatic = vWorldPos;
	float3 vWorldN = normalize(mul((float3x3)matWorld, vNormal)); //normal in world space

	if (surface_movement && vWindVector.w > 0 && vPosition.z > 0.0)
		vWorldPos.xyz += apply_flora_wind(type, vPosition.xyz, vWorldPos.xyz);

	Out.Pos = mul(matViewProj, vWorldPos);
	Out.Tex0 = tc;

	Out.Ligthing.rgb = (0.5f + 0.5f * abs(dot(-vSkyDir, vWorldN))) * get_sky_color();
	if (vLightning.w > 0.0f)
		Out.Ligthing.rgb += (0.5f + 0.5f * abs(dot(normalize(vLightning.xyz - vWorldPos.xyz), vWorldN))) * saturate(1.0f / pow(distance(vLightning.xyz, vWorldPos.xyz) / lightning_radius, 2.0f));
	Out.Ligthing.a = 0.5f + 0.5f * abs(dot(-vSunDir, vWorldN));
	
	if (PcfMode != PCF_NONE)
	{
		float4 ShadowPos = mul(matSunViewProj, (type == 0)? vWorldPos : vWorldPosStatic);
		Out.ShadowTexCoord = ShadowPos;
		Out.ShadowTexCoord.z /= ShadowPos.w;
		Out.ShadowTexCoord.w = 1.0f;
	}
	
	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vWorldPos.z);
	
	Out.VertexColor = vMaterialColor * vVertexColor;
	if (type == 0)
		Out.VertexColor.a *= min(1.0f,(1.0f - (d / 50.0f)) * 2.0f);
	
	return Out;
}

PS_OUTPUT ps_flora(VS_OUTPUT_FLORA In, 
	uniform const int PcfMode)
{
	PS_OUTPUT Output;
	Output.RGBColor = In.VertexColor * tex2D(MeshTextureSampler, In.Tex0);
	Output.RGBColor.rgb *= In.Ligthing.rgb + In.Ligthing.a * GetSunAmount(PcfMode, In.ShadowTexCoord) * vSunColor;
	return Output;
}

technique flora{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NONE,1,false);
	PixelShader = compile PS_2_X ps_flora(PCF_NONE);}}
technique flora_SHDW{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_DEFAULT,1,false);
	PixelShader = compile PS_2_X ps_flora(PCF_DEFAULT);}}
technique flora_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NVIDIA,1,false);
	PixelShader = compile PS_2_X ps_flora(PCF_NVIDIA);}}
	
technique flora_move{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NONE,1,true);
	PixelShader = compile PS_2_X ps_flora(PCF_NONE);}}
technique flora_move_SHDW{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_DEFAULT,1,true);
	PixelShader = compile PS_2_X ps_flora(PCF_DEFAULT);}}
technique flora_move_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NVIDIA,1,true);
	PixelShader = compile PS_2_X ps_flora(PCF_NVIDIA);}}
	
// technique flora_trunk_move{pass P0{
	// VertexShader = compile VS_X vs_flora(PCF_NONE,2,true);
	// PixelShader = compile PS_2_X ps_flora(PCF_NONE,2);}}
// technique flora_trunk_move_SHDW{pass P0{
	// VertexShader = compile VS_X vs_flora(PCF_DEFAULT,2,true);
	// PixelShader = compile PS_2_X ps_flora(PCF_DEFAULT,2);}}
// technique flora_trunk_move_SHDWNVIDIA{pass P0{
	// VertexShader = compile VS_X vs_flora(PCF_NVIDIA,2,true);
	// PixelShader = compile PS_2_X ps_flora(PCF_NVIDIA,2);}}

technique flora_PRESHADED{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NONE,1,false);
	PixelShader = compile PS_2_X ps_flora(PCF_NONE);}}
	
technique flora_Instanced{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NONE,1,false);
	PixelShader = compile PS_2_X ps_flora(PCF_NONE);}}

technique grass{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NONE,0,true);
	PixelShader = compile PS_2_X ps_flora(PCF_NONE);}}
technique grass_SHDW{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_DEFAULT,0,true);
	PixelShader = compile PS_2_X ps_flora(PCF_DEFAULT);}}
technique grass_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NVIDIA,0,true);
	PixelShader = compile PS_2_X ps_flora(PCF_NVIDIA);}}
	
technique grass_no_shadow{pass P0{
	VertexShader = compile VS_X vs_flora(PCF_NONE,0,false);
	PixelShader = compile PS_2_X ps_flora(PCF_NONE);}}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef MAP_SHADERS
technique diffuse_map{}
technique diffuse_map_bump{}
technique map_mountain{}
technique map_mountain_bump{}
technique map_water{}
technique map_water_high{}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef SOFT_PARTICLE_SHADERS
struct VS_DEPTHED_FLARE
{
	float4 Pos					: POSITION;
	float4 Color				: COLOR0;
	float3 Ligthing				: COLOR1;
	float2 Tex0					: TEXCOORD0;
	float  Fog				    : FOG;

	float4 projCoord			: TEXCOORD1;
	float  Depth				: TEXCOORD2;
	float4 ShadowTexCoord		: TEXCOORD3;
};

VS_DEPTHED_FLARE vs_main_depthed_flare(
	uniform const int PcfMode, 
	uniform const bool blend_adding, 
	float4 vPosition : POSITION,//already in world space
	float4 vColor : COLOR, 
	float2 tc : TEXCOORD0)
{
	INITIALIZE_OUTPUT(VS_DEPTHED_FLARE, Out);

	Out.Pos = mul(matWorldViewProj, vPosition);

	Out.Tex0 = tc;
	Out.Color = vColor * vMaterialColor;
	
	if(!blend_adding && PcfMode != PCF_NONE) 
	{
		Out.Ligthing = get_sky_color();
		if (vLightning.w > 0.0f)
			Out.Ligthing += saturate(1.0f / pow(distance(vLightning.xyz, vPosition.xyz) / lightning_radius, 2.0f));
	
		if (PcfMode != PCF_NONE)
		{
			float4 ShadowPos = mul(matSunViewProj, vPosition);
			Out.ShadowTexCoord = ShadowPos;
			Out.ShadowTexCoord.z /= ShadowPos.w;
			Out.ShadowTexCoord.w = 1.0f;
		}
	}

	if(use_depth_effects) 
	{
		Out.projCoord.xy = (float2(Out.Pos.x, -Out.Pos.y)+Out.Pos.w)/2.0f;
		Out.projCoord.xy += (vDepthRT_HalfPixel_ViewportSizeInv.xy * Out.Pos.w);
		Out.projCoord.zw = Out.Pos.zw;
		Out.Depth = Out.Pos.z * far_clip_Inv;
	}
	
	//apply fog
	float3 P = mul(matWorldView, vPosition); //position in view space
	float d = length(P);
	Out.Fog = get_fog_amount(d, vPosition.z);

	return Out;
}

PS_OUTPUT ps_main_depthed_flare(VS_DEPTHED_FLARE In, 
	uniform const int PcfMode, 
	uniform const bool blend_adding)
{
	PS_OUTPUT Output;

	Output.RGBColor = In.Color * tex2D(MeshTextureSampler, In.Tex0);
	if(!blend_adding)
		Output.RGBColor.rgb *= In.Ligthing + GetSunAmount(PcfMode, In.ShadowTexCoord) * vSunColor;

	if(use_depth_effects)
		Output.RGBColor.a *= saturate(abs(tex2Dproj(DepthTextureSampler, In.projCoord).r - In.Depth) * 40000.0f);//soft edges
	
	return Output;
}

technique soft_sunflare{pass P0{
	VertexShader = compile VS_X vs_main_depthed_flare(PCF_NONE,true);
	PixelShader = compile PS_2_X ps_main_depthed_flare(PCF_NONE,true);}}
technique soft_particle_add{pass P0{
	VertexShader = compile VS_X vs_main_depthed_flare(PCF_NONE,true);
	PixelShader = compile PS_2_X ps_main_depthed_flare(PCF_NONE,true);}}

technique soft_particle_modulate{pass P0{
	VertexShader = compile VS_X vs_main_depthed_flare(PCF_NONE,false);
	PixelShader = compile PS_2_X ps_main_depthed_flare(PCF_NONE,false);}}
technique soft_particle_modulate_SHDW{pass P0{
	VertexShader = compile VS_X vs_main_depthed_flare(PCF_DEFAULT,false);
	PixelShader = compile PS_2_X ps_main_depthed_flare(PCF_DEFAULT,false);}}
technique soft_particle_modulate_SHDWNVIDIA{pass P0{
	VertexShader = compile VS_X vs_main_depthed_flare(PCF_NVIDIA,false);
	PixelShader = compile PS_2_X ps_main_depthed_flare(PCF_NVIDIA,false);}}

#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef OCEAN_SHADERS
technique simple_ocean{}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef NEWTREE_SHADERS
technique tree_billboards_flora{}
technique tree_billboards_dot3_alpha{}
#endif