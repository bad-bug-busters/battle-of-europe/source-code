# -*- coding: cp1252 -*-
from header_factions import *

####################################################################################################################
#  Each faction record contains the following fields:
#  1) Faction id: used for referencing factions in other files.
#     The prefix fac_ is automatically added before each faction id.
#  2) Faction name.
#  3) Faction flags. See header_factions.py for a list of available flags
#  4) Faction coherence. Relation between members of this faction.
#  5) Relations. This is a list of relation records.
#     Each relation record is a tuple that contains the following fields:
#    5.1) Faction. Which other faction this relation is referring to
#    5.2) Value: Relation value between the two factions.
#         Values range between -1 and 1.
#  6) Ranks
#  7) Faction color (default is gray)
####################################################################################################################

factions = [
  ("no_faction","No Faction",0, 0.9, [], []),
  ("commoners","Commoners",0, 0.1,[], []),
  ("outlaws","Outlaws", max_player_rating(-30), 0.5,[], [], 0x888888),
# Factions before this point are hardwired into the game end their order should not be changed.

#MOD begin
  ("kingdom_1", "Kingdom of England", 0, 0.9, [], [], 0xffe1e1),
  ("kingdom_2", "Polish Lithuanian Union", 0, 0.9, [], [], 0xff8080),
  ("kingdom_3", "Ottoman Empire", 0, 0.9, [], [], 0xff0000),
  ("kingdom_4", "Kingdom of France", 0, 0.9, [], [], 0x0000ff),
  ("kingdom_5", "Kingdom of Scotland", 0, 0.9, [], [], 0x00ffff),
  ("kingdom_6", "Holy Roman Empire", 0, 0.9, [], [], 0xffff00),
  ("kingdom_7", "Portuguese Empire", 0, 0.9, [], [], 0x00bf00),
  ("kingdom_8", "Spanish Empire", 0, 0.9, [], [], 0xff7f00),
  ("kingdom_9", "Union of Kalmar", 0, 0.9, [], [], 0x007f7f),
  ("kingdom_10", "Kingdom of Hungary", 0, 0.9, [], [], 0x006600),
  ("kingdom_11", "Papal States", 0, 0.9, [], [], 0xffd480),
  ("kingdom_12", "Republic of Venice", 0, 0.9, [], [], 0xa52121),
  ("kingdom_13", "Orders State", 0, 0.9, [], [], 0x7f7f7f),
  ("kingdom_14", "Grand Duchy of Moscow", 0, 0.9, [], [], 0x7f007f),
  ("kingdom_15", "Wattasid Dynasty", 0, 0.9, [], [], 0x827f00),
  ("kingdom_16", "Saadi Dynasty", 0, 0.9, [], [], 0xff00ff),
  ("kingdom_17", "Zayyanid Dynasty", 0, 0.9, [], [], 0x7c37d9),
  ("kingdom_18", "Hafsid Dynasty", 0, 0.9, [], [], 0xac936f),
  ("kingdom_19", "Provinces of Sweden", 0, 0.9, [], [], 0x4badd2),
  ("kingdom_20", "Kingdom of Imereti", 0, 0.9, [], [], 0xffffff),
#MOD end
  ("kingdoms_end","{!}kingdoms_end", 0, 0,[], []),
]