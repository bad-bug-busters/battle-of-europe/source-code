from header_meshes import *
from module_constants import * #MOD

####################################################################################################################
#  Each mesh record contains the following fields:
#  1) Mesh id: used for referencing meshes in other files. The prefix mesh_ is automatically added before each mesh id.
#  2) Mesh flags. See header_meshes.py for a list of available flags
#  3) Mesh resource name: Resource name of the mesh
#  4) Mesh translation on x axis: Will be done automatically when the mesh is loaded
#  5) Mesh translation on y axis: Will be done automatically when the mesh is loaded
#  6) Mesh translation on z axis: Will be done automatically when the mesh is loaded
#  7) Mesh rotation angle over x axis: Will be done automatically when the mesh is loaded
#  8) Mesh rotation angle over y axis: Will be done automatically when the mesh is loaded
#  9) Mesh rotation angle over z axis: Will be done automatically when the mesh is loaded
#  10) Mesh x scale: Will be done automatically when the mesh is loaded
#  11) Mesh y scale: Will be done automatically when the mesh is loaded
#  12) Mesh z scale: Will be done automatically when the mesh is loaded
####################################################################################################################

meshes = [
  ("mp_score_a", 0, "mp_score_a", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_score_b", 0, "mp_score_b", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("portrait_blend_out", 0, "portrait_blend_out", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("load_window", 0, "load_window", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("checkbox_off", render_order_plus_1, "checkbox_off", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("checkbox_on", render_order_plus_1, "checkbox_on", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("white_plane", 0, "white_plane", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("white_dot", 0, "white_dot", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("player_dot", 0, "player_dot", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("flag_infantry", 0, "flag_infantry", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("flag_archers", 0, "flag_archers", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("flag_cavalry", 0, "flag_cavalry", 0, 0, 0, 0, 0, 0, 1, 1, 1),

  ("inv_slot", 0, "inv_slot", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ingame_menu", 0, "mp_ingame_menu", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_inventory_left", 0, "mp_inventory_left", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_inventory_right", 0, "mp_inventory_right", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_inventory_choose", 0, "mp_inventory_choose", 0, 0, 0, 0, 0, 0, 1, 1, 1),
#MOD begin 
  ("mp_inventory_slot_helmet", 0, "mp_inventory_slot_helmet", 0, 0, 0, 0, 0, 0, 1, 1, 1),
	("mp_inventory_slot_armor", 0, "mp_inventory_slot_armor", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_inventory_slot_boot", 0, "mp_inventory_slot_boot", 0, 0, 0, 0, 0, 0, 1, 1, 1),
	("mp_inventory_slot_glove", 0, "mp_inventory_slot_glove", 0, 0, 0, 0, 0, 0, 1, 1, 1),
	("mp_inventory_slot_horse", 0, "mp_inventory_slot_horse", 0, 0, 0, 0, 0, 0, 1, 1, 1),
	("mp_inventory_slot_invalid", 0, "mp_inventory_slot_invalid", 0, 0, 0, 0, 0, 0, 1, 1, 1),
#MOD end
  ("mp_inventory_slot_empty", 0, "mp_inventory_slot_empty", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_inventory_slot_equip", 0, "mp_inventory_slot_equip", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_inventory_left_arrow", 0, "mp_inventory_left_arrow", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_inventory_right_arrow", 0, "mp_inventory_right_arrow", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_main", 0, "mp_ui_host_main", 0, 0, 0, 0, 0, 0, 1, 1, 1),

  ("mp_ui_host_maps_1", 0, "mp_ui_host_maps_a1", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_2", 0, "mp_ui_host_maps_a2", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_3", 0, "mp_ui_host_maps_c", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_4", 0, "mp_ui_host_maps_ruinedf", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_5", 0, "mp_ui_host_maps_a1", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_6", 0, "mp_ui_host_maps_a1", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_7", 0, "mp_ui_host_maps_fieldby", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_8", 0, "mp_ui_host_maps_castle2", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_9", 0, "mp_ui_host_maps_snovyv", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_10", 0, "mp_ui_host_maps_castle3", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_11", 0, "mp_ui_host_maps_c1", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_12", 0, "mp_ui_host_maps_c2", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_13", 0, "mp_ui_host_maps_c3", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_randomp", 0, "mp_ui_host_maps_randomp", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_randoms", 0, "mp_ui_host_maps_randoms", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_command_panel", 0, "mp_ui_command_panel", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_command_border_l", 0, "mp_ui_command_border_l", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_command_border_r", 0, "mp_ui_command_border_r", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_welcome_panel", 0, "mp_ui_welcome_panel", 0, 0, 0, 0, 0, 0, 1, 1, 1),

  ("tableau_mesh_shield_round_1",  0, "tableau_mesh_shield_round_1", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_round_2",  0, "tableau_mesh_shield_round_2", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_round_3",  0, "tableau_mesh_shield_round_3", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_round_4",  0, "tableau_mesh_shield_round_4", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_round_5",  0, "tableau_mesh_shield_round_5", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_small_round_1",  0, "tableau_mesh_shield_small_round_1", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_small_round_2",  0, "tableau_mesh_shield_small_round_2", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_small_round_3",  0, "tableau_mesh_shield_small_round_3", 0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_kite_4",   0, "tableau_mesh_shield_kite_4",  0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_heater_2", 0, "tableau_mesh_shield_heater_2",  0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_pavise_1", 0, "tableau_mesh_shield_pavise_1",  0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_pavise_2", 0, "tableau_mesh_shield_pavise_2",  0, 0, 0, 0, 0, 0, 10, 10, 10),

  ("tableau_mesh_shield_otto1", 0, "tableau_mesh_shield_otto1",  0, 0, 0, 0, 0, 0, 10, 10, 10),
  ("tableau_mesh_shield_otto2", 0, "tableau_mesh_shield_otto2",  0, 0, 0, 0, 0, 0, 10, 10, 10),

  ("tableau_mesh_gambeson", 0, "tableau_mesh_gambeson",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_drz_kaftan", 0, "tableau_mesh_drz_kaftan",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_gothic_armour", 0, "tableau_mesh_gothic_armour",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_churburg_13", 0, "tableau_mesh_churburg_13",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_corrazina", 0, "tableau_mesh_corrazina",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_drz_mail_shirt", 0, "tableau_mesh_drz_mail_shirt",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_leather_jerkin", 0, "tableau_mesh_leather_jerkin",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_ragged_outfit", 0, "tableau_mesh_ragged_outfit",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_leather_vest_a", 0, "tableau_mesh_leather_vest_a",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_lamellar_leather", 0, "tableau_mesh_lamellar_leather",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_padded_cloth_a", 0, "tableau_mesh_padded_cloth_a",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_padded_cloth_b", 0, "tableau_mesh_padded_cloth_b",  0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("tableau_mesh_lamellar_vest_a", 0, "tableau_mesh_lamellar_vest_a",  0, 0, 0, 0, 0, 0, 1, 1, 1),
#MOD end
]

#MOD begin
for i in range(1, 136):
	if i < 10:
		str_name = 'banner_clan_00' +str(i)
	elif i < 100:
		str_name = 'banner_clan_0' +str(i)
	else:
		str_name = 'banner_clan_' +str(i)
	
	if i <= highest_clan_id +1:
		meshes += (str_name, 0, str_name, 0, 0, 0, -90, 0, 0, 1, 1, 1),
	else:
		meshes += (str_name, 0, "banner_clan_dummy", 0, 0, 0, -90, 0, 0, 1, 1, 1),
#MOD end

meshes += [
  ("banner_kingdom_01", 0, "banner_kingdom_01", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banner_kingdom_02", 0, "banner_kingdom_02", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banner_kingdom_03", 0, "banner_kingdom_03", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banner_kingdom_04", 0, "banner_kingdom_04", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banner_kingdom_05", 0, "banner_kingdom_05", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banner_kingdom_06", 0, "banner_kingdom_06", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
#MOD begin
  ("banner_kingdom_07", 0, "banner_kingdom_07", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_08", 0, "banner_kingdom_08", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_09", 0, "banner_kingdom_09", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_10", 0, "banner_kingdom_10", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_11", 0, "banner_kingdom_11", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_12", 0, "banner_kingdom_12", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_13", 0, "banner_kingdom_13", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_14", 0, "banner_kingdom_14", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_15", 0, "banner_kingdom_15", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_16", 0, "banner_kingdom_16", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_17", 0, "banner_kingdom_17", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_18", 0, "banner_kingdom_18", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_19", 0, "banner_kingdom_19", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("banner_kingdom_20", 0, "banner_kingdom_20", 0, 0, 0, -90, 0, 0, 1, 1, 1),
#MOD end
  ("banner_f21", 0, "banner_clan_dummy", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit

  ("banners_default_a", 0, "banner_clan_dummy", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banners_default_b", 0, "banner_clan_dummy", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banners_default_c", 0, "banner_clan_dummy", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banners_default_d", 0, "banner_clan_dummy", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  ("banners_default_e", 0, "banner_clan_dummy", 0, 0, 0, -90, 0, 0, 1, 1, 1),#MOD edit
  
  ("troop_label_banner",  0, "troop_label_banner", 0, 0, 0, 0, 0, 0, 10, 10, 10),

#MOD begin
  ("ui_kingdom_shield_01", 0, "ui_kingdom_shield_01", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_02", 0, "ui_kingdom_shield_02", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_03", 0, "ui_kingdom_shield_03", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_04", 0, "ui_kingdom_shield_04", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_05", 0, "ui_kingdom_shield_05", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_06", 0, "ui_kingdom_shield_06", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_07", 0, "ui_kingdom_shield_07", 0, 0, 0, 0, 0, 0, 1, 1, 1),  
  ("ui_kingdom_shield_08", 0, "ui_kingdom_shield_08", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_09", 0, "ui_kingdom_shield_09", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_10", 0, "ui_kingdom_shield_10", 0, 0, 0, 0, 0, 0, 1, 1, 1), 
  ("ui_kingdom_shield_11", 0, "ui_kingdom_shield_11", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_12", 0, "ui_kingdom_shield_12", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_13", 0, "ui_kingdom_shield_13", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_14", 0, "ui_kingdom_shield_14", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_15", 0, "ui_kingdom_shield_15", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_16", 0, "ui_kingdom_shield_16", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_17", 0, "ui_kingdom_shield_17", 0, 0, 0, 0, 0, 0, 1, 1, 1),  
  ("ui_kingdom_shield_18", 0, "ui_kingdom_shield_18", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_19", 0, "ui_kingdom_shield_19", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("ui_kingdom_shield_20", 0, "ui_kingdom_shield_20", 0, 0, 0, 0, 0, 0, 1, 1, 1),
	
  ("flag_kingdom_01", 0, "flag_kingdom_01", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_02", 0, "flag_kingdom_02", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_03", 0, "flag_kingdom_03", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_04", 0, "flag_kingdom_04", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_05", 0, "flag_kingdom_05", 0, 0, 0, -90, 0, 0, 1, 1, 1), 
  ("flag_kingdom_06", 0, "flag_kingdom_06", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_07", 0, "flag_kingdom_07", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_08", 0, "flag_kingdom_08", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_09", 0, "flag_kingdom_09", 0, 0, 0, -90, 0, 0, 1, 1, 1), 
  ("flag_kingdom_10", 0, "flag_kingdom_10", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_11", 0, "flag_kingdom_11", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_12", 0, "flag_kingdom_12", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_13", 0, "flag_kingdom_13", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_14", 0, "flag_kingdom_14", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_15", 0, "flag_kingdom_15", 0, 0, 0, -90, 0, 0, 1, 1, 1),  
  ("flag_kingdom_16", 0, "flag_kingdom_16", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_17", 0, "flag_kingdom_17", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_18", 0, "flag_kingdom_18", 0, 0, 0, -90, 0, 0, 1, 1, 1),
  ("flag_kingdom_19", 0, "flag_kingdom_19", 0, 0, 0, -90, 0, 0, 1, 1, 1), 
  ("flag_kingdom_20", 0, "flag_kingdom_20", 0, 0, 0, -90, 0, 0, 1, 1, 1),
#MOD end

  ("mouse_arrow_down", 0, "mouse_arrow_down", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mouse_arrow_right", 0, "mouse_arrow_right", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mouse_arrow_left", 0, "mouse_arrow_left", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mouse_arrow_up", 0, "mouse_arrow_up", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mouse_arrow_plus", 0, "mouse_arrow_plus", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mouse_left_click", 0, "mouse_left_click", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mouse_right_click", 0, "mouse_right_click", 0, 0, 0, 0, 0, 0, 1, 1, 1),

  ("status_ammo_ready", 0, "status_ammo_ready", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("main_menu_background", 0, "main_menu_nord", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("loading_background", 0, "load_screen_2", 0, 0, 0, 0, 0, 0, 1, 1, 1),

  ("ui_quick_battle_a", 0, "ui_quick_battle_a", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("white_bg_plane_a", 0, "white_bg_plane_a", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_icon_infantry", 0, "cb_ui_icon_infantry", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_icon_archer", 0, "cb_ui_icon_archer", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_icon_horseman", 0, "cb_ui_icon_horseman", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_main", 0, "cb_ui_main", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_01", 0, "cb_ui_maps_scene_01", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_02", 0, "cb_ui_maps_scene_02", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_03", 0, "cb_ui_maps_scene_03", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_04", 0, "cb_ui_maps_scene_04", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_05", 0, "cb_ui_maps_scene_05", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_06", 0, "cb_ui_maps_scene_06", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_07", 0, "cb_ui_maps_scene_07", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_08", 0, "cb_ui_maps_scene_08", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("cb_ui_maps_scene_09", 0, "cb_ui_maps_scene_09", 0, 0, 0, 0, 0, 0, 1, 1, 1),

  ("mp_ui_host_maps_14", 0, "mp_ui_host_maps_c4", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_15", 0, "mp_ui_host_maps_c5", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("quit_adv", 0, "quit_adv", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("quit_adv_b", 0, "quit_adv_b", 0, 0, 0, 0, 0, 0, 1, 1, 1),

#MOD disable
  ("mp_ui_host_maps_16", 0, "mp_ui_host_maps_d1", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_17", 0, "mp_ui_host_maps_d2", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_18", 0, "mp_ui_host_maps_d3", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_19", 0, "mp_ui_host_maps_e2", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("mp_ui_host_maps_20", 0, "mp_ui_host_maps_e1", 0, 0, 0, 0, 0, 0, 1, 1, 1),

#MOD begin
	("mp_ui_gold", 0, "mp_ico_gold", 0, 0, 0, 0, 0, 0, 1, 1, 1),

  ("note_window", 0, "note_window", 0, 0, 0, 0, 0, 0, 1, 1, 1),
  ("white_plane_center", 0, "white_plane_center", 0, 0, 0, 0, 0, 0, 1, 1, 1),
	("order_frame", 0, "order_frame", 0, 0, 0, 0, 0, 0, 1, 1, 1),
	
	("ui_blur_edges", 0, "ui_blur_edges", 0, 0, 0, 0, 0, 0, 1, 1, 1),
#MOD end
]