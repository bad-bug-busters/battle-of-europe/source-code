from header_skins import *
from ID_particle_systems import *

####################################################################################################################
#  Each skin record contains the following fields:
#  1) Skin id: used for referencing skins.
#  2) Skin flags. Not used yet. Should be 0.
#  3) Body mesh.
#  4) Calf mesh (left one).
#  5) Hand mesh (left one).
#  6) Head mesh.
#  7) Face keys (list)
#  8) List of hair meshes.
#  9) List of beard meshes.
# 10) List of hair textures.
# 11) List of beard textures.
# 12) List of face textures.
# 13) List of voices.
# 14) Skeleton name
# 15) Scale (doesn't fully work yet)
# 16) Blood particles 1 (do not add this if you wish to use the default particles)
# 17) Blood particles 2 (do not add this if you wish to use the default particles)
# 17) Face key constraints (do not add this if you do not wish to use it)
####################################################################################################################

#MOD info
#(20,0,0.7,-0.6, "Chin Size"),
#20 - time counter for the chin size morph
#0 - not sure what this does (maybe the starting point?)
#0.7 - chin size morph at 70%
#-0.6 - chin size morph at -60%

man_face_keys = [
(240,0,-0.5,0.25, "Chin Size"), 
(230,0,-0.25,0.25, "Chin Shape"), 
(250,0,-0.75,0.75, "Chin Forward"), 
(130,0,-0.75,0.5, "Jaw Width"),
(120,0,-0.25,0.25, "Lower Lip"),
(110,0,-0.25,0.25, "Upper Lip"),
(100,0,0.5,-0.25, "Mouth-Nose Distance"),
(90,0,0.5,-0.5, "Mouth Width"),

(30,0,-0.25,0.25, "Nostril Size"),
(60,0,0.5,-0.25, "Nose Height"),
(40,0,-0.25,0.5, "Nose Width"),
(70,0,-0.25,0.25, "Nose Size"),
(50,0,0.5,-0.25, "Nose Shape"),
(80,0,-0.5,0.5, "Nose Bridge"),

(160,0,-0.25,0.5, "Eye Width"),
(190,0,-0.75,0.25, "Eye to Eye Dist"),
(170,0,-0.75,0.75, "Eye Shape"),
(200,0,-0.5,0.5, "Eye Depth"),
(180,0,1.0,-0.5, "Eyelids"),

(20,0,0.5,-0.5, "Cheeks"),
(260,0,-0.5,0.75, "Cheek Bones"),
(220,0,0.5,-0.5, "Eyebrow Height"),
(210,0,-0.25,0.25, "Eyebrow Shape"),
(10,0,-0.5,0.25, "Temple Width"),

(270,0,-0.5,0.5, "Face Depth"),
(150,0,-0.25,0.25, "Face Ratio"),
(140,0,-0.75,0.5, "Face Width"),

(280,0,1.0,1.0, "Post-Edit"),
]
# Face width-Jaw width Temple width
woman_face_keys = [
(230,0,1.0,-1.0, "Chin Size"), 
(220,0,-0.75,1.0, "Chin Shape"), 
(10,0,-0.75,1.0, "Chin Forward"),
(20,0, -1.0, 1.0, "Jaw Width"), 
(40,0,-1.0,1.0, "Jaw Position"),
(270,0,-1.0,1.0, "Mouth-Nose Distance"),
(30,0,-0.5,1.0, "Mouth Width"),
(50,0, -1.0,1.0, "Cheeks"),

(60,0,-0.5,0.75, "Nose Height"),
(70,0,-1.0,1.25, "Nose Width"),
(80,0,1.5,-1.25, "Nose Size"),
(240,0,-1.5,1.5, "Nose Shape"),
(90,0,-1.25,0.75, "Nose Bridge"),

(100,0,-1.0,1.25, "Cheek Bones"),
(150,0,-1.0,0.5, "Eye Width"),
(110,0,1.5,-1.0, "Eye to Eye Dist"),
(120,0,-0.75,0.5, "Eye Shape"),
(130,0,-0.75,0.75, "Eye Depth"),
(140,0,1.0,-0.5, "Eyelids"),

(160,0,1.0,-0.75, "Eyebrow Position"),
(170,0,-0.5,0.5, "Eyebrow Height"),
(250,0,-0.75,0.75, "Eyebrow Depth"),
(180,0,-0.75,0.5, "Eyebrow Shape"),
(260,0,1.0,-1.0, "Temple Width"),

(200,0,-0.75,0.75, "Face Depth"),
(210,0,-0.75,0.25, "Face Ratio"),
(190,0,-0.5,0.5, "Face Width"),

(280,0,0.0,1.0, "Post-Edit"),
]
undead_face_keys = []


chin_size = 0
chin_shape = 1
chin_forward = 2
jaw_width = 3
jaw_position = 4
mouth_nose_distance = 5
mouth_width = 6
cheeks = 7
nose_height = 8
nose_width = 9
nose_size = 10
nose_shape = 11
nose_bridge = 12
cheek_bones = 13
eye_width = 14
eye_to_eye_dist = 15
eye_shape = 16
eye_depth = 17
eyelids = 18
eyebrow_position = 19
eyebrow_height = 20
eyebrow_depth = 21
eyebrow_shape = 22
temple_width = 23
face_depth = 24
face_ratio = 25
face_width = 26

comp_less_than = -1;
comp_greater_than = 1;

skins = [
  (
    "man", 0,
    "man_body", "man_calf_l", "m_handL",
    "male_head", man_face_keys,
    [
			"man_hair_s",
			"man_hair_m",
			"man_hair_n",
			"man_hair_o",
			"man_hair_y10",
			"man_hair_y12",
			"man_hair_p",
			"man_hair_r",
			"man_hair_q",
			"man_hair_v",
			"man_hair_t",
			"man_hair_y6",
			"man_hair_y3",
			"man_hair_y7",
			"man_hair_y9",
			"man_hair_y11",
			"man_hair_u",
			"man_hair_y",
			"man_hair_y2",
			"man_hair_y4",
			
			"man_hair_y5",
			"man_hair_y8",
		], #man_hair_meshes
    [
			"beard_e",
			"beard_d",
			"beard_k",
			"beard_l",
			"beard_i",
			"beard_j",
			"beard_z",
			"beard_m",
			"beard_n",
			"beard_y",
			"beard_p",
			"beard_o",
			"beard_v",
			"beard_f",
			"beard_b",
			"beard_c",
			"beard_t",
			"beard_u",
			"beard_r",
			"beard_s",
			"beard_a",
			"beard_h",
			"beard_g",
			
			"beard_q",
		], #beard meshes
    ["hair_blonde"], #hair textures
    ["beard_blonde"], #beard_materials
    [
			("manface_young_2",0xffcbe0e0,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_midage",0xffdfefe1,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_young",0xffd0e0e0,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_young_3",0xffdceded,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_7",0xffc0c8c8,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("manface_midage_2",0xfde4c8d8,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_rugged",0xffb0aab5,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
			("manface_african",0xff807c8a,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
			
			("manface_white2",0xffe0e8e8,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_mideast1",0xffaeb0a6,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("manface_mideast2",0xffd0c8c1,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("manface_black1",0xff87655c,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
			("manface_black2",0xff5a342d,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
			("manface_new_young",0xffcbe0e0,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_new_young_2",0xffdceded,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_new_young_3",0xffd0e0e0,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_new_midage",0xffdfefe1,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_new_midage_2",0xfde4c8d8,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_white1",0xffe0e8e8,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("manface_white3",0xffe0e8e8,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("manface_new_7",0xffc0c8c8,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("manface_mideast3",0xffe0e8e8,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("manface_new_rugged",0xffb0aab5,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
			("manface_new_african",0xff807c8a,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
    ], #man_face_textures,
    [
			(voice_die,"snd_man_die"),
			(voice_hit,"snd_man_hit"),
			(voice_grunt,"snd_man_grunt"),
			(voice_grunt_long,"snd_man_grunt_long"),
			(voice_yell,"snd_man_yell"),
			(voice_stun,"snd_man_stun"),
			(voice_victory,"snd_man_victory")
		], #voice sounds
    "skel_human", 1.0,
    psys_game_blood,psys_game_blood_2,
  ),
  
  (
    "woman", skf_use_morph_key_10,
    "woman_body",  "woman_calf_l", "f_handL",
    "female_head", woman_face_keys,
    [
			"woman_hair_p",
			"woman_hair_n",
			"woman_hair_o",
			"woman_hair_q",
			"woman_hair_r",
			"woman_hair_t",
			"woman_hair_s",
			
			"sib_curly",
			"longstraight",
			"courthair",
			"man_hair_r",
			"man_hair_p",
			"man_hair_q",
			"man_hair_n",
			"man_hair_s",
			"man_hair_m",
			"man_hair_o",
		], #woman_hair_meshes
    [
			"acc1",
			"acc2",
			"acc3",
			"acc4",
			"acc5",
			"acc6",
			"acc7",
			"acc8",
			"acc9",
			"acc10",
			"acc11",
			"acc12",
			"acc13",
			"acc14"
		],
    ["hair_blonde"], #hair textures
    ["jewellery"],
    [
			("womanface_young",0xffe3e8ef,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("womanface_b",0xffdfdfdf,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("womanface_a",0xffe8dfe5,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("womanface_brown",0xffaf9f7e,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("womanface_african",0xff808080,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
		 
			("womanface_new_young",0xffe3e8ef,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("womanface_new_b",0xffdfdfdf,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("womanface_new_a",0xffe8dfe5,["hair_blonde"],[0xffffffff, 0xff171313, 0xff803526]),
			("womanface_new_brown",0xffaf9f7e,["hair_blonde"],[0xff333333, 0xff171313, 0xff0a0a0a]),
			("womanface_new_african",0xff808080,["hair_blonde"],[0xff171313, 0xff0a0a0a]),
			
#     ("womanface_midage",0xffe5eaf0,["hair_black","hair_brunette","hair_red","hair_white"],[0xffffcded, 0xffbbcded, 0xff99eebb]),
    ],#woman_face_textures
    [
			(voice_die,"snd_woman_die"),
			(voice_hit,"snd_woman_hit"),
			(voice_yell,"snd_woman_yell")
		], #voice sounds
    "skel_human", 1.0,
    psys_game_blood,psys_game_blood_2,
  ),
  
	#workarround to disable hardcodes face-borrowing when skin number is <=2
  ("dummy", 0, "man_body", "man_calf_l", "m_handL", "male_head", man_face_keys, [], [], [], [], [], [], "skel_human", 1.0, psys_game_blood, psys_game_blood_2),
#MOD end
]

