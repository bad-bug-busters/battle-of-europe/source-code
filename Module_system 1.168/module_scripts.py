# -*- coding: cp1254 -*-
from header_common import *
from header_operations import *
from module_constants import *
from header_parties import *
from header_skills import *
from header_mission_templates import *
from header_items import *
from header_triggers import *
from header_terrain_types import *
from header_music import *
from header_map_icons import *
from ID_animations import *
#MOD begin
from module_items import items
from module_scene_props import scene_props
from module_troops import troops
# from ID_troops import *
import math
#MOD end

####################################################################################################################
# scripts is a list of script records.
# Each script record contns the following two fields:
# 1) Script id: The prefix "script_" will be inserted when referencing scripts.
# 2) Operation block: This must be a valid operation block. See header_operations.py for reference.
####################################################################################################################

#MOD begin
###carry-slots###
# itcf_carry_mask crashes the game on weapon change #not used
#slot 0: #cant sheath# has no carry slot
#slot 1: #back 1# itcf_carry_quiver_back, itcf_carry_axe_back, itcf_carry_sword_back, itcf_carry_spear
#slot 2: #back 2# itcf_carry_crossbow_back, itcf_carry_bow_back
#slot 3: #back 3# itcf_carry_kite_shield, itcf_carry_round_shield, itcf_carry_board_shield
#slot 4: #left stomach# itcf_carry_pistol_front_left, itcf_carry_dagger_front_left
#slot 5: #right stomach# itcf_carry_dagger_front_right
#slot 6: #left hip 1# itcf_carry_sword_left_hip, itcf_carry_axe_left_hip, itcf_carry_mace_left_hip, itcf_carry_katana
#slot 7: #left hip 2# itcf_carry_wakizashi
#slot 8: #left hip 3# itcf_carry_bowcase_left
#slot 9: #left hip 4# itcf_carry_buckler_left
#slot 10: #right hip# itcf_carry_quiver_right_vertical, itcf_carry_quiver_front_right #flipped, itcf_carry_quiver_back_right
#slot 11: #right upper leg# itcf_carry_revolver_right	  

def init_game_data():
    objects = []
    for i in range (len(items)):
        item_type = items[i][3]&0x000000f
        if items[i][0].endswith('_alt_mode'):
			if item_type <= itp_type_polearm and items[i-1][3] & itp_next_item_as_melee == 0: #no default alt mode
				objects.append((item_set_slot, i, slot_item_is_alt_mode, 2))
			else:
				objects.append((item_set_slot, i, slot_item_is_alt_mode, 1))

        carry = items[i][4] & itcf_carry_mask
        if carry == itcf_carry_quiver_back or carry == itcf_carry_axe_back or carry == itcf_carry_sword_back or carry == itcf_carry_spear:
            objects.append((item_set_slot, i, slot_item_carry_slot, 1))
        elif carry == itcf_carry_crossbow_back or carry == itcf_carry_bow_back:
            objects.append((item_set_slot, i, slot_item_carry_slot, 2))
        elif carry == itcf_carry_kite_shield or carry == itcf_carry_round_shield or carry == itcf_carry_board_shield:
            objects.append((item_set_slot, i, slot_item_carry_slot, 3))
        elif carry == itcf_carry_pistol_front_left or carry == itcf_carry_dagger_front_left:
            objects.append((item_set_slot, i, slot_item_carry_slot, 4))
        elif carry == itcf_carry_dagger_front_right:
            objects.append((item_set_slot, i, slot_item_carry_slot, 5))
        elif carry == itcf_carry_sword_left_hip or carry == itcf_carry_axe_left_hip or carry == itcf_carry_mace_left_hip or carry == itcf_carry_katana:
            objects.append((item_set_slot, i, slot_item_carry_slot, 6))
        elif carry == itcf_carry_wakizashi:
            objects.append((item_set_slot, i, slot_item_carry_slot, 7))
        elif carry == itcf_carry_bowcase_left:
            objects.append((item_set_slot, i, slot_item_carry_slot, 8))
        elif carry == itcf_carry_buckler_left:
            objects.append((item_set_slot, i, slot_item_carry_slot, 9))
        elif carry == itcf_carry_quiver_right_vertical or carry == itcf_carry_quiver_front_right or carry == itcf_carry_quiver_back_right:
            objects.append((item_set_slot, i, slot_item_carry_slot, 10))
        elif carry == itcf_carry_revolver_right:
            objects.append((item_set_slot, i, slot_item_carry_slot, 11))

        # if (i < itm_mod_1_items_begin or i > itm_special_items_begin) and item_type <= itp_type_polearm and items[i-1][3] & itp_next_item_as_melee == 0:
        #     reach = get_weapon_length(items[i][6])
        #     pob = int(get_missile_speed(items[i][6]) + reach * 0.2)
        #     offset = int(reach * 0.1)
        #     length = int(reach * 1.3)
        #     factor_1 = 1.0 / max(pob, 1)
        #     factor_2 = 1.0 / max(length - pob, 1)

        #     weight_multi = 1000
        #     speed_multi = 1000.0 / (reach + 45)

        #     if i < itm_mod_1_items_begin:
        #         loop_1_end = 4
        #     else:
        #         loop_1_end = 1

        #     if items[i][3] & itp_next_item_as_melee:
        #         loop_2_end = 2
        #     else:
        #         loop_2_end = 1

        #     max_momentum = 0.0
        #     for j in range (offset, offset + reach + 1):
        #         cur_pos = j - offset
        #         if j <= pob:
        #             relation = j * factor_1
        #         else:
        #             relation = 1 + (pob - j) * factor_2
        #         rel_weight = (0.5 - 0.5 * math.cos(relation * math.pi)) * weight_multi
        #         weight_slot = slot_item_pos_weight_begin + cur_pos

        #         rel_speed = (cur_pos + 45) * speed_multi
        #         speed_slot = slot_item_pos_speed_begin + cur_pos

        #         if max_momentum >= 0.0:
        #             cur_momentum = rel_weight * rel_speed
        #             if cur_momentum > max_momentum:
        #                 max_momentum = cur_momentum
        #             else:
        #                 max_momentum = -1.0 #max momentum found
        #         else:
        #             max_momentum = -2.0 #break

        #         for k in range (0, loop_1_end):
        #             for l in range (0, loop_2_end):
        #                 cur_item = i + k * itm_mod_1_items_begin + l
        #                 objects.append((item_set_slot, cur_item, weight_slot, rel_weight))
        #                 objects.append((item_set_slot, cur_item, speed_slot, rel_speed))
        #                 if max_momentum == -1.0:
        #                     objects.append((item_set_slot, cur_item, slot_item_center_of_percussion, cur_pos - 1)) #was last pos

    for i in range (len(scene_props)):
        if scene_props[i][1] & 0x0000000000008000:
            objects.append((troop_set_slot, "trp_prop_destructible", i, 1))

    for i in range (len(troops)):
        if troops[i][5] > 0:
            objects.append((troop_set_slot, i, slot_troop_army_percentage, troops[i][5]))

    return objects[:]
#MOD end

scripts = [
  #script_game_start:
  # This script is called when a new game is started
  # INPUT: none
  ("game_start",[]),

  #script_game_get_use_string
  # This script is called from the game engine for getting using information text
  # INPUT: used_scene_prop_id  
  # OUTPUT: s0
  ("game_get_use_string",
   [
     (store_script_param, ":instance_id", 1),

     (prop_instance_get_scene_prop_kind, ":scene_prop_id", ":instance_id"),

#MOD begin
    (try_begin),
      (eq, ":scene_prop_id", "spr_cannon_barrel_1_32lb"),
      (try_begin),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 0),
				(str_store_item_name, s1, itm_refiller),
				(str_store_string, s0, "@Refill Gunpowder^({s1} weapon mode needed)"),
      (else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 1),
				(str_store_item_name, s1, itm_refiller_alt_mode),
				(str_store_string, s0, "@Compress Gunpowder^({s1} weapon mode needed)"),
      (else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 2),
				(str_store_item_name, s1, itm_round_shot_32lb),
				(str_store_item_name, s2, itm_canister_shot_32lb),
				(str_store_string, s0, "@Place Ammunition^({s1} or {s2} needed)"),
      (else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 3),
				(str_store_item_name, s1, itm_refiller_alt_mode),
				(str_store_string, s0, "@Ram Down Ammunition^({s1} weapon mode needed)"),
      (else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 4),
				(str_store_string, s0, "@Push Cannon"),
      (else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 5),
				(str_store_item_name, s1, itm_linstock),
				(str_store_string, s0, "@Take Control^({s1} needed)"),
      (else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 6),
				(str_store_string, s0, "@Cannon is already used"),
      (try_end),
	  
    (else_try),
			(eq, ":scene_prop_id", "spr_cannon_ammo_box_32lb_1"),
			(str_store_string, s0, "str_choose_ammo"),
	  
    (else_try),
			(this_or_next|eq, ":scene_prop_id", "spr_ladder_move_5m"),
      (is_between, ":scene_prop_id", "spr_pavise_prop_b", "spr_shields_end"),
      (str_store_string, s0, "str_equip"),
     
    (else_try),
      (neq, ":scene_prop_id", "spr_cannon_barrel_1_32lb"),
      (neq, ":scene_prop_id", "spr_pavise_prop_b"),
      (neq, ":scene_prop_id", "spr_pavise_prop_c"),

      (str_store_string, s0, "@shit"),
#MOD end 
     
     (try_begin),
       (this_or_next|eq, ":scene_prop_id", "spr_winch_b"),
       (eq, ":scene_prop_id", "spr_winch"),
       (assign, ":effected_object", "spr_portcullis"),
     (else_try),
       (this_or_next|eq, ":scene_prop_id", "spr_door_destructible"),
       (this_or_next|eq, ":scene_prop_id", "spr_castle_f_door_b"),
       (this_or_next|eq, ":scene_prop_id", "spr_castle_e_sally_door_a"),
       (this_or_next|eq, ":scene_prop_id", "spr_castle_f_sally_door_a"),
       (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_left"),
       (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_right"),
       (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),
       (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_right"),
       (this_or_next|eq, ":scene_prop_id", "spr_castle_f_door_a"),
       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_6m"),
       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_8m"),
       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_10m"),
       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_12m"),
       (eq, ":scene_prop_id", "spr_siege_ladder_move_14m"),
       (assign, ":effected_object", ":scene_prop_id"),
     (try_end),   

     (scene_prop_get_slot, ":item_situation", ":instance_id", scene_prop_open_or_close_slot),

     (try_begin), #opening/closing portcullis
       (eq, ":effected_object", "spr_portcullis"),

       (try_begin),
         (eq, ":item_situation", 0),
         (str_store_string, s0, "str_open_gate"),
       (else_try), 
         (str_store_string, s0, "str_close_gate"),
       (try_end),
     (else_try), #opening/closing door
       (this_or_next|eq, ":effected_object", "spr_door_destructible"),
       (this_or_next|eq, ":effected_object", "spr_castle_f_door_b"),
       (this_or_next|eq, ":effected_object", "spr_castle_e_sally_door_a"),
       (this_or_next|eq, ":effected_object", "spr_castle_f_sally_door_a"),
       (this_or_next|eq, ":effected_object", "spr_earth_sally_gate_left"),
       (this_or_next|eq, ":effected_object", "spr_earth_sally_gate_right"),
       (this_or_next|eq, ":effected_object", "spr_viking_keep_destroy_sally_door_left"),
       (this_or_next|eq, ":effected_object", "spr_viking_keep_destroy_sally_door_right"),
       (eq, ":effected_object", "spr_castle_f_door_a"),

       (try_begin),
         (eq, ":item_situation", 0),
         (str_store_string, s0, "str_open_door"),
       (else_try),
         (str_store_string, s0, "str_close_door"),
       (try_end),
     (else_try), #raising/dropping ladder
       (try_begin),
         (eq, ":item_situation", 0),
         (str_store_string, s0, "str_raise_ladder"),
       (else_try),
         (str_store_string, s0, "str_drop_ladder"),
       (try_end),
     (try_end),
#MOD begin
     (try_end),
#MOD end
   ]),

  #script_game_quick_start
  # This script is called from the game engine for initializing the global variables for tutorial, multiplayer and custom battle modes.
  # INPUT:
  # none
  # OUTPUT:
  # none
  ("game_quick_start",
	[
			(call_script, "script_init_game_data"),

      #for multiplayer mode
      (assign, "$g_multiplayer_selected_map", "scn_multi_scene_1"),
      (assign, "$g_multiplayer_respawn_period", 10),# MOD edit
      (assign, "$g_multiplayer_round_max_seconds", 900),# MOD edit
      # (assign, "$g_multiplayer_game_max_minutes", 30),# MOD edit
      (assign, "$g_multiplayer_game_max_points", 1000),# MOD edit

      (server_get_renaming_server_allowed, "$g_multiplayer_renaming_server_allowed"),
      (server_get_changing_game_type_allowed, "$g_multiplayer_changing_game_type_allowed"),
      # (assign, "$g_multiplayer_point_gained_from_flags", 100),#MOD disable
      # (assign, "$g_multiplayer_point_gained_from_capturing_flag", 5),#MOD disable
      (assign, "$g_multiplayer_game_type", 0),
      (assign, "$g_multiplayer_team_1_faction", "fac_kingdom_1"),
      (assign, "$g_multiplayer_team_2_faction", "fac_kingdom_2"),
      (assign, "$g_multiplayer_next_team_1_faction", "$g_multiplayer_team_1_faction"),
      (assign, "$g_multiplayer_next_team_2_faction", "$g_multiplayer_team_2_faction"),
      (assign, "$g_multiplayer_num_bots_team_1", 0),
      (assign, "$g_multiplayer_num_bots_team_2", 0),
      (assign, "$g_multiplayer_number_of_respawn_count", 0),
      (assign, "$g_multiplayer_num_bots_voteable", 100),
      (assign, "$g_multiplayer_max_num_bots", 10001),#MOD edit #was 101
      (assign, "$g_multiplayer_factions_voteable", 1),
      (assign, "$g_multiplayer_maps_voteable", 1),
      (assign, "$g_multiplayer_kick_voteable", 1),
      (assign, "$g_multiplayer_ban_voteable", 1),
      # (assign, "$g_multiplayer_valid_vote_ratio", 51), #more than 50 percent
      (assign, "$g_multiplayer_auto_team_balance_limit", 3), #auto balance
      (assign, "$g_multiplayer_player_respawn_as_bot", 1),
      (assign, "$g_multiplayer_stats_chart_opened_manually", 0),
      (assign, "$g_multiplayer_mission_end_screen", 0),
      (assign, "$g_multiplayer_ready_for_spawning_agent", 1),
      (assign, "$g_multiplayer_welcome_message_shown", 0),
      (assign, "$g_multiplayer_allow_player_banners", 1),
      # (assign, "$g_multiplayer_force_default_armor", 0),#MOD disable
      # (assign, "$g_multiplayer_disallow_ranged_weapons", 0),
      
#MOD disable
      # (assign, "$g_multiplayer_initial_gold_multiplier", 100),
      # (assign, "$g_multiplayer_battle_earnings_multiplier", 100),
      # (assign, "$g_multiplayer_round_earnings_multiplier", 100),

#MOD begin
      (assign, "$player_start_gold", 600),
      (assign, "$player_tick_gold", 100),
      (assign, "$player_score_gold", 1),

			(assign, "$g_close_multiplayer_admin_panel", 0),

      (assign, "$g_multiplayer_cloud_amount", -1),
			(assign, "$g_multiplayer_precipitation_strength", -1),
			(assign, "$g_multiplayer_fog_distance", -1),
			(assign, "$g_multiplayer_thunderstorm", -1),
			(assign, "$g_multiplayer_wind_strength", -1),
			(assign, "$g_multiplayer_wind_direction", -1),
      (assign, "$g_multiplayer_day_time", -1),
			
			(assign, "$g_multiplayer_wt_val_1", 0),
			(assign, "$g_multiplayer_wt_val_2", 0),

      # (assign, "$g_multiplayer_disallow_melee_weapons", 0),
      # (assign, "$g_multiplayer_disallow_armors", 0),
      # (assign, "$g_multiplayer_disallow_horses", 0),
			
			(assign, "$g_multiplayer_announcement_interval", 180),
			
			(assign, "$g_team_hits_til_kick", 10),
			(assign, "$g_team_hit_kicks_til_ban", 3),

      (assign, "$g_show_damage_report", 0),
      (assign, "$g_show_shot_distance", 0),
      (assign, "$g_show_movement_speed", 0),
	  
			(assign, "$last_player_count", -1),
			
			(assign, "$g_cur_custom_chat", -1),
			(assign, "$g_init_on_round_end", 0),
			
			(assign, "$g_cur_troop_presentation", -1),
			
			(assign, "$g_player_first_connect", 0),
			
			(assign, "$g_request_length", 0),

			# (assign, "$g_replace_editor_props", 1),
			
			(assign, "$g_cur_scene_type", 0),
			(assign, "$g_cur_scene_water", 0),
			
			(assign, "$g_cloud_amount", 0),
			(assign, "$g_precipitation_strength", 0),
			(assign, "$g_fog_distance", 10000),
			(assign, "$g_wind_strength", 0),
			(assign, "$g_wind_direction", 0),
			(assign, "$g_wind_x_mm", 0),
			(assign, "$g_wind_y_mm", 0),
			(assign, "$g_day_time", 12),
			
			(assign, "$agent_deployed_shield", -1),
			(assign, "$custom_camera", 0),
      # (assign, "$camera_height", 8),
			
			(assign, "$g_show_debug_messages", 0),
			(assign, "$g_disable_hud", 0),
			
			(assign, "$g_quick_artillery", 0),
			(assign, "$g_server_announcements", 1),
			(assign, "$g_inventory_all_items", 1),
			(assign, "$g_allow_online_chars", 0),
			(assign, "$g_allow_faction_troops", 1),
			(assign, "$agent_deployed_shield", -1),
			# (assign, "$next_spawn_prop_prune_time", -1),
			(assign, "$next_spawn_prop_hp", -1),
			(assign, "$spawn_prop_different_hp", -1),
			(assign, "$g_last_parent_prop", -1),
			
			(assign, "$g_last_gold", 0),
			(assign, "$g_last_stamina", 0),
			(assign, "$g_last_drown_time", 0),
			
			(assign, "$g_timer_a_last_value", 0),
			
			(assign, "$g_thunderbolt_end_time", -1),
			(assign, "$g_thunderbolt_next_time", -1),
			(assign, "$g_thunderbolt_state", 0),
			(assign, "$g_thunderbolt_x", 0),
			(assign, "$g_thunderbolt_y", 0),
			(assign, "$g_thunderbolt_z", 0),
			
			(assign, "$i_size", 2),

      # (assign, "$frist_time", 0),


			
			# (assign, "$g_enable_ssao", 0),

			#assign skill wpt bonus
			(try_for_range, ":cur_troop", multiplayer_ai_troops_begin, multiplayer_ai_troops_end),
				(store_skill_level, ":pp_wpt", "skl_power_pull", ":cur_troop"),
				(store_skill_level, ":pr_wpt", "skl_power_reload", ":cur_troop"),
				(store_mul, ":skl_wpt_archery", ":pp_wpt", pp_archery_bonus),
				(store_mul, ":skl_wpt_crossbow", ":pp_wpt", pp_crossbow_bonus),
				(store_mul, ":skl_wpt_firearm", ":pr_wpt", pr_firearm_bonus),
				(troop_raise_proficiency_linear, ":cur_troop", wpt_archery, ":skl_wpt_archery"),
				(troop_raise_proficiency_linear, ":cur_troop", wpt_crossbow, ":skl_wpt_crossbow"),
				(troop_raise_proficiency_linear, ":cur_troop", wpt_firearm, ":skl_wpt_firearm"),
			(try_end),
			
			#sound channels
			(try_for_range, ":cur_sound_channel", "trp_sound_channel_0", "trp_sound_channels_end"),
				(troop_set_slot, ":cur_sound_channel", sound_slot_id, -1),
			(try_end),
			
			#init child prop values
			(try_for_range, ":cur_child", 0, 20),
				(troop_set_slot, "trp_cannon_1_childs", ":cur_child", -1),
			(try_end),
			(troop_set_slot, "trp_cannon_1_childs", 0, "spr_cannon_barrel_1_32lb"),
			(try_for_range, ":cur_child", 1, 5),
				(troop_set_slot, "trp_cannon_1_childs", ":cur_child", "spr_cannon_wheel_1"),
			(try_end),
			#x=0, y=1, z=2
			(troop_set_slot, "trp_cannon_1_child_1_pos", pos_x, 0),
			(troop_set_slot, "trp_cannon_1_child_1_pos", pos_y, 35),
			(troop_set_slot, "trp_cannon_1_child_1_pos", pos_z, 80),
			
			(troop_set_slot, "trp_cannon_1_child_2_pos", pos_x, -47),
			(troop_set_slot, "trp_cannon_1_child_2_pos", pos_y, 65),
			(troop_set_slot, "trp_cannon_1_child_2_pos", pos_z, 20),
			
			(troop_set_slot, "trp_cannon_1_child_3_pos", pos_x, 47),
			(troop_set_slot, "trp_cannon_1_child_3_pos", pos_y, 65),
			(troop_set_slot, "trp_cannon_1_child_3_pos", pos_z, 20),
			
			(troop_set_slot, "trp_cannon_1_child_4_pos", pos_x, -47),
			(troop_set_slot, "trp_cannon_1_child_4_pos", pos_y, -65),
			(troop_set_slot, "trp_cannon_1_child_4_pos", pos_z, 20),
			
			(troop_set_slot, "trp_cannon_1_child_5_pos", pos_x, 47),
			(troop_set_slot, "trp_cannon_1_child_5_pos", pos_y, -65),
			(troop_set_slot, "trp_cannon_1_child_5_pos", pos_z, 20),
			
			#init construct prop build point cost
			(troop_set_slot, "trp_construct_prop_cost", "spr_construct_box_a", 6),
			(troop_set_slot, "trp_construct_prop_cost", "spr_construct_siege_large_shield_a", 24),
			(troop_set_slot, "trp_construct_prop_cost", "spr_construct_spike_group_a", 15),
			(troop_set_slot, "trp_construct_prop_cost", "spr_construct_plank", 4),
			(troop_set_slot, "trp_construct_prop_cost", "spr_construct_earthwork", 5),
			(troop_set_slot, "trp_construct_prop_cost", "spr_construct_gabion", 12),

      #faction banners
      (assign, ":cur_faction_banner", "mesh_banner_kingdom_01"),
			(assign, ":cur_faction_flag", "mesh_flag_kingdom_01"),
      (try_for_range, ":cur_faction", npc_kingdoms_begin, npc_kingdoms_end),
        (faction_set_slot, ":cur_faction", slot_faction_banner, ":cur_faction_banner"),
				(faction_set_slot, ":cur_faction", slot_faction_flag, ":cur_faction_flag"),
        (val_add, ":cur_faction_banner", 1),
				(val_add, ":cur_faction_flag", 1),
      (try_end),
      (call_script, "script_initialize_banner_info"),
	  
			#set online level xp
			(assign, ":cur_xp", online_lvl_xp_base),
			(assign, ":add_xp", online_lvl_xp_base),
			(try_for_range, ":cur_level", online_min_lvl +1, online_max_lvl +1),
				(troop_set_slot, "trp_online_level_xp", ":cur_level", ":cur_xp"),
				# (assign, reg10, ":cur_level"),
				# (assign, reg11, ":cur_xp"),
				# (display_message, "@level {reg10} needed xp {reg11}"),
				(try_begin),
					(lt, ":cur_level", online_retire_lvl -1),
					(store_mul, ":cur_adder", ":cur_level", online_lvl_xp_adder),
					(val_add, ":add_xp", ":cur_adder"),
				(else_try),
					(assign, ":add_xp", ":cur_xp"),
				(try_end),
				(val_add, ":cur_xp", ":add_xp"),
			(try_end),
      
      # (try_for_range, ":cur_item", all_items_begin, all_items_end),
        # (try_for_range, ":cur_faction", npc_kingdoms_begin, npc_kingdoms_end),
          # (store_sub, ":faction_index", ":cur_faction", npc_kingdoms_begin),
          # (val_add, ":faction_index", slot_item_multiplayer_faction_price_multipliers_begin),
          # (item_set_slot, ":cur_item", ":faction_index", 100), #100 is the default price multiplier
        # (try_end),
      # (try_end),
			
			(troop_set_slot, "trp_fog_color", 0, 0x101216),
			
			(troop_set_slot, "trp_fog_color", 1, 0x101215),
			(troop_set_slot, "trp_fog_color", 2, 0x101215),
			(troop_set_slot, "trp_fog_color", 3, 0x0f1113),
			(troop_set_slot, "trp_fog_color", 4, 0x383d3e),
			(troop_set_slot, "trp_fog_color", 5, 0x667481),
			(troop_set_slot, "trp_fog_color", 6, 0x748396),
			(troop_set_slot, "trp_fog_color", 7, 0x78899e),
			(troop_set_slot, "trp_fog_color", 8, 0x7c8da3),
			(troop_set_slot, "trp_fog_color", 9, 0x8191a6),
			(troop_set_slot, "trp_fog_color",10, 0x8192a8),
			(troop_set_slot, "trp_fog_color",11, 0x8393aa),
			
			(troop_set_slot, "trp_fog_color",12, 0x8393aa),
			
			(troop_set_slot, "trp_fog_color",13, 0x8393aa),
			(troop_set_slot, "trp_fog_color",14, 0x8192a8),
			(troop_set_slot, "trp_fog_color",15, 0x8191a6),
			(troop_set_slot, "trp_fog_color",16, 0x7c8da3),
			(troop_set_slot, "trp_fog_color",17, 0x78899e),
			(troop_set_slot, "trp_fog_color",18, 0x748396),
			(troop_set_slot, "trp_fog_color",19, 0x667481),
			(troop_set_slot, "trp_fog_color",20, 0x383d3e),
			(troop_set_slot, "trp_fog_color",21, 0x0f1113),
			(troop_set_slot, "trp_fog_color",22, 0x101215),
			(troop_set_slot, "trp_fog_color",23, 0x101215),

			(try_for_range, ":cur_troop", multiplayer_troops_begin, multiplayer_troops_end),
				(troop_get_inventory_capacity, ":inv_size", ":cur_troop"),
				(try_for_range, ":i", 0, ":inv_size"),
					(troop_get_inventory_slot, ":cur_item", ":cur_troop", ":i"),
					(gt, ":cur_item", -1),
					(call_script, "script_multiplayer_set_item_available_for_troop", ":cur_item", ":cur_troop", 1),
				(try_end),
			(try_end),
#MOD end
      ]),

  #script_game_set_multiplayer_mission_end
  # This script is called from the game engine when a multiplayer map is ended in clients (not in server).
  # INPUT:
  # none
  # OUTPUT:
  # none
  ("game_set_multiplayer_mission_end",
	[
		(assign, "$g_multiplayer_mission_end_screen", 1),
  ]),

  ("game_enable_cheat_menu",[]),

  #script_game_get_console_command
  # This script is called from the game engine when a console command is entered from the dedicated server.
  # INPUT: anything
  # OUTPUT: s0 = result text
  ("game_get_console_command",
   [
     (store_script_param, ":input", 1),
     (store_script_param, ":val1", 2),
     (try_begin),
       #getting val2 for some commands
       (eq, ":input", 2),
       (store_script_param, ":val2", 3),
     (end_try),
     (try_begin),
       (eq, ":input", 1),
       
       # (assign, reg0, ":val1"),
       # (try_begin),
         # (eq, ":val1", 1),
         # (assign, reg1, "$g_multiplayer_num_bots_team_1"),
         # (str_store_string, s0, "str_team_reg0_bot_count_is_reg1"),
       # (else_try),
         # (eq, ":val1", 2),
         # (assign, reg1, "$g_multiplayer_num_bots_team_2"),
         # (str_store_string, s0, "str_team_reg0_bot_count_is_reg1"),
       # (else_try),
         # (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       # (try_end),
     (else_try),
       (eq, ":input", 2),
       (assign, reg0, ":val1"),
       (assign, reg1, ":val2"),
       (try_begin),
         (eq, ":val1", 1),
         (ge, ":val2", 0),
         (assign, "$g_multiplayer_num_bots_team_1", ":val2"),
         (str_store_string, s0, "str_team_reg0_bot_count_is_reg1"),
       (else_try),
         (eq, ":val1", 2),
         (ge, ":val2", 0),
         (assign, "$g_multiplayer_num_bots_team_2", ":val2"),
         (str_store_string, s0, "str_team_reg0_bot_count_is_reg1"),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 3),
       (assign, reg0, "$g_multiplayer_round_max_seconds"),
       (str_store_string, s0, "str_maximum_seconds_for_round_is_reg0"),
     (else_try),
       (eq, ":input", 4),
       (assign, reg0, ":val1"),
       (try_begin),
         (is_between, ":val1", multiplayer_round_max_seconds_min, multiplayer_round_max_seconds_max),
         (assign, "$g_multiplayer_round_max_seconds", ":val1"),
         (str_store_string, s0, "str_maximum_seconds_for_round_is_reg0"),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_round_max_seconds, ":val1"),
         (try_end),            
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 5),
       (assign, reg0, "$g_multiplayer_respawn_period"),
       (str_store_string, s0, "str_respawn_period_is_reg0_seconds"),
     (else_try),
       (eq, ":input", 6),
       (assign, reg0, ":val1"),
       (try_begin),
         (is_between, ":val1", multiplayer_respawn_period_min, multiplayer_respawn_period_max),
         (assign, "$g_multiplayer_respawn_period", ":val1"),
         (str_store_string, s0, "str_respawn_period_is_reg0_seconds"),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 7),
       (assign, reg0, "$g_multiplayer_num_bots_voteable"),
       (str_store_string, s0, "str_bots_upper_limit_for_votes_is_reg0"),
     (else_try),
       (eq, ":input", 8),
       (try_begin),
         (is_between, ":val1", 0, 51),
         (assign, "$g_multiplayer_num_bots_voteable", ":val1"),
         (store_add, "$g_multiplayer_max_num_bots", ":val1", 1),
         (assign, reg0, "$g_multiplayer_num_bots_voteable"),
         (str_store_string, s0, "str_bots_upper_limit_for_votes_is_reg0"),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_num_bots_voteable, ":val1"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 9),
       (try_begin),
         (eq, "$g_multiplayer_maps_voteable", 1),
         (str_store_string, s0, "str_map_is_voteable"),
       (else_try),
         (str_store_string, s0, "str_map_is_not_voteable"),
       (try_end),
     (else_try),
       (eq, ":input", 10),
       (try_begin),
         (is_between, ":val1", 0, 2),
         (assign, "$g_multiplayer_maps_voteable", ":val1"),
         (try_begin),
           (eq, ":val1", 1),
           (str_store_string, s0, "str_map_is_voteable"),
         (else_try),
           (str_store_string, s0, "str_map_is_not_voteable"),
         (try_end),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_maps_voteable, ":val1"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 11),
       (try_begin),
         (eq, "$g_multiplayer_factions_voteable", 1),
         (str_store_string, s0, "str_factions_are_voteable"),
       (else_try),
         (str_store_string, s0, "str_factions_are_not_voteable"),
       (try_end),
     (else_try),
       (eq, ":input", 12),
       (try_begin),
         (is_between, ":val1", 0, 2),
         (assign, "$g_multiplayer_factions_voteable", ":val1"),
         (try_begin),
           (eq, ":val1", 1),
           (str_store_string, s0, "str_factions_are_voteable"),
         (else_try),
           (str_store_string, s0, "str_factions_are_not_voteable"),
         (try_end),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_factions_voteable, ":val1"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 13),
       (try_begin),
         (eq, "$g_multiplayer_player_respawn_as_bot", 1),
         (str_store_string, s0, "str_players_respawn_as_bot"),
       (else_try),
         (str_store_string, s0, "str_players_do_not_respawn_as_bot"),
       (try_end),
     (else_try),
       (eq, ":input", 14),
       (try_begin),
         (is_between, ":val1", 0, 2),
         (assign, "$g_multiplayer_player_respawn_as_bot", ":val1"),
         (try_begin),
           (eq, ":val1", 1),
           (str_store_string, s0, "str_players_respawn_as_bot"),
         (else_try),
           (str_store_string, s0, "str_players_do_not_respawn_as_bot"),
         (try_end),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_player_respawn_as_bot, ":val1"),
         (try_end),            
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 15),
       (try_begin),
         (eq, "$g_multiplayer_kick_voteable", 1),
         (str_store_string, s0, "str_kicking_a_player_is_voteable"),
       (else_try),
         (str_store_string, s0, "str_kicking_a_player_is_not_voteable"),
       (try_end),
     (else_try),
       (eq, ":input", 16),
       (try_begin),
         (is_between, ":val1", 0, 2),
         (assign, "$g_multiplayer_kick_voteable", ":val1"),
         (try_begin),
           (eq, ":val1", 1),
           (str_store_string, s0, "str_kicking_a_player_is_voteable"),
         (else_try),
           (str_store_string, s0, "str_kicking_a_player_is_not_voteable"),
         (try_end),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_kick_voteable, ":val1"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 17),
       (try_begin),
         (eq, "$g_multiplayer_ban_voteable", 1),
         (str_store_string, s0, "str_banning_a_player_is_voteable"),
       (else_try),
         (str_store_string, s0, "str_banning_a_player_is_not_voteable"),
       (try_end),
     (else_try),
       (eq, ":input", 18),
       (try_begin),
         (is_between, ":val1", 0, 2),
         (assign, "$g_multiplayer_ban_voteable", ":val1"),
         (try_begin),
           (eq, ":val1", 1),
           (str_store_string, s0, "str_banning_a_player_is_voteable"),
         (else_try),
           (str_store_string, s0, "str_banning_a_player_is_not_voteable"),
         (try_end),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_ban_voteable, ":val1"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     # (else_try),
       # (eq, ":input", 19),
       # (assign, reg0, "$g_multiplayer_valid_vote_ratio"),
       # (str_store_string, s0, "str_percentage_of_yes_votes_required_for_a_poll_to_get_accepted_is_reg0"),
     # (else_try),
       # (eq, ":input", 20),
       # (try_begin),
         # (is_between, ":val1", 50, 101),
         # (assign, "$g_multiplayer_valid_vote_ratio", ":val1"),
         # (assign, reg0, ":val1"),
         # (str_store_string, s0, "str_percentage_of_yes_votes_required_for_a_poll_to_get_accepted_is_reg0"),
       # (else_try),
         # (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       # (try_end),
     (else_try),
       (eq, ":input", 21),
       (assign, reg0, "$g_multiplayer_auto_team_balance_limit"),
       (str_store_string, s0, "str_auto_team_balance_threshold_is_reg0"),
     (else_try),
       (eq, ":input", 22),
       (try_begin),
         (is_between, ":val1", 2, 7),
         (assign, "$g_multiplayer_auto_team_balance_limit", ":val1"),
         (assign, reg0, "$g_multiplayer_auto_team_balance_limit"),
         (str_store_string, s0, "str_auto_team_balance_threshold_is_reg0"),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_auto_team_balance_limit, ":val1"),
         (try_end),
       (else_try),
         (ge, ":val1", 7),
         (assign, "$g_multiplayer_auto_team_balance_limit", 1000),
         (assign, reg0, "$g_multiplayer_auto_team_balance_limit"),
         (str_store_string, s0, "str_auto_team_balance_threshold_is_reg0"),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_auto_team_balance_limit, ":val1"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
#MOD begin
     (else_try),
       (eq, ":input", 23),
       (assign, reg0, "$player_start_gold"),
       (str_store_string, s0, "str_starting_gold_ratio_is_reg0"),
     (else_try),
       (eq, ":input", 24),
       (try_begin),
         (is_between, ":val1", 0, 10001),
         (assign, "$player_start_gold", ":val1"),
         (assign, reg0, "$player_start_gold"),
         (str_store_string, s0, "str_starting_gold_ratio_is_reg0"),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 25),
       (assign, reg0, "$player_score_gold"),
       (str_store_string, s0, "str_combat_gold_bonus_ratio_is_reg0"),
     (else_try),
       (eq, ":input", 26),
       (try_begin),
         (is_between, ":val1", 0, 11),
         (assign, "$player_score_gold", ":val1"),
         (assign, reg0, "$player_score_gold"),
         (str_store_string, s0, "str_combat_gold_bonus_ratio_is_reg0"),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 27),
       (assign, reg0, "$player_tick_gold"),
       (str_store_string, s0, "str_round_gold_bonus_ratio_is_reg0"),
     (else_try),
       (eq, ":input", 28),
       (try_begin),
         (is_between, ":val1", 0, 1001),
         (assign, "$player_tick_gold", ":val1"),
         (assign, reg0, "$player_tick_gold"),
         (str_store_string, s0, "str_round_gold_bonus_ratio_is_reg0"),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
#MOD end
     (else_try),
       (eq, ":input", 29),
       (try_begin),
         (eq, "$g_multiplayer_allow_player_banners", 1),
         (str_store_string, s0, "str_player_banners_are_allowed"),
       (else_try),
         (str_store_string, s0, "str_player_banners_are_not_allowed"),
       (try_end),
     (else_try),
       (eq, ":input", 30),
       (try_begin),
         (is_between, ":val1", 0, 2),
         (assign, "$g_multiplayer_allow_player_banners", ":val1"),
         (try_begin),
           (eq, ":val1", 1),
           (str_store_string, s0, "str_player_banners_are_allowed"),
         (else_try),
           (str_store_string, s0, "str_player_banners_are_not_allowed"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 31),
       (try_begin),
         (eq, "$g_inventory_all_items", 1),
         (str_store_string, s0, "str_default_armor_is_forced"),
       (else_try),
         (str_store_string, s0, "str_default_armor_is_not_forced"),
       (try_end),
     (else_try),
       (eq, ":input", 32),
       (try_begin),
         (is_between, ":val1", 0, 2),
         (assign, "$g_inventory_all_items", ":val1"),
         (try_begin),
           (eq, ":val1", 1),
           (str_store_string, s0, "str_default_armor_is_forced"),
         (else_try),
           (str_store_string, s0, "str_default_armor_is_not_forced"),
         (try_end),
         (try_for_players, ":player", 1),
            (multiplayer_send_3_int_to_player, ":player", multiplayer_event_return_admin_settings, 0, sub_event_all_items, "$g_inventory_all_items"),
         (try_end),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
#MOD disable
     # (else_try),
       # (eq, ":input", 33),
       # (assign, reg0, "$g_multiplayer_point_gained_from_flags"),
       # (str_store_string, s0, "str_point_gained_from_flags_is_reg0"),
     # (else_try),
       # (eq, ":input", 34),
       # (try_begin),
         # (is_between, ":val1", 25, 401),
         # (assign, "$g_multiplayer_point_gained_from_flags", ":val1"),
         # (assign, reg0, "$g_multiplayer_point_gained_from_flags"),
         # (str_store_string, s0, "str_point_gained_from_flags_is_reg0"),
       # (else_try),
         # (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       # (try_end),
     # (else_try),
       # (eq, ":input", 35),
       # (assign, reg0, "$g_multiplayer_point_gained_from_capturing_flag"),
       # (str_store_string, s0, "str_point_gained_from_capturing_flag_is_reg0"),
     # (else_try),
       # (eq, ":input", 36),
       # (try_begin),
         # (is_between, ":val1", 0, 11),
         # (assign, "$g_multiplayer_point_gained_from_capturing_flag", ":val1"),
         # (assign, reg0, "$g_multiplayer_point_gained_from_capturing_flag"),
         # (str_store_string, s0, "str_point_gained_from_capturing_flag_is_reg0"),
       # (else_try),
         # (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       # (try_end),
     # (else_try),
       # (eq, ":input", 37),
       # (assign, reg0, "$g_multiplayer_game_max_minutes"),
       # (str_store_string, s0, "str_map_time_limit_is_reg0"),
     # (else_try),
       # (eq, ":input", 38),
       # (try_begin),
         # (is_between, ":val1", 5, 121),
         # (assign, "$g_multiplayer_game_max_minutes", ":val1"),
         # (assign, reg0, "$g_multiplayer_game_max_minutes"),
         # (str_store_string, s0, "str_map_time_limit_is_reg0"),
       # (else_try),
         # (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       # (try_end),
     (else_try),
       (eq, ":input", 39),
       (assign, reg0, "$g_multiplayer_game_max_points"),
       (str_store_string, s0, "str_team_points_limit_is_reg0"),
     (else_try),
       (eq, ":input", 40),
       (try_begin),
         (is_between, ":val1", 3, 1001),
         (assign, "$g_multiplayer_game_max_points", ":val1"),
         (assign, reg0, "$g_multiplayer_game_max_points"),
         (str_store_string, s0, "str_team_points_limit_is_reg0"),
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
     (else_try),
       (eq, ":input", 41),
       (assign, reg0, "$g_multiplayer_number_of_respawn_count"),
       (try_begin),
         (eq, reg0, 0),
         (str_store_string, s1, "str_unlimited"),
       (else_try),
         (str_store_string, s1, "str_reg0"),
       (try_end),
       (str_store_string, s0, "str_defender_spawn_count_limit_is_s1"),
     (else_try),
       (eq, ":input", 42),
       (try_begin),
         (is_between, ":val1", 0, 6),
         (assign, "$g_multiplayer_number_of_respawn_count", ":val1"),
         (assign, reg0, "$g_multiplayer_number_of_respawn_count"),
         (try_begin),
           (eq, reg0, 0),
           (str_store_string, s1, "str_unlimited"),
         (else_try),
           (str_store_string, s1, "str_reg0"),
         (try_end),
         (str_store_string, s0, "str_defender_spawn_count_limit_is_s1"),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_respawn_count, ":val1"),
         (try_end),                  
       (else_try),
         (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       (try_end),
#MOD disable
     # (else_try),
       # (eq, ":input", 43),
       # (try_begin),
         # (eq, "$g_multiplayer_disallow_ranged_weapons", 1),
         # (str_store_string, s0, "str_ranged_weapons_are_disallowed"),
       # (else_try),
         # (str_store_string, s0, "str_ranged_weapons_are_allowed"),
       # (try_end),
     # (else_try),
       # (eq, ":input", 44),
       # (try_begin),
         # (is_between, ":val1", 0, 2),
         # (assign, "$g_multiplayer_disallow_ranged_weapons", ":val1"),
         # (try_begin),
           # (eq, ":val1", 1),
           # (str_store_string, s0, "str_ranged_weapons_are_disallowed"),
         # (else_try),
           # (str_store_string, s0, "str_ranged_weapons_are_allowed"),
         # (try_end),
       # (else_try),
         # (str_store_string, s0, "str_input_is_not_correct_for_the_command_type_help_for_more_information"),
       # (try_end),
     (else_try),
       (str_store_string, s0, "@{!}DEBUG : SYSTEM ERROR!"),
     (try_end),
  ]),
  
	("game_event_party_encounter",[]),
  ("game_event_simulate_battle",[]),
  ("game_event_battle_end",[]),
  ("game_get_item_buy_price_factor",[]),
  ("game_get_item_sell_price_factor",[]),
  ("game_event_buy_item",[]),
  ("game_event_sell_item",[]),
  ("game_get_troop_wage",[]),
  ("game_get_total_wage",[]),
  ("game_get_join_cost",[]),
  ("game_get_upgrade_xp",[]),
  ("game_get_upgrade_cost",[]),
  ("game_get_prisoner_price",[]),
  ("game_check_prisoner_can_be_sold",[]),
  ("game_get_morale_of_troops_from_faction",[]),
  ("game_event_detect_party",[]),
  ("game_event_undetect_party",[]),
  ("game_get_statistics_line",[]),
  ("game_get_date_text",[]),
  ("game_get_money_text",[]),
  ("game_get_party_companion_limit",[]),
  ("game_reset_player_party_name",[]),
  ("game_get_troop_note",[]),
  ("game_get_center_note",[]),
  ("game_get_faction_note",[]),
  ("game_get_quest_note",[]),
  ("game_get_info_page_note",[]),

  #script_game_get_scene_name
  # This script is called from the game engine when a name for the scene is needed.
  # INPUT: arg1 = scene_no
  # OUTPUT: s0 = name
  ("game_get_scene_name",
	[
#MOD begin
		(store_script_param, ":scene_no", 1),
		(val_add, ":scene_no", "str_scene_0"),
		(str_store_string, s0, ":scene_no"),
#MOD end
	]),
  
  #script_game_get_mission_template_name
  # This script is called from the game engine when a name for the mission template is needed.
  # INPUT: arg1 = mission_template_no
  # OUTPUT: s0 = name
  ("game_get_mission_template_name",
    [
      (store_script_param, ":mission_template_no", 1),
      (call_script, "script_multiplayer_get_mission_template_game_type", ":mission_template_no"),
      (assign, ":game_type", reg0),
      (try_begin),
        (is_between, ":game_type", 0, multiplayer_num_game_types),
        (store_add, ":string_id", ":game_type", multiplayer_game_type_names_begin),
        (str_store_string, s0, ":string_id"),
      (try_end),
     ]),

  #script_add_kill_death_counts
  # INPUT: arg1 = killer_agent_no, arg2 = dead_agent_no
  # OUTPUT: none
  ("add_kill_death_counts",
   [
      (store_script_param, ":killer_agent_no", 1),
      (store_script_param, ":dead_agent_no", 2),
      
      (try_begin),
        (ge, ":killer_agent_no", 0),
        (agent_get_team, ":killer_agent_team", ":killer_agent_no"),
      (else_try),
        (assign, ":killer_agent_team", -1),
      (try_end),

      (try_begin),
        (ge, ":dead_agent_no", 0),
        (agent_get_team, ":dead_agent_team", ":dead_agent_no"),
      (else_try),
        (assign, ":dead_agent_team", -1),
      (try_end),
      
      #adjusting kill counts of players/bots
      (try_begin), 
        (try_begin), 
          (ge, ":killer_agent_no", 0),
          (ge, ":dead_agent_no", 0),
          (agent_is_human, ":killer_agent_no"),
          (agent_is_human, ":dead_agent_no"),
          (neq, ":killer_agent_no", ":dead_agent_no"),
          
          (this_or_next|neq, ":killer_agent_team", ":dead_agent_team"),
          (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
          (eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
          
          (agent_get_player_id, ":killer_agent_player", ":killer_agent_no"),
          (try_begin),
            (agent_is_non_player, ":killer_agent_no"), #if killer agent is bot then increase bot kill counts of killer agent's team by one. 
            (agent_get_team, ":killer_agent_team", ":killer_agent_no"),
            (team_get_bot_kill_count, ":killer_agent_team_bot_kill_count", ":killer_agent_team"),
            (val_add, ":killer_agent_team_bot_kill_count", 1),
            (team_set_bot_kill_count, ":killer_agent_team", ":killer_agent_team_bot_kill_count"),            
          (else_try), #if killer agent is not bot then increase kill counts of killer agent's player by one.
            (player_is_active, ":killer_agent_player"),
            (player_get_kill_count, ":killer_agent_player_kill_count", ":killer_agent_player"),
            (val_add, ":killer_agent_player_kill_count", 1),
            (player_set_kill_count, ":killer_agent_player", ":killer_agent_player_kill_count"),
#MOD begin
						(try_begin),
							(neg|agent_is_non_player, ":dead_agent_no"),
							(player_get_slot, ":cur_player_kills", ":killer_agent_player", slot_player_kills),
							(val_add, ":cur_player_kills", 1),
							(player_set_slot, ":killer_agent_player", slot_player_kills, ":cur_player_kills"),
						(try_end),
#MOD end					
          (try_end),
        (try_end),

        (try_begin), 
          (ge, ":dead_agent_no", 0),
          (agent_is_human, ":dead_agent_no"),
          (try_begin),
            (agent_is_non_player, ":dead_agent_no"), #if dead agent is bot then increase bot kill counts of dead agent's team by one.
            (agent_get_team, ":dead_agent_team", ":dead_agent_no"),
            (team_get_bot_death_count, ":dead_agent_team_bot_death_count", ":dead_agent_team"),
            (val_add, ":dead_agent_team_bot_death_count", 1),
            (team_set_bot_death_count, ":dead_agent_team", ":dead_agent_team_bot_death_count"),
          (else_try), #if dead agent is not bot then increase death counts of dead agent's player by one.
            (agent_get_player_id, ":dead_agent_player", ":dead_agent_no"),
            (player_is_active, ":dead_agent_player"),
            (player_get_death_count, ":dead_agent_player_death_count", ":dead_agent_player"),
            (val_add, ":dead_agent_player_death_count", 1),
            (player_set_death_count, ":dead_agent_player", ":dead_agent_player_death_count"),
#MOD begin
						(try_begin),
							(ge, ":killer_agent_no", 0),
							(neg|agent_is_non_player, ":killer_agent_no"),
							(player_get_slot, ":cur_player_deaths", ":dead_agent_player", slot_player_deaths),
							(val_add, ":cur_player_deaths", 1),
							(player_set_slot, ":dead_agent_player", slot_player_deaths, ":cur_player_deaths"),
						(try_end),
#MOD end	
          (try_end),

          (try_begin),
            (assign, ":continue", 0),
#MOD begin  
#            (try_begin),
#              (this_or_next|lt, ":killer_agent_no", 0), #if he killed himself (1a(team change) or 1b(self kill)) then decrease kill counts of killer player by one.
#              (eq, ":killer_agent_no", ":dead_agent_no"),
#              (assign, ":continue", 1),
#            (try_end),
#MOD end
            (try_begin),
#MOD begin
              (neq, ":killer_agent_no", ":dead_agent_no"),
#MOD end
              (eq, ":killer_agent_team", ":dead_agent_team"), #if he killed a teammate and game mod is not deathmatch then decrease kill counts of killer player by one.
              (neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
              (neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
              (assign, ":continue", 1),
            (try_end),

            (eq, ":continue", 1),
                    
#            (try_begin), 
              (ge, ":killer_agent_no", 0),
              (assign, ":responsible_agent", ":killer_agent_no"),                
#MOD begin
#            (else_try), 
#              (assign, ":responsible_agent", ":dead_agent_no"),
#MOD begin
#            (try_end),
  
            (try_begin),
              (ge, ":responsible_agent", 0),
              (neg|agent_is_non_player, ":responsible_agent"),
              (agent_get_player_id, ":responsible_player", ":responsible_agent"),
              (player_is_active, ":responsible_player"),#MOD
              (player_get_kill_count, ":dead_agent_player_kill_count", ":responsible_player"),
              (val_add, ":dead_agent_player_kill_count", -1),
              (player_set_kill_count, ":responsible_player", ":dead_agent_player_kill_count"),
#MOD begin
							(try_begin),
								(neg|agent_is_non_player, ":responsible_agent"),
								(player_get_slot, ":cur_player_kills", ":responsible_player", slot_player_kills),
								(val_add, ":cur_player_kills", -1),
								(player_set_slot, ":responsible_player", slot_player_kills, ":cur_player_kills"),
							(try_end),
#MOD end	
            (try_end),
          (try_end),               
        (try_end),
      (try_end),
    ]),

  #script_warn_player_about_auto_team_balance
  # INPUT: none
  # OUTPUT: none
  ("warn_player_about_auto_team_balance",
   [
     (assign, "$g_multiplayer_message_type", multiplayer_message_type_auto_team_balance_next),
     (start_presentation, "prsnt_multiplayer_message_2"),
     ]),

  #script_check_team_balance
  # INPUT: none
  # OUTPUT: none
  ("check_team_balance",
   [
     (try_begin),
       (multiplayer_is_server),
  
       (assign, ":number_of_players_at_team_1", 0),
       (assign, ":number_of_players_at_team_2", 0),
       (get_max_players, ":num_players"),
       (try_for_range, ":cur_player", 0, ":num_players"),
         (player_is_active, ":cur_player"),
         (player_get_team_no, ":player_team", ":cur_player"),
         (try_begin),
           (eq, ":player_team", 0),
           (val_add, ":number_of_players_at_team_1", 1),
         (else_try),
           (eq, ":player_team", 1),
           (val_add, ":number_of_players_at_team_2", 1),
         (try_end),         
       (try_end),
 
       (store_sub, ":difference_of_number_of_players", ":number_of_players_at_team_1", ":number_of_players_at_team_2"),
       (assign, ":number_of_players_will_be_moved", 0),
       (try_begin),
         (try_begin),
           (store_mul, ":checked_value", "$g_multiplayer_auto_team_balance_limit", -1),
           (le, ":difference_of_number_of_players", ":checked_value"),
           (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", -2),
           (assign, ":team_with_more_players", 1),
           (assign, ":team_with_less_players", 0),
         (else_try),
           (ge, ":difference_of_number_of_players", "$g_multiplayer_auto_team_balance_limit"),
           (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", 2),
           (assign, ":team_with_more_players", 0),
           (assign, ":team_with_less_players", 1),
         (try_end),          
       (try_end),         
       #team balance checks are done
       (try_begin),
         (gt, ":number_of_players_will_be_moved", 0),
         (try_begin),
           (eq, "$g_team_balance_next_round", 1), #if warning is given
           
           #auto team balance starts
           (try_for_range, ":unused", 0, ":number_of_players_will_be_moved"), 
             (assign, ":max_player_join_time", 0),
             (assign, ":latest_joined_player_no", -1),
             (get_max_players, ":num_players"),                               
             (try_for_range, ":player_no", 0, ":num_players"),
               (player_is_active, ":player_no"),
               (player_get_team_no, ":player_team", ":player_no"),
               (eq, ":player_team", ":team_with_more_players"),
               (player_get_slot, ":player_join_time", ":player_no", slot_player_join_time),
               (try_begin),
                 (gt, ":player_join_time", ":max_player_join_time"),
                 (assign, ":max_player_join_time", ":player_join_time"),
                 (assign, ":latest_joined_player_no", ":player_no"),
               (try_end),
             (try_end),
             (try_begin),
               (ge, ":latest_joined_player_no", 0),
               (try_begin),
                 #if player is living add +1 to his kill count because he will get -1 because of team change while living.
                 (player_get_agent_id, ":latest_joined_agent_id", ":latest_joined_player_no"), 
                 (ge, ":latest_joined_agent_id", 0),
                 (agent_is_alive, ":latest_joined_agent_id"),
#MOD begin								 
#                 (player_get_kill_count, ":player_kill_count", ":latest_joined_player_no"), #adding 1 to his kill count, because he will lose 1 undeserved kill count for dying during team change
#                 (val_add, ":player_kill_count", 1),
#                 (player_set_kill_count, ":latest_joined_player_no", ":player_kill_count"),
#MOD end
                 (player_get_death_count, ":player_death_count", ":latest_joined_player_no"), #subtracting 1 to his death count, because he will gain 1 undeserved death count for dying during team change
                 (val_sub, ":player_death_count", 1),
                 (player_set_death_count, ":latest_joined_player_no", ":player_death_count"),
#MOD begin
								(try_begin),
									(player_get_slot, ":cur_player_deaths", ":latest_joined_player_no", slot_player_deaths),
									(val_sub, ":cur_player_deaths", 1),
									(player_set_slot, ":latest_joined_player_no", slot_player_deaths, ":cur_player_deaths"),
								(try_end),
#MOD end									 

#MOD begin								 
#                 (player_get_score, ":player_score", ":latest_joined_player_no"), #adding 1 to his score count, because he will lose 1 undeserved score for dying during team change
#                 (val_add, ":player_score", 1),
#                 (player_set_score, ":latest_joined_player_no", ":player_score"),
#MOD end
                 (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
                   (player_is_active, ":player_no"),
#MOD begin									 
                   #(multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_set_player_score_kill_death, ":latest_joined_player_no", ":player_score", ":player_kill_count", ":player_death_count"),
									 #(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_score_count, ":latest_joined_player_no", ":player_score"),
									 #(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_kill_count, ":latest_joined_player_no", ":player_kill_count"),
									 (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_death_count, ":latest_joined_player_no", ":player_death_count"),
#MOD end
									(try_end),

#MOD begin
				 # (player_get_slot, ":troop_no", ":latest_joined_player_no", slot_player_use_troop),
				 # (try_begin),
				   # (neq, ":troop_no", "trp_online_character"),
                   # (player_get_value_of_original_items, ":old_items_value", ":latest_joined_player_no"),
                   # (player_get_gold, ":player_gold", ":latest_joined_player_no"),
                   # (val_add, ":player_gold", ":old_items_value"),
                   # (player_set_gold, ":latest_joined_player_no", ":player_gold", multi_max_gold_that_can_be_stored),
				 # (try_end),
#MOD end
               (end_try),

#               (player_set_troop_id, ":latest_joined_player_no", -1),#MOD disable
#MOD begin
               (player_set_slot, ":latest_joined_player_no", slot_player_use_troop, -1),
#MOD end
               (player_set_team_no, ":latest_joined_player_no", ":team_with_less_players"),
               (multiplayer_send_message_to_player, ":latest_joined_player_no", multiplayer_event_force_start_team_selection),
             (try_end),
           (try_end),
     
           #for only server itself-----------------------------------------------------------------------------------------------
           (call_script, "script_show_multiplayer_message", multiplayer_message_type_auto_team_balance_done, 0), #0 is useless here
           #for only server itself-----------------------------------------------------------------------------------------------     
           (get_max_players, ":num_players"),                               
           (try_for_range, ":player_no", 1, ":num_players"),
             (player_is_active, ":player_no"),
             (multiplayer_send_int_to_player, ":player_no", multiplayer_event_show_multiplayer_message, multiplayer_message_type_auto_team_balance_done), 
           (try_end),
           (assign, "$g_team_balance_next_round", 0),
           #auto team balance done
         (else_try),
           #tutorial message (next round there will be auto team balance)
           (assign, "$g_team_balance_next_round", 1),
     
           #for only server itself-----------------------------------------------------------------------------------------------
           (call_script, "script_show_multiplayer_message", multiplayer_message_type_auto_team_balance_next, 0), #0 is useless here
           #for only server itself-----------------------------------------------------------------------------------------------     
           (get_max_players, ":num_players"),                               
           (try_for_range, ":player_no", 1, ":num_players"),
             (player_is_active, ":player_no"),
             (multiplayer_send_int_to_player, ":player_no", multiplayer_event_show_multiplayer_message, multiplayer_message_type_auto_team_balance_next), 
           (try_end),
         (try_end),
       (else_try),
         (assign, "$g_team_balance_next_round", 0),
       (try_end),
     (try_end),
   ]),

  #script_check_creating_ladder_dust_effect
  # INPUT: arg1 = instance_id, arg2 = remaining_time
  # OUTPUT: none
  ("check_creating_ladder_dust_effect",
   [
      (store_trigger_param_1, ":instance_id"),
      (store_trigger_param_2, ":remaining_time"),

      (try_begin),
        (lt, ":remaining_time", 15), #less then 0.15 seconds
        (gt, ":remaining_time", 3), #more than 0.03 seconds
      
        (scene_prop_get_slot, ":smoke_effect_done", ":instance_id", scene_prop_smoke_effect_done),
        (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot),

        (try_begin),
          (eq, ":smoke_effect_done", 0),
          (eq, ":opened_or_closed", 0),
      
          (prop_instance_get_position, pos0, ":instance_id"),

          (assign, ":smallest_dist", -1),
          (try_for_range, ":entry_point_no", multi_entry_points_for_usable_items_start, multi_entry_points_for_usable_items_end),
            (entry_point_get_position, pos1, ":entry_point_no"),
            (get_sq_distance_between_positions, ":dist", pos0, pos1),
            (this_or_next|eq, ":smallest_dist", -1),
            (lt, ":dist", ":smallest_dist"),
            (assign, ":smallest_dist", ":dist"),
            (assign, ":nearest_entry_point", ":entry_point_no"),
          (try_end),

          (try_begin),
            (set_fixed_point_multiplier, 100),

            (ge, ":smallest_dist", 0),
            (lt, ":smallest_dist", 22500), #max 15m distance
      
            (entry_point_get_position, pos1, ":nearest_entry_point"),
            (position_rotate_x, pos1, -90),

            (prop_instance_get_scene_prop_kind, ":scene_prop_kind", ":instance_id"),
            (try_begin),
              (eq, ":scene_prop_kind", "spr_siege_ladder_move_6m"),              
              (init_position, pos2),
              (position_set_z, pos2, 300),
              (position_transform_position_to_parent, pos3, pos1, pos2),
              (particle_system_burst, "psys_ladder_dust_6m", pos3, 100),
              (particle_system_burst, "psys_ladder_straw_6m", pos3, 100),
            (else_try),
              (eq, ":scene_prop_kind", "spr_siege_ladder_move_8m"),
              (init_position, pos2),
              (position_set_z, pos2, 400),
              (position_transform_position_to_parent, pos3, pos1, pos2),
              (particle_system_burst, "psys_ladder_dust_8m", pos3, 100),
              (particle_system_burst, "psys_ladder_straw_8m", pos3, 100),
            (else_try),
              (eq, ":scene_prop_kind", "spr_siege_ladder_move_10m"),
              (init_position, pos2),
              (position_set_z, pos2, 500),
              (position_transform_position_to_parent, pos3, pos1, pos2),
              (particle_system_burst, "psys_ladder_dust_10m", pos3, 100),
              (particle_system_burst, "psys_ladder_straw_10m", pos3, 100),
            (else_try),
              (eq, ":scene_prop_kind", "spr_siege_ladder_move_12m"),
              (init_position, pos2),
              (position_set_z, pos2, 600),
              (position_transform_position_to_parent, pos3, pos1, pos2),
              (particle_system_burst, "psys_ladder_dust_12m", pos3, 100),
              (particle_system_burst, "psys_ladder_straw_12m", pos3, 100),
            (else_try),
              (eq, ":scene_prop_kind", "spr_siege_ladder_move_14m"),
              (init_position, pos2),
              (position_set_z, pos2, 700),
              (position_transform_position_to_parent, pos3, pos1, pos2),
              (particle_system_burst, "psys_ladder_dust_14m", pos3, 100),
              (particle_system_burst, "psys_ladder_straw_14m", pos3, 100),
            (try_end),

            (scene_prop_set_slot, ":instance_id", scene_prop_smoke_effect_done, 1),
          (try_end),
        (try_end),
      (try_end),
      ]),

    ("initialize_banner_info",	
    [	
      #Banners
#MOD disable
      # (try_for_range, ":cur_troop", active_npcs_begin, kingdom_ladies_end),
        # (troop_set_slot, ":cur_troop", slot_troop_custom_banner_flag_type, -1),
        # (troop_set_slot, ":cur_troop", slot_troop_custom_banner_map_flag_type, -1),
      # (try_end),
      # (troop_set_slot, "trp_player", slot_troop_custom_banner_flag_type, -1),
      # (troop_set_slot, "trp_player", slot_troop_custom_banner_map_flag_type, -1),
#      (store_random_in_range, "$g_election_date", 0, 45), #setting a random election date #MOD disable
      #Assigning global constant
      #(call_script, "script_store_average_center_value_per_faction"),

      # (troop_set_slot, "trp_player", slot_troop_custom_banner_bg_color_1, 0xFFFFFFFF),
      # (troop_set_slot, "trp_player", slot_troop_custom_banner_bg_color_2, 0xFFFFFFFF),
      # (troop_set_slot, "trp_player", slot_troop_custom_banner_charge_color_1, 0xFFFFFFFF),
      # (troop_set_slot, "trp_player", slot_troop_custom_banner_charge_color_2, 0xFFFFFFFF),
      # (troop_set_slot, "trp_player", slot_troop_custom_banner_charge_color_3, 0xFFFFFFFF),
      # (troop_set_slot, "trp_player", slot_troop_custom_banner_charge_color_4, 0xFFFFFFFF),

      #Setting background colors for banners
      (troop_set_slot, "trp_banner_background_color_array", 0, 0xFF8f4531),
      (troop_set_slot, "trp_banner_background_color_array", 1, 0xFFd9d7d1),
      (troop_set_slot, "trp_banner_background_color_array", 2, 0xFF373736),
      (troop_set_slot, "trp_banner_background_color_array", 3, 0xFFa48b28),
      (troop_set_slot, "trp_banner_background_color_array", 4, 0xFF497735),
      (troop_set_slot, "trp_banner_background_color_array", 5, 0xFF82362d),
      (troop_set_slot, "trp_banner_background_color_array", 6, 0xFF793329),
      (troop_set_slot, "trp_banner_background_color_array", 7, 0xFF262521),
      (troop_set_slot, "trp_banner_background_color_array", 8, 0xFFd9dad1),
      (troop_set_slot, "trp_banner_background_color_array", 9, 0xFF524563),
      (troop_set_slot, "trp_banner_background_color_array", 10, 0xFF91312c),
      (troop_set_slot, "trp_banner_background_color_array", 11, 0xFFafa231),
      (troop_set_slot, "trp_banner_background_color_array", 12, 0xFF706d3c),
      (troop_set_slot, "trp_banner_background_color_array", 13, 0xFFd6d3ce),
      (troop_set_slot, "trp_banner_background_color_array", 14, 0xFF521c08),
      (troop_set_slot, "trp_banner_background_color_array", 15, 0xFF394584),
      (troop_set_slot, "trp_banner_background_color_array", 16, 0xFF42662e),
      (troop_set_slot, "trp_banner_background_color_array", 17, 0xFFdfded6),
      (troop_set_slot, "trp_banner_background_color_array", 18, 0xFF292724),
      (troop_set_slot, "trp_banner_background_color_array", 19, 0xFF58611b),
      (troop_set_slot, "trp_banner_background_color_array", 20, 0xFF313a67),
      (troop_set_slot, "trp_banner_background_color_array", 21, 0xFF9c924a),
      (troop_set_slot, "trp_banner_background_color_array", 22, 0xFF998b39),
      (troop_set_slot, "trp_banner_background_color_array", 23, 0xFF365168),
      (troop_set_slot, "trp_banner_background_color_array", 24, 0xFFd6d3ce),
      (troop_set_slot, "trp_banner_background_color_array", 25, 0xFF94a642),
      (troop_set_slot, "trp_banner_background_color_array", 26, 0xFF944131),
      (troop_set_slot, "trp_banner_background_color_array", 27, 0xFF893b34),
      (troop_set_slot, "trp_banner_background_color_array", 28, 0xFF425510),
      (troop_set_slot, "trp_banner_background_color_array", 29, 0xFF94452e),
      (troop_set_slot, "trp_banner_background_color_array", 30, 0xFF475a94),
      (troop_set_slot, "trp_banner_background_color_array", 31, 0xFFd1b231),
      (troop_set_slot, "trp_banner_background_color_array", 32, 0xFFe1e2df),
      (troop_set_slot, "trp_banner_background_color_array", 33, 0xFF997c1e),
      (troop_set_slot, "trp_banner_background_color_array", 34, 0xFFc6b74d),
      (troop_set_slot, "trp_banner_background_color_array", 35, 0xFFad9a18),
      (troop_set_slot, "trp_banner_background_color_array", 36, 0xFF212421),
      (troop_set_slot, "trp_banner_background_color_array", 37, 0xFF8c2021),
      (troop_set_slot, "trp_banner_background_color_array", 38, 0xFF4d7136),
      (troop_set_slot, "trp_banner_background_color_array", 39, 0xFF395d84),
      (troop_set_slot, "trp_banner_background_color_array", 40, 0xFF527539),
      (troop_set_slot, "trp_banner_background_color_array", 41, 0xFF9c3c39),
      (troop_set_slot, "trp_banner_background_color_array", 42, 0xFF42518c),
      (troop_set_slot, "trp_banner_background_color_array", 43, 0xFFa46a2c),
      (troop_set_slot, "trp_banner_background_color_array", 44, 0xFF9f5141),
      (troop_set_slot, "trp_banner_background_color_array", 45, 0xFF2c6189),
      (troop_set_slot, "trp_banner_background_color_array", 46, 0xFF556421),
      (troop_set_slot, "trp_banner_background_color_array", 47, 0xFF9d621e),
      (troop_set_slot, "trp_banner_background_color_array", 48, 0xFFdeded6),
      (troop_set_slot, "trp_banner_background_color_array", 49, 0xFF6e4891),
      (troop_set_slot, "trp_banner_background_color_array", 50, 0xFF865a29),
      (troop_set_slot, "trp_banner_background_color_array", 51, 0xFFdedfd9),
      (troop_set_slot, "trp_banner_background_color_array", 52, 0xFF524273),
      (troop_set_slot, "trp_banner_background_color_array", 53, 0xFF8c3821),
      (troop_set_slot, "trp_banner_background_color_array", 54, 0xFFd1cec6),
      (troop_set_slot, "trp_banner_background_color_array", 55, 0xFF313031),
      (troop_set_slot, "trp_banner_background_color_array", 56, 0xFF47620d),
      (troop_set_slot, "trp_banner_background_color_array", 57, 0xFF6b4139),
      (troop_set_slot, "trp_banner_background_color_array", 58, 0xFFd6d7d6),
      (troop_set_slot, "trp_banner_background_color_array", 59, 0xFF2e2f2c),
      (troop_set_slot, "trp_banner_background_color_array", 60, 0xFF604283),
      (troop_set_slot, "trp_banner_background_color_array", 61, 0xFF395584),
      (troop_set_slot, "trp_banner_background_color_array", 62, 0xFF313031),
      (troop_set_slot, "trp_banner_background_color_array", 63, 0xFF7e3f2e),
      (troop_set_slot, "trp_banner_background_color_array", 64, 0xFF343434),
      (troop_set_slot, "trp_banner_background_color_array", 65, 0xFF3c496b),
      (troop_set_slot, "trp_banner_background_color_array", 66, 0xFFd9d8d1),
      (troop_set_slot, "trp_banner_background_color_array", 67, 0xFF99823c),
      (troop_set_slot, "trp_banner_background_color_array", 68, 0xFF9f822e),
      (troop_set_slot, "trp_banner_background_color_array", 69, 0xFF393839),
      (troop_set_slot, "trp_banner_background_color_array", 70, 0xFFa54931),
      (troop_set_slot, "trp_banner_background_color_array", 71, 0xFFdfdcd6),
      (troop_set_slot, "trp_banner_background_color_array", 72, 0xFF9f4a36),
      (troop_set_slot, "trp_banner_background_color_array", 73, 0xFF8c7521),
      (troop_set_slot, "trp_banner_background_color_array", 74, 0xFF9f4631),
      (troop_set_slot, "trp_banner_background_color_array", 75, 0xFF793324),
      (troop_set_slot, "trp_banner_background_color_array", 76, 0xFF395076),
      (troop_set_slot, "trp_banner_background_color_array", 77, 0xFF2c2b2c),
      (troop_set_slot, "trp_banner_background_color_array", 78, 0xFF657121),
      (troop_set_slot, "trp_banner_background_color_array", 79, 0xFF7e3121),
      (troop_set_slot, "trp_banner_background_color_array", 80, 0xFF76512e),
      (troop_set_slot, "trp_banner_background_color_array", 81, 0xFFe7e3de),
      (troop_set_slot, "trp_banner_background_color_array", 82, 0xFF947921),
      (troop_set_slot, "trp_banner_background_color_array", 83, 0xFF4d7b7c),
      (troop_set_slot, "trp_banner_background_color_array", 84, 0xFF343331),
      (troop_set_slot, "trp_banner_background_color_array", 85, 0xFFa74d36),
      (troop_set_slot, "trp_banner_background_color_array", 86, 0xFFe7e3de),
      (troop_set_slot, "trp_banner_background_color_array", 87, 0xFFd6d8ce),
      (troop_set_slot, "trp_banner_background_color_array", 88, 0xFF3e4d67),
      (troop_set_slot, "trp_banner_background_color_array", 89, 0xFF9f842e),
      (troop_set_slot, "trp_banner_background_color_array", 90, 0xFF4d6994),
      (troop_set_slot, "trp_banner_background_color_array", 91, 0xFF4a6118),
      (troop_set_slot, "trp_banner_background_color_array", 92, 0xFF943c29),
      (troop_set_slot, "trp_banner_background_color_array", 93, 0xFF394479),
      (troop_set_slot, "trp_banner_background_color_array", 94, 0xFF343331),
      (troop_set_slot, "trp_banner_background_color_array", 95, 0xFF3f4d5d),
      (troop_set_slot, "trp_banner_background_color_array", 96, 0xFF4a6489),
      (troop_set_slot, "trp_banner_background_color_array", 97, 0xFF313031),
      (troop_set_slot, "trp_banner_background_color_array", 98, 0xFFd6d7ce),
      (troop_set_slot, "trp_banner_background_color_array", 99, 0xFFc69e00),
      (troop_set_slot, "trp_banner_background_color_array", 100, 0xFF638e52),
      (troop_set_slot, "trp_banner_background_color_array", 101, 0xFFdcdbd3),
      (troop_set_slot, "trp_banner_background_color_array", 102, 0xFFdbdcd3),
      (troop_set_slot, "trp_banner_background_color_array", 103, 0xFF843831),
      (troop_set_slot, "trp_banner_background_color_array", 104, 0xFFcecfc6),
      (troop_set_slot, "trp_banner_background_color_array", 105, 0xFFc39d31),
      (troop_set_slot, "trp_banner_background_color_array", 106, 0xFFcbb670),
      (troop_set_slot, "trp_banner_background_color_array", 107, 0xFF394a18),
      (troop_set_slot, "trp_banner_background_color_array", 108, 0xFF372708),
      (troop_set_slot, "trp_banner_background_color_array", 109, 0xFF9a6810),
      (troop_set_slot, "trp_banner_background_color_array", 110, 0xFFb27910),
      (troop_set_slot, "trp_banner_background_color_array", 111, 0xFF8c8621),
      (troop_set_slot, "trp_banner_background_color_array", 112, 0xFF975a03),
      (troop_set_slot, "trp_banner_background_color_array", 113, 0xFF2c2924),
      (troop_set_slot, "trp_banner_background_color_array", 114, 0xFFaa962c),
      (troop_set_slot, "trp_banner_background_color_array", 115, 0xFFa2822e),
      (troop_set_slot, "trp_banner_background_color_array", 116, 0xFF7b8a8c),
      (troop_set_slot, "trp_banner_background_color_array", 117, 0xFF3c0908),
      (troop_set_slot, "trp_banner_background_color_array", 118, 0xFFFF00FF),
      (troop_set_slot, "trp_banner_background_color_array", 119, 0xFF671e14),
      (troop_set_slot, "trp_banner_background_color_array", 120, 0xFF103042),
      (troop_set_slot, "trp_banner_background_color_array", 121, 0xFF4a4500),
      (troop_set_slot, "trp_banner_background_color_array", 122, 0xFF703324),
      (troop_set_slot, "trp_banner_background_color_array", 123, 0xFF24293c),
      (troop_set_slot, "trp_banner_background_color_array", 124, 0xFF5d6966),
      (troop_set_slot, "trp_banner_background_color_array", 125, 0xFFbd9631),
      (troop_set_slot, "trp_banner_background_color_array", 126, 0xFFc6b26b),
      (troop_set_slot, "trp_banner_background_color_array", 127, 0xFF394918),

	#Default banners
	(troop_set_slot, "trp_banner_background_color_array", 128, 0xFF212221),
	(troop_set_slot, "trp_banner_background_color_array", 129, 0xFF212221),
	(troop_set_slot, "trp_banner_background_color_array", 130, 0xFF2E3B10),
	(troop_set_slot, "trp_banner_background_color_array", 131, 0xFF425D7B),
	(troop_set_slot, "trp_banner_background_color_array", 132, 0xFF394608),

#MOD begin
        (troop_set_slot, "trp_banner_background_color_array", 133, 0xFFFFFFFF),#???
        (troop_set_slot, "trp_banner_background_color_array", 134, 0xFFFFFFFF),#???

    #Kingdom banners
	(troop_set_slot, "trp_banner_background_color_array", 135, 0xFFFFFF),#Kingdom of England #1  #was nord
	(troop_set_slot, "trp_banner_background_color_array", 136, 0xCE3031),#Union of Mielnik #2  #was vaegir
	(troop_set_slot, "trp_banner_background_color_array", 137, 0x892020),#Ottoman Empire #3  #was khergit
	(troop_set_slot, "trp_banner_background_color_array", 138, 0x10253F),#Kingdom of France #4  #was rhodok
	(troop_set_slot, "trp_banner_background_color_array", 139, 0x214F6D),#Kingdom of Scotland #5  #was sarranid
	(troop_set_slot, "trp_banner_background_color_array", 140, 0xD8D126),#Holy Roman Empire #6  #was swadia
	(troop_set_slot, "trp_banner_background_color_array", 141, 0xFFFFFF),#Portuguese Empire #7  #was unused
	(troop_set_slot, "trp_banner_background_color_array", 142, 0xFFFFFF),#Spanish Empire #8
	(troop_set_slot, "trp_banner_background_color_array", 143, 0xD8D126),#Union of Kalmar #9
	(troop_set_slot, "trp_banner_background_color_array", 144, 0xCE3031),#Kingdom of Hungary #10
	(troop_set_slot, "trp_banner_background_color_array", 145, 0xD8D126),#Papal States #11
	(troop_set_slot, "trp_banner_background_color_array", 146, 0xCE3031),#Republic of Venice #12
	(troop_set_slot, "trp_banner_background_color_array", 147, 0xFFFFFF),#Orders State #13
	(troop_set_slot, "trp_banner_background_color_array", 148, 0xCE3031),#Grand Duchy of Moscow #14
	(troop_set_slot, "trp_banner_background_color_array", 149, 0xD6843F),#Wattasid Dynasty #15
	(troop_set_slot, "trp_banner_background_color_array", 150, 0x5A8426),#Saadi Dynasty #16
	(troop_set_slot, "trp_banner_background_color_array", 151, 0x1B4565),#Zayyanid Dynasty #17
	(troop_set_slot, "trp_banner_background_color_array", 152, 0x997660),#Hafsid Dynasty #18
	(troop_set_slot, "trp_banner_background_color_array", 153, 0x10253F),#Crimean Khanate #19
	(troop_set_slot, "trp_banner_background_color_array", 154, 0xFFFFFF),#Kingdom of Imereti #20
#MOD end
    ]),
	 
  #script_initialize_all_scene_prop_slots
  # INPUT: arg1 = scene_prop_no
  # OUTPUT: none
  ("initialize_all_scene_prop_slots",
  [
    (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_6m"),
    (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_8m"),
    (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_10m"),
    (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_12m"),
    (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_14m"),
    (call_script, "script_initialize_scene_prop_slots", "spr_castle_e_sally_door_a"),
    (call_script, "script_initialize_scene_prop_slots", "spr_castle_f_sally_door_a"),
    (call_script, "script_initialize_scene_prop_slots", "spr_earth_sally_gate_left"),
    (call_script, "script_initialize_scene_prop_slots", "spr_earth_sally_gate_right"),
    (call_script, "script_initialize_scene_prop_slots", "spr_viking_keep_destroy_sally_door_left"),
    (call_script, "script_initialize_scene_prop_slots", "spr_viking_keep_destroy_sally_door_right"),
    (call_script, "script_initialize_scene_prop_slots", "spr_castle_f_door_a"),
    # (call_script, "script_initialize_scene_prop_slots", "spr_belfry_a"),
    # (call_script, "script_initialize_scene_prop_slots", "spr_belfry_b"),
    (call_script, "script_initialize_scene_prop_slots", "spr_winch_b"),
	 
#MOD begin
		(assign, ":continue", 0),
    (try_begin),
			(multiplayer_is_dedicated_server),
			(assign, ":continue", 1),
		(else_try),
			(multiplayer_get_my_player, ":my_player_no"),
			(player_is_active, ":my_player_no"),
			(store_mission_timer_a, ":cur_time"),
			(player_get_slot, ":player_join_time", ":my_player_no", slot_player_join_time),
			(store_sub, ":time_dif", ":cur_time", ":player_join_time"),
			(gt, ":time_dif", 2),
			(assign, ":continue", 1),
		(try_end),
	 
		(try_begin),
			(eq, ":continue", 1),
			(try_begin),
				(eq, "$g_show_debug_messages", 1),
				(display_message, "@[DEBUG] init slots and other stuff"),
			(try_end),
			
			(store_mission_timer_a, ":cur_time"),
			(try_for_range, ":cur_scene_prop_no", "spr_fire_xs", "spr_barrier_20m"),
				(scene_prop_get_num_instances, ":num_instances_of_scene_prop", ":cur_scene_prop_no"),     
				(try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
					(scene_prop_get_instance, ":cur_instance_id", ":cur_scene_prop_no", ":cur_instance"),
					(scene_prop_slot_ge, ":cur_instance_id", scene_prop_prune_time, 0),
					(scene_prop_set_slot, ":cur_instance_id", scene_prop_prune_time, ":cur_time"),
				(try_end),
			(try_end),
			
			(set_fixed_point_multiplier, 100),
      (try_for_range, ":cur_scene_prop_no", destructible_props_begin, destructible_props_end),
        (scene_prop_get_num_instances, ":num_instances_of_scene_prop", ":cur_scene_prop_no"),     
        (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
          (scene_prop_get_instance, ":cur_instance_id", ":cur_scene_prop_no", ":cur_instance"),
					
					(try_begin),
						(multiplayer_is_server),
						(neq, ":cur_scene_prop_no", "spr_castle_e_sally_door_a"),
						(neq, ":cur_scene_prop_no", "spr_castle_f_sally_door_a"),
						(neq, ":cur_scene_prop_no", "spr_earth_sally_gate_left"),
						(neq, ":cur_scene_prop_no", "spr_earth_sally_gate_right"),
						(neq, ":cur_scene_prop_no", "spr_viking_keep_destroy_sally_door_left"),
						(neq, ":cur_scene_prop_no", "spr_viking_keep_destroy_sally_door_right"),
						(neq, ":cur_scene_prop_no", "spr_castle_f_door_a"),
						(neq, ":cur_scene_prop_no", "spr_castle_f_door_b"),
						(try_begin),#move spawned props during round under ground
							(scene_prop_slot_ge, ":cur_instance_id", scene_prop_spawn_time, 1),
							(call_script, "script_move_scene_prop_under_ground", ":cur_instance_id", 0),
						(else_try),#reset position of underground props
							(scene_prop_slot_eq, ":cur_instance_id", scene_prop_is_used, 0),
							(call_script, "script_reset_scene_prop_position", ":cur_instance_id", 0),
						(try_end),
					(try_end),
					
					(try_begin),
						(eq, ":cur_scene_prop_no", "spr_cannon_base_1"),

						(prop_instance_get_starting_position, pos0, ":cur_instance_id"),
						(prop_instance_set_position, ":cur_instance_id", pos0, 1),
						(scene_prop_set_slot, ":cur_instance_id", scene_prop_rot_z, 0),
						
						(try_for_range, ":cur_slot_child_prop", scene_prop_child_1, scene_prop_child_5 +1),
							(scene_prop_get_slot, ":cur_child_instance_id", ":cur_instance_id", ":cur_slot_child_prop"),
							(prop_instance_is_valid, ":cur_child_instance_id"),
							#stop previous animations
							(prop_instance_is_animating, ":animating", ":cur_child_instance_id"),
							(try_begin),
								(eq, ":animating", 1),
								(prop_instance_stop_animating, ":cur_child_instance_id"),
							(try_end),
							(store_sub, ":pos_offset_array", ":cur_slot_child_prop", scene_prop_child_1),
							(val_add, ":pos_offset_array", "trp_cannon_1_child_1_pos"),
							(troop_get_slot, ":pos_x", ":pos_offset_array", pos_x),
							(troop_get_slot, ":pos_y", ":pos_offset_array", pos_y),
							(troop_get_slot, ":pos_z", ":pos_offset_array", pos_z),
							(copy_position, pos_move, pos0),
							(position_move_x, pos_move, ":pos_x"),
							(position_move_y, pos_move, ":pos_y"),
							(position_move_z, pos_move, ":pos_z"),
							(try_begin),#assign wheel rotation
								(is_between, ":cur_slot_child_prop", scene_prop_child_2, scene_prop_child_5 +1),
								(scene_prop_get_slot, ":rot_x", ":cur_child_instance_id", scene_prop_default_rot_x),
								#(store_random_in_range, ":rot_x", 0, 36000),#randomize wheel rotation
								(position_rotate_x_floating, pos_move, ":rot_x"),
								(scene_prop_set_slot, ":cur_child_instance_id", scene_prop_rot_x, ":rot_x"),
							(else_try),#barrel
								(try_begin),
									(eq, "$g_quick_artillery", 1),
									(scene_prop_set_slot, ":cur_child_instance_id", scene_prop_usage_state, 2),
								(else_try),
									(scene_prop_set_slot, ":cur_child_instance_id", scene_prop_usage_state, 0),
								(try_end),
								(scene_prop_set_slot, ":cur_child_instance_id", scene_prop_rot_x, 0),
							(try_end),
							(prop_instance_set_position, ":cur_child_instance_id", pos_move, 1),
						(try_end),
						
					(else_try),
						(eq, ":cur_scene_prop_no", "spr_cannon_ammo_box_32lb_1"),
						(scene_prop_get_slot, ":max_ammo_1", ":cur_instance_id", scene_prop_item_1_max_ammo),
						(scene_prop_get_slot, ":max_ammo_2", ":cur_instance_id", scene_prop_item_2_max_ammo),
						(scene_prop_set_slot, ":cur_instance_id", scene_prop_item_1_cur_ammo, ":max_ammo_1"),
						(scene_prop_set_slot, ":cur_instance_id", scene_prop_item_2_cur_ammo, ":max_ammo_2"),
					(try_end),
        (try_end),
			(try_end),
	  (try_end),
#MOD end
  ]),

  #script_initialize_scene_prop_slots
  # INPUT: arg1 = scene_prop_no
  # OUTPUT: none
  ("initialize_scene_prop_slots",
   [
     (store_script_param, ":scene_prop_no", 1),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", ":scene_prop_no"),
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", ":scene_prop_no", ":cur_instance"),
       (try_for_range, ":cur_slot", 0, scene_prop_smoke_effect_done +1),#MOD edit #was scene_prop_slots_end
         (scene_prop_set_slot, ":cur_instance_id", ":cur_slot", 0),
       (try_end),
     (try_end),
     ]),

#MOD begin
  #script_start_use_item
  # INPUT: arg1 = agent_id, arg2 = instance_id
  # OUTPUT: none
  ("start_use_item",
  [
    (store_script_param, ":instance_id", 1),
    (store_script_param, ":user_id", 2),

    (prop_instance_get_scene_prop_kind, ":scene_prop_id", ":instance_id"),    

    (try_begin),
      (eq, ":scene_prop_id", "spr_cannon_barrel_1_32lb"),
      (agent_get_horse, ":horse", ":user_id"),
      (eq, ":horse", -1),
      (agent_get_wielded_item, ":item_id", ":user_id", 0),
			(try_begin),
				(this_or_next|scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 0),#refill gunpowder
				(this_or_next|scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 1),#compress gunpowder
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 3),#ram down ammuntion
				(try_begin),
					(this_or_next|multiplayer_is_server),
					(neg|game_in_multiplayer_mode),
					(this_or_next|eq, ":item_id", itm_refiller),
					(eq, ":item_id", itm_refiller_alt_mode),
					(agent_set_animation, ":user_id", "anim_reload_barrel", 0),
				(try_end),
			(else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 2),#place ammuntion
        (try_begin),
          (this_or_next|multiplayer_is_server),
          (neg|game_in_multiplayer_mode),
          (this_or_next|eq, ":item_id", "itm_round_shot_32lb"),
          (eq, ":item_id", "itm_canister_shot_32lb"),
					(agent_set_animation, ":user_id", "anim_add_ammo", 0),
				(try_end),
			(else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 4),#push cannon forward
				(scene_prop_get_slot, ":parent_prop", ":instance_id", scene_prop_parent),
				(try_begin),
					(prop_instance_is_valid, ":parent_prop"),
				
					(set_fixed_point_multiplier, 100),
					(prop_instance_get_position, pos0, ":parent_prop"),
			 		
					(position_move_y, pos0, cannon_1_push_distance),
					(prop_instance_enable_physics, ":instance_id", 0),#dont use the prop as ground
					(prop_instance_enable_physics, ":parent_prop", 0),#dont use the prop as ground
					(position_set_z_to_ground_level, pos0),
				
					#(position_get_rotation_around_x, reg30, pos0),
					#(position_get_rotation_around_y, reg31, pos0),
					#(position_get_rotation_around_z, reg32, pos0),
					#(display_message, "@angles cannon x={reg30} y={reg31} z={reg32}"),

					(position_get_rotation_around_x, ":rot_x", pos0),
					(position_get_rotation_around_y, ":rot_y", pos0),
					
					(copy_position, pos_calc, pos0),
					(call_script, "script_get_angle_of_ground_at_pos", 0, 1, cannon_1_wheel_right_x, cannon_1_wheel_right_x, cannon_1_wheel_front_y, cannon_1_wheel_front_y),
						
					(val_sub, reg0, ":rot_x"),
					(val_sub, reg1, ":rot_y"),	        
					(position_rotate_x, pos0, reg0),
					(position_rotate_y, pos0, reg1),
				
					(prop_instance_enable_physics, ":instance_id", 1),
					(prop_instance_enable_physics, ":parent_prop", 1),
					
					(try_begin),#only animate clientside, reduces network traffic
						(neg|multiplayer_is_dedicated_server),
						(prop_instance_animate_to_position, ":parent_prop", pos0, cannon_1_push_forward_time),
					(else_try),#serverside
						(prop_instance_set_position, ":parent_prop", pos0, 1),
					(try_end),

					(assign, ":cur_child_instance_id", ":instance_id"),
					(try_for_range, ":cur_slot_child_prop", scene_prop_child_1, scene_prop_child_5 +1),
						(try_begin),#dont get it on barrel
							(gt, ":cur_slot_child_prop", scene_prop_child_1),
							(scene_prop_get_slot, ":cur_child_instance_id", ":parent_prop", ":cur_slot_child_prop"),
						(try_end),
						
						(prop_instance_is_valid, ":cur_child_instance_id"),
						(store_sub, ":pos_offset_array", ":cur_slot_child_prop", scene_prop_child_1),
						(val_add, ":pos_offset_array", "trp_cannon_1_child_1_pos"),
						(troop_get_slot, ":pos_x", ":pos_offset_array", pos_x),
						(troop_get_slot, ":pos_y", ":pos_offset_array", pos_y),
						(troop_get_slot, ":pos_z", ":pos_offset_array", pos_z),
						(copy_position, pos_move, pos0),
						(position_move_x, pos_move, ":pos_x"),
						(position_move_y, pos_move, ":pos_y"),
						(position_move_z, pos_move, ":pos_z"),
						
						(scene_prop_get_slot, ":rot_x", ":cur_child_instance_id", scene_prop_rot_x),
						(try_begin),#wheels
							(is_between, ":cur_slot_child_prop", scene_prop_child_2, scene_prop_child_5 +1),
							(store_div, ":wheel_angle", 36000, cannon_1_wheel_circumference),
							(val_mul, ":wheel_angle", cannon_1_push_distance),
							(val_add, ":rot_x", ":wheel_angle"),
							(try_begin),
								(gt, ":rot_x", 35999),
								(val_sub, ":rot_x", 36000),
							(try_end),
							(scene_prop_set_slot, ":cur_child_instance_id", scene_prop_rot_x, ":rot_x"),
						(try_end),
						(position_rotate_x_floating, pos_move, ":rot_x"),

						(try_begin),#only animate clientside, reduces network traffic
							(neg|multiplayer_is_dedicated_server),
							(prop_instance_animate_to_position, ":cur_child_instance_id", pos_move, cannon_1_push_forward_time),
						(else_try),#serverside
							(prop_instance_set_position, ":cur_child_instance_id", pos_move, 1),
						(try_end),
					(try_end),
				(try_end),
			 
				(scene_prop_enable_after_time, ":instance_id", cannon_1_push_forward_time),
				(scene_prop_set_slot, ":instance_id", scene_prop_usage_state, 5),

			(else_try),
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 5),#take control
				(try_begin),
					(this_or_next|multiplayer_is_server),
					(neg|game_in_multiplayer_mode),
					(try_begin),
						(agent_has_item_equipped, ":user_id", "itm_linstock"),
						(agent_set_wielded_item, ":user_id", "itm_linstock"),
					(try_end),
					(scene_prop_get_slot, ":parent_prop", ":instance_id", scene_prop_parent),
					(prop_instance_is_valid, ":parent_prop"),
					(prop_instance_get_position, pos0, ":parent_prop"),
					(position_move_y, pos0, cannon_1_aim_platform_y),
					(agent_set_position, ":user_id", pos0),
					(agent_set_animation, ":user_id", "anim_stand_to_crouch_keep"),
				(try_end),
				
				(agent_set_slot, ":user_id", slot_agent_use_scene_prop, ":instance_id"),
				(scene_prop_set_slot, ":instance_id", scene_prop_user_agent, ":user_id"),
				
				(scene_prop_set_slot, ":instance_id", scene_prop_usage_state, 6),#aim
				
				#enable custom camera
				(neg|multiplayer_is_dedicated_server),
				(call_script, "script_client_get_player_agent"),
				(eq, ":user_id", reg0),
				(lt, "$custom_camera", 1),
				(call_script, "script_client_custom_camera", 1),
				(start_presentation,"prsnt_artillery_hud"),
			(try_end),
    (try_end),
  ]),
#MOD end
  
  #script_use_item
  # INPUT: arg1 = agent_id, arg2 = instance_id
  # OUTPUT: none
  ("use_item",
   [
     (store_script_param, ":instance_id", 1),
     (store_script_param, ":user_id", 2),

#MOD begin
    (prop_instance_get_scene_prop_kind, ":scene_prop_id", ":instance_id"),

    (try_begin),
      (this_or_next|eq, ":scene_prop_id", "spr_cannon_barrel_1_32lb"),
			(this_or_next|eq, ":scene_prop_id", "spr_cannon_ammo_box_32lb_1"),
      (this_or_next|is_between, ":scene_prop_id", "spr_pavise_prop_b", "spr_shields_end"),
			(eq, ":scene_prop_id", "spr_ladder_move_5m"),

      (agent_get_horse, ":horse", ":user_id"),
      (eq, ":horse", -1),

      (try_begin),
        (eq, ":scene_prop_id", "spr_cannon_barrel_1_32lb"),
        (scene_prop_enable_after_time, ":instance_id", 100),
        (try_begin),
					(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 0),#refill gunpowder
          (agent_get_wielded_item, ":item_id", ":user_id", 0),
          (try_begin),
            (eq, ":item_id", "itm_refiller"),
            (try_begin),
              (this_or_next|multiplayer_is_server),
              (neg|game_in_multiplayer_mode),
              (agent_unequip_item, ":user_id", itm_refiller, 0),
              (agent_equip_item, ":user_id", itm_refiller_alt_mode, 0),
              (agent_set_wielded_item, ":user_id", itm_refiller_alt_mode),
            (try_end),
            (scene_prop_set_slot, ":instance_id", scene_prop_usage_state, 1),
          (try_end),

        (else_try),
					(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 1),#compress gunpowder
          (agent_get_wielded_item, ":item_id", ":user_id", 0),
          (try_begin),
            (eq, ":item_id", itm_refiller_alt_mode),
            (scene_prop_set_slot, ":instance_id", scene_prop_usage_state, 2),
          (try_end),

        (else_try),
					(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 2),#place ammuntion
          (agent_get_wielded_item, ":item_id", ":user_id", 0),
          (try_begin),
            (this_or_next|eq, ":item_id", "itm_round_shot_32lb"),
            (eq, ":item_id", "itm_canister_shot_32lb"),
            (try_begin),
              (this_or_next|multiplayer_is_server),
              (neg|game_in_multiplayer_mode),
              (agent_unequip_item, ":user_id", ":item_id", 0),
              (call_script, "script_agent_set_weight", ":user_id", ":item_id", agent_sub_carry_weight),
            (try_end),
            (store_sub, ":missile_id", ":item_id", "itm_cannon_ammo_begin"),
            (val_add, ":missile_id", "itm_cannon_missile_begin"),
            (scene_prop_set_slot, ":instance_id", scene_prop_ammo_type, ":missile_id"),
						(try_begin),
							(eq, "$g_quick_artillery", 1),
							(scene_prop_set_slot, ":instance_id", scene_prop_usage_state, 4),
							(call_script, "script_start_use_item", ":instance_id", ":user_id"),
							(try_begin),
								(agent_has_item_equipped, ":user_id", "itm_linstock"),
								(agent_set_wielded_item, ":user_id", "itm_linstock"),
							(try_end),
						(else_try),
							(scene_prop_set_slot, ":instance_id", scene_prop_usage_state, 3),
							(try_begin),
								(agent_has_item_equipped, ":user_id", "itm_refiller_alt_mode"),
								(agent_set_wielded_item, ":user_id", "itm_refiller_alt_mode"),
							(try_end),
						(try_end),
					(try_end),

				(else_try),
					(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 3),#ram down ammuntion
					(agent_get_wielded_item, ":item_id", ":user_id", 0),
					(try_begin),
						(eq, ":item_id", itm_refiller_alt_mode),
						(try_begin),
							(this_or_next|multiplayer_is_server),
							(neg|game_in_multiplayer_mode),
							(agent_unequip_item, ":user_id", itm_refiller_alt_mode, 0),
							(agent_equip_item, ":user_id", itm_refiller, 0),
							(agent_set_wielded_item, ":user_id", itm_refiller),
						(try_end),
						(scene_prop_set_slot, ":instance_id", scene_prop_usage_state, 4),
					(try_end),
				(try_end),
			
			(else_try),
				(eq, ":scene_prop_id", "spr_cannon_ammo_box_32lb_1"),
				(scene_prop_enable_after_time, ":instance_id", 500),
				(agent_set_slot, ":user_id", slot_agent_use_scene_prop, ":instance_id"),
				(try_begin),
					(neg|multiplayer_is_dedicated_server),
					(call_script, "script_client_get_player_agent"),
					(assign, ":player_agent", reg0),
					(agent_is_active, ":player_agent"),
					(agent_is_alive, ":player_agent"),
					(agent_slot_eq, ":player_agent", slot_agent_use_scene_prop, ":instance_id"),
					(start_presentation,"prsnt_ammo_box_hud"),
				(try_end),

			(else_try),
				(is_between, ":scene_prop_id", "spr_pavise_prop_b", "spr_shields_end"),
				(try_begin),
					(this_or_next|multiplayer_is_server),
					(neg|game_in_multiplayer_mode),

					(assign, ":item_slot_used", 4),
					(try_for_range, ":item_slots", 0, ":item_slot_used"),
						(agent_get_item_slot, ":item", ":user_id", ":item_slots"),
						(eq, ":item", -1),
						(assign, ":item_slot_used", -1),#stop loop
						
						(assign, ":props_begin", "spr_pavise_prop_b"),
						(assign, ":props_end", "spr_pavise_prop_b_+1"),
						(assign, ":items_begin", itm_tab_shield_pavise_b),
						(assign, ":range_end", 4),
						(try_for_range, ":i", 0, ":range_end"),
							(try_begin),
								(eq, ":i", 1),
								(assign, ":props_begin", "spr_pavise_prop_b_+1"),
								(assign, ":props_end", "spr_pavise_prop_b_+2"),
								(assign, ":items_begin", itm_tab_shield_pavise_b +loom_1_item),
							(else_try),
								(eq, ":i", 2),
								(assign, ":props_begin", "spr_pavise_prop_b_+2"),
								(assign, ":props_end", "spr_pavise_prop_b_+3"),
								(assign, ":items_begin", itm_tab_shield_pavise_b +loom_2_item),
							(else_try),
								(eq, ":i", 3),
								(assign, ":props_begin", "spr_pavise_prop_b_+3"),
								(assign, ":props_end", "spr_shields_end"),
								(assign, ":items_begin", itm_tab_shield_pavise_b +loom_3_item),
							(try_end),
						
							(is_between, ":scene_prop_id", ":props_begin", ":props_end"),
							(store_sub, ":offset", ":scene_prop_id", ":props_begin"),
							(store_add, ":shield_item", ":items_begin", ":offset"),
							(assign, ":range_end", -1), #break loop
						(try_end),

						(assign, ":continue", 0),
						(item_get_slot, ":carry_slot", ":shield_item", slot_item_carry_slot),
						(store_add, ":agent_slot", slot_agent_carry_slot_0_used, ":carry_slot"),
						(try_begin),
							(agent_slot_eq, ":user_id" , ":agent_slot", 0),
							(agent_set_slot, ":user_id", ":agent_slot", 1),
							(assign, ":continue", 1),
						(else_try),
							(assign, ":slot_count", 0),
							(try_for_range, ":i", 0, 4),
								(agent_get_item_slot, ":item", ":user_id", ":i"),
								(gt, ":item", -1),
								(item_slot_eq, ":item", slot_item_carry_slot, ":carry_slot"),
								(val_add, ":slot_count", 1),
							(try_end),
							(eq, ":slot_count", 0),#check if the agent realy has NO item with the same carry slot equiped
							(assign, ":continue", 1),
						(try_end),
					 
						(eq, ":continue", 1),
						(scene_prop_get_hit_points, ":cur_hitpoints", ":instance_id"),
						# (assign, reg1, ":cur_hitpoints"),
						# (display_message, "@pick up hp {reg1}"),
						(call_script, "script_move_scene_prop_under_ground", ":instance_id", 0),
						(agent_equip_item, ":user_id", ":shield_item"),
						(agent_set_wielded_item, ":user_id", ":shield_item"),
						(call_script, "script_agent_set_weight", ":user_id", ":shield_item", agent_add_carry_weight),
						(agent_set_slot, ":user_id", slot_agent_cur_shield_hp, ":cur_hitpoints"),
					(try_end),
				(try_end),
			(else_try),
				(eq, ":scene_prop_id", "spr_ladder_move_5m"),
				(try_begin),
					(this_or_next|multiplayer_is_server),
					(neg|game_in_multiplayer_mode),
					
					(assign, ":item_slot_used", 4),
					(try_for_range, ":item_slots", 0, ":item_slot_used"),
						(agent_get_item_slot, ":item", ":user_id", ":item_slots"),
						(eq, ":item", -1),
						(assign, ":item_slot_used", -1),#stop loop
						
						(assign, ":continue", 0),
						(try_begin),
							(agent_slot_eq, ":user_id" , slot_agent_carry_slot_0_used, 0),
							(agent_set_slot, ":user_id", slot_agent_carry_slot_0_used, 1),
							(assign, ":continue", 1),
						(else_try),
							(assign, ":slot_count", 0),
							(try_for_range, ":i", 0, 4),
								(agent_get_item_slot, ":item", ":user_id", ":i"),
								(gt, ":item", -1),
								(item_slot_eq, ":item", slot_item_carry_slot, 0),
								(val_add, ":slot_count", 1),
							(try_end),
							(eq, ":slot_count", 0),#check if the agent realy has NO item with the same carry slot equiped
							(assign, ":continue", 1),
						(try_end),
					 
						(eq, ":continue", 1),
						
						(call_script, "script_move_scene_prop_under_ground", ":instance_id", 0),
						(agent_equip_item, ":user_id", itm_ladder_5m),
						(agent_set_wielded_item, ":user_id", itm_ladder_5m),
						(call_script, "script_agent_set_weight", ":user_id", itm_ladder_5m, agent_add_carry_weight),
					(try_end),
				(try_end),
			(try_end),
     
		(else_try),
#MOD end
#     (prop_instance_get_position, pos0, ":instance_id"),#MOD disable
     (try_begin),
       (game_in_multiplayer_mode),
#       (prop_instance_get_scene_prop_kind, ":scene_prop_id", ":instance_id"),#MOD disable
       (eq, ":scene_prop_id", "spr_winch_b"),

       (multiplayer_get_my_player, ":my_player_no"),

       (this_or_next|gt, ":my_player_no", 0),
       (neg|multiplayer_is_dedicated_server),

       (ge, ":my_player_no", 0),
       (player_get_agent_id, ":my_agent_id", ":my_player_no"),
       (ge, ":my_agent_id", 0),
       (agent_is_active, ":my_agent_id"),
       (agent_get_team, ":my_team_no", ":my_agent_id"),
       (eq, ":my_team_no", 0),
                             
       (ge, ":user_id", 0),
       (agent_is_active, ":user_id"),
       (agent_get_player_id, ":user_player", ":user_id"),
       (str_store_player_username, s7, ":user_player"),

       (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot), 
       (try_begin),
         (eq, ":opened_or_closed", 0),
         (display_message, "@{s7} opened the gate"),
       (else_try),  
         (display_message, "@{s7} closed the gate"),
       (try_end),
     (try_end),  

#     (prop_instance_get_scene_prop_kind, ":scene_prop_id", ":instance_id"),#MOD disable
     
     (try_begin),
       (this_or_next|eq, ":scene_prop_id", "spr_winch_b"),
       (eq, ":scene_prop_id", "spr_winch"),
       (assign, ":effected_object", "spr_portcullis"),
     (else_try),
#       (this_or_next|eq, ":scene_prop_id", "spr_door_destructible"),
#       (this_or_next|eq, ":scene_prop_id", "spr_castle_f_door_b"),
#       (this_or_next|eq, ":scene_prop_id", "spr_castle_e_sally_door_a"),
#       (this_or_next|eq, ":scene_prop_id", "spr_castle_f_sally_door_a"),
#       (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_left"),
#       (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_right"),
#       (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),
#       (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_right"),
#       (this_or_next|eq, ":scene_prop_id", "spr_castle_f_door_a"),
#       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_6m"),
#       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_8m"),
#       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_10m"),
#       (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_12m"),
#       (eq, ":scene_prop_id", "spr_siege_ladder_move_14m"),
       (assign, ":effected_object", ":scene_prop_id"),
     (try_end),

     (assign, ":smallest_dist", -1),
     (prop_instance_get_position, pos0, ":instance_id"),#MOD disable
     (scene_prop_get_num_instances, ":num_instances_of_effected_object", ":effected_object"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_effected_object"),
       (scene_prop_get_instance, ":cur_instance_id", ":effected_object", ":cur_instance"),
       (prop_instance_get_position, pos1, ":cur_instance_id"),
       (get_sq_distance_between_positions, ":dist", pos0, pos1),
       (this_or_next|eq, ":smallest_dist", -1),
       (lt, ":dist", ":smallest_dist"),
       (assign, ":smallest_dist", ":dist"),
       (assign, ":effected_object_instance_id", ":cur_instance_id"),
     (try_end),

     (try_begin),
       (ge, ":instance_id", 0),
       (ge, ":smallest_dist", 0),

       (try_begin),     
         (eq, ":effected_object", "spr_portcullis"),
         (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot),

         (try_begin),
           (eq, ":opened_or_closed", 0), #open gate
           (scene_prop_enable_after_time, ":instance_id", 400), #4 seconds
           (try_begin),
             (this_or_next|multiplayer_is_server),
             (neg|game_in_multiplayer_mode),
             (prop_instance_get_position, pos0, ":effected_object_instance_id"),
             (position_move_z, pos0, 375),
             (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 400),
           (try_end),
           (scene_prop_set_slot, ":instance_id", scene_prop_open_or_close_slot, 1),

           (try_begin),
             (eq, ":scene_prop_id", "spr_winch_b"),
             (this_or_next|multiplayer_is_server),
             (neg|game_in_multiplayer_mode),
             (prop_instance_get_position, pos1, ":instance_id"),
             (prop_instance_rotate_to_position, ":instance_id", pos1, 400, 72000),
           (try_end),
     
         (else_try), #close gate     
           (scene_prop_enable_after_time, ":instance_id", 400), #4 seconds
           (try_begin),
             (this_or_next|multiplayer_is_server),
             (neg|game_in_multiplayer_mode),
             (prop_instance_get_position, pos0, ":effected_object_instance_id"),
             (position_move_z, pos0, -375),
             (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 400),
           (try_end),
           (scene_prop_set_slot, ":instance_id", scene_prop_open_or_close_slot, 0),
           (try_begin),
             (eq, ":scene_prop_id", "spr_winch_b"),
             (this_or_next|multiplayer_is_server),
             (neg|game_in_multiplayer_mode),
             (prop_instance_get_position, pos1, ":instance_id"),
             (prop_instance_rotate_to_position, ":instance_id", pos1, 400, -72000),
           (try_end),
         (try_end),
       (else_try),
         (this_or_next|eq, ":effected_object", "spr_siege_ladder_move_6m"),
         (this_or_next|eq, ":effected_object", "spr_siege_ladder_move_8m"),
         (this_or_next|eq, ":effected_object", "spr_siege_ladder_move_10m"),
         (this_or_next|eq, ":effected_object", "spr_siege_ladder_move_12m"),
         (eq, ":effected_object", "spr_siege_ladder_move_14m"),

         (try_begin),
           (eq, ":effected_object", "spr_siege_ladder_move_6m"),
           (assign, ":animation_time_drop", 120),
           (assign, ":animation_time_elevate", 240),
         (else_try),
           (eq, ":effected_object", "spr_siege_ladder_move_8m"),
           (assign, ":animation_time_drop", 140),
           (assign, ":animation_time_elevate", 280),
         (else_try),
           (eq, ":effected_object", "spr_siege_ladder_move_10m"),
           (assign, ":animation_time_drop", 160),
           (assign, ":animation_time_elevate", 320),
         (else_try),
           (eq, ":effected_object", "spr_siege_ladder_move_12m"),
           (assign, ":animation_time_drop", 190),
           (assign, ":animation_time_elevate", 360),
         (else_try),
           (eq, ":effected_object", "spr_siege_ladder_move_14m"),
           (assign, ":animation_time_drop", 230),
           (assign, ":animation_time_elevate", 400),
         (try_end),
     
         (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot),

         (try_begin),
           (scene_prop_enable_after_time, ":effected_object_instance_id", ":animation_time_elevate"), #3 seconds in average
           (eq, ":opened_or_closed", 0), #ladder at ground
           (try_begin),#MOD edit
             (this_or_next|multiplayer_is_server),
	     (neg|game_in_multiplayer_mode),
             (prop_instance_get_starting_position, pos0, ":effected_object_instance_id"),
#           (prop_instance_enable_physics, ":effected_object_instance_id", 0),#MOD disable
             (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 300),
           (try_end),
           (scene_prop_set_slot, ":effected_object_instance_id", scene_prop_open_or_close_slot, 1),
         (else_try), #ladder at wall
           (scene_prop_enable_after_time, ":effected_object_instance_id", ":animation_time_drop"), #1.5 seconds in average
           (prop_instance_get_position, pos0, ":instance_id"),

           (assign, ":smallest_dist", -1),
           (try_for_range, ":entry_point_no", multi_entry_points_for_usable_items_start, multi_entry_points_for_usable_items_end),
             (entry_point_get_position, pos1, ":entry_point_no"),
             (get_sq_distance_between_positions, ":dist", pos0, pos1),
             (this_or_next|eq, ":smallest_dist", -1),
             (lt, ":dist", ":smallest_dist"),
             (assign, ":smallest_dist", ":dist"),
             (assign, ":nearest_entry_point", ":entry_point_no"),
           (try_end),

           (try_begin),
             (ge, ":smallest_dist", 0),
             (lt, ":smallest_dist", 22500), #max 15m distance
             (scene_prop_set_slot, ":effected_object_instance_id", scene_prop_smoke_effect_done, 0),
             (try_begin),#MOD edit
               (this_or_next|multiplayer_is_server),
	           (neg|game_in_multiplayer_mode),
               (entry_point_get_position, pos1, ":nearest_entry_point"),
               (position_rotate_x, pos1, -90),
#             (prop_instance_enable_physics, ":effected_object_instance_id", 0),#MOD disable
               (prop_instance_animate_to_position, ":effected_object_instance_id", pos1, 130),
             (try_end),
           (try_end),
           (scene_prop_set_slot, ":effected_object_instance_id", scene_prop_open_or_close_slot, 0),
         (try_end),
       (else_try),
         (this_or_next|eq, ":effected_object", "spr_door_destructible"),
         (this_or_next|eq, ":effected_object", "spr_castle_f_door_b"),
         (this_or_next|eq, ":scene_prop_id", "spr_castle_e_sally_door_a"),     
         (this_or_next|eq, ":scene_prop_id", "spr_castle_f_sally_door_a"),     
         (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_left"),     
         (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_right"),     
         (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),     
         (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_right"),     
         (eq, ":scene_prop_id", "spr_castle_f_door_a"),
     
         (assign, ":effected_object_instance_id", ":instance_id"),
         (scene_prop_get_slot, ":opened_or_closed", ":effected_object_instance_id", scene_prop_open_or_close_slot),

         (try_begin),
           (eq, ":opened_or_closed", 0),
           (scene_prop_enable_after_time, ":effected_object_instance_id", 100),
           (try_begin),#MOD edit
             (this_or_next|multiplayer_is_server),
	         (neg|game_in_multiplayer_mode),
             (prop_instance_get_starting_position, pos0, ":effected_object_instance_id"),
             (try_begin),
               (neq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),
               (neq, ":scene_prop_id", "spr_earth_sally_gate_left"),
               (position_rotate_z, pos0, -85),
             (else_try),  
               (position_rotate_z, pos0, 85),
             (try_end),
             (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 100),
           (try_end),
           (scene_prop_set_slot, ":effected_object_instance_id", scene_prop_open_or_close_slot, 1),
         (else_try),
           (scene_prop_enable_after_time, ":effected_object_instance_id", 100),
           (try_begin),#MOD edit
             (this_or_next|multiplayer_is_server),
						 (neg|game_in_multiplayer_mode),
             (prop_instance_get_starting_position, pos0, ":effected_object_instance_id"),
             (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 100),
           (try_end),
           (scene_prop_set_slot, ":effected_object_instance_id", scene_prop_open_or_close_slot, 0),
         (try_end),
       (try_end),
     (try_end),
#MOD begin
    (try_end),
#MOD end
     ]),

  #script_determine_team_flags
  # INPUT: none
  # OUTPUT: none
  ("determine_team_flags",
  [
		(store_script_param, ":team_no", 1),          
#MOD begin
		(team_get_faction, ":team_faction_no", 0),
		(assign, ":end_faction", "fac_kingdoms_end"), 
		(try_for_range, ":cur_faction", "fac_kingdom_1", ":end_faction"),
			(eq, ":team_faction_no", ":cur_faction"),
			# (store_sub, ":offset", ":cur_faction", "fac_kingdom_1"),
			# (store_add, ":cur_flag", "spr_headquarters_flag_kingdom_1", ":offset"),
			(assign, ":cur_flag", "spr_headquarters_flag_kingdom"),
			(try_begin),
        (eq, ":team_no", 0),
				(assign, "$team_1_flag_scene_prop", ":cur_flag"),
			(else_try),
				(assign, "$team_2_flag_scene_prop", ":cur_flag"),
			(try_end),
			(assign, ":end_faction", -1),#break loop
		(try_end),
#MOD end
	]),
   

  #script_calculate_flag_move_time
  # INPUT: arg1 = number_of_total_agents_around_flag, arg2 = dist_between_flag_and_its_pole
  # OUTPUT: reg0 = flag move time
  ("calculate_flag_move_time",
   [
     (store_script_param, ":number_of_total_agents_around_flag", 1),
     (store_script_param, ":dist_between_flag_and_its_target", 2),

     (try_begin), #(if no one is around flag it again moves to its current owner situation but 5 times slower than normal)
       (eq, ":number_of_total_agents_around_flag", 0),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 2500),#5.00 * 1.00 * (500 stable) = 2000 
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 1),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 500), #1.00 * (500 stable) = 500
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 2),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 300), #0.60(0.60) * (500 stable) = 300
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 3),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 195), #0.39(0.60 * 0.65) * (500 stable) = 195
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 4),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 137), #0.273(0.60 * 0.65 * 0.70) * (500 stable) = 136.5 >rounding> 137
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 5),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 102), #0.20475(0.60 * 0.65 * 0.70 * 0.75) * (500 stable) = 102.375 >rounding> 102
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 6),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 82),  #0.1638(0.60 * 0.65 * 0.70 * 0.75 * 0.80) * (500 stable) = 81.9 >rounding> 82
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 7),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 66),  #0.13104(0.60 * 0.65 * 0.70 * 0.75 * 0.80 * 0.85) * (500 stable) = 65.52 >rounding> 66
     (else_try),
       (eq, ":number_of_total_agents_around_flag", 8),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 59),  #0.117936(0.60 * 0.65 * 0.70 * 0.75 * 0.80 * 0.85 * 0.90) * (500 stable) = 58.968 >rounding> 59
     (else_try),
       (store_mul, reg0, ":dist_between_flag_and_its_target", 56),  #0.1120392(0.60 * 0.65 * 0.70 * 0.75 * 0.80 * 0.85 * 0.90 * 0.95) * (500 stable) = 56.0196 >rounding> 56
     (try_end), 

     (assign, ":number_of_players", 0),
     (get_max_players, ":num_players"),                               
     (try_for_range, ":cur_player", 0, ":num_players"),
       (player_is_active, ":cur_player"),
       (val_add, ":number_of_players", 1),
     (try_end),

     (try_begin),
       (lt, ":number_of_players", 10),
       (val_mul, reg0, 50),
     (else_try),
       (lt, ":number_of_players", 35),
       (store_sub, ":number_of_players_multipication", 35, ":number_of_players"),
       (val_mul, ":number_of_players_multipication", 2),
       (store_sub, ":number_of_players_multipication", 100, ":number_of_players_multipication"),
       (val_mul, reg0, ":number_of_players_multipication"),
     (else_try),
       (val_mul, reg0, 100),
     (try_end),

     (try_begin),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
       (val_mul, reg0, 2),
     (try_end),

     (val_div, reg0, 10000), #100x for number of players around flag, 100x for number of players in game
     ]),

  #script_move_flag
  # INPUT: arg1 = shown_flag_id, arg2 = move time in seconds, pos0 = target position
  # OUTPUT: none
  ("move_flag",
   [
     (store_script_param, ":shown_flag_id", 1),
     (store_script_param, ":shown_flag_move_time", 2),

     (try_begin),
       (multiplayer_is_server), #added after auto-animating
     
       (try_begin),
         (eq, ":shown_flag_move_time", 0), #stop
         (prop_instance_stop_animating, ":shown_flag_id"),
       (else_try),
         (prop_instance_animate_to_position, ":shown_flag_id", pos0, ":shown_flag_move_time"),
       (try_end),
     (try_end),
   ]),

  #script_move_headquarters_flags
  # INPUT: arg1 = current_owner, arg2 = number_of_agents_around_flag_team_1, arg3 = number_of_agents_around_flag_team_2
  # OUTPUT: none
  ("move_headquarters_flags",
   [
     (store_script_param, ":flag_no", 1),
     (store_script_param, ":number_of_agents_around_flag_team_1", 2),
     (store_script_param, ":number_of_agents_around_flag_team_2", 3),

     (store_add, ":cur_flag_slot", multi_data_flag_owner_begin, ":flag_no"),
     (troop_get_slot, ":current_owner", "trp_multiplayer_data", ":cur_flag_slot"),

     (scene_prop_get_num_instances, ":num_instances", "spr_headquarters_flag_gray_code_only"),
     (try_begin),
       (assign, ":visibility", 0),
       (lt, ":flag_no", ":num_instances"),
       (scene_prop_get_instance, ":flag_id", "spr_headquarters_flag_gray_code_only", ":flag_no"),
       (scene_prop_get_visibility, ":visibility", ":flag_id"),
     (try_end),

     (try_begin),
       (eq, ":visibility", 1),
       (assign, ":shown_flag", 0),
       (assign, ":shown_flag_id", ":flag_id"),
     (else_try),
       (scene_prop_get_num_instances, ":num_instances", "$team_1_flag_scene_prop"),
       (try_begin),
         (assign, ":visibility", 0),
         (lt, ":flag_no", ":num_instances"),
         (scene_prop_get_instance, ":flag_id", "$team_1_flag_scene_prop", ":flag_no"),
         (scene_prop_get_visibility, ":visibility", ":flag_id"),
       (try_end),

       #(scene_prop_get_instance, ":flag_id", "$team_1_flag_scene_prop", ":flag_no"),
       #(scene_prop_get_visibility, ":visibility", ":flag_id"),       
       (try_begin),
         (eq, ":visibility", 1),
         (assign, ":shown_flag", 1),
         (assign, ":shown_flag_id", ":flag_id"),
       (else_try),
         (scene_prop_get_num_instances, ":num_instances", "$team_2_flag_scene_prop"),
         (try_begin),
           (assign, ":visibility", 0),
           (lt, ":flag_no", ":num_instances"),
           (scene_prop_get_instance, ":flag_id", "$team_2_flag_scene_prop", ":flag_no"),
           (scene_prop_get_visibility, ":visibility", ":flag_id"),
         (try_end),

         #(scene_prop_get_instance, ":flag_id", "$team_2_flag_scene_prop", ":flag_no"),
         #(scene_prop_get_visibility, ":visibility", ":flag_id"),              
         (try_begin),
           (eq, ":visibility", 1),
           (assign, ":shown_flag", 2),
           (assign, ":shown_flag_id", ":flag_id"),
         (try_end),
       (try_end),
     (try_end),

     (try_begin),
       (scene_prop_get_instance, ":pole_id", "spr_headquarters_pole_code_only", ":flag_no"),
     (try_end),

     (try_begin),       
       (eq, ":shown_flag", ":current_owner"), #situation 1 : (current owner is equal shown flag)
       (try_begin),
         (ge, ":number_of_agents_around_flag_team_1", 1),
         (ge, ":number_of_agents_around_flag_team_2", 1),         
         (assign, ":flag_movement", 0), #0:stop
       (else_try),  
         (eq, ":number_of_agents_around_flag_team_1", 0),
         (eq, ":number_of_agents_around_flag_team_2", 0),
         (assign, ":flag_movement", 1), #1:rise (slow)
       (else_try),
         (try_begin),
           (ge, ":number_of_agents_around_flag_team_1", 1),
           (eq, ":number_of_agents_around_flag_team_2", 0),
           (eq, ":current_owner", 1),
           (assign, ":flag_movement", 1), #1:rise (fast)
         (else_try),
           (eq, ":number_of_agents_around_flag_team_1", 0),
           (ge, ":number_of_agents_around_flag_team_2", 1),
           (eq, ":current_owner", 2),
           (assign, ":flag_movement", 1), #1:rise (fast)
         (else_try),
           (assign, ":flag_movement", -1), #-1:drop (fast)
         (try_end),
       (try_end),
     (else_try), #situation 2 : (current owner is different than shown flag)
       (try_begin),
         (ge, ":number_of_agents_around_flag_team_1", 1),
         (ge, ":number_of_agents_around_flag_team_2", 1),
         (assign, ":flag_movement", 0), #0:stop
       (else_try),  
         (eq, ":number_of_agents_around_flag_team_1", 0),
         (eq, ":number_of_agents_around_flag_team_2", 0),
         (assign, ":flag_movement", -1), #-1:drop (slow)
       (else_try),
         (try_begin),
           (ge, ":number_of_agents_around_flag_team_1", 1),
           (eq, ":number_of_agents_around_flag_team_2", 0),
           (try_begin),
             (eq, ":shown_flag", 1),
             (assign, ":flag_movement", 1), #1:rise (fast)
           (else_try),
             (neq, ":current_owner", 1),
             (assign, ":flag_movement", -1), #-1:drop (fast)
           (try_end),
         (else_try),
           (eq, ":number_of_agents_around_flag_team_1", 0),
           (ge, ":number_of_agents_around_flag_team_2", 1),
           (try_begin),
             (eq, ":shown_flag", 2),
             (assign, ":flag_movement", 1), #1:rise (fast)
           (else_try),
             (neq, ":current_owner", 2),
             (assign, ":flag_movement", -1), #-1:drop (fast)
           (try_end),
         (try_end),
       (try_end),
     (try_end),

     (store_add, ":number_of_total_agents_around_flag", ":number_of_agents_around_flag_team_1", ":number_of_agents_around_flag_team_2"),

     (try_begin),
       (eq, ":flag_movement", 0),
       (assign, reg0, 0),
     (else_try),
       (eq, ":flag_movement", 1),
       (prop_instance_get_position, pos1, ":shown_flag_id"),
       (prop_instance_get_position, pos0, ":pole_id"),
       (position_move_z, pos0, multi_headquarters_pole_height),
       (get_distance_between_positions, ":dist_between_flag_and_its_target", pos0, pos1),
       (call_script, "script_calculate_flag_move_time", ":number_of_total_agents_around_flag", ":dist_between_flag_and_its_target"),
     (else_try),  
       (eq, ":flag_movement", -1),
       (prop_instance_get_position, pos1, ":shown_flag_id"),
       (prop_instance_get_position, pos0, ":pole_id"),
       (get_distance_between_positions, ":dist_between_flag_and_its_target", pos0, pos1),
       (call_script, "script_calculate_flag_move_time", ":number_of_total_agents_around_flag", ":dist_between_flag_and_its_target"),
     (try_end),

     (call_script, "script_move_flag", ":shown_flag_id", reg0), #pos0 : target position
     ]),

  #script_set_num_agents_around_flag
  # INPUT: arg1 = flag_no, arg2 = owner_code
  # OUTPUT: none
  ("set_num_agents_around_flag",
   [
     (store_script_param, ":flag_no", 1),
     (store_script_param, ":current_owner_code", 2),

     (store_div, ":number_of_agents_around_flag_team_1", ":current_owner_code", 100),
     (store_mod, ":number_of_agents_around_flag_team_2", ":current_owner_code", 100),

     (store_add, ":cur_flag_owner_counts_slot", multi_data_flag_players_around_begin, ":flag_no"),
     (troop_set_slot, "trp_multiplayer_data", ":cur_flag_owner_counts_slot", ":current_owner_code"),

     (call_script, "script_move_headquarters_flags", ":flag_no", ":number_of_agents_around_flag_team_1", ":number_of_agents_around_flag_team_2"),
  ]),

  #script_move_object_to_nearest_entry_point
  # INPUT: none
  # OUTPUT: none
  ("move_object_to_nearest_entry_point",
   [
     (store_script_param, ":scene_prop_no", 1),

     (scene_prop_get_num_instances, ":num_instances", ":scene_prop_no"),

     (try_for_range, ":instance_no", 0, ":num_instances"),
       (scene_prop_get_instance, ":instance_id", ":scene_prop_no", ":instance_no"),
       (prop_instance_get_position, pos0, ":instance_id"),

       (assign, ":smallest_dist", -1),
       (try_for_range, ":entry_point_no", multi_entry_points_for_usable_items_start, multi_entry_points_for_usable_items_end),
         (entry_point_get_position, pos1, ":entry_point_no"),
         (get_sq_distance_between_positions, ":dist", pos0, pos1),
         (this_or_next|eq, ":smallest_dist", -1),
         (lt, ":dist", ":smallest_dist"),
         (assign, ":smallest_dist", ":dist"),
         (assign, ":nearest_entry_point", ":entry_point_no"),
       (try_end),

       (try_begin),
         (ge, ":smallest_dist", 0),
         (lt, ":smallest_dist", 22500), #max 15m distance
         (entry_point_get_position, pos1, ":nearest_entry_point"),
         (position_rotate_x, pos1, -90),
         (prop_instance_animate_to_position, ":instance_id", pos1, 1),
       (try_end),
     (try_end),
   ]),


  #script_multiplayer_server_on_agent_spawn_common
  # INPUT: arg1 = agent_no
  # OUTPUT: none
  ("multiplayer_server_on_agent_spawn_common",
   [
     (store_script_param, ":agent_no", 1),
     (agent_set_slot, ":agent_no", slot_agent_in_duel_with, -1),
     (try_begin),
       (agent_is_non_player, ":agent_no"),
       (assign, "$g_multiplayer_ready_for_spawning_agent", 1),
     (try_end),
     ]),

  #script_multiplayer_server_player_joined_common
  # INPUT: arg1 = player_no
  # OUTPUT: none
	("multiplayer_server_player_joined_common",
	[
		(store_script_param, ":player_no", 1),
		(try_begin),
			(this_or_next|player_is_active, ":player_no"),
			(eq, ":player_no", 0),
			(call_script, "script_multiplayer_init_player_slots", ":player_no"),
			(store_mission_timer_a, ":player_join_time"),
			(player_set_slot, ":player_no", slot_player_join_time, ":player_join_time"),
			(player_set_slot, ":player_no", slot_player_first_spawn, 1),
			#fight and destroy only
			#(player_set_slot, ":player_no", slot_player_damage_given_to_target_1, 0),
			#(player_set_slot, ":player_no", slot_player_damage_given_to_target_2, 0),
			#fight and destroy only end

#MOD begin
      (player_set_gold, ":player_no", "$player_start_gold"),
			(player_get_unique_id, ":unique_id", ":player_no"),

			(try_begin),
				(ge, ":player_no", 1),#no server
				(try_begin),
					(multiplayer_is_dedicated_server),
          (eq, "$g_allow_online_chars", 1),
					(assign, reg1, ":unique_id"),
					(str_store_string, s1, "str_server_security_token"),
					(str_store_server_name, s2),
					(str_store_player_username, s3, ":player_no"),
					(assign, reg2, "str_service_action_2"),
					(assign, reg3, 0),
					(str_store_string, s10, "str_service_action_2"),
					(str_store_string, s10, "str_service_url_1"),
					# (str_encode_url, s10),
					(send_message_to_url, s10),
					(str_store_string, s0, "@[DEBUG]: Send http request: {s10}"),
					(server_add_message_to_log, s0),
					(display_message, s0),
					#(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
				(try_end),
				
				#sync troop stats
				(get_max_players, ":max_players"),
				(try_for_range, ":cur_player", 1, ":max_players"),
					(player_is_active, ":cur_player"),
					(player_slot_eq, ":cur_player", slot_player_use_troop, "trp_online_character"),
					(player_get_troop_id, ":used_troop", ":cur_player"),
					
					(try_for_range, ":i", 0, 3),
						(try_begin),
							(eq, ":i", 0),
							(assign, ":range_begin", ca_strength),
							(assign, ":range_end", ca_intelligence),
							(assign, ":event_type", multiplayer_event_client_set_attribute),
						(else_try),
							(eq, ":i", 1),
							(assign, ":range_begin", wpt_one_handed_weapon),
							(assign, ":range_end", wpt_firearm + 1),
							(assign, ":event_type", multiplayer_event_client_set_proficiency),
						(else_try),
							(assign, ":range_begin", "skl_horse_archery"),
							(assign, ":range_end", "skl_reserved_17"),
							(assign, ":event_type", multiplayer_event_client_set_skill),
						(try_end),
			
						(try_for_range, ":cur_type", ":range_begin", ":range_end"),
							(assign, ":continue", 0),
							(try_begin),
								(eq, ":i", 0),
								(store_attribute_level, ":cur_value", ":used_troop", ":cur_type"),
								(neq, ":cur_value", 3),
								(assign, ":continue", 1),
							(else_try),
								(eq, ":i", 1),
								(store_proficiency_level, ":cur_value", ":used_troop", ":cur_type"),
								(neq, ":cur_value", 100),
								(assign, ":continue", 1),
							(else_try),
								(eq, ":i", 2),
								(this_or_next|is_between, ":cur_type", "skl_horse_archery", "skl_shield"),
								(this_or_next|eq, ":cur_type", "skl_weapon_master"),
								(this_or_next|eq, ":cur_type", "skl_resistance"),
								(this_or_next|eq, ":cur_type", "skl_power_throw"),
								(this_or_next|eq, ":cur_type", "skl_power_strike"),
								(is_between, ":cur_type", "skl_power_pull", "skl_reserved_17"),
								(store_skill_level, ":cur_value", ":cur_type", ":used_troop"),
								(neq, ":cur_value", 0),
								(assign, ":continue", 1),
							(try_end),
						
							(eq, ":continue", 1),
							(store_mul, ":send_type", ":cur_type", 1000),
							(store_add, ":send_value", ":cur_value", ":send_type"),
							(multiplayer_send_2_int_to_player, ":player_no", ":event_type", ":used_troop", ":send_value"),
						(try_end),
					(try_end),
				(try_end),
				
				#set weather and time
				(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_return_weather_time, "$g_multiplayer_wt_val_1", "$g_multiplayer_wt_val_2"),
			(try_end),

			(try_begin),#reset round bonus if joins too late
				(gt, ":player_join_time", 10),
			
				(store_div, ":offset", ":unique_id", 1048576),
				(store_mul, ":id_sub", ":offset", 1048576),
				(val_sub, ":unique_id", ":id_sub"),
				
				(store_add, ":array", "trp_unquie_id_round_bonus_0", ":offset"),
				(troop_set_slot, ":array", ":unique_id", 0),
			(try_end),

			(store_add, ":player_troop", "trp_player_troop_0", ":player_no"),
			(call_script, "script_multiplayer_server_set_troop_id", ":player_no", ":player_troop"),
			#save online values to backup troop later
			(store_add, ":player_troop_backup", "trp_player_troop_online_0", ":player_no"),
			(troop_set_slot, ":player_troop", slot_troop_online_troop_backup, ":player_troop_backup"),	
			
			(try_begin),
				(eq, "$g_show_debug_messages", 1),
				(assign, reg1, ":player_join_time"),
				(str_store_troop_name, s1, ":player_troop"),
				(str_store_troop_name, s2, ":player_troop_backup"),
				(str_store_player_username, s3, ":player_no"),
				(str_store_string, s0, "@[DEBUG] Time: {reg1}, {s1} Player {s3} has joined. Troop {s1} and {s2} used."),
				(server_add_message_to_log, s0),
				(display_message, s0),
			(try_end),
		
			# (try_begin),
				# (player_is_admin, ":player_no"),
				# #Broadcast message
	# #      (player_get_unique_id, reg0, ":player_no"),
				# (str_store_player_username, s1, ":player_no"),
				# (str_store_string, s0, "@Administrator {s1} has joined the server."),
				# (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
			# (try_end),

			(neq, ":player_no", 0),#dont send to server
			#reset advanced orders
			(try_for_range, ":cur_array", advanced_order_arrays_begin, advanced_order_arrays_end),
				(troop_set_slot, ":cur_array", ":player_no", 0),
			(try_end),
#MOD end
			(call_script, "script_multiplayer_send_initial_information", ":player_no"),
		(try_end),
	]),

  #script_multiplayer_server_before_mission_start_common
  # INPUT: none
  # OUTPUT: none
  ("multiplayer_server_before_mission_start_common",
   [
#MOD disable
     # (try_begin),
       # (scene_allows_mounted_units),
       # (assign, "$g_horses_are_avaliable", 1),
     # (else_try),
       # (assign, "$g_horses_are_avaliable", 0),
     # (try_end),
#     (scene_set_day_time, 15),
     (assign, "$g_multiplayer_mission_end_screen", 0),

     (get_max_players, ":num_players"),
     (try_for_range, ":player_no", 0, ":num_players"),
       (player_is_active, ":player_no"),
       (call_script, "script_multiplayer_init_player_slots", ":player_no"),
#MOD begin
			 (player_set_slot, ":player_no", slot_player_first_spawn, 1), #not required in siege, bt, fd
#MOD end
			(try_end),
     ]),

  #script_multiplayer_server_on_agent_killed_or_wounded_common
  # INPUT: arg1 = dead_agent_no, arg2 = killer_agent_no
  # OUTPUT: none
  ("multiplayer_server_on_agent_killed_or_wounded_common",
   [
     (store_script_param, ":dead_agent_no", 1),
     (store_script_param, ":killer_agent_no", 2),

     (call_script, "script_multiplayer_event_agent_killed_or_wounded", ":dead_agent_no", ":killer_agent_no"),
     #adding 1 score points to agent which kills enemy agent at server
     (try_begin),
       (multiplayer_is_server),
#MOD begin
        (try_begin),
          (neg|agent_is_non_player, ":dead_agent_no"), 
          (agent_get_player_id, ":dead_player_no", ":dead_agent_no"),
          (player_is_active, ":dead_player_no"),
          (player_get_gold, ":gold", ":dead_player_no"),
          (try_begin),
            (lt, ":gold", "$player_start_gold"),
            (player_set_gold, ":dead_player_no", "$player_start_gold", 0),
          (try_end),
        (try_end),
#MOD end

#MOD disable
       # (try_begin), #killing myself because of some reason (friend hit, fall, team change)
         # (lt, ":killer_agent_no", 0),
         # (ge, ":dead_agent_no", 0),
         # (neg|agent_is_non_player, ":dead_agent_no"),
         # (agent_get_player_id, ":dead_agent_player_id", ":dead_agent_no"),
         # (player_is_active, ":dead_agent_player_id"),
         # (player_get_score, ":dead_agent_player_score", ":dead_agent_player_id"),
         # (val_add, ":dead_agent_player_score", -1),
         # (player_set_score, ":dead_agent_player_id", ":dead_agent_player_score"),
       # (else_try), #killing teammate
         # (ge, ":killer_agent_no", 0),
         # (ge, ":dead_agent_no", 0),

         # (agent_get_team, ":killer_team_no", ":killer_agent_no"),
         # (agent_get_team, ":dead_team_no", ":dead_agent_no"),
         # (eq, ":killer_team_no", ":dead_team_no"),
         # (neg|agent_is_non_player, ":killer_agent_no"),
         # (agent_get_player_id, ":killer_agent_player_id", ":killer_agent_no"),
         # (player_is_active, ":killer_agent_player_id"),
         # (player_get_score, ":killer_agent_player_score", ":killer_agent_player_id"),
         # (val_add, ":killer_agent_player_score", -1),
         # (player_set_score, ":killer_agent_player_id", ":killer_agent_player_score"),

         # #(player_get_kill_count, ":killer_agent_player_kill_count", ":killer_agent_player_id"),
         # #(val_add, ":killer_agent_player_kill_count", -2),
         # #(player_set_kill_count, ":killer_agent_player_id", ":killer_agent_player_kill_count"),
       # (else_try), #killing enemy
         (ge, ":killer_agent_no", 0),
         (ge, ":dead_agent_no", 0),
         (agent_is_human, ":dead_agent_no"),
         (agent_is_human, ":killer_agent_no"),
#MOD begin
				 (agent_get_team, ":killer_team_no", ":killer_agent_no"),
         (agent_get_team, ":dead_team_no", ":dead_agent_no"),
         (neq, ":killer_team_no", ":dead_team_no"),

         # (try_begin),
           # (eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
           # (try_begin),
             # (eq, "$g_battle_death_mode_started", 1),
             # (neq, ":dead_agent_no", ":killer_agent_no"),
             # (call_script, "script_calculate_new_death_waiting_time_at_death_mod"),
           # (try_end),
         # (try_end),
        
        (try_begin),
          (neg|agent_is_non_player, ":dead_agent_no"), 
          (agent_get_player_id, ":dead_player_no", ":dead_agent_no"),
          (ge, ":dead_player_no", 0),
          (player_is_active, ":dead_player_no"),

           (try_begin),
             (eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
                     
             (try_for_agents, ":cur_agent"),
               (agent_is_non_player, ":cur_agent"),
               (agent_is_human, ":cur_agent"),
               (agent_is_alive, ":cur_agent"),
               (agent_get_group, ":agent_group", ":cur_agent"),
               (try_begin),
                 (eq, ":dead_player_no", ":agent_group"),
                 (agent_set_group, ":cur_agent", -1),                 
               (try_end),
             (try_end),
           (try_end),
         (try_end),
#MOD disable
         # (neg|agent_is_non_player, ":killer_agent_no"),
         # (agent_get_player_id, ":killer_agent_player_id", ":killer_agent_no"),
         # (player_is_active, ":killer_agent_player_id"),
         # (player_get_score, ":killer_agent_player_score", ":killer_agent_player_id"),
         # (agent_get_team, ":killer_agent_team", ":killer_agent_no"),
         # (agent_get_team, ":dead_agent_team", ":dead_agent_no"),
         # (try_begin),
           # (neq, ":killer_agent_team", ":dead_agent_team"),
           # (val_add, ":killer_agent_player_score", 1),
         # (else_try),
           # (val_add, ":killer_agent_player_score", -1),
         # (try_end),
         # (player_set_score, ":killer_agent_player_id", ":killer_agent_player_score"),
      # (try_end),
     (try_end),

      #Restore health, shield, ammo and horse after killing in duel mode
     (try_begin),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
       (ge, ":killer_agent_no", 0),
       (agent_is_human, ":killer_agent_no"),
       (ge, ":dead_agent_no", 0),
			 (agent_is_human, ":dead_agent_no"),
			 (call_script, "script_restore_agent", ":killer_agent_no"),
     (try_end),
#MOD end

     (call_script, "script_add_kill_death_counts", ":killer_agent_no", ":dead_agent_no"),
     #money management
     # (call_script, "script_money_management_after_agent_death", ":killer_agent_no", ":dead_agent_no"),#MOD disable
     ]),

  #script_multiplayer_close_gate_if_it_is_open
  # INPUT: none
  # OUTPUT: none
  ("multiplayer_close_gate_if_it_is_open",
   [
     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_winch_b"),
     (try_for_range, ":cur_prop_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":prop_instance_id", "spr_winch_b", ":cur_prop_instance"),
       (scene_prop_slot_eq, ":prop_instance_id", scene_prop_open_or_close_slot, 1),
       (scene_prop_get_instance, ":effected_object_instance_id", "spr_portcullis", ":cur_prop_instance"),
       (prop_instance_get_starting_position, pos0, ":effected_object_instance_id"),      
       (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 1),
     (try_end),
   ]),  

  #script_multiplayer_move_moveable_objects_initial_positions
  # INPUT: none
  # OUTPUT: none
  ("multiplayer_move_moveable_objects_initial_positions",
   [
     (call_script, "script_move_object_to_nearest_entry_point", "spr_siege_ladder_move_6m"),
     (call_script, "script_move_object_to_nearest_entry_point", "spr_siege_ladder_move_8m"),
     (call_script, "script_move_object_to_nearest_entry_point", "spr_siege_ladder_move_10m"),
     (call_script, "script_move_object_to_nearest_entry_point", "spr_siege_ladder_move_12m"),
     (call_script, "script_move_object_to_nearest_entry_point", "spr_siege_ladder_move_14m"),
   ]),

  #script_team_set_score
  # INPUT: arg1 = team_no, arg2 = score
  # OUTPUT: none
  ("team_set_score",
   [
     (store_script_param, ":team_no", 1),
     (store_script_param, ":score", 2),
     
     (team_set_score, ":team_no", ":score"),
   ]),

  #script_initialize_objects
  # INPUT: none
  # OUTPUT: none
  ("initialize_objects",
   [
#MOD disable
     # (assign, ":number_of_players", 0),
     # (get_max_players, ":num_players"),
     # (try_for_range, ":player_no", 0, ":num_players"),
       # (player_is_active, ":player_no"),
       # (val_add, ":number_of_players", 1),
     # (try_end),

     # 1 player = (Sqrt(1) - 1) * 200 + 1200 = 1200, 1800 (minimum)
     # 4 player = (Sqrt(4) - 1) * 200 + 1200 = 1400, 2100
     # 9 player = (Sqrt(9) - 1) * 200 + 1200 = 1600, 2400
     # 16 player = (Sqrt(16) - 1) * 200 + 1200 = 1800, 2700 (general used)
     # 25 player = (Sqrt(25) - 1) * 200 + 1200 = 2000, 3000 (average)
     # 36 player = (Sqrt(36) - 1) * 200 + 1200 = 2200, 3300
     # 49 player = (Sqrt(49) - 1) * 200 + 1200 = 2400, 3600
     # 64 player = (Sqrt(49) - 1) * 200 + 1200 = 2600, 3900

     # (set_fixed_point_multiplier, 100),
     # (val_mul, ":number_of_players", 100),
     # (store_sqrt, ":number_of_players", ":number_of_players"),
     # (val_sub, ":number_of_players", 100),
     # (val_max, ":number_of_players", 0),
     # (store_mul, ":effect_of_number_of_players", ":number_of_players", 2),
     # (store_add, ":health_catapult", multi_minimum_target_health, ":effect_of_number_of_players"),     
     # (store_mul, ":health_trebuchet", ":health_catapult", 15), #trebuchet's health is 1.5x of catapult's     
     # (val_div, ":health_trebuchet", 10),
     # (store_mul, ":health_sally_door", ":health_catapult", 18), #sally door's health is 1.8x of catapult's     
     # (val_div, ":health_sally_door", 10),
     # (store_mul, ":health_sally_door_double", ":health_sally_door", 2),

     # (assign, "$g_number_of_targets_destroyed", 0),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_e_sally_door_a"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_e_sally_door_a", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),
     
     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_f_sally_door_a"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_f_sally_door_a", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_earth_sally_gate_left"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_earth_sally_gate_left", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_double"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_earth_sally_gate_right"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_earth_sally_gate_right", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_double"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_viking_keep_destroy_sally_door_left"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_viking_keep_destroy_sally_door_left", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),     

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_viking_keep_destroy_sally_door_right"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_viking_keep_destroy_sally_door_right", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),     

     #(store_div, ":health_sally_door_div_3", ":health_sally_door", 3),#MOD disable

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_f_door_a"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_f_door_a", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_div_3"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),     

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_f_door_b"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_f_door_b", ":cur_instance"),
       (prop_instance_get_starting_position, pos0, ":cur_instance_id"),
       (prop_instance_stop_animating, ":cur_instance_id"),
       (prop_instance_set_position, ":cur_instance_id", pos0),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_div_3"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),
     ]),
  
  #script_initialize_objects_clients
  # INPUT: none
  # OUTPUT: none
  ("initialize_objects_clients",
   [
#MOD disable
     # (assign, ":number_of_players", 0),
     # (get_max_players, ":num_players"),
     # (try_for_range, ":player_no", 0, ":num_players"),
       # (player_is_active, ":player_no"),
       # (val_add, ":number_of_players", 1),
     # (try_end),

     # 1 player = (Sqrt(1) - 1) * 200 + 1200 = 1200, 1800 (minimum)
     # 4 player = (Sqrt(4) - 1) * 200 + 1200 = 1400, 2100
     # 9 player = (Sqrt(9) - 1) * 200 + 1200 = 1600, 2400
     # 16 player = (Sqrt(16) - 1) * 200 + 1200 = 1800, 2700 (general used)
     # 25 player = (Sqrt(25) - 1) * 200 + 1200 = 2000, 3000 (average)
     # 36 player = (Sqrt(36) - 1) * 200 + 1200 = 2200, 3300
     # 49 player = (Sqrt(49) - 1) * 200 + 1200 = 2400, 3600
     # 64 player = (Sqrt(49) - 1) * 200 + 1200 = 2600, 3900

     # (set_fixed_point_multiplier, 100),
     # (val_mul, ":number_of_players", 100),
     # (store_sqrt, ":number_of_players", ":number_of_players"),
     # (val_sub, ":number_of_players", 100),
     # (val_max, ":number_of_players", 0),
     # (store_mul, ":effect_of_number_of_players", ":number_of_players", 2),
     # (store_add, ":health_catapult", multi_minimum_target_health, ":effect_of_number_of_players"),     
     # (store_mul, ":health_trebuchet", ":health_catapult", 15), #trebuchet's health is 1.5x of catapult's
     # (val_div, ":health_trebuchet", 10),
     # (store_mul, ":health_sally_door", ":health_catapult", 18), #trebuchet's health is 1.8x of trebuchet's
     # (val_div, ":health_sally_door", 10),
     # (store_mul, ":health_sally_door_double", ":health_sally_door", 2),

     # (assign, "$g_number_of_targets_destroyed", 0),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_e_sally_door_a"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_e_sally_door_a", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),
     
     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_f_sally_door_a"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_f_sally_door_a", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_earth_sally_gate_left"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_earth_sally_gate_left", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_double"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_earth_sally_gate_right"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_earth_sally_gate_right", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_double"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_viking_keep_destroy_sally_door_left"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_viking_keep_destroy_sally_door_left", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),             

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_viking_keep_destroy_sally_door_right"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_viking_keep_destroy_sally_door_right", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),             

#     (store_div, ":health_sally_door_div_3", ":health_sally_door", 3),#MOD disable

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_f_door_a"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_f_door_a", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_div_3"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),     

     (scene_prop_get_num_instances, ":num_instances_of_scene_prop", "spr_castle_f_door_b"),     
     (try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
       (scene_prop_get_instance, ":cur_instance_id", "spr_castle_f_door_b", ":cur_instance"),
       (prop_instance_enable_physics, ":cur_instance_id", 1),
       #(scene_prop_set_hit_points, ":cur_instance_id", ":health_sally_door_div_3"),#MOD disable
#MOD begin
			 (prop_instance_refill_hit_points, ":cur_instance_id"),
			 (scene_prop_set_slot, ":cur_instance_id", scene_prop_is_used, 1),
#MOD end
     (try_end),     
     ]),

  #script_show_multiplayer_message
  # INPUT: arg1 = multiplayer_message_type
  # OUTPUT: none
  ("show_multiplayer_message",
   [
    (store_script_param, ":multiplayer_message_type", 1),
    (store_script_param, ":value", 2),

    (assign, "$g_multiplayer_message_type", ":multiplayer_message_type"),

    (try_begin),
      (eq, ":multiplayer_message_type", multiplayer_message_type_round_result_in_battle_mode),
      (assign, "$g_multiplayer_message_value_1", ":value"),
      (start_presentation, "prsnt_multiplayer_message_1"),
      
      # (try_begin), #end of round in clients
        # (neg|multiplayer_is_server),
        # (assign, "$g_battle_death_mode_started", 0),
      # (try_end),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_auto_team_balance_done),
      (assign, "$g_multiplayer_message_value_1", ":value"),
      (start_presentation, "prsnt_multiplayer_message_2"),
      (assign, "$g_team_balance_next_round", 0), 
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_auto_team_balance_next),
      (assign, "$g_team_balance_next_round", 1),
      (call_script, "script_warn_player_about_auto_team_balance"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_auto_team_balance_no_need),
      (assign, "$g_team_balance_next_round", 0),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_capture_the_flag_score),
      (assign, "$g_multiplayer_message_value_1", ":value"),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_flag_returned_home),
      (assign, "$g_multiplayer_message_value_1", ":value"),    
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_capture_the_flag_stole),
      (assign, "$g_multiplayer_message_value_1", ":value"),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_poll_result),
      (assign, "$g_multiplayer_message_value_3", ":value"),
      (start_presentation, "prsnt_multiplayer_message_3"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_flag_neutralized),      
      (assign, "$g_multiplayer_message_value_1", ":value"),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_flag_captured),
      (assign, "$g_multiplayer_message_value_1", ":value"),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_flag_is_pulling),
      (assign, "$g_multiplayer_message_value_1", ":value"),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_round_draw),
      (start_presentation, "prsnt_multiplayer_message_1"),
#MOD disable
    # (else_try),
      # (eq, ":multiplayer_message_type", multiplayer_message_type_target_destroyed),
    
      # (try_begin), #destroy score (condition : a target destroyed)
        # (eq, "$g_defender_team", 0),
        # (assign, ":attacker_team_no", 1),
      # (else_try),
        # (assign, ":attacker_team_no", 0),
      # (try_end),
       
      # (team_get_score, ":team_score", ":attacker_team_no"),
      # (val_add, ":team_score", 1),
      # (call_script, "script_team_set_score", ":attacker_team_no", ":team_score"), #destroy score end

      # (assign, "$g_multiplayer_message_value_1", ":value"),
      # (start_presentation, "prsnt_multiplayer_message_1"),
    # (else_try),
      # (eq, ":multiplayer_message_type", multiplayer_message_type_defenders_saved_n_targets),      
      # (assign, "$g_multiplayer_message_value_1", ":value"),
      # (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":multiplayer_message_type", multiplayer_message_type_attackers_won_the_round),
      (try_begin),
        # (eq, "$g_defender_team", 0),
        (assign, "$g_multiplayer_message_value_1", 1),
      (else_try),
        (assign, "$g_multiplayer_message_value_1", 0),
      (try_end),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (try_end),
    ]),

  #script_draw_this_round
  # INPUT: arg1 = value
  ("draw_this_round",
   [
    (store_script_param, ":value", 1),
    (try_begin),
      (eq, ":value", -9), #destroy mod round end
      (assign, "$g_round_ended", 1),
      (store_mission_timer_a, "$g_round_finish_time"),
      #(assign, "$g_multiplayer_message_value_1", -1),
      #(assign, "$g_multiplayer_message_type", multiplayer_message_type_round_draw),
      #(start_presentation, "prsnt_multiplayer_message_1"),
    (else_try),
      (eq, ":value", -1), #draw
      (assign, "$g_round_ended", 1),
      (store_mission_timer_a, "$g_round_finish_time"),
      (assign, "$g_multiplayer_message_value_1", -1),
      (assign, "$g_multiplayer_message_type", multiplayer_message_type_round_draw),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try), 
      (eq, ":value", 0), #defender wins
      #THIS_IS_OUR_LAND achievement
      (try_begin),
        (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
        (multiplayer_get_my_player, ":my_player_no"),
        (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
        (player_get_agent_id, ":my_player_agent", ":my_player_no"),
        (ge, ":my_player_agent", 0),
        (agent_is_alive, ":my_player_agent"),
        (agent_get_team, ":my_player_agent_team_no", ":my_player_agent"),
        (eq, ":my_player_agent_team_no", 0), #defender
        (unlock_achievement, ACHIEVEMENT_THIS_IS_OUR_LAND),
      (try_end),
      #THIS_IS_OUR_LAND achievement end
      (assign, "$g_round_ended", 1),
      (store_mission_timer_a, "$g_round_finish_time"),
        
      (team_get_faction, ":faction_of_winner_team", 0),
      (team_get_score, ":team_1_score", 0),
      (val_add, ":team_1_score", 1),
      (team_set_score, 0, ":team_1_score"),
      (assign, "$g_winner_team", 0),
      (str_store_faction_name, s1, ":faction_of_winner_team"),

      (assign, "$g_multiplayer_message_value_1", ":value"),
      (try_begin),
        (neq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),#MOD
        (assign, "$g_multiplayer_message_type", multiplayer_message_type_round_result_in_siege_mode),
      (else_try),
        (assign, "$g_multiplayer_message_type", multiplayer_message_type_round_result_in_battle_mode),
      (try_end),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (else_try), 
      (eq, ":value", 1), #attacker wins
      (assign, "$g_round_ended", 1),
      (store_mission_timer_a, "$g_round_finish_time"),
  
      (team_get_faction, ":faction_of_winner_team", 1),
      (team_get_score, ":team_2_score", 1),
      (val_add, ":team_2_score", 1),
      (team_set_score, 1, ":team_2_score"),
      (assign, "$g_winner_team", 1),
      (str_store_faction_name, s1, ":faction_of_winner_team"),

      (assign, "$g_multiplayer_message_value_1", ":value"),
      (try_begin),
        (neq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),#MOD
        (assign, "$g_multiplayer_message_type", multiplayer_message_type_round_result_in_siege_mode),
      (else_try),
        (assign, "$g_multiplayer_message_type", multiplayer_message_type_round_result_in_battle_mode),
      (try_end),
      (start_presentation, "prsnt_multiplayer_message_1"),
    (try_end),
    #LAST_MAN_STANDING achievement
    (try_begin),
      (is_between, ":value", 0, 2), #defender or attacker wins
      (try_begin),
        (eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
        (multiplayer_get_my_player, ":my_player_no"),
        (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
        (player_get_agent_id, ":my_player_agent", ":my_player_no"),
        (ge, ":my_player_agent", 0),
        (agent_is_alive, ":my_player_agent"),
        (agent_get_team, ":my_player_agent_team_no", ":my_player_agent"),
        (eq, ":my_player_agent_team_no", ":value"), #winner team
        (unlock_achievement, ACHIEVEMENT_LAST_MAN_STANDING),
      (try_end),
			#LAST_MAN_STANDING achievement end
#MOD begin
			(try_begin),
				(multiplayer_is_server),
				(get_max_players, ":num_players"),
				(try_for_range, ":cur_player_no", 1, ":num_players"),
					(player_is_active, ":cur_player_no"),
					(player_get_unique_id, ":cur_unique_id", ":cur_player_no"),
					(store_div, ":offset", ":cur_unique_id", 1048576),
					(store_mul, ":id_sub", ":offset", 1048576),
					(val_sub, ":cur_unique_id", ":id_sub"),
					
					(store_add, ":array", "trp_unquie_id_team_hits_0", ":offset"),
					(troop_set_slot, ":array", ":cur_unique_id", 0),#reset
					#apply round bonus
					(multiplayer_is_dedicated_server),
					(player_get_team_no, ":cur_player_team", ":cur_player_no"),
					(store_add, ":array", "trp_unquie_id_round_bonus_0", ":offset"),
					(try_begin),
						(eq, ":cur_player_team", ":value"),
						(troop_get_slot, ":cur_round_bonus", ":array", ":cur_unique_id"),
						(val_add, ":cur_round_bonus", 1),
						(troop_set_slot, ":array", ":cur_unique_id", ":cur_round_bonus"),
					(else_try),#reset
						(troop_set_slot, ":array", ":cur_unique_id", 0),
					(try_end),
				(try_end),
			(try_end),
#MOD end
    (try_end),
    ]),   
    
  #script_check_achievement_last_man_standing
  #INPUT: arg1 = value
  ("check_achievement_last_man_standing",
   [
   #LAST_MAN_STANDING achievement
	  (try_begin),
	    (store_script_param, ":value", 1),
		(is_between, ":value", 0, 2), #defender or attacker wins
	    (try_begin),
		  (eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
		  (multiplayer_get_my_player, ":my_player_no"),
		  (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
		  (player_get_agent_id, ":my_player_agent", ":my_player_no"),
		  (ge, ":my_player_agent", 0),
		  (agent_is_alive, ":my_player_agent"),
		  (agent_get_team, ":my_player_agent_team_no", ":my_player_agent"),
		  (eq, ":my_player_agent_team_no", ":value"), #winner team
		  (unlock_achievement, ACHIEVEMENT_LAST_MAN_STANDING),
		(try_end),
	  (try_end),
    #LAST_MAN_STANDING achievement end
    ]),

  #script_find_most_suitable_bot_to_control
  # INPUT: arg1 = value
  ("find_most_suitable_bot_to_control",
   [
      (set_fixed_point_multiplier, 100),
      (store_script_param, ":player_no", 1),
      (player_get_team_no, ":player_team", ":player_no"),

      (player_get_slot, ":x_coor", ":player_no", slot_player_death_pos_x),
      (player_get_slot, ":y_coor", ":player_no", slot_player_death_pos_y),
      (player_get_slot, ":z_coor", ":player_no", slot_player_death_pos_z),

      (init_position, pos0),
      (position_set_x, pos0, ":x_coor"),
      (position_set_y, pos0, ":y_coor"),
      (position_set_z, pos0, ":z_coor"),

      (assign, ":most_suitable_bot", -1),
      (assign, ":max_bot_score", -1),

      (try_for_agents, ":cur_agent"),
        (agent_is_alive, ":cur_agent"),
        (agent_is_human, ":cur_agent"),
        (agent_is_non_player, ":cur_agent"),
        (agent_get_team ,":cur_team", ":cur_agent"),
        (eq, ":cur_team", ":player_team"),
        (agent_get_position, pos1, ":cur_agent"),

        #getting score for distance of agent to death point (0..3000)
        (get_distance_between_positions_in_meters, ":dist", pos0, pos1),

        (try_begin),
          (lt, ":dist", 500),
          (store_sub, ":bot_score", 500, ":dist"),
        (else_try),
          (assign, ":bot_score", 0),
        (try_end),
        (val_mul, ":bot_score", 6),

        #getting score for distance of agent to enemy & friend agents (0..300 x agents)
        (try_for_agents, ":cur_agent_2"),
          (agent_is_alive, ":cur_agent_2"),
          (agent_is_human, ":cur_agent_2"),
          (neq, ":cur_agent", ":cur_agent_2"),      
          (agent_get_team ,":cur_team_2", ":cur_agent_2"),
          (try_begin),
            (neq, ":cur_team_2", ":player_team"),
            (agent_get_position, pos1, ":cur_agent_2"),
            (get_distance_between_positions, ":dist_2", pos0, pos1),
            (try_begin),
              (lt, ":dist_2", 300),
              (assign, ":enemy_near_score", ":dist_2"),
            (else_try),
              (assign, ":enemy_near_score", 300),
            (try_end),
            (val_add, ":bot_score", ":enemy_near_score"),
          (else_try),
            (agent_get_position, pos1, ":cur_agent_2"),
            (get_distance_between_positions, ":dist_2", pos0, pos1),
            (try_begin),
              (lt, ":dist_2", 300),
              (assign, ":friend_near_score", 300, ":dist_2"),
            (else_try),
              (assign, ":friend_near_score", 0),
            (try_end),
            (val_add, ":bot_score", ":friend_near_score"),
          (try_end),
        (try_end),

        #getting score for health (0..200)
        (store_agent_hit_points, ":agent_hit_points", ":cur_agent"),
        (val_mul, ":agent_hit_points", 2),
        (val_add, ":bot_score", ":agent_hit_points"),

        (ge, ":bot_score", ":max_bot_score"),
        (assign, ":max_bot_score", ":bot_score"),
        (assign, ":most_suitable_bot", ":cur_agent"),
      (try_end),

      (assign, reg0, ":most_suitable_bot"),
    ]),

  #script_game_receive_url_response
  #response format should be like this:
  #  [a number or a string]|[another number or a string]|[yet another number or a string] ...
  # here is an example response:
  # 12|Player|100|another string|142|323542|34454|yet another string
  # INPUT: arg1 = num_integers, arg2 = num_strings
  # reg0, reg1, reg2, ... up to 128 registers contain the integer values
  # s0, s1, s2, ... up to 128 strings contain the string values
  ("game_receive_url_response",
	[
    (assign, "$get_url_response", 1),#prevent from over-writing registers and strings
    (store_script_param, ":num_integers", 1),
    (store_script_param, ":num_strings", 2),
	
		(assign, ":http_status_reg", 0),
		(assign, ":service_action_reg", 1),
		(assign, ":status_reg", 2),
		(assign, ":unique_id_reg", 3),
		(assign, ":clan_reg", 4),
		(assign, ":gen_reg", 5),
		(assign, ":xp_reg", 6),
		(assign, ":gold_reg", 7),
		(assign, ":build_reg", 8),
		(assign, ":attribute_reg", 9),
		(assign, ":skill_reg", 11),
		(assign, ":proficiency_reg", 21),
		(assign, ":items_reg", 28),
		
		(assign, ":cur_http_status", -1),
		(assign, ":cur_service_action", -1),
		(assign, ":cur_status", -1),
	
		(assign, ":update_stats", 0),
		(assign, ":player_no", 0),#set to server at first
		
		(try_for_range, ":cur_reg", 0, ":num_integers"),
			(try_begin),(eq, ":cur_reg", 0),(assign, ":cur_value", reg0),
			(else_try),(eq, ":cur_reg", 1),(assign, ":cur_value", reg1),
			(else_try),(eq, ":cur_reg", 2),(assign, ":cur_value", reg2),
			(else_try),(eq, ":cur_reg", 3),(assign, ":cur_value", reg3),
			(else_try),(eq, ":cur_reg", 4),(assign, ":cur_value", reg4),
			(else_try),(eq, ":cur_reg", 5),(assign, ":cur_value", reg5),
			(else_try),(eq, ":cur_reg", 6),(assign, ":cur_value", reg6),
			(else_try),(eq, ":cur_reg", 7),(assign, ":cur_value", reg7),
			(else_try),(eq, ":cur_reg", 8),(assign, ":cur_value", reg8),
			(else_try),(eq, ":cur_reg", 9),(assign, ":cur_value", reg9),
			(else_try),(eq, ":cur_reg", 10),(assign, ":cur_value", reg10),
			(else_try),(eq, ":cur_reg", 11),(assign, ":cur_value", reg11),
			(else_try),(eq, ":cur_reg", 12),(assign, ":cur_value", reg12),
			(else_try),(eq, ":cur_reg", 13),(assign, ":cur_value", reg13),
			(else_try),(eq, ":cur_reg", 14),(assign, ":cur_value", reg14),
			(else_try),(eq, ":cur_reg", 15),(assign, ":cur_value", reg15),
			(else_try),(eq, ":cur_reg", 16),(assign, ":cur_value", reg16),
			(else_try),(eq, ":cur_reg", 17),(assign, ":cur_value", reg17),
			(else_try),(eq, ":cur_reg", 18),(assign, ":cur_value", reg18),
			(else_try),(eq, ":cur_reg", 19),(assign, ":cur_value", reg19),
			(else_try),(eq, ":cur_reg", 20),(assign, ":cur_value", reg20),
			(else_try),(eq, ":cur_reg", 21),(assign, ":cur_value", reg21),
			(else_try),(eq, ":cur_reg", 22),(assign, ":cur_value", reg22),
			(else_try),(eq, ":cur_reg", 23),(assign, ":cur_value", reg23),
			(else_try),(eq, ":cur_reg", 24),(assign, ":cur_value", reg24),
			(else_try),(eq, ":cur_reg", 25),(assign, ":cur_value", reg25),
			(else_try),(eq, ":cur_reg", 26),(assign, ":cur_value", reg26),
			(else_try),(eq, ":cur_reg", 27),(assign, ":cur_value", reg27),
			(else_try),(eq, ":cur_reg", 28),(assign, ":cur_value", reg28),
			(else_try),(eq, ":cur_reg", 29),(assign, ":cur_value", reg29),
			(else_try),(eq, ":cur_reg", 30),(assign, ":cur_value", reg30),
			(else_try),(eq, ":cur_reg", 31),(assign, ":cur_value", reg31),
			(else_try),(eq, ":cur_reg", 32),(assign, ":cur_value", reg32),
			(else_try),(eq, ":cur_reg", 33),(assign, ":cur_value", reg33),
			(else_try),(eq, ":cur_reg", 34),(assign, ":cur_value", reg34),
			(else_try),(eq, ":cur_reg", 35),(assign, ":cur_value", reg35),
			(else_try),(eq, ":cur_reg", 36),(assign, ":cur_value", reg36),
			(else_try),(eq, ":cur_reg", 37),(assign, ":cur_value", reg37),
			(else_try),(eq, ":cur_reg", 38),(assign, ":cur_value", reg38),
			(else_try),(eq, ":cur_reg", 39),(assign, ":cur_value", reg39),
			(else_try),(eq, ":cur_reg", 40),(assign, ":cur_value", reg40),
			(else_try),(eq, ":cur_reg", 41),(assign, ":cur_value", reg41),
			(else_try),(eq, ":cur_reg", 42),(assign, ":cur_value", reg42),
			(else_try),(eq, ":cur_reg", 43),(assign, ":cur_value", reg43),
			(else_try),(eq, ":cur_reg", 44),(assign, ":cur_value", reg44),
			(else_try),(eq, ":cur_reg", 45),(assign, ":cur_value", reg45),
			(else_try),(eq, ":cur_reg", 46),(assign, ":cur_value", reg46),
			(else_try),(eq, ":cur_reg", 47),(assign, ":cur_value", reg47),
			(else_try),(eq, ":cur_reg", 48),(assign, ":cur_value", reg48),
			(else_try),(eq, ":cur_reg", 49),(assign, ":cur_value", reg49),
			(else_try),(eq, ":cur_reg", 50),(assign, ":cur_value", reg50),
			(else_try),(eq, ":cur_reg", 51),(assign, ":cur_value", reg51),
			(else_try),(eq, ":cur_reg", 52),(assign, ":cur_value", reg52),
			(else_try),(eq, ":cur_reg", 53),(assign, ":cur_value", reg53),
			(else_try),(eq, ":cur_reg", 54),(assign, ":cur_value", reg54),
			(else_try),(eq, ":cur_reg", 55),(assign, ":cur_value", reg55),
			(else_try),(eq, ":cur_reg", 56),(assign, ":cur_value", reg56),
			(else_try),(eq, ":cur_reg", 57),(assign, ":cur_value", reg57),
			(else_try),(eq, ":cur_reg", 58),(assign, ":cur_value", reg58),
			(else_try),(eq, ":cur_reg", 59),(assign, ":cur_value", reg59),
			(else_try),(eq, ":cur_reg", 60),(assign, ":cur_value", reg60),
			(else_try),(eq, ":cur_reg", 61),(assign, ":cur_value", reg61),
			(else_try),(eq, ":cur_reg", 62),(assign, ":cur_value", reg62),
			(else_try),(eq, ":cur_reg", 63),(assign, ":cur_value", reg63),
			(else_try),(eq, ":cur_reg", 64),(assign, ":cur_value", reg64),
			(else_try),(eq, ":cur_reg", 65),(assign, ":cur_value", reg65),
			(else_try),(eq, ":cur_reg", 66),(assign, ":cur_value", reg66),
			(else_try),(eq, ":cur_reg", 67),(assign, ":cur_value", reg67),
			(else_try),(eq, ":cur_reg", 68),(assign, ":cur_value", reg68),
			(else_try),(eq, ":cur_reg", 69),(assign, ":cur_value", reg69),
			(else_try),(eq, ":cur_reg", 70),(assign, ":cur_value", reg70),
			(else_try),(eq, ":cur_reg", 71),(assign, ":cur_value", reg71),
			(else_try),(eq, ":cur_reg", 72),(assign, ":cur_value", reg72),
			(else_try),(eq, ":cur_reg", 73),(assign, ":cur_value", reg73),
			(else_try),(eq, ":cur_reg", 74),(assign, ":cur_value", reg74),
			(else_try),(eq, ":cur_reg", 75),(assign, ":cur_value", reg75),
			(else_try),(eq, ":cur_reg", 76),(assign, ":cur_value", reg76),
			(else_try),(eq, ":cur_reg", 77),(assign, ":cur_value", reg77),
			(else_try),(eq, ":cur_reg", 78),(assign, ":cur_value", reg78),
			(else_try),(eq, ":cur_reg", 79),(assign, ":cur_value", reg79),
			(else_try),(eq, ":cur_reg", 80),(assign, ":cur_value", reg80),
			(else_try),(eq, ":cur_reg", 81),(assign, ":cur_value", reg81),
			(else_try),(eq, ":cur_reg", 82),(assign, ":cur_value", reg82),
			(else_try),(eq, ":cur_reg", 83),(assign, ":cur_value", reg83),
			(else_try),(eq, ":cur_reg", 84),(assign, ":cur_value", reg84),
			(else_try),(eq, ":cur_reg", 85),(assign, ":cur_value", reg85),
			(else_try),(eq, ":cur_reg", 86),(assign, ":cur_value", reg86),
			(else_try),(eq, ":cur_reg", 87),(assign, ":cur_value", reg87),
			(else_try),(eq, ":cur_reg", 88),(assign, ":cur_value", reg88),
			(else_try),(eq, ":cur_reg", 89),(assign, ":cur_value", reg89),
			(else_try),(eq, ":cur_reg", 90),(assign, ":cur_value", reg90),
			(else_try),(eq, ":cur_reg", 91),(assign, ":cur_value", reg91),
			(else_try),(eq, ":cur_reg", 92),(assign, ":cur_value", reg92),
			(else_try),(eq, ":cur_reg", 93),(assign, ":cur_value", reg93),
			(else_try),(eq, ":cur_reg", 94),(assign, ":cur_value", reg94),
			(else_try),(eq, ":cur_reg", 95),(assign, ":cur_value", reg95),
			(else_try),(eq, ":cur_reg", 96),(assign, ":cur_value", reg96),
			(else_try),(eq, ":cur_reg", 97),(assign, ":cur_value", reg97),
			(else_try),(assign, ":cur_value", reg98),(try_end),
	  
			(assign, reg99, ":cur_value"),
			(try_begin),
				(eq, ":cur_reg", 0),
        (str_store_string, s64, "@{reg99}"),
			(else_try),
				(str_store_string, s64, "@{s64}, {reg99}"),
			(try_end),
		
			(try_begin),
				(eq, ":cur_reg", ":http_status_reg"),
				(assign, ":cur_http_status", ":cur_value"),
		
			(else_try),
				(eq, ":cur_reg", ":service_action_reg"),
				(assign, ":cur_service_action", ":cur_value"),
		
			(else_try),
				(eq, ":cur_reg", ":status_reg"),
				(assign, ":cur_status", ":cur_value"),
				(is_between, ":cur_http_status", 200, 300),#all ok
				(try_begin),#check server status
					(eq, ":cur_service_action", "str_service_action_1"),
          (try_begin),
            (eq, ":cur_status", 1),#all ok
						(str_store_string, s66, "@[DEBUG]: All OK"),
					(else_try),
						(eq, ":cur_status", 2),#token incorrect
						(str_store_string, s66, "@[DEBUG]: token incorrect"),
					(else_try),
						(eq, ":cur_status", 3),#server banned
						(str_store_string, s66, "@[DEBUG]: server banned"),
					(try_end),
					(display_message, s66),
					(server_add_message_to_log, s66),
				(try_end),
		
			(else_try),
				(ge, ":cur_reg", ":unique_id_reg"),
				(this_or_next|eq, ":cur_service_action", "str_service_action_2"),
				(eq, ":cur_service_action", "str_service_action_5"),
				(is_between, ":cur_http_status", 200, 300),#all ok
				(try_begin),
					(eq, ":cur_reg", ":unique_id_reg"),
					#get current player_no for unique_id
					(get_max_players, ":num_players"),
					(try_for_range, ":cur_player", 1, ":num_players"),#0 is server so starting from 1
						(player_is_active, ":cur_player"),
						(player_get_unique_id, ":unique_id", ":cur_player"),
						(eq, ":cur_value", ":unique_id"),
						(assign, ":num_players", -1),#break loop
						(assign, ":player_no", ":cur_player"),#set player_no to cur_player
						#do not use script_multiplayer_server_set_player_slot for this
						(try_begin),
							(eq, ":cur_service_action", "str_service_action_2"),
							(try_begin),
								(eq, ":cur_status", online_status_banned),
								(ban_player, ":player_no", 1, 0),
							(else_try),
								(player_set_slot, ":player_no", slot_player_online_status, ":cur_status"),#set slot on server
								(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_slot, slot_player_online_status, ":cur_status"),
								
								#handle admins
								(try_begin),
									(eq, ":cur_status", online_status_admin),
									(try_begin),
										(neg|player_is_admin, ":player_no"),
										(player_set_is_admin, ":player_no", 1),
										(multiplayer_send_int_to_player, ":player_no", multiplayer_event_client_set_admin, 1),
										# (str_store_player_username, s1, ":player_no"),
										# (str_store_string, s0, "@Administrator {s1} has joined the server."),
										# (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
									(try_end),
								(else_try),
									(player_is_admin, ":player_no"),
									(player_set_is_admin, ":player_no", 0),
									(multiplayer_send_int_to_player, ":player_no", multiplayer_event_client_set_admin, 0),
								(try_end),
								
								(try_begin),
									(eq, ":cur_status", online_status_incorrect_name),
									(multiplayer_send_string_to_player, ":player_no", multiplayer_event_store_string, s0),
								(else_try),
									(eq, ":cur_status", online_status_new_player),
									(multiplayer_send_string_to_player, ":player_no", multiplayer_event_store_string, s0),
								(try_end),
							(try_end),
						(else_try),#reset password
							(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_slot, slot_player_online_status, online_status_reset_password),
							(str_store_string, s0, "@^Account Name: {s0}^Code: {s1}"),
							(multiplayer_send_string_to_player, ":player_no", multiplayer_event_store_string, s0),
						(try_end),
					(try_end),
					
				(else_try),
					(eq, ":cur_reg", ":clan_reg"),
					(player_set_slot, ":player_no", slot_player_online_clan, ":cur_value"),
					(player_get_banner_id, ":player_banner", ":player_no"),
					(neq, ":player_banner", ":cur_value"),
					(multiplayer_send_int_to_player, ":player_no", multiplayer_event_client_set_banner, ":cur_value"),
					
				(else_try),
					(eq, ":cur_reg", ":gen_reg"),
					(player_set_slot, ":player_no", slot_player_online_generation, ":cur_value"),
					(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_slot, slot_player_online_generation, ":cur_value"),
					
				(else_try),
					(eq, ":cur_reg", ":xp_reg"),
					(try_begin),
						(neg|player_slot_eq, ":player_no", slot_player_online_xp, ":cur_value"),
						(player_set_slot, ":player_no", slot_player_online_xp, ":cur_value"),
						(multiplayer_send_int_to_player, ":player_no", multiplayer_event_client_set_experience, ":cur_value"),
						(assign, ":last_lvl", online_max_lvl +1),
						(try_for_range, ":cur_lvl", online_min_lvl +1, ":last_lvl"),
							(troop_get_slot, ":cur_lvl_xp", "trp_online_level_xp", ":cur_lvl"),
							(lt, ":cur_value", ":cur_lvl_xp"),
							(val_sub, ":cur_lvl", 1),
							(player_set_slot, ":player_no", slot_player_online_lvl, ":cur_lvl"),
							(assign, ":last_lvl", -1),#break loop
						(try_end),
					(try_end),
					
				(else_try),
					(eq, ":cur_reg", ":gold_reg"),
					(player_set_gold, ":player_no", ":cur_value", 0),
					
				(else_try),
					(eq, ":cur_reg", ":build_reg"),
					(try_begin),
						(neg|player_slot_eq, ":player_no", slot_player_online_next_build, ":cur_value"),
						(player_set_slot, ":player_no", slot_player_online_next_build, ":cur_value"),
					(try_end),
					(player_get_agent_id, ":player_agent_no", ":player_no"),
					(try_begin),
						(agent_is_active, ":player_agent_no"),
						(agent_is_alive, ":player_agent_no"),
						(try_begin),#only on same build, avoids build changing during a round
							(player_slot_eq, ":player_no", slot_player_online_cur_build, ":cur_value"),
							(assign, ":update_stats", 1),
						(try_end),
					# (else_try),
						# (assign, ":update_stats", 1),
						# (player_set_slot, ":player_no", slot_player_online_cur_build, ":cur_value"),
					(try_end),
		
				(else_try),
					(is_between, ":cur_reg", ":attribute_reg", ":items_reg"),
					#update stats on backup troop
					(try_begin),
						(this_or_next|eq, ":cur_status", online_status_registered),
						(eq, ":cur_status", online_status_admin),
						(assign, ":val_to_add", 1),
						(player_get_troop_id, ":troop_no", ":player_no"),
						(troop_get_slot, ":backup_troop_no", ":troop_no", slot_troop_online_troop_backup),
					
						(try_begin),
							(is_between, ":cur_reg", ":attribute_reg", ":skill_reg"),
							(try_begin),
								(eq, ":cur_reg", ":attribute_reg"),
								(assign, ":cur_type", ca_strength),
							(try_end),
							(store_attribute_level, ":last_value", ":backup_troop_no", ":cur_type"),
							(store_sub, ":value", ":cur_value", ":last_value"),
							(troop_raise_attribute, ":backup_troop_no", ":cur_type", ":value"),
							(assign, ":event", multiplayer_event_client_set_attribute),
						(else_try),
              (is_between, ":cur_reg", ":skill_reg", ":proficiency_reg"),
							(try_begin),
								(eq, ":cur_reg", ":skill_reg"),
								(assign, ":cur_type", "skl_horse_archery"),
							(try_end),
							(store_skill_level, ":last_value", ":cur_type", ":backup_troop_no"),
							(store_sub, ":value", ":cur_value", ":last_value"),
							(troop_raise_skill, ":backup_troop_no", ":cur_type", ":value"),
							(assign, ":event", multiplayer_event_client_set_skill),
							(try_begin),
								(eq, ":cur_type", "skl_athletics"),
								(store_sub, ":val_to_add", "skl_weapon_master", ":cur_type"),
							(else_try),
								(eq, ":cur_type", "skl_resistance"),
								(store_sub, ":val_to_add", "skl_power_throw", ":cur_type"),
							# (else_try),
								# (eq, ":cur_type", "skl_power_strike"),
								# (store_sub, ":val_to_add", "skl_power_pull", ":cur_type"),
							(try_end),
            (else_try),
							(try_begin),
								(eq, ":cur_reg", ":proficiency_reg"),
								(assign, ":cur_type", wpt_one_handed_weapon),
							(try_end),
							(store_proficiency_level, ":last_value", ":backup_troop_no", ":cur_type"),
							(store_sub, ":value", ":cur_value", ":last_value"),
							(troop_raise_proficiency_linear, ":backup_troop_no", ":cur_type", ":value"),
							(assign, ":event", multiplayer_event_client_set_proficiency),
						(try_end),

						#update stats on current troop
						(try_begin),
							(eq, ":update_stats", 1),
							(try_begin),
								(is_between, ":cur_reg", ":attribute_reg", ":skill_reg"),
								(store_attribute_level, ":last_value", ":troop_no", ":cur_type"),
							(else_try),
								(is_between, ":cur_reg", ":skill_reg", ":proficiency_reg"),
								(store_skill_level, ":last_value", ":cur_type", ":troop_no"),
							(else_try),
								(store_proficiency_level, ":last_value", ":troop_no", ":cur_type"),
								(try_begin),#apply skill bonus
									(this_or_next|is_between, ":cur_type", wpt_archery, wpt_throwing),
									(eq, ":cur_type", wpt_firearm),
									(try_begin),
										(eq, ":cur_type", wpt_archery),
										(store_skill_level, ":skl_wpt", "skl_power_pull", ":troop_no"),
										(val_mul, ":skl_wpt", pp_archery_bonus),
									(else_try),
										(eq, ":cur_type", wpt_crossbow),
										(store_skill_level, ":skl_wpt", "skl_power_pull", ":troop_no"),
										(val_mul, ":skl_wpt", pp_crossbow_bonus),
									(else_try),
										(store_skill_level, ":skl_wpt", "skl_power_reload", ":troop_no"),
										(val_mul, ":skl_wpt", pr_firearm_bonus),
									(try_end),
									(val_add, ":cur_value", ":skl_wpt"),
								(try_end),
								
								(agent_get_slot, ":armor_weight", ":player_agent_no", slot_agent_armor_weight),
								(store_div, ":armor_weight_kg", ":armor_weight", 10),#set to kg
								(assign, ":weight_lowers_wpf", 0),
								(try_begin),
									(gt, ":armor_weight_kg", weight_limit),
									(store_add, ":end_loop_weight", ":armor_weight_kg", 1),
									(try_for_range, ":cur_weight", weight_limit +1, ":end_loop_weight"),
										(store_sub, ":cur_wpf_reduction", ":cur_weight", 1),
										(val_div, ":cur_wpf_reduction", 10),
										(val_add, ":weight_lowers_wpf", ":cur_wpf_reduction"),
									(try_end),
								(try_end),
								(val_sub, ":cur_value", ":weight_lowers_wpf"),
							(try_end),
			
							(try_begin),
								(neq, ":cur_value", ":last_value"),
								(call_script, "script_multiplayer_set_troop_value", ":troop_no", ":event", ":cur_type", ":cur_value"),
							(try_end),
            (try_end),
						(val_add, ":cur_type", ":val_to_add"),
					(try_end),

				#set inventory
				(else_try),
					(this_or_next|eq, ":cur_status", online_status_registered),
					(this_or_next|eq, ":cur_status", online_status_new_player),
					(eq, ":cur_status", online_status_admin),
					(try_begin),
						(eq, ":cur_reg", ":items_reg"),
						(assign, ":cur_item_slot", slot_player_selected_item_indices_begin),
					(try_end),
          (try_begin),
            (gt, ":cur_value", -1),
						(call_script, "script_multiplayer_set_item_available_for_troop", ":cur_value", ":backup_troop_no", 1),
						(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_item_availability, ":cur_value", 1),
					(try_end),
					(try_begin),#set default item
						(lt, ":cur_item_slot", slot_player_selected_item_indices_end),
						(call_script, "script_multiplayer_server_set_player_slot", ":player_no", ":cur_item_slot", ":cur_value", to_player),
						#(display_message, "@set default inventory server", 0xCCCCCC),
						(val_add, ":cur_item_slot", 1),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
	
		(assign, reg97, ":num_integers"),
		(assign, reg98, ":num_strings"),
	
		(try_begin),
			(eq, ":num_strings", 1),
			(str_store_string, s65, "@^Str: {s0}"),
		(else_try),
			(eq, ":num_strings", 2),
			(str_store_string, s65, "@^Str: {s0}, {s1}"),
		(else_try),
			(str_clear, s65),
		(try_end),
		(try_begin),
			(gt, ":num_integers", 0),
			(str_store_string, s63, "@[Network DEBUG]: Got {reg97} integers and {reg98} strings ^Int: {s64} {s65}"),
		(else_try),
			(str_store_string, s63, "@[Network DEBUG]: Got {reg97} integers and {reg98} strings ^{s65}"),
		(try_end),
		
		(server_add_message_to_log, s63),
		(display_message, s63),
		
		(assign, "$get_url_response", 0),
  ]),  
  
      
  ("game_get_cheat_mode",
  [
    (assign, reg0, "$cheat_mode"),
  ]),    
  
  #script_game_receive_network_message
  # This script is called from the game engine when a new network message is received.
  # INPUT: arg1 = player_no, arg2 = event_type, arg3 = value, arg4 = value_2, arg5 = value_3, arg6 = value_4
  ("game_receive_network_message",
    [
      (store_script_param, ":player_no", 1),
      (store_script_param, ":event_type", 2),

      (try_begin),
        ###############
        #SERVER EVENTS#
        ###############
        (eq, ":event_type", multiplayer_event_set_item_selection),
        (store_script_param, ":slot_no", 3),
        (store_script_param, ":value", 4),  
        (try_begin),
          #valid slot check
          (is_between, ":slot_no", slot_player_selected_item_indices_begin, slot_player_selected_item_indices_end),
          #condition checks are done
          (player_set_slot, ":player_no", ":slot_no", ":value"),
        (try_end),
      (else_try),
        (eq, ":event_type", multiplayer_event_set_bot_selection),
        (store_script_param, ":slot_no", 3),
        (store_script_param, ":value", 4),
        (try_begin),
          #condition check
          (is_between, ":slot_no", slot_player_bot_type_1_wanted, slot_player_bot_type_20_wanted + 1),#MOD edit
          (is_between, ":value", 0, 2),
          #condition checks are done
          (player_set_slot, ":player_no", ":slot_no", ":value"),
        (try_end),
      (else_try),
        (eq, ":event_type", multiplayer_event_change_team_no),
        (store_script_param, ":value", 3),
        (try_begin),
          #validity check
          (player_get_team_no, ":player_team", ":player_no"),
          (neq, ":player_team", ":value"),

          #condition checks are done
          (try_begin),
            #check if available
            (call_script, "script_cf_multiplayer_team_is_available", ":player_no", ":value"),
            #reset troop_id to -1
#            (player_set_troop_id, ":player_no", -1),#MOD disable
#MOD begin
            (player_set_slot, ":player_no", slot_player_use_troop, -1),
#MOD end
            (player_set_team_no, ":player_no", ":value"),
           
            (try_begin),
              (neq, ":value", multi_team_spectator),
              (neq, ":value", multi_team_unassigned),
      
              (store_mission_timer_a, ":player_last_team_select_time"),         
              (player_set_slot, ":player_no", slot_player_last_team_select_time, ":player_last_team_select_time"),
      
              (multiplayer_send_message_to_player, ":player_no", multiplayer_event_return_confirmation),
            (try_end),
          (else_try),
            #reject request
            (multiplayer_send_message_to_player, ":player_no", multiplayer_event_return_rejection),
          (try_end),
        (try_end),
      (else_try),
        (eq, ":event_type", multiplayer_event_change_troop_id),
        (store_script_param, ":value", 3),
        #troop-faction validity check
        #(try_begin),
          #(eq, ":value", -1),
#          (player_set_troop_id, ":player_no", -1),#MOD disable
#MOD begin
          #(player_set_slot, ":player_no", slot_player_use_troop, -1),
#MOD end
        #(else_try),
          #(is_between, ":value", multiplayer_troops_begin, multiplayer_troops_end),
          #(player_get_team_no, ":player_team", ":player_no"),
          #(is_between, ":player_team", 0, multi_team_spectator),
          #(team_get_faction, ":team_faction", ":player_team"),
          #(store_troop_faction, ":new_troop_faction", ":value"),
          #(eq, ":new_troop_faction", ":team_faction"),

#            (player_set_troop_id, ":player_no", ":value"),#MOD disable
#MOD begin  
				(player_set_slot, ":player_no", slot_player_use_troop, ":value"),
				(call_script, "script_multiplayer_clear_player_selected_items", ":player_no"),

				#send http request to database
				(try_begin),
					(multiplayer_is_dedicated_server),
          (eq, "$g_allow_online_chars", 1),
					(eq, ":value", "trp_online_character"),

					(player_get_unique_id, reg1, ":player_no"),
					(str_store_string, s1, "str_server_security_token"),
					(str_store_server_name, s2),
					(str_store_player_username, s3, ":player_no"),
					(assign, reg2, "str_service_action_2"),
					(assign, reg3, 1),
					(str_store_string, s10, "str_service_action_2"),
					(str_store_string, s10, "str_service_url_1"),
					# (str_encode_url, s10),
					(send_message_to_url, s10),
					(str_store_string, s0, "@[DEBUG]: Send http request: {s10}"),
					(server_add_message_to_log, s0),
					(display_message, s0),
					#(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
     		(try_end),
#MOD end

      (else_try),
        (eq, ":event_type", multiplayer_event_admin_set_server_name),
				#validity check
				(player_is_admin, ":player_no"),
        (server_get_renaming_server_allowed, "$g_multiplayer_renaming_server_allowed"),
        (eq, "$g_multiplayer_renaming_server_allowed", 1),
        #condition checks are done
        (server_set_name, s0), #validity is checked inside this function
		
				(get_max_players, ":num_players"),                               
        (try_for_range, ":cur_player", 1, ":num_players"),
          (player_is_active, ":cur_player"),
					(neq, ":cur_player", ":player_no"),
          (multiplayer_send_string_to_player, ":cur_player", multiplayer_event_return_server_name, s0),
        (try_end),
#MOD begin
        (try_begin),
          (player_is_active, ":player_no"),
          (str_store_player_username, s1, ":player_no"),
          (str_store_string, s0, "@Server name changed to {s0} by {s1}."),
          (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
        (try_end),
#MOD end		
      (else_try),
        (eq, ":event_type", multiplayer_event_admin_set_game_password),
				#validity check
				(player_is_admin, ":player_no"),
        #condition checks are done
        (server_set_password, s0), #validity is checked inside this function
		
				(get_max_players, ":num_players"),                               
        (try_for_range, ":cur_player", 1, ":num_players"),
          (player_is_active, ":cur_player"),
					(player_is_admin, ":cur_player"),
					(neq, ":cur_player", ":player_no"),
          (multiplayer_send_string_to_player, ":cur_player", multiplayer_event_return_game_password, s0),
        (try_end),
#MOD begin
        (try_begin),
          (player_is_active, ":player_no"),
          (str_store_player_username, s1, ":player_no"),
          (try_begin),
            (neg|str_is_empty, s0),
            (str_store_string, s0, "@Password changed by {s1}."),
          (else_try),
            (str_store_string, s0, "@Password disabled by {s1}."),
          (try_end),
          (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
        (try_end),
#MOD end
      (else_try),
        (eq, ":event_type", multiplayer_event_admin_set_welcome_message),
				#validity check
				(player_is_admin, ":player_no"),
        #condition checks are done
        (server_set_welcome_message, s0), #validity is checked inside this function
#MOD begin
        (try_begin),
          (player_is_active, ":player_no"),
          (str_store_player_username, s1, ":player_no"),
          (str_store_string, s0, "@Welcome message updated by {s1}."),
          (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
        (try_end),
#MOD end		
		
      (else_try),
        (eq, ":event_type", multiplayer_event_start_map),
				(store_script_param, ":value", 3),
        (store_script_param, ":value_2", 4),
        (is_between, ":value_2", 0, multiplayer_num_game_types),
				(server_get_changing_game_type_allowed, "$g_multiplayer_changing_game_type_allowed"),
				(this_or_next|eq, "$g_multiplayer_changing_game_type_allowed", 1),
				(eq, "$g_multiplayer_game_type", ":value_2"),
				(call_script, "script_multiplayer_fill_map_game_types", ":value_2"),
				(assign, ":num_maps", reg0),
				(assign, ":is_valid", 0),
				(store_add, ":end_cond", multi_data_maps_for_game_type_begin, ":num_maps"),
	  
				(try_for_range, ":i_map", multi_data_maps_for_game_type_begin, ":end_cond"),
          (troop_slot_eq, "trp_multiplayer_data", ":i_map", ":value"),
          (assign, ":is_valid", 1),
          (assign, ":end_cond", 0),
				(try_end),
				(eq, ":is_valid", 1),
				#condition checks are done
				(assign, "$g_multiplayer_game_type", ":value_2"),
				(assign, "$g_multiplayer_selected_map", ":value"),
				(team_set_faction, 0, "$g_multiplayer_next_team_1_faction"),
				(team_set_faction, 1, "$g_multiplayer_next_team_2_faction"),

				(get_max_players, ":num_players"),                               
				(try_for_range, ":cur_player", 1, ":num_players"),
          (player_is_active, ":cur_player"),
					(neq, ":cur_player", ":player_no"),
          (multiplayer_send_4_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_next_team_faction, 1, "$g_multiplayer_next_team_1_faction"),
          (multiplayer_send_4_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_next_team_faction, 2, "$g_multiplayer_next_team_2_faction"),
				(try_end),

				(call_script, "script_game_multiplayer_get_game_type_mission_template", "$g_multiplayer_game_type"),
				(start_multiplayer_mission, reg0, "$g_multiplayer_selected_map", 1),
		
#MOD begin
				(try_begin),
					(player_is_active, ":player_no"),
					(str_store_player_username, s1, ":player_no"),
					(store_add, ":gametype", "$g_multiplayer_game_type", "str_multi_game_type_1"),
					(str_store_string, s5, ":gametype"),
					(str_store_faction_name, s3, "$g_multiplayer_next_team_1_faction"),
					(str_store_faction_name, s4, "$g_multiplayer_next_team_2_faction"),
					(call_script, "script_game_get_scene_name", "$g_multiplayer_selected_map"), #goes to s0
					(str_store_string, s0, "@Restarting. ^Gametype: {s5} ^Map: {s0} ^Factions: {s3} vs. {s4}"),
					(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
				(try_end),
#MOD end
		  
      (else_try),	  
				(eq, ":event_type", multiplayer_event_set_admin_settings),
				(store_script_param, ":sub_type", 3),
        (store_script_param, ":value", 4),
        (store_script_param, ":value_2", 5),
				#validity check
				(player_is_admin, ":player_no"),
				(call_script, "script_multiplayer_set_server_settings", ":player_no", ":sub_type", ":value", ":value_2"),
				
				# (assign, reg0, ":player_no"),
				# (assign, reg1, ":sub_type"),
				# (assign, reg2, ":value"),
				# (assign, reg3, ":value_2"),
				# (display_message, "@(server) player:{reg0} type:{reg1} value1:{reg2} value2:{reg3}"),
				
      (else_try),
        (eq, ":event_type", multiplayer_event_start_new_poll),
        (try_begin),
          (store_script_param, ":value", 3),
          (store_script_param, ":value_2", 4),
           #validity check
          (eq, "$g_multiplayer_poll_running", 0),
          (store_mission_timer_a, ":mission_timer"),
          (player_get_slot, ":poll_disable_time", ":player_no", slot_player_poll_disabled_until_time),
          (ge, ":mission_timer", ":poll_disable_time"),
          (assign, ":continue", 0),
          (try_begin),
            (eq, ":value", 1), # kicking a player
            (try_begin),
              (eq, "$g_multiplayer_kick_voteable", 1),
              (player_is_active, ":value_2"),
              (assign, ":continue", 1),
            (try_end),
          (else_try),
            (eq, ":value", 2), # banning a player
            (try_begin),
              (eq, "$g_multiplayer_ban_voteable", 1),
              (player_is_active, ":value_2"),
              (save_ban_info_of_player, ":value_2"),
              (assign, ":continue", 1),
            (try_end),
          (else_try), # vote for map
            (eq, ":value", 0),
            (try_begin),
              (eq, "$g_multiplayer_maps_voteable", 1),
              (call_script, "script_multiplayer_fill_map_game_types", "$g_multiplayer_game_type"),
              (assign, ":num_maps", reg0),
              (try_for_range, ":i_map", 0, ":num_maps"),
                (store_add, ":map_slot", ":i_map", multi_data_maps_for_game_type_begin),
                (troop_slot_eq, "trp_multiplayer_data", ":map_slot", ":value_2"),
                (assign, ":continue", 1),
                (assign, ":num_maps", 0), #break
              (try_end),
            (try_end),
          (else_try),
            (eq, ":value", 3), #vote for map and factions
            (try_begin),
              (eq, "$g_multiplayer_factions_voteable", 1),
              (store_script_param, ":value_3", 5),
              (store_script_param, ":value_4", 6),
              (call_script, "script_multiplayer_fill_map_game_types", "$g_multiplayer_game_type"),
              (assign, ":num_maps", reg0),
              (try_for_range, ":i_map", 0, ":num_maps"),
                (store_add, ":map_slot", ":i_map", multi_data_maps_for_game_type_begin),
                (troop_slot_eq, "trp_multiplayer_data", ":map_slot", ":value_2"),
                (assign, ":continue", 1),
                (assign, ":num_maps", 0), #break
              (try_end),
              (try_begin),
                (eq, ":continue", 1),
                (this_or_next|neg|is_between, ":value_3", npc_kingdoms_begin, npc_kingdoms_end),
                (this_or_next|neg|is_between, ":value_4", npc_kingdoms_begin, npc_kingdoms_end),
                (eq, ":value_3", ":value_4"),
                (assign, ":continue", 0),
              (try_end),
            (try_end),
          (else_try),
            (eq, ":value", 4), #vote for number of bots
            (store_script_param, ":value_3", 5),
            (store_add, ":upper_limit", "$g_multiplayer_num_bots_voteable", 1),
            (is_between, ":value_2", 0, ":upper_limit"),
            (is_between, ":value_3", 0, ":upper_limit"),
            (assign, ":continue", 1),
          (try_end),
          (eq, ":continue", 1),
          #condition checks are done
          (str_store_player_username, s0, ":player_no"),
          (try_begin),
            (eq, ":value", 1), #kicking a player
            (str_store_player_username, s1, ":value_2"),
            (server_add_message_to_log, "str_poll_kick_player_s1_by_s0"),
#MOD begin
            (str_store_string, s0, "str_poll_kick_player_s1_by_s0"),
            (call_script, "script_multiplayer_broadcast_message", 0, color_server_message),
#MOD end
          (else_try),
            (eq, ":value", 2), #banning a player
            (str_store_player_username, s1, ":value_2"),
            (server_add_message_to_log, "str_poll_ban_player_s1_by_s0"),
#MOD begin
            (str_store_string, s0, "str_poll_ban_player_s1_by_s0"),
            (call_script, "script_multiplayer_broadcast_message", 0, color_server_message),
#MOD end
          (else_try),
            (eq, ":value", 0), #vote for map
            (store_add, ":string_index", ":value_2", "str_scene_0"),#MOD
            (str_store_string, s1, ":string_index"),
            (server_add_message_to_log, "str_poll_change_map_to_s1_by_s0"),
#MOD begin
            (str_store_string, s0, "str_poll_change_map_to_s1_by_s0"),
            (call_script, "script_multiplayer_broadcast_message", 0, color_server_message),
#MOD end
          (else_try),
            (eq, ":value", 3), #vote for map and factions
            (store_add, ":string_index", ":value_2", "str_scene_0"),#MOD
            (str_store_string, s1, ":string_index"),
            (str_store_faction_name, s2, ":value_3"),
            (str_store_faction_name, s3, ":value_4"),
            (server_add_message_to_log, "str_poll_change_map_to_s1_and_factions_to_s2_and_s3_by_s0"),
#MOD begin
            (str_store_string, s0, "str_poll_change_map_to_s1_and_factions_to_s2_and_s3_by_s0"),
            (call_script, "script_multiplayer_broadcast_message", 0, color_server_message),
#MOD end
          (else_try),
            (eq, ":value", 4), #vote for number of bots
            (assign, reg0, ":value_2"),
            (assign, reg1, ":value_3"),
            (server_add_message_to_log, "str_poll_change_number_of_bots_to_reg0_and_reg1_by_s0"),
#MOD begin
            (str_store_string, s0, "str_poll_change_number_of_bots_to_reg0_and_reg1_by_s0"),
            (call_script, "script_multiplayer_broadcast_message", 0, color_server_message),
#MOD end
          (try_end),
          (assign, "$g_multiplayer_poll_running", 1),
          (assign, "$g_multiplayer_poll_ended", 0),
          (assign, "$g_multiplayer_poll_num_sent", 0),
          (assign, "$g_multiplayer_poll_yes_count", 0),
          (assign, "$g_multiplayer_poll_no_count", 0),
          (assign, "$g_multiplayer_poll_to_show", ":value"),
          (assign, "$g_multiplayer_poll_value_to_show", ":value_2"),
          (try_begin),
            (eq, ":value", 3),
            (assign, "$g_multiplayer_poll_value_2_to_show", ":value_3"),
            (assign, "$g_multiplayer_poll_value_3_to_show", ":value_4"),
          (else_try),
            (eq, ":value", 4),
            (assign, "$g_multiplayer_poll_value_2_to_show", ":value_3"),
            (assign, "$g_multiplayer_poll_value_3_to_show", -1),
          (else_try),
            (assign, "$g_multiplayer_poll_value_2_to_show", -1),
            (assign, "$g_multiplayer_poll_value_3_to_show", -1),
          (try_end),
          (store_add, ":poll_disable_until", ":mission_timer", multiplayer_poll_disable_period),
          (player_set_slot, ":player_no", slot_player_poll_disabled_until_time, ":poll_disable_until"),
          (store_add, "$g_multiplayer_poll_end_time", ":mission_timer", 60),
          (get_max_players, ":num_players"),
          (try_for_range, ":cur_player", 0, ":num_players"),
            (player_is_active, ":cur_player"),
            (player_set_slot, ":cur_player", slot_player_can_answer_poll, 1),
            (val_add, "$g_multiplayer_poll_num_sent", 1),
            (multiplayer_send_4_int_to_player, ":cur_player", multiplayer_event_ask_for_poll, "$g_multiplayer_poll_to_show", "$g_multiplayer_poll_value_to_show", "$g_multiplayer_poll_value_2_to_show", "$g_multiplayer_poll_value_3_to_show"),
          (try_end),
        (try_end),
      (else_try),
        (eq, ":event_type", multiplayer_event_answer_to_poll),
        (try_begin),
          (store_script_param, ":value", 3),
          #validity check
          (eq, "$g_multiplayer_poll_running", 1),
          (is_between, ":value", 0, 2),
          (player_slot_eq, ":player_no", slot_player_can_answer_poll, 1),
          #condition checks are done
          (player_set_slot, ":player_no", slot_player_can_answer_poll, 0),
          (try_begin),
            (eq, ":value", 0),
            (val_add, "$g_multiplayer_poll_no_count", 1),
          (else_try),
            (eq, ":value", 1),
            (val_add, "$g_multiplayer_poll_yes_count", 1),
          (try_end),
        (try_end),
      # (else_try),
        # (eq, ":event_type", multiplayer_event_admin_kick_player),
        # (try_begin),
          # (store_script_param, ":value", 3),
          # #validity check
          # (player_is_admin, ":player_no"),
          # (player_is_active, ":value"),
          # #condition checks are done
          # (kick_player, ":value"),
# #MOD begin
          # (try_begin),
            # (player_is_active, ":player_no"),
            # (player_is_active, ":value"),
            # (str_store_player_username, s1, ":player_no"),
            # (str_store_player_username, s2, ":value"),
            # (str_store_string, s0, "@{s1} kicked {s2}."),
            # (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
          # (try_end),
# #MOD end
        # (try_end),
      # (else_try),
        # (eq, ":event_type", multiplayer_event_admin_ban_player),
        # (try_begin),
          # (store_script_param, ":value", 3),
          # #validity check
          # (player_is_admin, ":player_no"),
          # (player_is_active, ":value"),
          # #condition checks are done
          # (ban_player, ":value", 0, ":player_no"),
# #MOD begin
          # (try_begin),
            # (player_is_active, ":player_no"),
            # (player_is_active, ":value"),
            # (str_store_player_username, s1, ":player_no"),
            # (str_store_player_username, s2, ":value"),
            # (str_store_string, s0, "@{s1} permanently banned {s2}."),
            # (call_script, "script_multiplayer_broadcast_message", 0, color_server_message),
          # (try_end),
# #MOD end
        # (try_end),

      (else_try),
        (eq, ":event_type", multiplayer_event_offer_duel),
        (try_begin),
          (store_script_param, ":value", 3),
          #validity check
          (eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
          (agent_is_active, ":value"),
          (agent_is_alive, ":value"),
          (agent_is_human, ":value"),
          (player_get_agent_id, ":player_agent_no", ":player_no"),
          (agent_is_active, ":player_agent_no"),
          (agent_is_alive, ":player_agent_no"),
          (agent_get_position, pos0, ":player_agent_no"),
          (agent_get_position, pos1, ":value"),
          (get_sq_distance_between_positions_in_meters, ":agent_dist_sq", pos0, pos1),
          (le, ":agent_dist_sq", 49),
          #allow duelists to receive new offers
          (this_or_next|agent_check_offer_from_agent, ":player_agent_no", ":value"),
          (agent_slot_eq, ":player_agent_no", slot_agent_in_duel_with, -1),
          (neg|agent_slot_eq, ":player_agent_no", slot_agent_in_duel_with, ":value"), #don't allow spamming duel offers during countdown
          #condition checks are done
          (try_begin),
            #accepting a duel
            (agent_check_offer_from_agent, ":player_agent_no", ":value"),
            (call_script, "script_multiplayer_accept_duel", ":player_agent_no", ":value"),
          (else_try),
            #sending a duel request
            (assign, ":display_notification", 1),
            (try_begin),
              (agent_check_offer_from_agent, ":value", ":player_agent_no"),
              (assign, ":display_notification", 0),
            (try_end),
            (agent_add_offer_with_timeout, ":value", ":player_agent_no", 10000), #10 second timeout
            (agent_get_player_id, ":value_player", ":value"),
            (try_begin),
              (player_is_active, ":value_player"), #might be AI
              (try_begin),
                (eq, ":display_notification", 1),
                (multiplayer_send_int_to_player, ":value_player", multiplayer_event_show_duel_request, ":player_agent_no"),
              (try_end),
            (else_try),
              (call_script, "script_multiplayer_accept_duel", ":value", ":player_agent_no"),
            (try_end),
          (try_end),
        (try_end),
#      (else_try),#MOD disable
#        (eq, ":sub_type", sub_event_disallow_ranged_weapons),
#        (try_begin),
#          (store_script_param, ":value", 3),
#          #validity check
#          (player_is_admin, ":player_no"),
#          (is_between, ":value", 0, 2),
#          #condition checks are done
#          (assign, "$g_multiplayer_disallow_ranged_weapons", ":value"),
#        (try_end),

#MOD begin
      (else_try),
        (eq, ":event_type", multiplayer_event_admin_manage_player),
        (store_script_param, ":cur_player", 3),
				(store_script_param, ":action", 4),
				
				(try_begin),
					(player_is_admin, ":player_no"),
					(player_is_active, ":cur_player"),
					
					(assign, ":send_message", 1),
					(str_store_player_username, s1, ":player_no"),
					(str_store_player_username, s2, ":cur_player"),
					
					(try_begin),
						(eq, ":action", "str_kick"),
						(kick_player, ":cur_player"),
						(str_store_string, s0, "@{s2} has been kicked by {s1}."),
						
					(else_try),
						(eq, ":action", "str_ban"),
						(ban_player, ":cur_player", 0, ":player_no"),
						(save_ban_info_of_player, ":player_no"),
						(str_store_string, s0, "@{s2} has been banned permanently by {s1}."),
						
					(else_try),
						(eq, ":action", "str_temp_ban"),
						(ban_player, ":cur_player", 1, ":player_no"),
						(str_store_string, s0, "@{s2} has been banned temporarily by {s1}."),
						
					(else_try),
						(is_between, ":action", "str_swap_team", "str_slay"),
						(player_get_agent_id, ":cur_player_agent_no", ":cur_player"),
						(try_begin),
							(agent_is_active, ":cur_player_agent_no"),
							(agent_is_alive, ":cur_player_agent_no"),
							(player_get_death_count, ":deaths", ":cur_player"),
							(val_sub, ":deaths", 1),
							(player_set_death_count, ":cur_player", ":deaths"),
							(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
								(player_is_active, ":cur_player_no"),
								(multiplayer_send_2_int_to_player, ":cur_player_no", multiplayer_event_client_set_player_death_count, ":cur_player", ":deaths"),
							(try_end),
						(try_end),
						(player_set_slot, ":cur_player", slot_player_use_troop, -1),
						(try_begin),
							(eq, ":action", "str_swap_team"),
							(player_get_team_no, ":cur_player_team", ":cur_player"),
							(try_begin),
								(eq, ":cur_player_team", 0),
								(player_set_team_no, ":cur_player", 1),
							(else_try),
								(player_set_team_no, ":cur_player", 0),
							(try_end),
							(str_store_string, s0, "@{s2} has been moved to the other team by {s1}."),
						(else_try),
							(player_set_team_no, ":cur_player", multi_team_spectator),
							(str_store_string, s0, "@{s2} has been moved to spectators by {s1}."),
						(try_end),
					
					(else_try),
						(gt, ":action", "str_swap_spec"),
						(player_get_agent_id, ":cur_player_agent_no", ":cur_player"),
						(agent_is_active, ":cur_player_agent_no"),
						(agent_is_alive, ":cur_player_agent_no"),
						(try_begin),
							(eq, ":action", "str_slay"),
							(player_get_death_count, ":deaths", ":cur_player"),
							(val_sub, ":deaths", 1),
							(player_set_death_count, ":cur_player", ":deaths"),
							(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
								(player_is_active, ":cur_player_no"),
								(multiplayer_send_2_int_to_player, ":cur_player_no", multiplayer_event_client_set_player_death_count, ":cur_player", ":deaths"),
							(try_end),
							(agent_set_hit_points, ":cur_player_agent_no", 0, 1),
							(agent_deliver_damage_to_agent, ":cur_player_agent_no", ":cur_player_agent_no", 0),
							(str_store_string, s0, "@{s2} has been slain by {s1}."),
						(else_try),
							(eq, ":action", "str_restore"),
							(call_script, "script_restore_agent", ":cur_player_agent_no"),
							(str_store_string, s0, "@{s2} has been restored by {s1}."),
						(else_try),
							(is_between, ":action", "str_tp_to_me", "str_tp_me"),
							(player_get_agent_id, ":player_agent_no", ":player_no"),
							(agent_is_active, ":player_agent_no"),
							(agent_is_alive, ":player_agent_no"),
							(try_begin),
								(eq, ":action", "str_tp_to_me"),
								(assign, ":target_agent", ":player_agent_no"),
								(assign, ":tp_agent", ":cur_player_agent_no"),
								(str_store_string, s0, "@{s2} has been teleported to {s1}."),
							(else_try),
								(assign, ":target_agent", ":cur_player_agent_no"),
								(assign, ":tp_agent", ":player_agent_no"),
								(assign, ":send_message", 0),
							(try_end),
							
							(agent_get_position, pos20, ":target_agent"),
							(agent_get_horse, ":player_horse", ":tp_agent"),
							(try_begin),
								(le, ":player_horse", 0),
								(agent_set_position, ":tp_agent", pos20),
							(else_try),
								(agent_set_position, ":player_horse", pos20),
							(try_end),
						(try_end),
					(try_end),
					(eq, ":send_message", 1),
					(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
				(try_end),

      (else_try),
        (eq, ":event_type", multiplayer_event_admin_cheats),
				(store_script_param, ":action", 3),
				(store_script_param, ":cur_player", 4),
				(try_begin),
					(player_is_admin, ":player_no"),
					(player_get_agent_id, ":player_agent_no", ":player_no"),
					(agent_is_active, ":player_agent_no"),
					(agent_is_alive, ":player_agent_no"),
					(agent_get_horse, ":player_horse", ":player_agent_no"),
					(try_begin),
						(le, ":player_horse", 0),
						(assign, ":tp_agent", ":player_agent_no"),
					(else_try),
						(assign, ":tp_agent", ":player_horse"),
					(try_end),
					(try_begin),
						(eq, ":action", "str_tp_me"),
						(agent_get_position, pos20, ":player_agent_no"),
						(position_move_y, pos20, 100),
						(agent_set_position, ":tp_agent", pos20),
					(try_end),
				(try_end),
				
      (else_try),
        (eq, ":event_type", multiplayer_event_agent_walk),
        (store_script_param, ":value", 3),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_agent_walk", ":player_agent_no", ":value"),
		
      (else_try),
        (eq, ":event_type", multiplayer_event_agent_sprint_limiter),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_agent_sprint_limiter", ":player_agent_no"),

      (else_try),
        (eq, ":event_type", multiplayer_event_agent_sprint_stop),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_agent_sprint_stop", ":player_agent_no"),
		
      (else_try),
        (eq, ":event_type", multiplayer_event_switch_weapon_mode),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_switch_weapon_mode", ":player_agent_no"),
		
      (else_try),
        (eq, ":event_type", multiplayer_event_shield_bash),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_shield_bash", ":player_agent_no"),
		
      (else_try),
        (eq, ":event_type", multiplayer_event_use_ammo_box),
        (store_script_param, ":value", 3),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_multiplayer_server_ammo_box", ":player_agent_no", ":value"),
		
      (else_try),
        (eq, ":event_type", multiplayer_event_agent_spawn_item),
        (store_script_param, ":value", 3),
        (player_is_admin, ":player_no"),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_agent_spawn_item", ":player_agent_no", ":value", 0),
		
      (else_try),
        (eq, ":event_type", multiplayer_event_agent_spawn_scene_prop),
        (store_script_param, ":value", 3),
        (player_is_admin, ":player_no"),
        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (call_script, "script_agent_spawn_scene_prop", ":player_agent_no", ":value"),

      (else_try),
        (eq, ":event_type", multiplayer_event_server_control_scene_prop),
        (store_script_param, ":value", 3),

        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
				
				(agent_get_slot, ":instance_id", ":player_agent_no", slot_agent_use_scene_prop),
				(prop_instance_is_valid, ":instance_id"),

        (call_script, "script_control_scene_prop", ":instance_id", ":value"),
		
				(get_max_players, ":num_players"),
				(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
          (player_is_active, ":cur_player_no"),
					(neq, ":cur_player_no", ":player_no"),#dont sent to player who is using it
					(multiplayer_send_2_int_to_player, ":cur_player_no", multiplayer_event_client_control_scene_prop, ":instance_id", ":value"),	
				(try_end),

			(else_try),
        (eq, ":event_type", multiplayer_event_server_set_player_slot),
        (store_script_param, ":slot_no", 3),
        (store_script_param, ":value", 4),

        (player_set_slot, ":player_no", ":slot_no", ":value"),

      (else_try),
        (eq, ":event_type", multiplayer_event_server_set_agent_slot),
        (store_script_param, ":slot_no", 3),
        (store_script_param, ":value", 4),

        (player_get_agent_id, ":player_agent_no", ":player_no"),
        (agent_is_active, ":player_agent_no"),
        (agent_is_alive, ":player_agent_no"),
        (agent_set_slot, ":player_agent_no", ":slot_no", ":value"),
				
      (else_try),
        (eq, ":event_type", multiplayer_event_server_set_advanced_ai_order),
        (store_script_param, ":group_no", 3),
				(store_script_param, ":order_type", 4),

        # (player_get_agent_id, ":player_agent_no", ":player_no"),
        # (agent_is_active, ":player_agent_no"),
        # (agent_is_alive, ":player_agent_no"),
				# (agent_get_team, ":player_team", ":player_agent_no"),
				(call_script, "script_ai_set_advanced_order", ":player_no", ":group_no", ":order_type"),

      (else_try),
        (eq, ":event_type", multiplayer_event_clan_chat),
        (str_store_player_username, s1, ":player_no"),
				(str_store_string, s0, "@*CLAN* [{s1}] {s0}"),
				# (player_get_slot, ":player_clan", ":player_no", slot_player_online_clan),
        (player_get_team_no, ":player_team", ":player_no"),
        (player_get_banner_id, ":player_clan", ":player_no"),
      
				(get_max_players, ":num_players"),
        (try_for_range, ":cur_player", 0, ":num_players"),
          (player_is_active, ":cur_player"),
					# (player_slot_eq, ":cur_player", slot_player_online_clan, ":player_clan"),#same clan
          (player_get_team_no, ":cur_player_team", ":cur_player"),
          (eq, ":cur_player_team", ":player_team"),
          (player_get_banner_id, ":cur_player_clan", ":cur_player"),
          (eq, ":cur_player_clan", ":player_clan"),
					(call_script, "script_multiplayer_send_message_to_player", ":cur_player", color_player_info),
				(try_end),
				
      (else_try),
        (eq, ":event_type", multiplayer_event_admin_chat),
        (str_store_player_username, s1, ":player_no"),
				
				(try_begin),
					(player_is_admin, ":player_no"),
					(str_store_string, s0, "@AC *ADMIN* [{s1}] {s0}"),
				(else_try),	
					(str_store_string, s0, "@AC [{s1}] {s0}"),
				(try_end),

				(get_max_players, ":num_players"),
        (try_for_range, ":cur_player", 0, ":num_players"),
          (player_is_active, ":cur_player"),
#          (player_get_team_no, ":team_no", ":cur_player"), #dedicated server has -1 for team_no
#          (neq, ":team_no", -1),
          (player_is_admin, ":cur_player"),
          #(str_store_string, s0, "str_admin_chat_s0"),
          (call_script, "script_multiplayer_send_message_to_player", ":cur_player", color_admin_chat),
        (try_end),

      (else_try),
        (eq, ":event_type", multiplayer_event_admin_message),
        (player_is_admin, ":player_no"),
        (str_store_player_username, s1, ":player_no"),
        (str_store_string, s0, "@*ADMIN* [{s1}] {s0}"),
        (call_script, "script_multiplayer_broadcast_message", 1, color_admin_message),
				
      (else_try),
        (eq, ":event_type", multiplayer_event_reset_account_password),
				(player_get_unique_id, reg1, ":player_no"),
				(str_store_string, s1, "str_server_security_token"),
				(str_store_server_name, s2),
				(assign, reg2, "str_service_action_5"),
				(str_store_string, s10, "str_service_action_5"),
				(str_store_string, s10, "str_service_url_1"),
				# (str_encode_url, s10),
				(send_message_to_url, s10),
				(str_store_string, s0, "@[DEBUG]: Send http request: {s10}"),
				(server_add_message_to_log, s0),
				(display_message, s0),
				
			(else_try),
        (eq, ":event_type", multiplayer_event_version_check),
				(store_script_param, ":client_version", 3),
				(neq, ":client_version", module_version),#client runs on a different version
				(store_div, reg1, ":client_version", 1000),
				(store_mod, reg2, ":client_version", 1000),
				(str_store_string, s0, "@{reg1}.{reg2}"),
				(store_div, reg1, module_version, 1000),
				(store_mod, reg2, module_version, 1000),
				(str_store_string, s1, "@{reg1}.{reg2}"),
				(str_store_string, s0, "@ERROR: Server module version({s1}) is different from yours({s0}), please update your mod."),
				(call_script, "script_multiplayer_send_message_to_player", ":player_no", color_player_warning),
				(kick_player, ":player_no"),
#MOD end

      (else_try),
        ###############
        #CLIENT EVENTS#
        ###############
        (neg|multiplayer_is_dedicated_server),

        (try_begin),
          (eq, ":event_type", multiplayer_event_return_admin_settings),
					(store_script_param, ":player", 3),
					(store_script_param, ":sub_type", 4),
          (store_script_param, ":value", 5),
          (store_script_param, ":value_2", 6),
					(call_script, "script_multiplayer_set_server_settings", ":player", ":sub_type", ":value", ":value_2"),
					
					# (assign, reg0, ":player_no"),
					# (assign, reg1, ":sub_type"),
					# (assign, reg2, ":value"),
					# (assign, reg3, ":value_2"),
					# (display_message, "@(client) player:{reg0} type:{reg1} value1:{reg2} value2:{reg3}"),
        (else_try),
          (eq, ":event_type", multiplayer_event_return_server_name),
          (server_set_name, s0),
        (else_try),
          (eq, ":event_type", multiplayer_event_return_game_password),
          (server_set_password, s0),
          #this is the last option in admin panel, so start the presentation
          #(start_presentation, "prsnt_game_multiplayer_admin_panel"),#MOD disable
#        (else_try),
#          (eq, ":event_type", multiplayer_event_return_open_game_rules),
          #this is the last message for game rules, so start the presentation
#          (assign, "$g_multiplayer_show_server_rules", 1),
#          (start_presentation, "prsnt_multiplayer_welcome_message"),			  
        (else_try),
          (eq, ":event_type", multiplayer_event_return_confirmation),
          (assign, "$g_confirmation_result", 1),
        (else_try),
          (eq, ":event_type", multiplayer_event_return_rejection),
          (assign, "$g_confirmation_result", -1),
        (else_try),
					(eq, ":event_type", multiplayer_event_return_server_mission_timer_while_player_joined),
					(store_script_param, ":value", 3),
          (assign, "$server_mission_timer_while_player_joined", ":value"),
#MOD begin
					(multiplayer_send_int_to_server, multiplayer_event_version_check, module_version),
#MOD end
        (else_try),
          (eq, ":event_type", multiplayer_event_show_multiplayer_message),
          (store_script_param, ":value", 3),
          (store_script_param, ":value_2", 4),
          (call_script, "script_show_multiplayer_message", ":value", ":value_2"),
        (else_try),
          (eq, ":event_type", multiplayer_event_draw_this_round),
          (store_script_param, ":value", 3),          
          (call_script, "script_draw_this_round", ":value"),
#MOD disable
        # (else_try),
          # (eq, ":event_type", multiplayer_event_set_attached_scene_prop),
          # (store_script_param, ":value", 3),
          # (store_script_param, ":value_2", 4),
          # (call_script, "script_set_attached_scene_prop", ":value", ":value_2"),   
        # (else_try),
          # (eq, ":event_type", multiplayer_event_set_team_flag_situation),
          # (store_script_param, ":value", 3),
          # (store_script_param, ":value_2", 4),
          # (call_script, "script_set_team_flag_situation", ":value", ":value_2"),
        (else_try),
          (eq, ":event_type", multiplayer_event_set_team_score),
          (store_script_param, ":value", 3),
          (store_script_param, ":value_2", 4),
          (call_script, "script_team_set_score", ":value", ":value_2"),

#MOD disable
        # (else_try),
          # (eq, ":event_type", multiplayer_event_set_player_score_kill_death), 
          # (store_script_param, ":value", 3),
          # (store_script_param, ":value_2", 4),
          # (store_script_param, ":value_3", 5),
          # (store_script_param, ":value_4", 6),
          # (call_script, "script_player_set_score", ":value", ":value_2"),
          # (call_script, "script_player_set_kill_count", ":value", ":value_3"),
          # (call_script, "script_player_set_death_count", ":value", ":value_4"),
				(else_try),
          (eq, ":event_type", multiplayer_event_set_num_agents_around_flag),
          (store_script_param, ":flag_no", 3),
          (store_script_param, ":current_owner_code", 4),
          (call_script, "script_set_num_agents_around_flag", ":flag_no", ":current_owner_code"),
        (else_try),
          (eq, ":event_type", multiplayer_event_ask_for_poll),
          (store_script_param, ":value", 3),
          (store_script_param, ":value_2", 4),
          (store_script_param, ":value_3", 5),
          (store_script_param, ":value_4", 6),
          (assign, ":continue_to_poll", 0),
          (try_begin),
            (this_or_next|eq, ":value", 1),
            (eq, ":value", 2),
            (player_is_active, ":value_2"), #might go offline before here
            (assign, ":continue_to_poll", 1),
          (else_try),
            (assign, ":continue_to_poll", 1),
          (try_end),
          (try_begin),
            (eq, ":continue_to_poll", 1),
            (assign, "$g_multiplayer_poll_to_show", ":value"),
            (assign, "$g_multiplayer_poll_value_to_show", ":value_2"),
            (assign, "$g_multiplayer_poll_value_2_to_show", ":value_3"),
            (assign, "$g_multiplayer_poll_value_3_to_show", ":value_4"),
            (store_mission_timer_a, ":mission_timer"),
            (store_add, "$g_multiplayer_poll_client_end_time", ":mission_timer", 60),
            (start_presentation, "prsnt_multiplayer_poll"),
          (try_end),
#MOD disable
        # (else_try),
          # (eq, ":event_type", multiplayer_event_change_flag_owner),
          # (store_script_param, ":flag_no", 3),
          # (store_script_param, ":owner_code", 4),
          # (call_script, "script_change_flag_owner", ":flag_no", ":owner_code"),
        (else_try),
          (eq, ":event_type", multiplayer_event_use_item),
          (store_script_param, ":value", 3),
          (store_script_param, ":value_2", 4),
          (call_script, "script_use_item", ":value", ":value_2"),
        (else_try),
          (eq, ":event_type", multiplayer_event_set_scene_prop_open_or_close),
          (store_script_param, ":instance_id", 3),       
        
          (scene_prop_set_slot, ":instance_id", scene_prop_open_or_close_slot, 1),

          (prop_instance_get_scene_prop_kind, ":scene_prop_id", ":instance_id"),

          (try_begin),
            (eq, ":scene_prop_id", "spr_winch_b"),
            (assign, ":effected_object", "spr_portcullis"),
          (else_try),
            (this_or_next|eq, ":scene_prop_id", "spr_castle_e_sally_door_a"),
            (this_or_next|eq, ":scene_prop_id", "spr_castle_f_sally_door_a"),     
            (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_left"),     
            (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_right"),     
            (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),                             
            (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_right"),                             
            (this_or_next|eq, ":scene_prop_id", "spr_castle_f_door_a"),
            (this_or_next|eq, ":scene_prop_id", "spr_door_destructible"),
            (this_or_next|eq, ":scene_prop_id", "spr_castle_f_door_b"),
            (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_6m"),
            (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_8m"),
            (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_10m"),
            (this_or_next|eq, ":scene_prop_id", "spr_siege_ladder_move_12m"),
            (eq, ":scene_prop_id", "spr_siege_ladder_move_14m"),
            (assign, ":effected_object", ":scene_prop_id"),
          (try_end),

          (try_begin),
            (eq, ":effected_object", "spr_portcullis"),

            (assign, ":smallest_dist", -1),
            (prop_instance_get_position, pos0, ":instance_id"),
            (scene_prop_get_num_instances, ":num_instances_of_effected_object", ":effected_object"),     
            (try_for_range, ":cur_instance", 0, ":num_instances_of_effected_object"),
              (scene_prop_get_instance, ":cur_instance_id", ":effected_object", ":cur_instance"),
              (prop_instance_get_position, pos1, ":cur_instance_id"),
              (get_sq_distance_between_positions, ":dist", pos0, pos1),
              (this_or_next|eq, ":smallest_dist", -1),
              (lt, ":dist", ":smallest_dist"),
              (assign, ":smallest_dist", ":dist"),
              (assign, ":effected_object_instance_id", ":cur_instance_id"),
            (try_end),

            (ge, ":smallest_dist", 0),
            (prop_instance_is_animating, ":is_animating", ":effected_object_instance_id"),
            (eq, ":is_animating", 0),

            (prop_instance_get_starting_position, pos0, ":effected_object_instance_id"),      
            (position_move_z, pos0, 375),
            (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 1),
          (else_try),
            (this_or_next|eq, ":scene_prop_id", "spr_castle_e_sally_door_a"),
            (this_or_next|eq, ":scene_prop_id", "spr_castle_f_sally_door_a"),     
            (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_left"),     
            (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_right"),     
            (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),     
            (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_right"),     
            (this_or_next|eq, ":scene_prop_id", "spr_castle_f_door_a"),
            (this_or_next|eq, ":scene_prop_id", "spr_door_destructible"),
            (eq, ":scene_prop_id", "spr_castle_f_door_b"),
            (assign, ":effected_object_instance_id", ":instance_id"),  
            (prop_instance_get_starting_position, pos0, ":effected_object_instance_id"),
            (position_rotate_z, pos0, -80),
            (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 1),
          (else_try),
            (assign, ":effected_object_instance_id", ":instance_id"),
            (prop_instance_is_animating, ":is_animating", ":effected_object_instance_id"),
            (eq, ":is_animating", 0),
            (prop_instance_get_starting_position, pos0, ":effected_object_instance_id"),      
            (prop_instance_animate_to_position, ":effected_object_instance_id", pos0, 1),          
          (try_end),
        (else_try),
          (eq, ":event_type", multiplayer_event_set_round_start_time),
          (store_script_param, ":value", 3),

          (try_begin),
            (neq, ":value", -9999),
            (assign, "$g_round_start_time", ":value"),
          (else_try),
            (store_mission_timer_a, "$g_round_start_time"),
						(call_script, "script_init_on_round_start"),#MOD
            #if round start time is assigning to current time (so new round is starting) then also initialize moveable object slots too.
            (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_6m"),
            (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_8m"),
            (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_10m"),
            (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_12m"),
            (call_script, "script_initialize_scene_prop_slots", "spr_siege_ladder_move_14m"),
            (call_script, "script_initialize_scene_prop_slots", "spr_winch_b"),         
          (try_end),
        (else_try),
          (eq, ":event_type", multiplayer_event_force_start_team_selection),
          (try_begin),
            (is_presentation_active, "prsnt_multiplayer_item_select"),
            (assign, "$g_close_equipment_selection", 1),
          (try_end),
          (start_presentation, "prsnt_multiplayer_troop_select"),
        # (else_try),     
          # (eq, ":event_type", multiplayer_event_start_death_mode),
          # (assign, "$g_battle_death_mode_started", 2),
          # (start_presentation, "prsnt_multiplayer_flag_projection_display_bt"),
          # (call_script, "script_start_death_mode"),
        (else_try),
          (eq, ":event_type", multiplayer_event_return_player_respawn_spent),
          (store_script_param, ":value", 3),
          (try_begin),
            (gt, "$g_my_spawn_count", 0),
            (store_add, "$g_my_spawn_count", "$g_my_spawn_count", ":value"),
          (else_try),
            (assign, "$g_my_spawn_count", ":value"),
          (try_end),
        (else_try),
          (eq, ":event_type", multiplayer_event_show_duel_request),
          (store_script_param, ":value", 3),
          (try_begin),
            (agent_is_active, ":value"),
            (agent_get_player_id, ":value_player_no", ":value"),
            (try_begin),
              (player_is_active, ":value_player_no"),
              (str_store_player_username, s0, ":value_player_no"),
            (else_try),
              (str_store_agent_name, s0, ":value"),
            (try_end),
            (display_message, "str_s0_offers_a_duel_with_you"),
            (try_begin),
              (get_player_agent_no, ":player_agent"),
              (agent_is_active, ":player_agent"),
              (agent_add_offer_with_timeout, ":player_agent", ":value", 10000), #10 second timeout
            (try_end),
          (try_end),
        (else_try),
          (eq, ":event_type", multiplayer_event_start_duel),
          (store_script_param, ":value", 3),
          (store_mission_timer_a, ":mission_timer"),
          (try_begin),
            (agent_is_active, ":value"),
            (get_player_agent_no, ":player_agent"),
            (agent_is_active, ":player_agent"),
            (agent_get_player_id, ":value_player_no", ":value"),
            (try_begin),
              (player_is_active, ":value_player_no"),
              (str_store_player_username, s0, ":value_player_no"),
            (else_try),
              (str_store_agent_name, s0, ":value"),
            (try_end),
            (display_message, "str_a_duel_between_you_and_s0_will_start_in_3_seconds"),
            (assign, "$g_multiplayer_duel_start_time", ":mission_timer"),
            (start_presentation, "prsnt_multiplayer_duel_start_counter"),
            (agent_set_slot, ":player_agent", slot_agent_in_duel_with, ":value"),
            (agent_set_slot, ":value", slot_agent_in_duel_with, ":player_agent"),
            (agent_set_slot, ":player_agent", slot_agent_duel_start_time, ":mission_timer"),
            (agent_set_slot, ":value", slot_agent_duel_start_time, ":mission_timer"),
            (agent_clear_relations_with_agents, ":player_agent"),
            (agent_clear_relations_with_agents, ":value"),
##            (agent_add_relation_with_agent, ":player_agent", ":value", -1),
          (try_end),
        (else_try),
          (eq, ":event_type", multiplayer_event_cancel_duel),
          (store_script_param, ":value", 3),
          (try_begin),
            (agent_is_active, ":value"),
            (agent_get_player_id, ":value_player_no", ":value"),
            (try_begin),
              (player_is_active, ":value_player_no"),
              (str_store_player_username, s0, ":value_player_no"),
            (else_try),
              (str_store_agent_name, s0, ":value"),
            (try_end),
            (display_message, "str_your_duel_with_s0_is_cancelled"),
          (try_end),
          (try_begin),
            (get_player_agent_no, ":player_agent"),
            (agent_is_active, ":player_agent"),
            (agent_set_slot, ":player_agent", slot_agent_in_duel_with, -1),
            (agent_clear_relations_with_agents, ":player_agent"),
          (try_end),
#        (else_try),#MOD disable
#          (eq, ":event_type", multiplayer_event_show_server_message),
#          (display_message, "str_server_s0", 0xFFFF6666),

#MOD begin
				(else_try),
          (eq, ":event_type", multiplayer_event_client_set_player_score_count),
					(store_script_param, ":player", 3),
          (store_script_param, ":value", 4),
					#(player_set_score, ":player_no", ":value"),
					(player_set_slot, ":player", slot_player_score, ":value"),
				
				(else_try),
          (eq, ":event_type", multiplayer_event_client_set_player_kill_count),
					(store_script_param, ":player", 3),
          (store_script_param, ":value", 4),
					(player_set_kill_count, ":player", ":value"),
				
				(else_try),
          (eq, ":event_type", multiplayer_event_client_set_player_death_count),
					(store_script_param, ":player", 3),
          (store_script_param, ":value", 4),
					(player_set_death_count, ":player", ":value"),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_prop_slot),
          (store_script_param, ":instance_id", 3),
          (store_script_param, ":slot_no", 4),
          (store_script_param, ":value", 5),
        
          (scene_prop_set_slot, ":instance_id", ":slot_no", ":value"),
          (try_begin),
            (eq, ":slot_no", scene_prop_is_used),
            (eq, ":value", 1),
            (call_script, "script_clear_scene_prop" ,":instance_id"),
          (try_end),

        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_troop_slot),
					(store_script_param, ":troop_id", 3),
          (store_script_param, ":slot_no", 4),
          (store_script_param, ":value", 5),
		  
          (troop_set_slot, ":troop_id", ":slot_no", ":value"),
		  
        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_player_slot),
          (store_script_param, ":slot_no", 3),
          (store_script_param, ":value", 4),
					(store_script_param, ":player", 5),
		  
					(try_begin),
						(lt, ":player", 1),
						(multiplayer_get_my_player, ":player"),
					(try_end),
          (player_is_active, ":player"),
          (player_set_slot, ":player", ":slot_no", ":value"),

        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_agent_slot),
          (store_script_param, ":slot_no", 3),
          (store_script_param, ":value", 4),
 
          (call_script, "script_client_get_player_agent"),
					(assign, ":player_agent", reg0),
          (agent_is_active, ":player_agent"),
          (agent_is_alive, ":player_agent"),
          (agent_set_slot, ":player_agent", ":slot_no", ":value"),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_banner),
					(store_script_param, ":value", 3),
					# (profile_set_banner_id, ":value"),
					# (display_message, "@Please rejoin the server in order to apply your correct banner.", color_player_warning),
 
        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_attribute),
          (store_script_param, ":troop", 3),
          (store_script_param, ":value", 4),
					
					(store_div, ":value_type", ":value", 1000),
					(store_mul, ":decrease", ":value_type", 1000),
					(val_sub, ":value", ":decrease"),
					(call_script, "script_multiplayer_set_troop_value", ":troop", ":event_type", ":value_type", ":value"),
		  
        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_proficiency),
          (store_script_param, ":troop", 3), 
          (store_script_param, ":value", 4),
					
					(store_div, ":value_type", ":value", 1000),
					(store_mul, ":decrease", ":value_type", 1000),
					(val_sub, ":value", ":decrease"),
					(call_script, "script_multiplayer_set_troop_value", ":troop", ":event_type", ":value_type", ":value"),

        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_skill),
          (store_script_param, ":troop", 3),
          (store_script_param, ":value", 4),
					
					(store_div, ":value_type", ":value", 1000),
					(store_mul, ":decrease", ":value_type", 1000),
					(val_sub, ":value", ":decrease"),
					(call_script, "script_multiplayer_set_troop_value", ":troop", ":event_type", ":value_type", ":value"),

        (else_try),
          (eq, ":event_type", multiplayer_event_client_raise_all_proficiencies),
          (store_script_param, ":troop", 3),  
          (store_script_param, ":value", 4),

					(call_script, "script_multiplayer_set_troop_value", ":troop", ":event_type", -1, ":value"),
		  
        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_experience),
          (store_script_param, ":value", 3),
					(multiplayer_get_my_player, ":player"),
					(try_begin),
						(player_is_active, ":player"),
						(player_set_slot, ":player", slot_player_online_xp, ":value"),
						(assign, ":last_lvl", online_max_lvl +1),
						(try_for_range, ":cur_lvl", online_min_lvl +1, ":last_lvl"),
							(troop_get_slot, ":cur_lvl_xp", "trp_online_level_xp", ":cur_lvl"),
							(lt, ":value", ":cur_lvl_xp"),
							(val_sub, ":cur_lvl", 1),
							(try_begin),
								(neg|player_slot_eq, ":player", slot_player_online_lvl, ":cur_lvl"),
								(player_set_slot, ":player", slot_player_online_lvl, ":cur_lvl"),
								(play_sound, "snd_gong", 0),
							(try_end),
							(assign, ":last_lvl", -1),#break loop
						(try_end),			
						
						(try_begin),
							(eq, "$g_show_debug_messages", 1),
							(eq, "$get_url_response", 0),
							(assign, reg50, ":player"),
							(assign, reg52, ":value"),
							(display_message, "@[DEBUG] client player {reg50} experience set to {reg52}", 0xCCCCCC),
						(try_end),
					(try_end),

        (else_try),
          (eq, ":event_type", multiplayer_event_client_item_availability),
          (store_script_param, ":item_id", 3),
					(store_script_param, ":value", 4),#MOD
					(multiplayer_get_my_player, ":player"),
					(try_begin),
            (player_is_active, ":player"),
						(call_script, "script_multiplayer_set_item_available_for_troop", ":item_id", "trp_online_character", ":value"),#MOD
					(try_end),

        (else_try),
          (eq, ":event_type", multiplayer_event_client_control_scene_prop),
					(store_script_param, ":instance_id", 3),
          (store_script_param, ":command", 4),
          (call_script, "script_control_scene_prop", ":instance_id", ":command"),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_client_prop_missile_init),
					(store_script_param, ":instance_id", 3),
          (store_script_param, ":init_y_vel", 4),
					(store_script_param, ":init_z_vel", 5),
					(call_script, "script_prop_missile_init", ":instance_id", ":init_y_vel", ":init_z_vel", -1),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_return_explosion_at_pos),
          (store_script_param, ":pos_x", 3),
          (store_script_param, ":pos_y", 4),
          (store_script_param, ":pos_z", 5),
            
          (set_fixed_point_multiplier, 100),
          (init_position, pos_hit),
          (position_set_x, pos_hit, ":pos_x"),
          (position_set_y, pos_hit, ":pos_y"),
          (position_set_z, pos_hit, ":pos_z"),
					(call_script, "script_multiplayer_server_explosion_at_position"),
      
        (else_try),
          (eq, ":event_type", multiplayer_event_start_use_item),
          (store_script_param, ":value", 3),
          (store_script_param, ":value_2", 4),
          (call_script, "script_start_use_item", ":value", ":value_2"),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_return_thunderbolt),
          (store_script_param, "$g_thunderbolt_x", 3),
          (store_script_param, "$g_thunderbolt_y", 4),
          (store_script_param, "$g_thunderbolt_z", 5),
          (call_script, "script_thunderbolt_hit"),

        (else_try),
          (eq, ":event_type", multiplayer_event_return_weather_time),
          (store_script_param, "$g_multiplayer_wt_val_1", 3),
          (store_script_param, "$g_multiplayer_wt_val_2", 4),
					
					(store_div, "$g_cloud_amount", "$g_multiplayer_wt_val_1", 2097152),
					(store_mul, ":sub_1", "$g_cloud_amount", 2097152),
					(store_sub, "$g_precipitation_strength", "$g_multiplayer_wt_val_1", ":sub_1"),
					(val_div, "$g_precipitation_strength", 16384),
					(store_mul, ":sub_2", "$g_precipitation_strength", 16384),
					(store_sub, "$g_fog_distance", "$g_multiplayer_wt_val_1", ":sub_1"),
					(val_sub, "$g_fog_distance", ":sub_2"),
					
					(store_div, "$g_wind_strength", "$g_multiplayer_wt_val_2", 16384),
					(store_mul, ":sub_1", "$g_wind_strength", 16384),
					(store_sub, "$g_wind_direction", "$g_multiplayer_wt_val_2", ":sub_1"),
					(val_div, "$g_wind_direction", 32),
					(store_mul, ":sub_2", "$g_wind_direction", 32),
					(store_sub, "$g_day_time", "$g_multiplayer_wt_val_2", ":sub_1"),
					(val_sub, "$g_day_time", ":sub_2"),
					
					(call_script, "script_apply_weather"),
          
        (else_try),
          (eq, ":event_type", multiplayer_event_return_message_color),
          (store_script_param, "$g_custom_message_color", 3),
      
        (else_try),
          (eq, ":event_type", multiplayer_event_show_message),
          (display_message, s0, "$g_custom_message_color"),
		  
        (else_try),
          (eq, ":event_type", multiplayer_event_store_string),
          (str_store_string, s64, s0),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_client_set_admin),
					(store_script_param, ":value", 3),
					
					(multiplayer_get_my_player, ":player"),
					(try_begin),
            (player_is_active, ":player"),
						(player_set_is_admin, ":player", ":value"),
					(try_end),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_client_agent_set_horse_speed),
					(store_script_param, ":agent", 3),
					(store_script_param, ":speed", 4),
					(agent_set_horse_speed_factor, ":agent", ":speed"),
					
        (else_try),
          (eq, ":event_type", multiplayer_event_client_agent_set_bleed_interval),
					(store_script_param, ":agent", 3),
					(store_script_param, ":value", 4),
					(agent_set_slot, ":agent", slot_agent_bleed_interval, ":value"),
#MOD end
        (try_end),
      (try_end),
     ]),

  # script_cf_multiplayer_evaluate_poll
  # Input: none
  # Output: none (can fail)
  ("cf_multiplayer_evaluate_poll",
   [
     (assign, ":result", 0),
     (assign, "$g_multiplayer_poll_ended", 1),
     (store_add, ":total_votes", "$g_multiplayer_poll_yes_count", "$g_multiplayer_poll_no_count"),
     (store_sub, ":abstain_votes", "$g_multiplayer_poll_num_sent", ":total_votes"),
     (store_mul, ":nos_from_abstains", 3, ":abstain_votes"),
     (val_div, ":nos_from_abstains", 10), #30% of abstains are counted as no
     (val_add, ":total_votes", ":nos_from_abstains"),
     (val_max, ":total_votes", 1), #if someone votes and only 1-3 abstain occurs?
     (store_mul, ":vote_ratio", 100, "$g_multiplayer_poll_yes_count"),
     (val_div, ":vote_ratio", ":total_votes"),
     (try_begin),
       (gt, ":vote_ratio", 50),#MOD edit
       (assign, ":result", 1),
       (try_begin),
         (eq, "$g_multiplayer_poll_to_show", 1), #kick player
         (try_begin),
           (player_is_active, "$g_multiplayer_poll_value_to_show"),
           (kick_player, "$g_multiplayer_poll_value_to_show"),
         (try_end),
       (else_try),
         (eq, "$g_multiplayer_poll_to_show", 2), #ban player
         (ban_player_using_saved_ban_info), #already loaded at the beginning of the poll
       (else_try),
         (eq, "$g_multiplayer_poll_to_show", 3), #change map with factions
         (team_set_faction, 0, "$g_multiplayer_poll_value_2_to_show"),
         (team_set_faction, 1, "$g_multiplayer_poll_value_3_to_show"),
       (else_try),
         (eq, "$g_multiplayer_poll_to_show", 4), #change number of bots
         (assign, "$g_multiplayer_num_bots_team_1", "$g_multiplayer_poll_value_to_show"),
         (assign, "$g_multiplayer_num_bots_team_2", "$g_multiplayer_poll_value_2_to_show"),
         (get_max_players, ":num_players"),                               
         (try_for_range, ":cur_player", 1, ":num_players"),
           (player_is_active, ":cur_player"),
           (multiplayer_send_4_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_num_bots_in_team, 1, "$g_multiplayer_num_bots_team_1"),
           (multiplayer_send_4_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, 0, sub_event_num_bots_in_team, 2, "$g_multiplayer_num_bots_team_2"),
         (try_end),
       (try_end),
     (else_try),
       (assign, "$g_multiplayer_poll_running", 0), #end immediately if poll fails. but end after some time if poll succeeds (apply the results first)
     (try_end),
     (get_max_players, ":num_players"),
     #for only server itself-----------------------------------------------------------------------------------------------
     (call_script, "script_show_multiplayer_message", multiplayer_message_type_poll_result, ":result"), #0 is useless here
     #for only server itself-----------------------------------------------------------------------------------------------     
     (try_for_range, ":cur_player", 1, ":num_players"),
       (player_is_active, ":cur_player"),
       (multiplayer_send_2_int_to_player, ":cur_player", multiplayer_event_show_multiplayer_message, multiplayer_message_type_poll_result, ":result"),
     (try_end),
     (eq, ":result", 1),
     ]),

  # script_multiplayer_accept_duel
  # Input: arg1 = agent_no, arg2 = agent_no_offerer
  # Output: none
  ("multiplayer_accept_duel",
   [
     (store_script_param, ":agent_no", 1),
     (store_script_param, ":agent_no_offerer", 2),
     (try_begin),
       (agent_slot_ge, ":agent_no", slot_agent_in_duel_with, 0),
       (agent_get_slot, ":ex_duelist", ":agent_no", slot_agent_in_duel_with),
       (agent_is_active, ":ex_duelist"),
       (agent_clear_relations_with_agents, ":ex_duelist"),
       (agent_set_slot, ":ex_duelist", slot_agent_in_duel_with, -1),
       (agent_get_player_id, ":player_no", ":ex_duelist"),
       (try_begin),
         (player_is_active, ":player_no"), #might be AI
         (multiplayer_send_int_to_player, ":player_no", multiplayer_event_cancel_duel, ":agent_no"),
       (else_try),
         (agent_force_rethink, ":ex_duelist"),
       (try_end),
     (try_end),
     (try_begin),
       (agent_slot_ge, ":agent_no_offerer", slot_agent_in_duel_with, 0),
       (agent_get_slot, ":ex_duelist", ":agent_no_offerer", slot_agent_in_duel_with),
       (agent_is_active, ":ex_duelist"),
       (agent_clear_relations_with_agents, ":ex_duelist"),
       (agent_set_slot, ":ex_duelist", slot_agent_in_duel_with, -1),
       (try_begin),
         (player_is_active, ":player_no"), #might be AI
         (multiplayer_send_int_to_player, ":player_no", multiplayer_event_cancel_duel, ":agent_no_offerer"),
       (else_try),
         (agent_force_rethink, ":ex_duelist"),
       (try_end),
     (try_end),
     (agent_set_slot, ":agent_no", slot_agent_in_duel_with, ":agent_no_offerer"),
     (agent_set_slot, ":agent_no_offerer", slot_agent_in_duel_with, ":agent_no"),
     (agent_clear_relations_with_agents, ":agent_no"),
     (agent_clear_relations_with_agents, ":agent_no_offerer"),
##     (agent_add_relation_with_agent, ":agent_no", ":agent_no_offerer", -1),
##     (agent_add_relation_with_agent, ":agent_no_offerer", ":agent_no", -1),
     (agent_get_player_id, ":player_no", ":agent_no"),
     (store_mission_timer_a, ":mission_timer"),
     (try_begin),
       (player_is_active, ":player_no"), #might be AI
       (multiplayer_send_int_to_player, ":player_no", multiplayer_event_start_duel, ":agent_no_offerer"),
     (else_try),
       (agent_force_rethink, ":agent_no"),
     (try_end),
     (agent_set_slot, ":agent_no", slot_agent_duel_start_time, ":mission_timer"),
     (agent_get_player_id, ":agent_no_offerer_player", ":agent_no_offerer"),
     (try_begin),
       (player_is_active, ":agent_no_offerer_player"), #might be AI
       (multiplayer_send_int_to_player, ":agent_no_offerer_player", multiplayer_event_start_duel, ":agent_no"),
     (else_try),
       (agent_force_rethink, ":agent_no_offerer"),
     (try_end),
     (agent_set_slot, ":agent_no_offerer", slot_agent_duel_start_time, ":mission_timer"),
     ]),

  # script_game_get_multiplayer_server_option_for_mission_template
  # Input: arg1 = mission_template_id, arg2 = option_index
  # Output: trigger_result = 1 for option available, 0 for not available
  # reg0 = option_value
  ("game_get_multiplayer_server_option_for_mission_template",
   [
     (store_script_param, ":mission_template_id", 1),
     (store_script_param, ":option_index", 2),
     (try_begin),
       (eq, ":option_index", 0),
       (assign, reg0, "$g_multiplayer_team_1_faction"),
       (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 1),
       (assign, reg0, "$g_multiplayer_team_2_faction"),
       (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 2),
       (assign, reg0, "$g_multiplayer_num_bots_team_1"),
       (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 3),
       (assign, reg0, "$g_multiplayer_num_bots_team_2"),
       (set_trigger_result, 1),
#MOD disable
     # (else_try),
       # (eq, ":option_index", 4),
       # (server_get_friendly_fire, reg0),
       # (set_trigger_result, 1),
     # (else_try),
       # (eq, ":option_index", 5),
       # (server_get_melee_friendly_fire, reg0),
       # (set_trigger_result, 1),
     # (else_try),
       # (eq, ":option_index", 6),
       # (server_get_friendly_fire_damage_self_ratio, reg0),
       # (set_trigger_result, 1),
     # (else_try),
       # (eq, ":option_index", 7),
       # (server_get_friendly_fire_damage_friend_ratio, reg0),
       # (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 8),
       (server_get_ghost_mode, reg0),
       (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 9),
       (server_get_control_block_dir, reg0),       
       (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 10),
       (server_get_combat_speed, reg0),
       (set_trigger_result, 1),
     # (else_try),
       # (eq, ":option_index", 11),
       # (assign, reg0, "$g_multiplayer_game_max_minutes"),
       # (set_trigger_result, 1),
     (else_try),
       # (try_begin),
         # (neq, ":mission_template_id", "mt_multiplayer_bt"),
         # (neq, ":mission_template_id", "mt_multiplayer_sg"),
         # (val_add, ":option_index", 1), #max round time
       # (try_end),
       (eq, ":option_index", 12),
       (assign, reg0, "$g_multiplayer_round_max_seconds"),
       (set_trigger_result, 1),
     (else_try),
       (try_begin),
         (neq, ":mission_template_id", "mt_multiplayer_bt"),
         (val_add, ":option_index", 1), #respawn as bot
       (try_end),
       (eq, ":option_index", 13),
       (assign, reg0, "$g_multiplayer_player_respawn_as_bot"),
       (set_trigger_result, 1),
     (else_try),
       (try_begin),
         (neq, ":mission_template_id", "mt_multiplayer_sg"),
         (val_add, ":option_index", 1), #respawn limit
       (try_end),
       (eq, ":option_index", 14),
       (assign, reg0, "$g_multiplayer_number_of_respawn_count"),
       (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 15),
       (assign, reg0, "$g_multiplayer_game_max_points"),
       (set_trigger_result, 1),
#MOD disable
     # (else_try),
       # (eq, ":option_index", 16),
       # (assign, reg0, "$g_multiplayer_point_gained_from_flags"),
       # (set_trigger_result, 1),
     # (else_try),
       # (val_add, ":option_index", 1), #point gained from capturing flag
       # (eq, ":option_index", 17),
       # (assign, reg0, "$g_multiplayer_point_gained_from_capturing_flag"),
       # (set_trigger_result, 1),
     (else_try),
       (eq, ":option_index", 18),
       (assign, reg0, "$g_multiplayer_respawn_period"),
       (set_trigger_result, 1),
#MOD disable
     # (else_try),
       # (eq, ":option_index", 19),
       # (assign, reg0, "$g_multiplayer_initial_gold_multiplier"),
       # (set_trigger_result, 1),
     # (else_try),
       # (eq, ":option_index", 20),
       # (assign, reg0, "$g_multiplayer_battle_earnings_multiplier"),
       # (set_trigger_result, 1),
     # (else_try),
       # (try_begin),
         # (neq, ":mission_template_id", "mt_multiplayer_bt"),
         # (neq, ":mission_template_id", "mt_multiplayer_sg"),
         # (val_add, ":option_index", 1),
       # (try_end),
       # (eq, ":option_index", 21),
       # (assign, reg0, "$g_multiplayer_round_earnings_multiplier"),
       # (set_trigger_result, 1),
     (try_end),
     ]),

  # script_game_multiplayer_server_option_for_mission_template_to_string
  # Input: arg1 = mission_template_id, arg2 = option_index, arg3 = option_value
  # Output: s0 = option_text
  ("game_multiplayer_server_option_for_mission_template_to_string",
   [
     (store_script_param, ":mission_template_id", 1),
     (store_script_param, ":option_index", 2),
     (store_script_param, ":option_value", 3),
     (str_clear, s0),
     (try_begin),
       (eq, ":option_index", 0),
       (assign, reg1, 1),
       (str_store_string, s0, "str_team_reg1_faction"),
       (str_store_faction_name, s1, ":option_value"),
       (str_store_string, s0, "str_s0_s1"),
     (else_try),
       (eq, ":option_index", 1),
       (assign, reg1, 2),
       (str_store_string, s0, "str_team_reg1_faction"),
       (str_store_faction_name, s1, ":option_value"),
       (str_store_string, s0, "str_s0_s1"),
     (else_try),
       (eq, ":option_index", 2),
       (assign, reg1, 1),
       (str_store_string, s0, "str_number_of_bots_in_team_reg1"),
       (assign, reg0, ":option_value"),
       (str_store_string, s0, "str_s0_reg0"),
     (else_try),
       (eq, ":option_index", 3),
       (assign, reg1, 2),
       (str_store_string, s0, "str_number_of_bots_in_team_reg1"),
       (assign, reg0, ":option_value"),
       (str_store_string, s0, "str_s0_reg0"),
#MOD disable
     # (else_try),
       # (eq, ":option_index", 4),
       # (str_store_string, s0, "str_allow_friendly_fire"),
       # (try_begin),
         # (eq, ":option_value", 0),
         # (str_store_string, s1, "str_no_wo_dot"),
       # (else_try),
         # (str_store_string, s1, "str_yes_wo_dot"),
       # (try_end),
       # (str_store_string, s0, "str_s0_s1"),
     # (else_try),
       # (eq, ":option_index", 5),
       # (str_store_string, s0, "str_allow_melee_friendly_fire"),
       # (try_begin),
         # (eq, ":option_value", 0),
         # (str_store_string, s1, "str_no_wo_dot"),
       # (else_try),
         # (str_store_string, s1, "str_yes_wo_dot"),
       # (try_end),
       # (str_store_string, s0, "str_s0_s1"),
     # (else_try),
       # (eq, ":option_index", 6),
       # (str_store_string, s0, "str_friendly_fire_damage_self_ratio"),
       # (assign, reg0, ":option_value"),
       # (str_store_string, s0, "str_s0_reg0"),
     # (else_try),
       # (eq, ":option_index", 7),
       # (str_store_string, s0, "str_friendly_fire_damage_friend_ratio"),
       # (assign, reg0, ":option_value"),
       # (str_store_string, s0, "str_s0_reg0"),
     (else_try),
       (eq, ":option_index", 8),
       (str_store_string, s0, "str_spectator_camera"),
       (try_begin),
         (eq, ":option_value", 0),
         (str_store_string, s1, "str_free"),
       (else_try),
         (eq, ":option_value", 1),
         (str_store_string, s1, "str_stick_to_any_player"),
       (else_try),
         (eq, ":option_value", 2),
         (str_store_string, s1, "str_stick_to_team_members"),
       (else_try),
         (str_store_string, s1, "str_stick_to_team_members_view"),
       (try_end),
       (str_store_string, s0, "str_s0_s1"),
     (else_try),
       (eq, ":option_index", 9),
       (str_store_string, s0, "str_control_block_direction"),
       (try_begin),
         (eq, ":option_value", 0),
         (str_store_string, s1, "str_automatic"),
       (else_try),
         (str_store_string, s1, "str_by_mouse_movement"),
       (try_end),
       (str_store_string, s0, "str_s0_s1"),
     (else_try),
       (eq, ":option_index", 10),
       (str_store_string, s0, "str_combat_speed"),
       (try_begin),
         (eq, ":option_value", 0),
         (str_store_string, s1, "str_combat_speed_0"),
       (else_try),
         (eq, ":option_value", 1),
         (str_store_string, s1, "str_combat_speed_1"),
       (else_try),
         (eq, ":option_value", 2),
         (str_store_string, s1, "str_combat_speed_2"),
       (else_try),
         (eq, ":option_value", 3),
         (str_store_string, s1, "str_combat_speed_3"),
       (else_try),
         (str_store_string, s1, "str_combat_speed_4"),
       (try_end),
       (str_store_string, s0, "str_s0_s1"),
     # (else_try),
       # (eq, ":option_index", 11),
       # (str_store_string, s0, "str_map_time_limit"),
       # (assign, reg0, ":option_value"),
       # (str_store_string, s0, "str_s0_reg0"),
     (else_try),
       # (try_begin),
         # (neq, ":mission_template_id", "mt_multiplayer_bt"),
         # (neq, ":mission_template_id", "mt_multiplayer_sg"),
         # (val_add, ":option_index", 1), #max round time
       # (try_end),
       (eq, ":option_index", 12),
       (str_store_string, s0, "str_round_time_limit"),
       (assign, reg0, ":option_value"),
       (str_store_string, s0, "str_s0_reg0"),
     (else_try),
       (try_begin),
         (neq, ":mission_template_id", "mt_multiplayer_bt"),
         (val_add, ":option_index", 1), #respawn as bot
       (try_end),
       (eq, ":option_index", 13),
       (str_store_string, s0, "str_players_take_control_of_a_bot_after_death"),
       (try_begin),
         (eq, ":option_value", 0),
         (str_store_string, s1, "str_no_wo_dot"),
       (else_try),
         (str_store_string, s1, "str_yes_wo_dot"),
       (try_end),
       (str_store_string, s0, "str_s0_s1"),
     (else_try),
       (try_begin),
         (neq, ":mission_template_id", "mt_multiplayer_sg"),
         (val_add, ":option_index", 1), #respawn limit
       (try_end),
       (eq, ":option_index", 14),
       (str_store_string, s0, "str_defender_spawn_count_limit"),
       (try_begin),
         (eq, ":option_value", 0),
         (str_store_string, s1, "str_unlimited"),
       (else_try),
         (assign, reg1, ":option_value"),
         (str_store_string, s1, "str_reg1"),
       (try_end),
       (str_store_string, s0, "str_s0_s1"),
     (else_try),
       (eq, ":option_index", 15),
       (str_store_string, s0, "str_team_points_limit"),
       (assign, reg0, ":option_value"),
       (str_store_string, s0, "str_s0_reg0"),
     (else_try),
       (val_add, ":option_index", 1), #point gained from flags
       (eq, ":option_index", 16),
       (str_store_string, s0, "str_point_gained_from_flags"),
       (assign, reg0, ":option_value"),
       (str_store_string, s0, "str_s0_reg0"),
     (else_try),
       (val_add, ":option_index", 1), #point gained from capturing flag
       (eq, ":option_index", 17),
       (str_store_string, s0, "str_point_gained_from_capturing_flag"),
       (assign, reg0, ":option_value"),
       (str_store_string, s0, "str_s0_reg0"),
     (else_try),
       (eq, ":option_index", 18),
       (str_store_string, s0, "str_respawn_period"),
       (assign, reg0, ":option_value"),
       (str_store_string, s0, "str_s0_reg0"),
#MOD disable
     # (else_try),
       # (eq, ":option_index", 19),
       # (str_store_string, s0, "str_initial_gold_multiplier"),
       # (assign, reg0, ":option_value"),
       # (str_store_string, s0, "str_s0_reg0"),
     # (else_try),
       # (eq, ":option_index", 20),
       # (str_store_string, s0, "str_battle_earnings_multiplier"),
       # (assign, reg0, ":option_value"),
       # (str_store_string, s0, "str_s0_reg0"),
     # (else_try),
       # (try_begin),
         # (neq, ":mission_template_id", "mt_multiplayer_bt"),
         # (neq, ":mission_template_id", "mt_multiplayer_sg"),
         # (val_add, ":option_index", 1),
       # (try_end),
       # (eq, ":option_index", 21),
       # (str_store_string, s0, "str_round_earnings_multiplier"),
       # (assign, reg0, ":option_value"),
       # (str_store_string, s0, "str_s0_reg0"),
     (try_end),
     ]),

  # script_cf_multiplayer_team_is_available
  # Input: arg1 = player_no, arg2 = team_no
  # Output: none, true or false 
  ("cf_multiplayer_team_is_available",
   [
     (store_script_param, ":player_no", 1),
     (store_script_param, ":team_no", 2),
     (assign, ":continue_change_team", 1),
     (try_begin),
       (neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
       (neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
       (is_between, ":team_no", 0, multi_team_spectator),
       (neg|teams_are_enemies, ":team_no", ":team_no"), #checking if it is a deathmatch or not
       (assign, ":continue_change_team", 0),
       #counting number of players for team balance checks
       (assign, ":number_of_players_at_team_1", 0),
       (assign, ":number_of_players_at_team_2", 0),
       (get_max_players, ":num_players"),
       (try_for_range, ":cur_player", 0, ":num_players"),
         (player_is_active, ":cur_player"),
         (neq, ":cur_player", ":player_no"),
         (player_get_team_no, ":player_team", ":cur_player"),
         (try_begin),
           (eq, ":player_team", 0),
           (val_add, ":number_of_players_at_team_1", 1),
         (else_try),
           (eq, ":player_team", 1),
           (val_add, ":number_of_players_at_team_2", 1),
         (try_end),
       (try_end),
       (store_sub, ":difference_of_number_of_players", ":number_of_players_at_team_1", ":number_of_players_at_team_2"),

       (try_begin),
         (ge, ":difference_of_number_of_players", 0),
         (val_add, ":difference_of_number_of_players", 1),
       (else_try),
         (val_add, ":difference_of_number_of_players", -1),
       (try_end),
     
       (try_begin),
         (eq, ":team_no", 0),
         (lt, ":difference_of_number_of_players", "$g_multiplayer_auto_team_balance_limit"),
         (assign, ":continue_change_team", 1),
       (else_try),
         (eq, ":team_no", 1),
         (store_mul, ":checked_value", "$g_multiplayer_auto_team_balance_limit", -1),
         (gt, ":difference_of_number_of_players", ":checked_value"),
         (assign, ":continue_change_team", 1),
       (try_end),
     (try_end),
     (eq, ":continue_change_team", 1),
     ]),

  # script_game_multiplayer_event_duel_offered
  # Input: arg1 = agent_no
  # Output: none
  ("game_multiplayer_event_duel_offered",
   [
     (store_script_param, ":agent_no", 1),
     (get_player_agent_no, ":player_agent_no"),
     (try_begin),
       (agent_is_active, ":player_agent_no"),
       (this_or_next|agent_slot_eq, ":player_agent_no", slot_agent_in_duel_with, -1),
       (agent_check_offer_from_agent, ":player_agent_no", ":agent_no"),
       (neg|agent_slot_eq, ":player_agent_no", slot_agent_in_duel_with, ":agent_no"), #don't allow spamming duel offers during countdown
       (multiplayer_send_int_to_server, multiplayer_event_offer_duel, ":agent_no"),
       (agent_get_player_id, ":player_no", ":agent_no"),
       (try_begin),
         (player_is_active, ":player_no"),
         (str_store_player_username, s0, ":player_no"),
       (else_try),
         (str_store_agent_name, s0, ":agent_no"),
       (try_end),
       (display_message, "str_a_duel_request_is_sent_to_s0"),
     (try_end),
     ]),
	 
  # script_game_get_multiplayer_game_type_enum
  # Input: none
  # Output: reg0:first type, reg1:type count
  ("game_get_multiplayer_game_type_enum",
   [
     (assign, reg0, multiplayer_game_type_deathmatch),
	 (assign, reg1, multiplayer_num_game_types),
	 ]),

  # script_game_multiplayer_get_game_type_mission_template
  # Input: arg1 = game_type
  # Output: mission_template 
  ("game_multiplayer_get_game_type_mission_template",
   [
     (assign, ":selected_mt", -1),
     (store_script_param, ":game_type", 1),
     (try_begin),
       (eq, ":game_type", multiplayer_game_type_deathmatch),
       (assign, ":selected_mt", "mt_multiplayer_dm"),
     (else_try),
       (eq, ":game_type", multiplayer_game_type_team_deathmatch),
       (assign, ":selected_mt", "mt_multiplayer_tdm"),
     (else_try),
       (eq, ":game_type", multiplayer_game_type_battle),
       (assign, ":selected_mt", "mt_multiplayer_bt"),
     (else_try),
       (eq, ":game_type", multiplayer_game_type_siege),
       (assign, ":selected_mt", "mt_multiplayer_sg"),
     (else_try),
       (eq, ":game_type", multiplayer_game_type_duel),
       (assign, ":selected_mt", "mt_multiplayer_duel"),
     (try_end),
     (assign, reg0, ":selected_mt"),
     ]),

  # script_multiplayer_get_mission_template_game_type
  # Input: arg1 = mission_template_no
  # Output: game_type 
  ("multiplayer_get_mission_template_game_type",
   [
     (store_script_param, ":mission_template_no", 1),
     (assign, ":game_type", -1),
     (try_begin),
       (eq, ":mission_template_no", "mt_multiplayer_dm"),
       (assign, ":game_type", multiplayer_game_type_deathmatch),
     (else_try),
       (eq, ":mission_template_no", "mt_multiplayer_tdm"),
       (assign, ":game_type", multiplayer_game_type_team_deathmatch),
     (else_try),
       (eq, ":mission_template_no", "mt_multiplayer_bt"),
       (assign, ":game_type", multiplayer_game_type_battle),
     (else_try),
       (eq, ":mission_template_no", "mt_multiplayer_sg"),
       (assign, ":game_type", multiplayer_game_type_siege),
     (else_try),
       (eq, ":mission_template_no", "mt_multiplayer_duel"),
       (assign, ":game_type", multiplayer_game_type_duel),
     (try_end),
     (assign, reg0, ":game_type"),
     ]),


  # script_multiplayer_fill_available_factions_combo_button
  # Input: arg1 = overlay_id, arg2 = selected_faction_no, arg3 = opposite_team_selected_faction_no
  # Output: none 
  ("multiplayer_fill_available_factions_combo_button",
   [
     (store_script_param, ":overlay_id", 1),
     (store_script_param, ":selected_faction_no", 2),
##     (store_script_param, ":opposite_team_selected_faction_no", 3),
##     (try_for_range, ":cur_faction", "fac_kingdom_1", "fac_kingdoms_end"),
##       (try_begin),
##         (eq, ":opposite_team_selected_faction_no", ":cur_faction"),
##         (try_begin),
##           (gt, ":selected_faction_no", ":opposite_team_selected_faction_no"),
##           (val_sub, ":selected_faction_no", 1),
##         (try_end),
##       (else_try),
##         (str_store_faction_name, s0, ":cur_faction"),
##         (overlay_add_item, ":overlay_id", s0),
##       (try_end),
##     (try_end),
##     (val_sub, ":selected_faction_no", "fac_kingdom_1"),
##     (overlay_set_val, ":overlay_id", ":selected_faction_no"),
     (try_for_range, ":cur_faction", npc_kingdoms_begin, npc_kingdoms_end),
       (str_store_faction_name, s0, ":cur_faction"),
       (overlay_add_item, ":overlay_id", s0),
     (try_end),
     (val_sub, ":selected_faction_no", "fac_kingdom_1"),
     (overlay_set_val, ":overlay_id", ":selected_faction_no"),
     ]),

  #script_multiplayer_clear_player_selected_items
  # Input: arg1 = player_no
  # Output: none
  ("multiplayer_clear_player_selected_items",
   [
     (store_script_param, ":player_no", 1),
     (try_for_range, ":slot_no", slot_player_selected_item_indices_begin, slot_player_selected_item_indices_end),
       (player_set_slot, ":player_no", ":slot_no", -1),
     (try_end),
     ]),
  

  #script_multiplayer_init_player_slots
  # Input: arg1 = player_no
  # Output: none
  ("multiplayer_init_player_slots",
   [
     (store_script_param, ":player_no", 1),
     (call_script, "script_multiplayer_clear_player_selected_items", ":player_no"),
     (player_set_slot, ":player_no", slot_player_spawned_this_round, 0),
#     (player_set_slot, ":player_no", slot_player_last_rounds_used_item_earnings, 0), #MOD disable
     (player_set_slot, ":player_no", slot_player_poll_disabled_until_time, 0),

#MOD begin
    (try_for_range, ":cur_slot", slot_player_bot_type_1_wanted, slot_player_gold_earned_since_upkeep +1),
			(try_begin),
				(eq, ":cur_slot", slot_player_online_clan),
				(player_set_slot, ":player_no", ":cur_slot", -1),
			(else_try),
				(player_set_slot, ":player_no", ":cur_slot", 0),
			(try_end),
		(try_end),
#MOD end
#     (player_set_slot, ":player_no", slot_player_bot_type_1_wanted, 0),
#     (player_set_slot, ":player_no", slot_player_bot_type_2_wanted, 0),
#     (player_set_slot, ":player_no", slot_player_bot_type_3_wanted, 0),
#     (player_set_slot, ":player_no", slot_player_bot_type_4_wanted, 0),
     ]),

  #script_send_open_close_information_of_object
  # Input: arg1 = mission_object_id
  # Output: none
  ("send_open_close_information_of_object",
   [
     (store_script_param, ":player_no", 1),
     (store_script_param, ":scene_prop_no", 2),
     
     (scene_prop_get_num_instances, ":num_instances", ":scene_prop_no"),

     (try_for_range, ":instance_no", 0, ":num_instances"),
       (scene_prop_get_instance, ":instance_id", ":scene_prop_no", ":instance_no"),
       (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot),
       (try_begin),
         (eq, ":opened_or_closed", 1),
         (multiplayer_send_int_to_player, ":player_no", multiplayer_event_set_scene_prop_open_or_close, ":instance_id"),
       (try_end),
     (try_end),
     ]),

  #script_multiplayer_send_initial_information
  # Input: arg1 = player_no
  # Output: none
  ("multiplayer_send_initial_information",
   [
     (store_script_param, ":player_no", 1),
#MOD begin        
     (multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_num_bots_in_team, 1, "$g_multiplayer_num_bots_team_1"),
     (multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_num_bots_in_team, 2, "$g_multiplayer_num_bots_team_2"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_auto_team_balance_limit, "$g_multiplayer_auto_team_balance_limit"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_num_bots_voteable, "$g_multiplayer_num_bots_voteable"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_factions_voteable, "$g_multiplayer_factions_voteable"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_maps_voteable, "$g_multiplayer_maps_voteable"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_kick_voteable, "$g_multiplayer_kick_voteable"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_ban_voteable, "$g_multiplayer_ban_voteable"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_allow_player_banners, "$g_multiplayer_allow_player_banners"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_force_default_armor, "$g_multiplayer_force_default_armor"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_disallow_ranged_weapons, "$g_multiplayer_disallow_ranged_weapons"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_disallow_melee_weapons, "$g_multiplayer_disallow_melee_weapons"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_disallow_armors, "$g_multiplayer_disallow_armors"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_disallow_horses, "$g_multiplayer_disallow_horses"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_respawn_period, "$g_multiplayer_respawn_period"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_round_max_seconds, "$g_multiplayer_round_max_seconds"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_game_type, "$g_multiplayer_game_type"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_player_respawn_as_bot, "$g_multiplayer_player_respawn_as_bot"),

     (server_get_max_num_players, ":max_num_players"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_max_num_players, ":max_num_players"),
     (multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_next_team_faction, 1, "$g_multiplayer_next_team_1_faction"),
     (multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_next_team_faction, 2, "$g_multiplayer_next_team_2_faction"),
     # (server_get_anti_cheat, ":server_anti_cheat"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_anti_cheat, ":server_anti_cheat"),
     # (server_get_friendly_fire, ":server_friendly_fire"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_friendly_fire, ":server_friendly_fire"),
     # (server_get_melee_friendly_fire, ":server_melee_friendly_fire"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_melee_friendly_fire, ":server_melee_friendly_fire"),
     # (server_get_friendly_fire_damage_self_ratio, ":friendly_fire_damage_self_ratio"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_friendly_fire_damage_self_ratio, ":friendly_fire_damage_self_ratio"),
     # (server_get_friendly_fire_damage_friend_ratio, ":friendly_fire_damage_friend_ratio"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_friendly_fire_damage_friend_ratio, ":friendly_fire_damage_friend_ratio"),
     (server_get_ghost_mode, ":server_ghost_mode"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_ghost_mode, ":server_ghost_mode"),
     (server_get_control_block_dir, ":server_control_block_dir"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_control_block_dir, ":server_control_block_dir"),
     (server_get_combat_speed, ":server_combat_speed"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_combat_speed, ":server_combat_speed"),
     # (server_get_add_to_game_servers_list, ":server_add_to_servers_list"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_add_to_servers_list, ":server_add_to_servers_list"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_game_max_minutes, "$g_multiplayer_game_max_minutes"),
     (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_game_max_points, "$g_multiplayer_game_max_points"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_point_gained_from_flags, "$g_multiplayer_point_gained_from_flags"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_point_gained_from_capturing_flag, "$g_multiplayer_point_gained_from_capturing_flag"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_initial_gold_multiplier, "$g_multiplayer_initial_gold_multiplier"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_battle_earnings_multiplier, "$g_multiplayer_battle_earnings_multiplier"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_round_earnings_multiplier, "$g_multiplayer_round_earnings_multiplier"),
     # (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_valid_vote_ratio, "$g_multiplayer_valid_vote_ratio"),
		 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_quick_artillery, "$g_quick_artillery"),
		 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_all_items, "$g_inventory_all_items"),
		 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_allow_online_chars, "$g_allow_online_chars"),
		 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_allow_faction_troops, "$g_allow_faction_troops"),
		 
     (str_store_server_name, s0),
     (multiplayer_send_string_to_player, ":player_no", multiplayer_event_return_server_name, s0),
     #(multiplayer_send_message_to_player, ":player_no", multiplayer_event_return_open_game_rules),
     	
     (try_begin),
       (player_is_admin, ":player_no"),
       #condition checks are done
       (server_get_renaming_server_allowed, "$g_multiplayer_renaming_server_allowed"),
       (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_renaming_server_allowed, "$g_multiplayer_renaming_server_allowed"),
       (server_get_changing_game_type_allowed, "$g_multiplayer_changing_game_type_allowed"),
       (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_changing_game_type_allowed, "$g_multiplayer_changing_game_type_allowed"),
       (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_max_num_bots, "$g_multiplayer_max_num_bots"),
       (str_store_server_password, s0),
       (multiplayer_send_string_to_player, ":player_no", multiplayer_event_return_game_password, s0),
#MOD begin
       (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_cloud_amount, "$g_multiplayer_cloud_amount"),
			 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_precipitation_strength, "$g_multiplayer_precipitation_strength"),
			 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_fog_distance, "$g_multiplayer_fog_distance"),
			 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_thunderstorm, "$g_multiplayer_thunderstorm"),
			 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_wind_strength, "$g_multiplayer_wind_strength"),
			 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_wind_direction, "$g_multiplayer_wind_direction"),
       (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_day_time, "$g_multiplayer_day_time"),
			 (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_server_announcements, "$g_server_announcements"),
     (try_end),
	 
     (store_mission_timer_a, ":mission_timer"),
     (multiplayer_send_int_to_player, ":player_no", multiplayer_event_return_server_mission_timer_while_player_joined, ":mission_timer"),

     (try_begin),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
       (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_return_admin_settings, 0, sub_event_respawn_count, "$g_multiplayer_number_of_respawn_count"),
     (try_end),
	 
#MOD end
     # (try_for_agents, ":cur_agent"),#send if any agent is carrying any scene object
       # (agent_is_human, ":cur_agent"),		 
       # (agent_get_attached_scene_prop, ":attached_scene_prop", ":cur_agent"),
       # (ge, ":attached_scene_prop", 0),
       # (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_set_attached_scene_prop, ":cur_agent", ":attached_scene_prop"),
     # (try_end),

     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_siege_ladder_move_6m"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_siege_ladder_move_8m"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_siege_ladder_move_10m"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_siege_ladder_move_12m"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_siege_ladder_move_14m"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_winch_b"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_castle_e_sally_door_a"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_castle_f_sally_door_a"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_earth_sally_gate_left"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_earth_sally_gate_right"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_viking_keep_destroy_sally_door_left"),     
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_viking_keep_destroy_sally_door_right"),     
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_castle_f_door_a"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_door_destructible"),
     (call_script, "script_send_open_close_information_of_object", ":player_no", "spr_castle_f_door_b"),
	 
#MOD begin
    (try_for_range, ":cur_prop", destructible_props_begin, destructible_props_end),
			(scene_prop_get_num_instances, ":num_instances", ":cur_prop"),
      (try_for_range, ":cur_instance", 0, ":num_instances"),
        (scene_prop_get_instance, ":instance_id", ":cur_prop", ":cur_instance"),
				
				# (scene_prop_get_slot, ":value", ":instance_id", scene_prop_spawn_time),
				# (multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", scene_prop_spawn_time, ":value"),
				
				(scene_prop_get_slot, ":value", ":instance_id", scene_prop_is_used),
				(try_begin),
					(neq, ":value", 0),
					(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", scene_prop_is_used, ":value"),
				(try_end),
				
				(scene_prop_get_slot, ":value", ":instance_id", scene_prop_usage_state),
				(try_begin),
					(neq, ":value", 0),
					(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", scene_prop_usage_state, ":value"),
        (try_end),
				
				(try_for_range, ":cur_prop_slot", scene_prop_pos_x, scene_prop_default_rot_z +1),
					(scene_prop_get_slot, ":value", ":instance_id", ":cur_prop_slot"),
					(neq, ":value", 0),
					(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", ":cur_prop_slot", ":value"),
				(try_end),

				(try_begin),
					(eq, ":cur_prop", "spr_cannon_barrel_1_32lb"),
					(scene_prop_get_slot, ":value", ":instance_id", scene_prop_ammo_type),
					(neq, ":value", 0),
					(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", scene_prop_ammo_type, ":value"),
        (try_end),

				(try_begin),
					(is_between, ":cur_prop", "spr_cannon_base_1", "spr_cannons_end"),
					(try_for_range, ":cur_prop_slot", scene_prop_user_agent, scene_prop_slots_end),
						(scene_prop_get_slot, ":value", ":instance_id", ":cur_prop_slot"),
						(neq, ":value", -1),
						(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", ":cur_prop_slot", ":value"),
					(try_end),
				(try_end),
				
				(try_begin),
					(eq, ":cur_prop", "spr_cannon_ammo_box_32lb_1"),
					(try_for_range, ":cur_prop_slot", scene_prop_item_1_cur_ammo, scene_prop_item_2_cur_ammo +1),
						(scene_prop_get_slot, ":value", ":instance_id", ":cur_prop_slot"),
						(neq, ":value", 12),
						(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", ":cur_prop_slot", ":value"),
					(try_end),
        (try_end),
				
				(try_begin),
					(eq, ":cur_prop", "spr_round_shot_missile"),
					(try_for_range, ":cur_prop_slot", scene_prop_move_x, scene_prop_move_z +1),
						(scene_prop_get_slot, ":value", ":instance_id", ":cur_prop_slot"),
						(neq, ":value", 0),
						(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", ":cur_prop_slot", ":value"),
					(try_end),
					(scene_prop_get_slot, ":value", ":instance_id", scene_prop_time),
					(neq, ":value", 0),
					(multiplayer_send_3_int_to_player, ":player_no", multiplayer_event_client_set_prop_slot, ":instance_id", scene_prop_time, ":value"),
        (try_end),
			(try_end),
		(try_end),
#MOD end

     (try_begin),
       (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
			 (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),#MOD
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),

       (store_mission_timer_a, ":current_time"),
       (val_sub, ":current_time", "$g_round_start_time"),
       (val_mul, ":current_time", -1),

       (multiplayer_send_int_to_player, ":player_no", multiplayer_event_set_round_start_time, ":current_time"),
     (else_try),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
       #if game type is headquarters send number of agents placed around each pole's around to player.
       (try_for_range, ":flag_no", 0, "$g_number_of_flags"),
         (assign, ":number_of_agents_around_flag_team_1", 0),
         (assign, ":number_of_agents_around_flag_team_2", 0),

         (scene_prop_get_instance, ":pole_id", "spr_headquarters_pole_code_only", ":flag_no"), 
         (prop_instance_get_position, pos0, ":pole_id"), #pos0 holds pole position. 

         (try_for_agents, ":cur_agent"),
           (agent_is_human, ":cur_agent"),
           (agent_is_alive, ":cur_agent"),
           (neg|agent_is_non_player, ":cur_agent"),
           (agent_get_team, ":cur_agent_team", ":cur_agent"),
           (agent_get_position, pos1, ":cur_agent"), #pos1 holds agent's position.
           (get_sq_distance_between_positions, ":squared_dist", pos0, pos1),
           (get_sq_distance_between_position_heights, ":squared_height_dist", pos0, pos1),
           (val_add, ":squared_dist", ":squared_height_dist"),
           (lt, ":squared_dist", multi_headquarters_max_distance_sq_to_raise_flags),
           (try_begin),
             (eq, ":cur_agent_team", 0),
             (val_add, ":number_of_agents_around_flag_team_1", 1),
           (else_try),
             (eq, ":cur_agent_team", 1),
             (val_add, ":number_of_agents_around_flag_team_2", 1),
           (try_end),
         (try_end),

         (store_mul, ":current_owner_code", ":number_of_agents_around_flag_team_1", 100),
         (val_add, ":current_owner_code", ":number_of_agents_around_flag_team_2"),        
         (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_set_num_agents_around_flag, ":flag_no", ":current_owner_code"),
       (try_end),

       #if game type is headquarters send owners of each pole to player.
       # (assign, "$g_placing_initial_flags", 1),
       # (try_for_range, ":cur_flag", 0, "$g_number_of_flags"),
         # (store_add, ":cur_flag_slot", multi_data_flag_owner_begin, ":cur_flag"),
         # (troop_get_slot, ":cur_flag_owner", "trp_multiplayer_data", ":cur_flag_slot"),
         # (store_mul, ":cur_flag_owner_code", ":cur_flag_owner", 100),
         # (val_add, ":cur_flag_owner_code", ":cur_flag_owner"),
         # (val_add, ":cur_flag_owner_code", 1),
         # (val_mul, ":cur_flag_owner_code", -1),
         # (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_change_flag_owner, ":cur_flag", ":cur_flag_owner_code"),
       # (try_end),
       # (assign, "$g_placing_initial_flags", 0),
     (try_end),
     #(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_set_day_time, "$g_round_day_time"),
    ]),
  
  #script_multiplayer_init_mission_variables
  ("multiplayer_init_mission_variables",
   [
     (assign, "$g_multiplayer_team_1_first_spawn", 1),
     (assign, "$g_multiplayer_team_2_first_spawn", 1),
     (assign, "$g_multiplayer_poll_running", 0),
##     (assign, "$g_multiplayer_show_poll_when_suitable", 0),
     (assign, "$g_waiting_for_confirmation_to_terminate", 0),
     (assign, "$g_confirmation_result", 0),
     (assign, "$g_team_balance_next_round", 0),
     (team_get_faction, "$g_multiplayer_team_1_faction", 0),
     (team_get_faction, "$g_multiplayer_team_2_faction", 1),
     (assign, "$g_multiplayer_next_team_1_faction", "$g_multiplayer_team_1_faction"),
     (assign, "$g_multiplayer_next_team_2_faction", "$g_multiplayer_team_2_faction"),

     (assign, "$g_multiplayer_bot_type_1_wanted", 0),
     (assign, "$g_multiplayer_bot_type_2_wanted", 0),
     (assign, "$g_multiplayer_bot_type_3_wanted", 0),
     (assign, "$g_multiplayer_bot_type_4_wanted", 0),
#MOD begin
	 (assign, "$g_multiplayer_bot_type_5_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_6_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_7_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_8_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_9_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_10_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_11_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_12_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_13_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_14_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_15_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_16_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_17_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_18_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_19_wanted", 0),
	 (assign, "$g_multiplayer_bot_type_20_wanted", 0),
#MOD end
     # (call_script, "script_music_set_situation_with_culture", mtf_sit_multiplayer_fight),
     ]),

  #script_multiplayer_event_mission_end
  # Input: none
  # Output: none
  ("multiplayer_event_mission_end",
   [
     #EVERY_BREATH_YOU_TAKE achievement
     (try_begin),
       (multiplayer_get_my_player, ":my_player_no"),
       (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
       (player_get_kill_count, ":kill_count", ":my_player_no"),
       (player_get_death_count, ":death_count", ":my_player_no"),
       (gt, ":kill_count", ":death_count"),
       (unlock_achievement, ACHIEVEMENT_EVERY_BREATH_YOU_TAKE),
     (try_end),
     #EVERY_BREATH_YOU_TAKE achievement end
     ]),
  

  #script_multiplayer_event_agent_killed_or_wounded
  # Input: arg1 = dead_agent_no, arg2 = killer_agent_no
  # Output: none
  ("multiplayer_event_agent_killed_or_wounded",
   [
     (store_script_param, ":dead_agent_no", 1),
     #(store_script_param, ":killer_agent_no", 2),#MOD disable
     
     (multiplayer_get_my_player, ":my_player_no"),
     (try_begin),
       (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
       (player_get_agent_id, ":my_player_agent", ":my_player_no"),
       (ge, ":my_player_agent", 0),
       (try_begin),
         (eq, ":my_player_agent", ":dead_agent_no"),
         (store_mission_timer_a, "$g_multiplayer_respawn_start_time"),
       (try_end),
     (try_end),

     (try_begin),
       (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
       (player_get_agent_id, ":player_agent", ":my_player_no"),
       (eq, ":dead_agent_no", ":player_agent"),
     
       (assign, ":show_respawn_counter", 0),
       (try_begin),
         #TODO: add other game types with no respawns here
         (neq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
         (assign, ":show_respawn_counter", 1),
       (else_try),         
         (eq, "$g_multiplayer_player_respawn_as_bot", 1),
         (player_get_team_no, ":my_player_team", ":my_player_no"),
         (assign, ":is_found", 0),
         (try_for_agents, ":cur_agent"),
           (eq, ":is_found", 0),
           (agent_is_alive, ":cur_agent"),
           (agent_is_human, ":cur_agent"),
           (agent_is_non_player, ":cur_agent"),
           (agent_get_team ,":cur_team", ":cur_agent"),
           (eq, ":cur_team", ":my_player_team"),
           (assign, ":is_found", 1),
         (try_end),
         (eq, ":is_found", 1),
         (assign, ":show_respawn_counter", 1),
       (try_end),
       
       (try_begin),
         #(player_get_slot, ":spawn_count", ":player_no", slot_player_spawn_count),
         (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
         (gt, "$g_multiplayer_number_of_respawn_count", 0),         
     
         (ge, "$g_my_spawn_count", "$g_multiplayer_number_of_respawn_count"),
     
         (multiplayer_get_my_player, ":my_player_no"),
         (player_get_team_no, ":my_player_team", ":my_player_no"),

         (this_or_next|eq, ":my_player_team", 0),
         (ge, "$g_my_spawn_count", 999),
    
         (assign, "$g_show_no_more_respawns_remained", 1),
       (else_try),
         (assign, "$g_show_no_more_respawns_remained", 0),
       (try_end),

       (eq, ":show_respawn_counter", 1),             

       (start_presentation, "prsnt_multiplayer_respawn_time_counter"),
     (try_end),
     ]),

  #script_multiplayer_calculate_cur_selected_items_cost
  # Input: arg1 = player_no
  # Output: reg0: total_cost
  ("multiplayer_calculate_cur_selected_items_cost",
   [
     (store_script_param, ":player_no", 1),
     (store_script_param, ":calculation_type", 2), #0 for normal calculation
     (assign, ":total_cost", 0),
     # (player_get_troop_id, ":troop_no", ":player_no"),

     (try_begin),
       (eq, ":calculation_type", 0),
       (assign, ":begin_cond", slot_player_cur_selected_item_indices_begin),
       (assign, ":end_cond", slot_player_cur_selected_item_indices_end),
     (else_try),
       (assign, ":begin_cond", slot_player_selected_item_indices_begin),
       (assign, ":end_cond", slot_player_selected_item_indices_end),
     (try_end),
     
     (try_for_range, ":i_item", ":begin_cond", ":end_cond"),
       (player_get_slot, ":item_id", ":player_no", ":i_item"),
       (ge, ":item_id", 0), #might be -1 for horses etc.
       (store_item_value, reg0, ":item_id"),#MOD
       # (call_script, "script_multiplayer_get_item_value_for_troop", ":item_id", ":troop_no"),
       (val_add, ":total_cost", reg0),
     (try_end),
     (assign, reg0, ":total_cost"),
     ]),

  #script_multiplayer_set_item_available_for_troop
  # Input: arg1 = item_no, arg2 = troop_no
  # Output: none
  ("multiplayer_set_item_available_for_troop",
   [
     (store_script_param, ":item_no", 1),
     (store_script_param, ":troop_no", 2),
		 (store_script_param, ":value", 3),#MOD
     (store_sub, ":item_troop_slot", ":troop_no", multiplayer_troops_begin),
     (val_add, ":item_troop_slot", slot_item_multiplayer_availability_linked_list_begin),
     (item_set_slot, ":item_no", ":item_troop_slot", ":value"),#MOD edit
     ]),

  #script_multiplayer_send_item_selections
  # Input: none
  # Output: none
  ("multiplayer_send_item_selections",
   [
     (multiplayer_get_my_player, ":my_player_no"),
     (try_for_range, ":i_item", slot_player_selected_item_indices_begin, slot_player_selected_item_indices_end),
       (player_get_slot, ":item_id", ":my_player_no", ":i_item"),
       (multiplayer_send_2_int_to_server, multiplayer_event_set_item_selection, ":i_item", ":item_id"),
     (try_end),
     ]),

  #script_multiplayer_set_default_item_selections_for_troop
  # Input: arg1 = troop_no
  # Output: none
  ("multiplayer_set_default_item_selections_for_troop",
  [
    (store_script_param, ":troop_no", 1),
    (multiplayer_get_my_player, ":my_player_no"),
    (call_script, "script_multiplayer_clear_player_selected_items", ":my_player_no"),
		
    (troop_get_inventory_capacity, ":inv_cap", ":troop_no"),
    (try_for_range, ":i_slot", 0, ":inv_cap"),
      (troop_get_inventory_slot, ":item_id", ":troop_no", ":i_slot"),
      (ge, ":item_id", 0),
#MOD begin
			(assign, ":inv_slots_used", 0),
			(item_get_type, ":item_type", ":item_id"),
			
			(assign, ":end", 9),
			(try_for_range, ":i", 0, ":end"),
				(assign, ":continue", 0),
				(store_add, ":selected_item_slot", slot_player_selected_item_indices_begin, ":i"),
				(try_begin),
					(player_slot_eq, ":my_player_no", ":selected_item_slot", -1),#free slot
					(try_begin),
						(is_between, ":i", 0, 4),
						(try_begin),
							(this_or_next|is_between, ":item_type", itp_type_one_handed_wpn, itp_type_goods),
							(is_between, ":item_type", itp_type_pistol, itp_type_animal),
							(assign, ":continue", 1),
						(try_end),
					(else_try),
						(is_between, ":i", 4, 8),
						(try_begin),
							(is_between, ":item_type", itp_type_head_armor, itp_type_pistol),
							(try_begin),
								(eq, ":i", 4),
								(eq, ":item_type", itp_type_head_armor),
								(assign, ":continue", 1),
							(else_try),
								(eq, ":i", 5),
								(eq, ":item_type", itp_type_body_armor),
								(assign, ":continue", 1),
							(else_try),
								(eq, ":i", 6),
								(eq, ":item_type", itp_type_foot_armor),
								(assign, ":continue", 1),
							(else_try),
								(eq, ":i", 7),
								(eq, ":item_type", itp_type_hand_armor),
								(assign, ":continue", 1),
							(try_end),
						(try_end),
					(else_try),
						(eq, ":item_type", itp_type_horse),
						(assign, ":continue", 1),
					(try_end),
					(eq, ":continue", 1),
					(player_set_slot, ":my_player_no", ":selected_item_slot", ":item_id"),
					(assign, ":end", -1),#break loop
					
				(else_try),
					(val_add, ":inv_slots_used", 1),
					(eq, ":inv_slots_used", 9),#all slots used
					(assign, ":inv_cap", -1),#break loop
				(try_end),
			(try_end),
#MOD end
		(try_end),
  ]),

  # script_multiplayer_fill_map_game_types
  # Input: game_type
  # Output: num_maps
  ("multiplayer_fill_map_game_types",
    [
      (store_script_param, ":game_type", 1),
      (try_for_range, ":i_multi", multi_data_maps_for_game_type_begin, multi_data_maps_for_game_type_end),
        (troop_set_slot, "trp_multiplayer_data", ":i_multi", -1),
      (try_end),
      (assign, ":num_maps", 0),
#MOD begin
      (try_begin),
        (this_or_next|eq, ":game_type", multiplayer_game_type_deathmatch),
        (this_or_next|eq, ":game_type", multiplayer_game_type_duel),
        (this_or_next|eq, ":game_type", multiplayer_game_type_team_deathmatch),
				(eq, ":game_type", multiplayer_game_type_battle),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin, "scn_multi_scene_1"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 1, "scn_multi_scene_2"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 2, "scn_multi_scene_4"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 3, "scn_multi_scene_5"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 4, "scn_multi_scene_6"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 5, "scn_multi_scene_7"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 6, "scn_multi_scene_17"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 7, "scn_multi_scene_19"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 8, "scn_multi_scene_20"),
				
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 9, "scn_quick_battle_1"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 10, "scn_quick_battle_4"),

				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 11, "scn_quick_battle_scene_1"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 12, "scn_quick_battle_scene_2"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 13, "scn_quick_battle_scene_3"),
				
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 14, "scn_multi_outer_2"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 15, "scn_multi_outer_3"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 16, "scn_multi_outer_4"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 17, "scn_multi_outer_5"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 18, "scn_multi_outer_6"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 19, "scn_multi_outer_7"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 20, "scn_multi_outer_8"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 21, "scn_multi_outer_9"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 22, "scn_multi_outer_10"),
				
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 23, "scn_boe_map_1"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 24, "scn_multi_scene_22"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 25, "scn_multi_scene_21"),
				
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 26, "scn_random_scene_steppe"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 27, "scn_random_scene_plain"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 28, "scn_random_scene_snow"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 29, "scn_random_scene_desert"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 30, "scn_random_scene_steppe_forest"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 31, "scn_random_scene_plain_forest"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 32, "scn_random_scene_snow_forest"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 33, "scn_random_scene_desert_forest"),
				
        (assign, ":num_maps", 34),

      (else_try),
        (eq, ":game_type", multiplayer_game_type_siege),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin, "scn_multi_scene_3"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 1, "scn_multi_scene_8"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 2, "scn_multi_scene_10"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 3, "scn_multi_scene_15"),
        (troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 4, "scn_multi_scene_16"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 5, "scn_boe_castle_1"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 6, "scn_boe_castle_2"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 7, "scn_multi_outer_1"),
				(troop_set_slot, "trp_multiplayer_data", multi_data_maps_for_game_type_begin + 8, "scn_multi_scene_23"),
        (assign, ":num_maps", 9),
      (try_end),
#MOD end
      (assign, reg0, ":num_maps"),
      ]),

  
  # script_multiplayer_count_players_bots
  # Input: none
  # Output: none
  ("multiplayer_count_players_bots",
    [
      (get_max_players, ":num_players"),
      (try_for_range, ":cur_player", 0, ":num_players"),
        (player_is_active, ":cur_player"),
        (player_set_slot, ":cur_player", slot_player_last_bot_count, 0),
      (try_end),

      (try_for_agents, ":cur_agent"),
        (agent_is_human, ":cur_agent"),
        (agent_is_alive, ":cur_agent"),
        (agent_get_player_id, ":agent_player", ":cur_agent"),
        (lt, ":agent_player", 0), #not a player
        (agent_get_group, ":agent_group", ":cur_agent"),
        (player_is_active, ":agent_group"),
        (player_get_slot, ":bot_count", ":agent_group", slot_player_last_bot_count),
        (val_add, ":bot_count", 1),
        (player_set_slot, ":agent_group", slot_player_last_bot_count, ":bot_count"),
      (try_end),
      ]),

  # script_multiplayer_find_player_leader_for_bot
  # Input: arg1 = team_no
  # Output: reg0 = player_no
  ("multiplayer_find_player_leader_for_bot",
    [
      (store_script_param, ":team_no", 1),
      (store_script_param, ":look_only_actives", 2),

      (team_get_faction, ":team_faction", ":team_no"),
      (assign, ":num_ai_troops", 0),
      (try_for_range, ":cur_ai_troop", multiplayer_ai_troops_begin, multiplayer_ai_troops_end),
        (store_troop_faction, ":ai_troop_faction", ":cur_ai_troop"),
        (eq, ":ai_troop_faction", ":team_faction"),
        (val_add, ":num_ai_troops", 1),
      (try_end),

      (call_script, "script_multiplayer_count_players_bots"),

      (assign, ":team_player_count", 0),

      (get_max_players, ":num_players"),
      (try_for_range, ":cur_player", 0, ":num_players"),
        (assign, ":continue", 0),
        (player_is_active, ":cur_player"),          
        (try_begin),
          (eq, ":look_only_actives", 0),
          (assign, ":continue", 1),
        (else_try),
          (neq, ":look_only_actives", 0),
          (player_get_agent_id, ":cur_agent", ":cur_player"),
          (ge, ":cur_agent", 0),
          (agent_is_alive, ":cur_agent"),
          (assign, ":continue", 1),
        (try_end),

        (eq, ":continue", 1),
      
        (player_get_team_no, ":player_team", ":cur_player"),
        (eq, ":team_no", ":player_team"),
        (val_add, ":team_player_count", 1),
      (try_end),
      (assign, ":result_leader", -1),
      (try_begin),      
        (gt, ":team_player_count", 0),
        (assign, ":total_bot_count", "$g_multiplayer_num_bots_team_1"),
        (try_begin),
          (eq, ":team_no", 1),
          (assign, ":total_bot_count", "$g_multiplayer_num_bots_team_2"),
        (try_end),
        (store_div, ":num_bots_for_each_player", ":total_bot_count", ":team_player_count"),
        (store_mul, ":check_remainder", ":num_bots_for_each_player", ":team_player_count"),
        (try_begin),
          (lt, ":check_remainder", ":total_bot_count"),
          (val_add, ":num_bots_for_each_player", 1),
        (try_end),
      
        (assign, ":total_bot_req", 0),
        (try_for_range, ":cur_player", 0, ":num_players"),
          (player_is_active, ":cur_player"),

          (player_get_agent_id, ":cur_agent", ":cur_player"),
          (ge, ":cur_agent", 0),
          (agent_is_alive, ":cur_agent"),
      
          (player_get_team_no, ":player_team", ":cur_player"),
          (eq, ":team_no", ":player_team"),
          (assign, ":ai_wanted", 0),
          (store_add, ":end_cond", slot_player_bot_type_1_wanted, ":num_ai_troops"),
          (try_for_range, ":bot_type_wanted_slot", slot_player_bot_type_1_wanted, ":end_cond"),
            (player_slot_ge, ":cur_player", ":bot_type_wanted_slot", 1),
            (assign, ":ai_wanted", 1),
            (assign, ":end_cond", 0), #break
          (try_end),
          (eq, ":ai_wanted", 1),
          (player_get_slot, ":player_bot_count", ":cur_player", slot_player_last_bot_count),
          (lt, ":player_bot_count", ":num_bots_for_each_player"),
          (val_add, ":total_bot_req", ":num_bots_for_each_player"),
          (val_sub, ":total_bot_req", ":player_bot_count"),
        (try_end),
        (gt, ":total_bot_req", 0),
      
        (store_random_in_range, ":random_bot", 0, ":total_bot_req"),
        (try_for_range, ":cur_player", 0, ":num_players"),
          (player_is_active, ":cur_player"),

          (player_get_agent_id, ":cur_agent", ":cur_player"),
          (ge, ":cur_agent", 0),
          (agent_is_alive, ":cur_agent"),

          (player_get_team_no, ":player_team", ":cur_player"),
          (eq, ":team_no", ":player_team"),
          (assign, ":ai_wanted", 0),
          (store_add, ":end_cond", slot_player_bot_type_1_wanted, ":num_ai_troops"),
          (try_for_range, ":bot_type_wanted_slot", slot_player_bot_type_1_wanted, ":end_cond"),
            (player_slot_ge, ":cur_player", ":bot_type_wanted_slot", 1),
            (assign, ":ai_wanted", 1),
            (assign, ":end_cond", 0), #break
          (try_end),
          (eq, ":ai_wanted", 1),
          (player_get_slot, ":player_bot_count", ":cur_player", slot_player_last_bot_count),
          (lt, ":player_bot_count", ":num_bots_for_each_player"),
          (val_sub, ":random_bot", ":num_bots_for_each_player"),
          (val_add, ":random_bot", ":player_bot_count"),
          (lt, ":random_bot", 0),
          (assign, ":result_leader", ":cur_player"),
          (assign, ":num_players", 0), #break
        (try_end),
      (try_end),
      (assign, reg0, ":result_leader"),
      ]),

  # script_multiplayer_find_bot_troop_and_group_for_spawn
  # Input: arg1 = team_no
  # Output: reg0 = troop_id, reg1 = group_id
  ("multiplayer_find_bot_troop_and_group_for_spawn",
    [
      (store_script_param, ":team_no", 1),
      (store_script_param, ":look_only_actives", 2),

      (call_script, "script_multiplayer_find_player_leader_for_bot", ":team_no", ":look_only_actives"),
      (assign, ":leader_player", reg0),

      (assign, ":available_troops_in_faction", 0),
      (assign, ":available_troops_to_spawn", 0),
      (team_get_faction, ":team_faction_no", ":team_no"),

#MOD begin
			(assign, ":total_percentage", 1),
#MOD end
      (try_for_range, ":troop_no", multiplayer_ai_troops_begin, multiplayer_ai_troops_end),
        (store_troop_faction, ":troop_faction", ":troop_no"),
        (eq, ":troop_faction", ":team_faction_no"),
        (store_add, ":wanted_slot", slot_player_bot_type_1_wanted, ":available_troops_in_faction"),
        (val_add, ":available_troops_in_faction", 1),
        (try_begin),
          (this_or_next|lt, ":leader_player", 0),
          (player_slot_ge, ":leader_player", ":wanted_slot", 1),
          (val_add, ":available_troops_to_spawn", 1),
#MOD begin
					(troop_set_slot, ":troop_no", slot_troop_start_percentage, ":total_percentage"),
					(troop_get_slot, ":percentage", ":troop_no", slot_troop_army_percentage),
					# (assign, reg1, ":troop_no"),
					# (assign, reg2, ":percentage"),
					# (display_message, "@trp:{reg1} per:{reg2}"),
					(val_add, ":total_percentage", ":percentage"),
					(troop_set_slot, ":troop_no", slot_troop_end_percentage, ":total_percentage"),
#MOD end 
        (try_end),
      (try_end),

      (assign, ":available_troops_in_faction", 0),
#MOD begin
			(store_random_in_range, ":percentage", 1, ":total_percentage"),
      (assign, ":end_cond", multiplayer_ai_troops_end),
      (try_for_range, ":troop_no", multiplayer_ai_troops_begin, ":end_cond"),
        (store_troop_faction, ":troop_faction", ":troop_no"),
        (eq, ":troop_faction", ":team_faction_no"),
        (store_add, ":wanted_slot", slot_player_bot_type_1_wanted, ":available_troops_in_faction"),
				(val_add, ":available_troops_in_faction", 1),
        (this_or_next|lt, ":leader_player", 0),
        (player_slot_ge, ":leader_player", ":wanted_slot", 1),
				# (display_message, "@1"),
				(troop_get_slot, ":start_percentage", ":troop_no", slot_troop_start_percentage),
				(troop_get_slot, ":end_percentage", ":troop_no", slot_troop_end_percentage),
				
				# (assign, reg0, ":start_percentage"),
				# (assign, reg1, ":end_percentage"),
				# (assign, reg2, ":percentage"),
				# (display_message, "@start:{reg0} end:{reg1} cur:{reg2}"),
				
				(is_between, ":percentage", ":start_percentage", ":end_percentage"),
				# (display_message, "@2"),
        (assign, ":end_cond", 0),
        (assign, ":selected_troop", ":troop_no"),
      (try_end),	
#MOD end
      (assign, reg0, ":selected_troop"),
      (assign, reg1, ":leader_player"),
      ]),	

  # script_multiplayer_change_leader_of_bot
  # Input: arg1 = agent_no
  # Output: none
  ("multiplayer_change_leader_of_bot",
    [
      (store_script_param, ":agent_no", 1),
      (agent_get_team, ":team_no", ":agent_no"),
      (call_script, "script_multiplayer_find_player_leader_for_bot", ":team_no", 1),
      (assign, ":leader_player", reg0),
      (agent_set_group, ":agent_no", ":leader_player"),
      ]),
      
  ("multiplayer_find_spawn_point",
  [
     (store_script_param, ":team_no", 1),
     (store_script_param, ":examine_all_spawn_points", 2), #0-dm, 1-tdm, 2-cf, 3-hq, 4-sg
     (store_script_param, ":is_horseman", 3), #0:no, 1:yes, -1:do not care
     
     (set_fixed_point_multiplier, 100),
                   
     (assign, ":flags", 0),
     
     (try_begin),
       (eq, ":examine_all_spawn_points", 1),
       (val_or, ":flags", spf_examine_all_spawn_points),
     (try_end),
     
     (try_begin),
       (eq, ":is_horseman", 1),
       (val_or, ":flags", spf_is_horseman),
     (try_end),

     (try_begin),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
       (val_or, ":flags", spf_all_teams_are_enemy),
       (val_or, ":flags", spf_try_to_spawn_close_to_at_least_one_enemy),
     (else_try),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
       (val_or, ":flags", spf_try_to_spawn_close_to_at_least_one_enemy),       
     (else_try),
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
       (val_or, ":flags", spf_team_1_spawn_far_from_entry_66), #team 1 agents will not spawn 70 meters around of entry 0
       (val_or, ":flags", spf_team_0_walkers_spawn_at_high_points),
       (val_or, ":flags", spf_team_0_spawn_near_entry_66),       
       (val_or, ":flags", spf_care_agent_to_agent_distances_less),
     (try_end),     

     (multiplayer_find_spawn_point, reg0, ":team_no", ":flags"),
  ]),
    
  # script_multiplayer_find_spawn_point_2
  # Input: arg1 = team_no, arg2 = examine_all_spawn_points, arg3 = is_horseman
  # Output: reg0 = entry_point_no
  ("multiplayer_find_spawn_point_2",
   [
     (store_script_param, ":team_no", 1),
     (store_script_param, ":examine_all_spawn_points", 2), #0-dm, 1-tdm, 2-cf, 3-hq, 4-sg
     (store_script_param, ":is_horseman", 3), #0:no, 1:yes, -1:do not care

     (assign, ":best_entry_point_score", -10000000),
     (assign, ":best_entry_point", 0),

     (assign, ":num_operations", 0),

     (assign, ":num_human_agents_div_3_plus_one", 0),
     (try_begin), #counting number of agents
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
       (try_for_agents, ":i_agent"),
         (agent_is_alive, ":i_agent"),
         (agent_is_human, ":i_agent"),
         (val_add, ":num_human_agents_div_3_plus_one", 1),
       (try_end),
     (try_end),

     (assign, ":num_human_agents_plus_one", ":num_human_agents_div_3_plus_one"),

     (try_begin), 
       (le, ":num_human_agents_plus_one", 4),
       (assign, ":random_number_upper_limit", 2), #this is not typo-mistake this should be 2 too, not 1.
     (else_try), 
       (le, ":num_human_agents_plus_one", 8),
       (assign, ":random_number_upper_limit", 2),
     (else_try), 
       (le, ":num_human_agents_plus_one", 16),
       (assign, ":random_number_upper_limit", 3),
     (else_try),
       (le, ":num_human_agents_plus_one", 24),
       (assign, ":random_number_upper_limit", 4),
     (else_try),
       (le, ":num_human_agents_plus_one", 32),
       (assign, ":random_number_upper_limit", 5),
     (else_try),
       (le, ":num_human_agents_plus_one", 40),
       (assign, ":random_number_upper_limit", 6),
     (else_try),
       (assign, ":random_number_upper_limit", 7),
     (try_end),

     (val_div, ":num_human_agents_div_3_plus_one", 3),
     (val_add, ":num_human_agents_div_3_plus_one", 1),
     (store_mul, ":negative_num_human_agents_div_3_plus_one", ":num_human_agents_div_3_plus_one", -1),

     (try_begin),
       (eq, ":examine_all_spawn_points", 1),
       (assign, ":random_number_upper_limit", 1),
     (try_end),

     (assign, ":first_agent", 0),
     (try_begin), #first spawned agents will be spawned at their base points in tdm, cf and hq mods.
       (eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
       (try_begin),
         (eq, ":team_no", 0),
         (eq, "$g_multiplayer_team_1_first_spawn", 1),
         (assign, ":first_agent", 1),
         (assign, "$g_multiplayer_team_1_first_spawn", 0),
       (else_try),
         (eq, ":team_no", 1),
         (eq, "$g_multiplayer_team_2_first_spawn", 1),
         (assign, ":first_agent", 1),
         (assign, "$g_multiplayer_team_2_first_spawn", 0),
       (try_end),
     (try_end),
     
     (try_begin),
       (eq, ":first_agent", 1),
       (store_mul, ":best_entry_point", ":team_no", multi_num_valid_entry_points_div_2),
     (else_try),
       (try_for_range, ":i_entry_point", 0, multi_num_valid_entry_points),
         (assign, ":minimum_enemy_distance", 3000), 
         (assign, ":second_minimum_enemy_distance", 3000), 
              
         (assign, ":entry_point_score", 0),
         (store_random_in_range, ":random_value", 0, ":random_number_upper_limit"), #in average it is 5
         (eq, ":random_value", 0),
         (entry_point_get_position, pos0, ":i_entry_point"), #pos0 holds current entry point position
         (try_for_agents, ":i_agent"),
           (agent_is_alive, ":i_agent"),
           (agent_is_human, ":i_agent"),
           (agent_get_team, ":agent_team", ":i_agent"),     
           (try_begin),
             (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
             (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
             (try_begin),
               (teams_are_enemies, ":team_no", ":agent_team"),
               (assign, ":multiplier", -2),
             (else_try),
               (assign, ":multiplier", 1),
             (try_end),
           (else_try),
             (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
             (eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
             (assign, ":multiplier", -1), 
           (try_end),     
           (agent_get_position, pos1, ":i_agent"),
           (get_distance_between_positions_in_meters, ":distance", pos0, pos1),
           (val_add, ":num_operations", 1),
           (try_begin),
             (try_begin), #find closest enemy soldiers
               (lt, ":multiplier", 0),
               (try_begin),
                 (lt, ":distance", ":minimum_enemy_distance"),
                 (assign, ":second_minimum_enemy_distance", ":minimum_enemy_distance"),
                 (assign, ":minimum_enemy_distance", ":distance"),
               (else_try),
                 (lt, ":distance", ":second_minimum_enemy_distance"),
                 (assign, ":second_minimum_enemy_distance", ":distance"),
               (try_end),
             (try_end),
     
             (lt, ":distance", 100),
             (try_begin), #do not spawn over or too near to another agent (limit is 2 meters, squared 4 meters)
               (lt, ":distance", 3),
               (try_begin),
                 (this_or_next|eq, ":examine_all_spawn_points", 0),
                 (this_or_next|lt, ":multiplier", 0), #new added 20.08.08
                 (neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
                 (try_begin),
                   (lt, ":distance", 1),                 
                   (assign, ":dist_point", -1000000), #never place 
                 (else_try),
                   (lt, ":distance", 2),
                   (try_begin),
                     (lt, ":multiplier", 0), 
                     (assign, ":dist_point", -20000),
                   (else_try),
                     (assign, ":dist_point", -2000), #can place, friend and distance is between 1-2 meters
                   (try_end), 
                 (else_try),
                   (try_begin),
                     (lt, ":multiplier", 0), 
                     (assign, ":dist_point", -10000), 
                   (else_try),
                     (assign, ":dist_point", -1000), #can place, friend and distance is between 2-3 meters
                   (try_end),
                 (try_end),
               (else_try),
                 #if examinining all spawn points and mod is siege only. This happens in new round start placings.
                 (try_begin),
                   (lt, ":distance", 1),                 
                   (assign, ":dist_point", -20000), #very hard to place distance is < 1 meter
                 (else_try),
                   (lt, ":distance", 2),
                   (assign, ":dist_point", -2000),
                 (else_try),       
                   (assign, ":dist_point", -1000), #can place, distance is between 2-3 meters
                 (try_end),                 
               (try_end),
     
               (val_mul, ":dist_point", ":num_human_agents_div_3_plus_one"),                 
             (else_try),
               (assign, ":dist_point", 0),
               (this_or_next|neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
               (this_or_next|lt, ":multiplier", 0),
               (eq, ":team_no", 1), #only attackers are effected by positive enemy & friend distance at siege mod, defenders only get negative score effect a bit     

               (try_begin), #in siege give no positive or negative score to > 40m distance. (6400 = 10000 - 3600(60 * 60))
                 (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),

                 (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch), #new added after moving below part to above
                 (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_duel), #new added after moving below part to above
                 (eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch), #new added after moving below part to above

                 (store_sub, ":dist_point", multiplayer_spawn_min_enemy_dist_limit, ":distance"), #up to 40 meters give (positive(if friend) or negative(if enemy)) points
                 (val_max, ":dist_point", 0),
                 (val_mul, ":dist_point", ":dist_point"),
               (else_try),
                 (store_mul, ":one_and_half_limit", multiplayer_spawn_min_enemy_dist_limit, 3),
                 (val_div, ":one_and_half_limit", 2),
                 (store_sub, ":dist_point", ":one_and_half_limit", ":distance"), #up to 60 meters give (positive(if friend) or negative(if enemy)) points
                 (val_mul, ":dist_point", ":dist_point"),
               (try_end),
            
               (val_mul, ":dist_point", ":multiplier"),
             (try_end),
             (val_add, ":entry_point_score", ":dist_point"),
           (try_end),
         (try_end),

         (try_begin),
           (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
           (store_mul, ":max_enabled_agent_distance_score", 1000, ":num_human_agents_div_3_plus_one"),
           (ge, ":entry_point_score", ":max_enabled_agent_distance_score"),
           (assign, ":entry_point_score", ":max_enabled_agent_distance_score"),
         (try_end),

         (try_begin),      
           (neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),

           #(assign, ":minimum_enemy_dist_score", 0), #close also these with displays
           #(assign, ":second_minimum_enemy_dist_score", 0), #close also these with displays
           #(assign, reg2, ":minimum_enemy_distance"), #close also these with displays
           #(assign, reg3, ":second_minimum_enemy_distance"), #close also these with displays
          
           (try_begin), #if minimum enemy dist score is greater than 40(multiplayer_spawn_above_opt_enemy_dist_point) meters then give negative score
             (lt, ":minimum_enemy_distance", 3000), 
             (try_begin),
               (gt, ":minimum_enemy_distance", multiplayer_spawn_above_opt_enemy_dist_point),
               (val_sub, ":minimum_enemy_distance", multiplayer_spawn_above_opt_enemy_dist_point),
               (store_mul, ":minimum_enemy_dist_score", ":minimum_enemy_distance", -50),
               (val_mul, ":minimum_enemy_dist_score", ":num_human_agents_div_3_plus_one"),
               (val_add, ":entry_point_score", ":minimum_enemy_dist_score"),
             (try_end),
           (try_end),

           (try_begin), #if second minimum enemy dist score is greater than 40(multiplayer_spawn_above_opt_enemy_dist_point) meters then give negative score
             (lt, ":second_minimum_enemy_distance", 3000), #3000 x 3000
             (try_begin),
               (gt, ":second_minimum_enemy_distance", multiplayer_spawn_above_opt_enemy_dist_point),
               (val_sub, ":second_minimum_enemy_distance", multiplayer_spawn_above_opt_enemy_dist_point),
               (store_mul, ":second_minimum_enemy_dist_score", ":second_minimum_enemy_distance", -50),
               (val_mul, ":second_minimum_enemy_dist_score", ":num_human_agents_div_3_plus_one"),
               (val_add, ":entry_point_score", ":second_minimum_enemy_dist_score"),
             (try_end),
           (try_end),
           
           #(assign, reg0, ":minimum_enemy_dist_score"), #close also above assignment lines with these displays
           #(assign, reg1, ":second_minimum_enemy_dist_score"), #close also above assignment lines with these displays
           #(display_message, "@{!}minimum enemy distance : {reg2}, score : {reg0}"), #close also above assignment lines with these displays
           #(display_message, "@{!}second minimum enemy distance : {reg3}, score : {reg1}"), #close also above assignment lines with these displays
         (try_end),
 
         (try_begin), #giving positive points for "distance of entry point position to ground" while searching for entry point for defender team
           (neq, ":is_horseman", -1), #if being horseman or rider is not (not important)

           #additional score to entry points which has distance to ground value of > 0 meters
           (position_get_distance_to_terrain, ":height_to_terrain", pos0),     
           (val_max, ":height_to_terrain", 0),
           (val_min, ":height_to_terrain", 300),
           (ge, ":height_to_terrain", 40),                  
           
           (store_mul, ":height_to_terrain_score", ":height_to_terrain", ":num_human_agents_div_3_plus_one"), #it was 8
           
           (try_begin),
             (eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
             (val_mul, ":height_to_terrain_score", 16),
           (else_try),  
             (val_mul, ":height_to_terrain_score", 4),
           (try_end),
           
           (try_begin),
             (eq, ":is_horseman", 0),
             (try_begin),
               (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege), #but only in siege mod, defender infantries will get positive points for spawning in high places.
               (eq, ":team_no", 0), 
               (val_add, ":entry_point_score", ":height_to_terrain_score"),
             (try_end),
           (else_try),
             (val_mul, ":height_to_terrain_score", 5),
             (val_sub, ":entry_point_score", ":height_to_terrain_score"),
           (try_end),
         (try_end),
    
         (try_begin), #additional random entry point score at deathmatch, teamdethmatch, capture the flag and siege
           (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
           (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
           (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
           (eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
           (try_begin),
             (neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
             (store_random_in_range, ":random_value", 0, 400),
           (else_try),
             (eq, ":team_no", 1),
             (store_random_in_range, ":random_value", 0, 600), #siege-attacker
           (else_try),
             (store_random_in_range, ":random_value", 0, 200), #siege-defender
           (try_end),
           (val_mul, ":random_value", ":num_human_agents_div_3_plus_one"),
           (val_add, ":entry_point_score", ":random_value"),
         (try_end),

         (try_begin),
           (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),

             (try_begin), #siege
               (eq, ":team_no", 0),
               (entry_point_get_position, pos1, multi_siege_flag_point), #our base is at pos1 (it was multi_initial_spawn_point_team_1 changed at v622)
               (entry_point_get_position, pos2, multi_initial_spawn_point_team_2), #enemy base is at pos2
             (else_try),
               (entry_point_get_position, pos1, multi_initial_spawn_point_team_2), #our base is at pos2
               (entry_point_get_position, pos2, multi_siege_flag_point), #enemy base is at pos1 (it was multi_initial_spawn_point_team_1 changed at v622)
             (try_end),

           (try_begin),         
             (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
             (position_get_z, ":pos0_z", pos0),
             (position_set_z, pos1, ":pos0_z"), #make z of our base same with entry point position z
             (position_set_z, pos2, ":pos0_z"), #make z of enemy base same with entry point position z
           (try_end),
           
           (get_sq_distance_between_positions_in_meters, ":sq_dist_to_our_base", pos0, pos1),
           (get_sq_distance_between_positions_in_meters, ":sq_dist_to_enemy_base", pos0, pos2),                 
           (get_distance_between_positions_in_meters, ":dist_to_enemy_base", pos0, pos2),

           #give positive points if this entry point is near to our base.
           (assign, ":dist_to_our_base_point", 0),     
           (try_begin), #capture the flag (points for being near to base)
             (lt, ":sq_dist_to_our_base", 10000), #in siege give entry points score until 100m distance is reached
             (try_begin),
               (eq, ":team_no", 0),
               (try_begin),
                 (lt, ":sq_dist_to_our_base", 2500), #if distance is < 50m in siege give all highest point possible
                 (assign, ":sq_dist_to_our_base", 0),
               (else_try),
                 (val_sub, ":sq_dist_to_our_base", 2500),
                 (val_mul, ":sq_dist_to_our_base", 2),
               (try_end),
             (try_end),

             (store_sub, ":dist_to_our_base_point", 10000, ":sq_dist_to_our_base"),

             #can be (10000 - (10000 - 2500) * 2) = -5000 (for only defenders) so we are adding this loss.
             (val_add, ":dist_to_our_base_point", 5000), #so score getting from being near to base changes between 0 to 15000

             (try_begin),
               (eq, ":team_no", 0), 
             (else_try), #in siege mod for attackers being near to base entry point has 45 times less importance
               (val_div, ":dist_to_our_base_point", 45),
             (try_end),
             (val_mul, ":dist_to_our_base_point", ":num_human_agents_div_3_plus_one"),
           (try_end),

           (val_add, ":entry_point_score", ":dist_to_our_base_point"),


           #give negative points if this entry point is near to enemy base.
           (assign, ":dist_to_enemy_base_point", 0),
           (try_begin),
             (this_or_next|neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
             (eq, ":team_no", 1),

             (assign, ":dist_to_enemy_base_point", 0),
     
             (try_begin),
               (neq, "$g_multiplayer_game_type", multiplayer_game_type_siege),

               (try_begin),
                 (lt, ":sq_dist_to_enemy_base", 10000),
                 (store_sub, ":dist_to_enemy_base_point", 10000, ":sq_dist_to_enemy_base"),
                 (val_div, ":dist_to_enemy_base_point", 4),
                 (val_mul, ":dist_to_enemy_base_point", ":negative_num_human_agents_div_3_plus_one"),
               (try_end),
             (else_try),     
               (val_max, ":dist_to_enemy_base", 60), #<60 meters has all most negative score

               (try_begin),
                 (eq, ":is_horseman", 1),
                 (assign, ":optimal_distance", 120),
               (else_try),
                 (assign, ":optimal_distance", 80),
               (try_end),
     
               (try_begin),     
                 (le, ":dist_to_enemy_base", ":optimal_distance"),
                 (store_sub, ":dist_to_enemy_base_point", ":optimal_distance", ":dist_to_enemy_base"),
                 (val_mul, ":dist_to_enemy_base_point", 180), #-3600 max
               (else_try),
                 (store_sub, ":dist_to_enemy_base_point", ":dist_to_enemy_base", ":optimal_distance"),
                 (val_mul, ":dist_to_enemy_base_point", 30), #-unlimited max but lower slope
               (try_end),

               (val_sub, ":dist_to_enemy_base_point", 600),
               (val_max, ":dist_to_enemy_base_point", 0),
     
               (val_mul, ":dist_to_enemy_base_point", ":negative_num_human_agents_div_3_plus_one"),
             (try_end),             
           (try_end),

           (val_add, ":entry_point_score", ":dist_to_enemy_base_point"),
         (try_end),

         #(assign, reg1, ":i_entry_point"),
         #(assign, reg2, ":entry_point_score"),
         #(display_message, "@{!}entry_no : {reg1} , entry_score : {reg2}"),

         (gt, ":entry_point_score", ":best_entry_point_score"),
         (assign, ":best_entry_point_score", ":entry_point_score"),
         (assign, ":best_entry_point", ":i_entry_point"),         
       (try_end),

       #(assign, reg0, ":best_entry_point"), 
       #(assign, reg1, ":best_entry_point_score"),
       #(assign, reg2, ":num_operations"),       
       #(assign, reg7, ":is_horseman"),
       #(display_message, "@{!},is horse:{reg7}, best entry:{reg0}, best entry score:{reg1}, num_operations:{reg2}"),
     (try_end),
     (assign, reg0, ":best_entry_point"), 
     ]),
  
#MOD begin
  #script_multiplayer_buy_agent_equipment
  # Input: arg1 = player_no
  # Output: none
  ("multiplayer_buy_agent_equipment",
	[
		(store_script_param, ":player_no", 1),

    (player_get_gold, ":player_gold", ":player_no"),
    (player_get_slot, ":my_troop_no", ":player_no", slot_player_use_troop),
  
		#moving original values to temp slots
		(try_for_range, ":i_item", slot_player_selected_item_indices_begin, slot_player_selected_item_indices_end),
			(player_get_slot, ":selected_item_index", ":player_no", ":i_item"),
			(store_sub, ":i_cur_selected_item", ":i_item", slot_player_selected_item_indices_begin),
			(val_add, ":i_cur_selected_item", slot_player_cur_selected_item_indices_begin),
			(player_set_slot, ":player_no", ":i_cur_selected_item", ":selected_item_index"),
		(try_end),
		 
    (assign, ":total_cost", 0),
		(try_for_range, ":i_item", slot_player_cur_selected_item_indices_begin, slot_player_cur_selected_item_indices_end),
			(player_get_slot, ":item_id", ":player_no", ":i_item"),
			(try_begin),
				(ge, ":item_id", 0), #might be -1 for horses etc.
				(store_sub, ":item_slot", ":i_item", slot_player_cur_selected_item_indices_begin),
						
				(assign, ":add_item", 1),
				(try_begin),#check for same carry slots
					(is_between, ":item_slot", ek_item_0, ek_head),#weapon slots
					(item_get_slot, ":carry_slot", ":item_id", slot_item_carry_slot),
					(assign, ":end", ":item_slot"),
					(try_for_range, ":j", ek_item_0, ":end"),
						(store_add, ":cur_slot", slot_player_cur_selected_item_indices_begin, ":j"),
						(player_get_slot, ":cur_item", ":player_no", ":cur_slot"),
						(gt, ":cur_item", -1),
						(item_slot_eq, ":cur_item", slot_item_carry_slot, ":carry_slot"),
						(assign, ":add_item", 0),
						(assign, ":end", -1),#break loop
					(try_end),
				(else_try),#check riding difficulty
					(eq, ":item_slot", ek_horse),
					(player_get_troop_id, ":player_troop", ":player_no"),
					(gt, ":player_troop", -1),
					(player_get_slot, ":used_troop", ":player_no", slot_player_use_troop),
					(try_begin),
						(eq, ":used_troop", "trp_online_character"),
						(troop_get_slot, ":used_troop", ":player_troop", slot_troop_online_troop_backup),
					(try_end),
					(store_skill_level, ":skill_value", "skl_riding", ":used_troop"),
					(item_get_difficulty, ":difficulty", ":item_id"),
					(lt, ":skill_value", ":difficulty"),
					(assign, ":add_item", 0),
				(try_end),

        (try_begin),
          (neq, ":my_troop_no", "trp_online_character"),
          (store_item_value, ":item_value", ":item_id"),
          (val_add, ":total_cost", ":item_value"),
          (gt, ":total_cost", ":player_gold"),
          (val_sub, ":total_cost", ":item_value"),
          (assign, ":add_item", 0),
        (try_end),

        (try_begin),
          (neg|player_is_admin, ":player_no"),
          (gt, ":item_id", itm_admin_items_begin),
          (assign, ":add_item", 0),
        (try_end),

				(try_begin),
					(eq, ":add_item", 1),
					(player_add_spawn_item, ":player_no", ":item_slot", ":item_id"),
				(try_end),
				###
				#(assign, reg50, ":i_item"),
				#(assign, reg51, ":item_id"),
				#(display_message, "@add item to spawn: slot {reg50} item {reg51}", 0xCCCCCC),
				###
			(try_end),
		(try_end),

    (try_begin),
      (neq, ":my_troop_no", "trp_online_character"),
      (gt, ":total_cost", 0),
      (val_sub, ":player_gold", ":total_cost"),
      (player_set_gold, ":player_no", ":player_gold", 0),
    (try_end),
	]),
#MOD end

  ("game_get_party_prisoner_limit",[]),

  #script_game_get_item_extra_text:
  # This script is called from the game engine when an item's properties are displayed.
  # INPUT: arg1 = item_no, arg2 = extra_text_id (this can be between 0-7 (7 included)), arg3 = item_modifier
  # OUTPUT: result_string = item extra text, trigger_result = text color (0 for default)
  ("game_get_item_extra_text",
    [
      (store_script_param, ":item_no", 1),
      (store_script_param, ":extra_text_id", 2),
      # (store_script_param, ":item_modifier", 3),
			
      # (try_begin),
        # (is_between, ":item_no", food_begin, food_end),
        # (try_begin),
          # (eq, ":extra_text_id", 0),
          # (assign, ":continue", 1),
          # (try_begin),
            # (this_or_next|eq, ":item_no", "itm_cattle_meat"),
            # (this_or_next|eq, ":item_no", "itm_pork"),
						# (eq, ":item_no", "itm_chicken"),
				
            # (eq, ":item_modifier", imod_rotten),
            # (assign, ":continue", 0),
          # (try_end),
          # (eq, ":continue", 1),
          # (item_get_slot, ":food_bonus", ":item_no", slot_item_food_bonus),
          # (assign, reg1, ":food_bonus"),
          # (set_result_string, "@+{reg1} to party morale"),
          # (set_trigger_result, 0x4444FF),
        # (try_end),
      # (else_try),
        # (is_between, ":item_no", readable_books_begin, readable_books_end),
        # (try_begin),
          # (eq, ":extra_text_id", 0),
          # (item_get_slot, reg1, ":item_no", slot_item_intelligence_requirement),
          # (set_result_string, "@Requires {reg1} intelligence to read"),
          # (set_trigger_result, 0xFFEEDD),
        # (else_try),
          # (eq, ":extra_text_id", 1),
          # (item_get_slot, ":progress", ":item_no", slot_item_book_reading_progress),
          # (val_div, ":progress", 10),
          # (assign, reg1, ":progress"),
          # (set_result_string, "@Reading Progress: {reg1}%"),
          # (set_trigger_result, 0xFFEEDD),
        # (try_end),
      # (else_try),
        # (is_between, ":item_no", reference_books_begin, reference_books_end),
        # (try_begin),
          # (eq, ":extra_text_id", 0),
          # (try_begin),
            # (eq, ":item_no", "itm_book_wound_treatment_reference"),
            # (str_store_string, s1, "@wound treament"),
          # (else_try),
            # (eq, ":item_no", "itm_book_training_reference"),
            # (str_store_string, s1, "@trainer"),
          # (else_try),
            # (eq, ":item_no", "itm_book_surgery_reference"),
            # (str_store_string, s1, "@surgery"),
          # (try_end),
          # (set_result_string, "@+1 to {s1} while in inventory"),
          # (set_trigger_result, 0xFFEEDD),
        # (try_end),
#MOD begin
        (item_get_type, ":item_type", ":item_no"),
        (try_begin),
          (this_or_next|is_between, ":item_type", itp_type_bow, itp_type_goods),
          (this_or_next|eq, ":item_type", itp_type_pistol),
          (eq, ":item_type", itp_type_musket),
          (try_begin),
						(eq, ":extra_text_id", 0),
						(eq, ":item_type", itp_type_thrown),
						(item_get_accuracy, reg0, ":item_no"),
						(set_result_string, "@Accuracy: {reg0}"),
            (set_trigger_result, 0xe8b8f4),
          (try_end),
				(else_try),
					(eq, ":item_type", itp_type_shield),
					(try_begin),
            (eq, ":extra_text_id", 0),
						(item_get_thrust_damage, reg1, ":item_no"),
						(item_get_thrust_damage_type, ":dmg_type", ":item_no"),
            (try_begin),
							(eq, ":dmg_type", 0),
              (str_store_string, s1, "@c"),
            (else_try),
							(eq, ":dmg_type", 1),
              (str_store_string, s1, "@p"),
            (else_try),
              (str_store_string, s1, "@b"),
            (try_end),
						(set_result_string, "str_shield_damage_reg1_s1"),
            (set_trigger_result, 0xf6aa9a),
					(try_end),
        (try_end),
				
        (try_begin),
          (this_or_next|is_between, ":item_type", itp_type_one_handed_wpn, itp_type_goods),
          (is_between, ":item_type", itp_type_pistol, itp_type_animal),
          (try_begin),
            (eq, ":extra_text_id", 3),
            (item_get_slot, ":carry_slot", ":item_no", slot_item_carry_slot),
						(store_add, ":carry_string", "str_carry_slot_0", ":carry_slot"),
						(str_store_string, s1, ":carry_string"),
            (set_result_string, "str_carry_slot_display_s1"),
            (set_trigger_result, 0x80ffff),
          (try_end),
        (try_end),
				
				(try_begin),
					(eq, ":extra_text_id", 2),
					(try_begin),
						(neq, ":item_type", itp_type_horse),
						(neg|is_between, ":item_type", itp_type_arrows, itp_type_bow),
						(neq, ":item_type", itp_type_goods),
						(lt, ":item_type", itp_type_bullets),
						
						(item_get_hit_points, reg1, ":item_no"),
						(set_result_string, "@HP: {reg1}"),
            (set_trigger_result, 0xff6666),
					(try_end),
				(else_try),
					(eq, ":extra_text_id", 4),
					(try_begin),
						(eq, ":item_no", itm_grenade),
						(set_result_string, "str_info_grenade"),
					(else_try),
						(eq, ":item_no", itm_firepot),
						(set_result_string, "str_info_firepot"),
					(else_try),
						(eq, ":item_no", itm_engineer_hammer),
						(set_result_string, "str_info_engineer_hammer"),
					(else_try),
						(eq, ":item_no", itm_surgeon_kit),
						(set_result_string, "str_info_surgeon_kit"),
					(else_try),
						(eq, ":item_no", itm_missile_ignition_kit),
						(set_result_string, "str_info_missile_ignition_kit"),
					(else_try),
						(eq, ":item_no", itm_ladder_5m),
						(set_result_string, "str_info_ladder_5m"),
					(try_end),
					(set_trigger_result, 0xffff80),
				(try_end),
#MOD end
      # (try_end),
  ]),

  ("game_on_disembark",[]),
  ("game_context_menu_get_buttons",[]),
  ("game_event_context_menu_button_clicked",[]),
  ("game_get_skill_modifier_for_troop",[]),
	("game_check_party_sees_party",[]),
	("game_get_party_speed_multiplier",[]),
	("game_troop_upgrades_button_clicked",[]),
	("game_character_screen_requested",[]),

  #script_agent_troop_get_banner_mesh
  # INPUT: agent_no, troop_no
  # OUTPUT: banner_mesh
  ("agent_troop_get_banner_mesh",
    [
       (store_script_param, ":agent_no", 1),
       # (store_script_param, ":troop_no", 2),
       # (assign, ":banner_troop", -1),
       (assign, ":banner_mesh", "mesh_banners_default_b"),
       (try_begin),
         (lt, ":agent_no", 0),
         # (try_begin),
           # (ge, ":troop_no", 0),
           # (this_or_next|troop_slot_ge, ":troop_no", slot_troop_banner_scene_prop, 1),
           # (eq, ":troop_no", "trp_player"),
           # (assign, ":banner_troop", ":troop_no"),
         # (else_try),
           # (is_between, ":troop_no", companions_begin, companions_end),
           # (assign, ":banner_troop", "trp_player"),
         # (else_try),
           (assign, ":banner_mesh", "mesh_banners_default_a"),
         # (try_end),
       # (else_try),
         # (eq, "$g_is_quick_battle", 1),
         # (agent_get_team, ":agent_team", ":agent_no"),
         # (try_begin),
           # (eq, ":agent_team", 0),
           # (assign, ":banner_mesh", "$g_quick_battle_team_0_banner"),
         # (else_try),
           # (assign, ":banner_mesh", "$g_quick_battle_team_1_banner"),
         # (try_end),
       (else_try),
         (game_in_multiplayer_mode),
         (agent_get_group, ":agent_group", ":agent_no"),
         (try_begin),
           (neg|player_is_active, ":agent_group"),
           (agent_get_player_id, ":agent_group", ":agent_no"),
         (try_end),
         (try_begin),
           #if player banners are not allowed, use the default banner mesh
           (eq, "$g_multiplayer_allow_player_banners", 1),
           (player_is_active, ":agent_group"),
           (player_get_banner_id, ":player_banner", ":agent_group"),
					 (is_between, ":player_banner", 0, highest_clan_id),#MOD fix
           (store_add, ":banner_mesh", ":player_banner", banner_meshes_begin),
           (assign, ":already_used", 0),
           (try_for_range, ":cur_faction", npc_kingdoms_begin, npc_kingdoms_end), #wrong client data check
             (faction_slot_eq, ":cur_faction", slot_faction_banner, ":banner_mesh"),
             (assign, ":already_used", 1),
           (try_end),
           (eq, ":already_used", 0), #otherwise use the default banner mesh
         (else_try),
           (agent_get_team, ":agent_team", ":agent_no"),
           (team_get_faction, ":team_faction_no", ":agent_team"),

           (try_begin),
             (agent_is_human, ":agent_no"),
             (faction_get_slot, ":banner_mesh", ":team_faction_no", slot_faction_banner),
           (else_try),
             (agent_get_rider, ":rider_agent_no", ":agent_no"),
             #(agent_get_position, pos1, ":agent_no"),
             #(position_get_x, ":pos_x", pos1),
             #(position_get_y, ":pos_y", pos1),
             #(assign, reg0, ":pos_x"),
             #(assign, reg1, ":pos_y"),
             #(assign, reg2, ":agent_no"),
             #(display_message, "@{!}agent_no:{reg2}, pos_x:{reg0} , posy:{reg1}"),
             (try_begin),
               (ge, ":rider_agent_no", 0),
               (agent_is_active, ":rider_agent_no"),
               (agent_get_team, ":rider_agent_team", ":rider_agent_no"),
               (team_get_faction, ":rider_team_faction_no", ":rider_agent_team"),
               (faction_get_slot, ":banner_mesh", ":rider_team_faction_no", slot_faction_banner),
             (else_try),
               (assign, ":banner_mesh", "mesh_banners_default_c"),
             (try_end),                   
           (try_end),             
         (try_end),
       # (else_try),
         # (agent_get_troop_id, ":troop_id", ":agent_no"),
         # (this_or_next|troop_slot_ge,  ":troop_id", slot_troop_banner_scene_prop, 1),
         # (eq, ":troop_no", "trp_player"),
         # (assign, ":banner_troop", ":troop_id"),
       # (else_try),
         # (agent_get_party_id, ":agent_party", ":agent_no"),
         # (try_begin),
           # (lt, ":agent_party", 0),
           # (is_between, ":troop_id", companions_begin, companions_end),
           # (main_party_has_troop, ":troop_id"),
           # (assign, ":agent_party", "p_main_party"),
         # (try_end),
         # (ge, ":agent_party", 0),
         # (party_get_template_id, ":party_template", ":agent_party"),
         # (try_begin),
           # (eq, ":party_template", "pt_deserters"),
           # (assign, ":banner_mesh", "mesh_banners_default_c"),
         # (else_try),
           # (is_between, ":agent_party", centers_begin, centers_end),
           # (is_between, ":troop_id", companions_begin, companions_end),
           # (neq, "$talk_context", tc_tavern_talk),
           # # this should be a captured companion in prison
           # (assign, ":banner_troop", "trp_player"),
         # (else_try),
           # (is_between, ":agent_party", centers_begin, centers_end),
           # (party_get_slot, ":town_lord", "$g_encountered_party", slot_town_lord),
           # (ge, ":town_lord", 0),
           # (assign, ":banner_troop", ":town_lord"),
         # (else_try),
           # (this_or_next|party_slot_eq, ":agent_party", slot_party_type, spt_kingdom_hero_party),
           # (eq, ":agent_party", "p_main_party"),
           # (party_get_num_companion_stacks, ":num_stacks", ":agent_party"),
           # (gt, ":num_stacks", 0),
           # (party_stack_get_troop_id, ":leader_troop_id", ":agent_party", 0),
           # (this_or_next|troop_slot_ge,  ":leader_troop_id", slot_troop_banner_scene_prop, 1),
           # (eq, ":leader_troop_id", "trp_player"),
           # (assign, ":banner_troop", ":leader_troop_id"),
         # (try_end),
       # (else_try), #Check if we are in a tavern
         # (eq, "$talk_context", tc_tavern_talk),
         # (neq, ":troop_no", "trp_player"),
         # (assign, ":banner_mesh", "mesh_banners_default_d"),
       # (else_try), #can't find party, this can be a town guard
         # (neq, ":troop_no", "trp_player"),
         # (is_between, "$g_encountered_party", walled_centers_begin, walled_centers_end),
         # (party_get_slot, ":town_lord", "$g_encountered_party", slot_town_lord),
         # (ge, ":town_lord", 0),
         # (assign, ":banner_troop", ":town_lord"),
       (try_end),
#MOD disable
       # (try_begin),
         # (ge, ":banner_troop", 0),
         # (try_begin),
           # (neg|troop_slot_ge, ":banner_troop", slot_troop_banner_scene_prop, 1),
           # (assign, ":banner_mesh", "mesh_banners_default_b"),
         # (else_try), 
           # (troop_get_slot, ":banner_spr", ":banner_troop", slot_troop_banner_scene_prop),
           # (store_add, ":banner_scene_props_end", banner_scene_props_end_minus_one, 1),
           # (is_between, ":banner_spr", banner_scene_props_begin, ":banner_scene_props_end"),
           # (val_sub, ":banner_spr", banner_scene_props_begin),
           # (store_add, ":banner_mesh", ":banner_spr", banner_meshes_begin),
         # (try_end),
       # (try_end),
       (assign, reg0, ":banner_mesh"),
     ]),
		 
#MOD begin	
  #script_item_init_banner
  ("item_init_banner",
	[
		(store_script_param, ":tableau_no", 1),
		(store_script_param, ":agent_no", 2),
		(store_script_param, ":troop_no", 3),
		
		(try_begin),
			(agent_is_active, ":agent_no"),
			(agent_get_team, ":agent_team", ":agent_no"),
			(team_get_faction, ":team_faction_no", ":agent_team"),
			(faction_get_slot, ":value", ":team_faction_no", slot_faction_flag),
			(val_mul, ":value", 1000),
			(call_script, "script_agent_troop_get_banner_mesh", ":agent_no", ":troop_no"),
			(val_add, ":value", reg0),
		(else_try),
			(assign, ":value", -1),
		(try_end),
    (cur_item_set_tableau_material, ":tableau_no", ":value"),
	]),

  #script_item_set_banner
  ("item_set_banner",
	[
		(store_script_param, ":value", 1),
		(store_script_param, ":tableau_mesh", 2),
		(store_script_param, ":flag_scale", 3),
		(store_script_param, ":banner_scale", 4),
		(store_script_param, ":pos_x", 5),
		(store_script_param, ":flag_pos_y", 6),
		(store_script_param, ":banner_pos_y", 7),
		
		(set_fixed_point_multiplier, 100),
		(try_begin),
			(eq, ":value", -1),
			(cur_tableau_set_background_color, 0xffffff),
		(else_try),
			(store_div, ":flag_mesh", ":value", 1000),
			(store_mul, ":offset", ":flag_mesh", 1000),
			(store_sub, ":banner_mesh", ":value", ":offset"),
			(try_begin),
				(gt, ":flag_scale", -1),
				(gt, ":banner_scale", -1),
				
				(init_position, pos1),
				(position_set_x, pos1, ":pos_x"),
				(position_set_y, pos1, ":flag_pos_y"),
				(cur_tableau_add_mesh, ":flag_mesh", pos1, ":flag_scale", 0),
				
				(init_position, pos1),
				(position_set_x, pos1, ":pos_x"),
				(position_set_y, pos1, ":banner_pos_y"),
				(position_set_z, pos1, 50),
				(cur_tableau_add_mesh, ":banner_mesh", pos1, ":banner_scale", 100),
			(else_try),
				(store_sub, ":background_slot", ":flag_mesh", "mesh_flag_kingdom_01"),
				(val_add, ":background_slot", 135),
				(troop_get_slot, ":background_color", "trp_banner_background_color_array", ":background_slot"),
				(cur_tableau_set_background_color, ":background_color"),
			(try_end),
		(try_end),

		(init_position, pos1),
		(position_set_z, pos1, 100),
		(cur_tableau_add_mesh, ":tableau_mesh", pos1, 0, 0),
		(try_begin),
			(is_between, ":tableau_mesh", "mesh_tableau_mesh_shield_round_1", "mesh_tableau_mesh_shield_kite_4"),
			(cur_tableau_set_camera_parameters, 0, 200, 100, 0, 100000),
		(else_try),
			(cur_tableau_set_camera_parameters, 0, 200, 200, 0, 100000),
		(try_end),
	]),
#MOD end
  
  #script_troop_agent_set_banner
  # INPUT: agent_no
  # OUTPUT: none
  ("troop_agent_set_banner",
    [
       (store_script_param, ":tableau_no",1),
       (store_script_param, ":agent_no", 2),
       (store_script_param, ":troop_no", 3),
       (call_script, "script_agent_troop_get_banner_mesh", ":agent_no", ":troop_no"),
       (cur_agent_set_banner_tableau_material, ":tableau_no", reg0),
     ]),

  #script_add_troop_to_cur_tableau
  # INPUT: troop_no
  # OUTPUT: none
  ("add_troop_to_cur_tableau",
    [
       (store_script_param, ":troop_no",1),

       (set_fixed_point_multiplier, 100),
       (assign, ":banner_mesh", -1),
#MOD disable
       # (troop_get_slot, ":banner_spr", ":troop_no", slot_troop_banner_scene_prop),
       # (store_add, ":banner_scene_props_end", banner_scene_props_end_minus_one, 1),
       # (try_begin),
         # (is_between, ":banner_spr", banner_scene_props_begin, ":banner_scene_props_end"),
         # (val_sub, ":banner_spr", banner_scene_props_begin),
         # (store_add, ":banner_mesh", ":banner_spr", banner_meshes_begin),
       # (try_end),

       (cur_tableau_clear_override_items),
       
#       (cur_tableau_set_override_flags, af_override_fullhelm),
       (cur_tableau_set_override_flags, af_override_head|af_override_weapons),
       
       (init_position, pos2),
       (cur_tableau_set_camera_parameters, 1, 6, 6, 10, 10000),

       (init_position, pos5),
       (assign, ":eye_height", 162),
       (store_mul, ":camera_distance", ":troop_no", 87323),
#       (val_mod, ":camera_distance", 5),
       (assign, ":camera_distance", 139),
       (store_mul, ":camera_yaw", ":troop_no", 124337),
       (val_mod, ":camera_yaw", 50),
       (val_add, ":camera_yaw", -25),
       (store_mul, ":camera_pitch", ":troop_no", 98123),
       (val_mod, ":camera_pitch", 20),
       (val_add, ":camera_pitch", -14),
       (assign, ":animation", "anim_stand_man"),
       
##       (troop_get_inventory_slot, ":horse_item", ":troop_no", ek_horse),
##       (try_begin),
##         (gt, ":horse_item", 0),
##         (assign, ":eye_height", 210),
##         (cur_tableau_add_horse, ":horse_item", pos2, anim_horse_stand, 0),
##         (assign, ":animation", anim_ride_0),
##         (position_set_z, pos5, 125),
##         (try_begin),
##           (is_between, ":camera_yaw", -10, 10), #make sure horse head doesn't obstruct face.
##           (val_min, ":camera_pitch", -5),
##         (try_end),
##       (try_end),
       (position_set_z, pos5, ":eye_height"),

       # camera looks towards -z axis
       (position_rotate_x, pos5, -90),
       (position_rotate_z, pos5, 180),

       # now apply yaw and pitch
       (position_rotate_y, pos5, ":camera_yaw"),
       (position_rotate_x, pos5, ":camera_pitch"),
       (position_move_z, pos5, ":camera_distance", 0),
       (position_move_x, pos5, 5, 0),

       (try_begin),
         (ge, ":banner_mesh", 0),

         (init_position, pos1),
         (position_set_z, pos1, -1500),
         (position_set_x, pos1, 265),
         (position_set_y, pos1, 400),
         (position_transform_position_to_parent, pos3, pos5, pos1),
         (cur_tableau_add_mesh, ":banner_mesh", pos3, 400, 0),
       (try_end),
       (cur_tableau_add_troop, ":troop_no", pos2, ":animation" , 0),

       (cur_tableau_set_camera_position, pos5),

       (copy_position, pos8, pos5),
       (position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
       (position_rotate_z, pos8, 30), 
       (position_rotate_x, pos8, -60), 
       (cur_tableau_add_sun_light, pos8, 175,150,125),
     ]),

  #script_add_troop_to_cur_tableau_for_character
  # INPUT: troop_no
  # OUTPUT: none
  ("add_troop_to_cur_tableau_for_character",
    [
       (store_script_param, ":troop_no",1),

       (set_fixed_point_multiplier, 100),

       (cur_tableau_clear_override_items),
       (cur_tableau_set_override_flags, af_override_fullhelm),
##       (cur_tableau_set_override_flags, af_override_head|af_override_weapons),
       
       (init_position, pos2),
       (cur_tableau_set_camera_parameters, 1, 4, 8, 10, 10000),

       (init_position, pos5),
       (assign, ":cam_height", 150),
#       (val_mod, ":camera_distance", 5),
       (assign, ":camera_distance", 360),
       (assign, ":camera_yaw", -15),
       (assign, ":camera_pitch", -18),
       (assign, ":animation", anim_stand_man),
       
       (position_set_z, pos5, ":cam_height"),

       # camera looks towards -z axis
       (position_rotate_x, pos5, -90),
       (position_rotate_z, pos5, 180),

       # now apply yaw and pitch
       (position_rotate_y, pos5, ":camera_yaw"),
       (position_rotate_x, pos5, ":camera_pitch"),
       (position_move_z, pos5, ":camera_distance", 0),
       (position_move_x, pos5, 5, 0),

       (try_begin),
         (troop_is_hero, ":troop_no"),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
       (else_try),
         (store_mul, ":random_seed", ":troop_no", 126233),
         (val_mod, ":random_seed", 1000),
         (val_add, ":random_seed", 1),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
       (try_end),
       (cur_tableau_set_camera_position, pos5),

       (copy_position, pos8, pos5),
       (position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
       (position_rotate_z, pos8, 30), 
       (position_rotate_x, pos8, -60), 
       (cur_tableau_add_sun_light, pos8, 175,150,125),
     ]),

  #script_add_troop_to_cur_tableau_for_inventory
  # INPUT: troop_no
  # OUTPUT: none
  ("add_troop_to_cur_tableau_for_inventory",
    [
       (store_script_param, ":troop_no",1),
       (store_mod, ":side", ":troop_no", 4), #side flag is inside troop_no value
       (val_div, ":troop_no", 4), #removing the flag bit
       (val_mul, ":side", 90), #to degrees

       (set_fixed_point_multiplier, 100),

       (cur_tableau_clear_override_items),
       
       (init_position, pos2),
       (position_rotate_z, pos2, ":side"),
       (cur_tableau_set_camera_parameters, 1, 4, 6, 10, 10000),

       (init_position, pos5),
       (assign, ":cam_height", 105),
#       (val_mod, ":camera_distance", 5),
       (assign, ":camera_distance", 380),
       (assign, ":camera_yaw", -15),
       (assign, ":camera_pitch", -18),
       (assign, ":animation", anim_stand_man),
       
       (position_set_z, pos5, ":cam_height"),

       # camera looks towards -z axis
       (position_rotate_x, pos5, -90),
       (position_rotate_z, pos5, 180),

       # now apply yaw and pitch
       (position_rotate_y, pos5, ":camera_yaw"),
       (position_rotate_x, pos5, ":camera_pitch"),
       (position_move_z, pos5, ":camera_distance", 0),
       (position_move_x, pos5, 5, 0),

       (try_begin),
         (troop_is_hero, ":troop_no"),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
       (else_try),
         (store_mul, ":random_seed", ":troop_no", 126233),
         (val_mod, ":random_seed", 1000),
         (val_add, ":random_seed", 1),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
       (try_end),
       (cur_tableau_set_camera_position, pos5),

       (copy_position, pos8, pos5),
       (position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
       (position_rotate_z, pos8, 30), 
       (position_rotate_x, pos8, -60), 
       (cur_tableau_add_sun_light, pos8, 175,150,125),
     ]),

  #script_add_troop_to_cur_tableau_for_profile
  # INPUT: troop_no
  # OUTPUT: none
  ("add_troop_to_cur_tableau_for_profile",
    [
       (store_script_param, ":troop_no",1),

       (set_fixed_point_multiplier, 100),

       (cur_tableau_clear_override_items),
       
       (cur_tableau_set_camera_parameters, 1, 4, 6, 10, 10000),

       (init_position, pos5),
       (assign, ":cam_height", 105),
#       (val_mod, ":camera_distance", 5),
       (assign, ":camera_distance", 380),
       (assign, ":camera_yaw", -15),
       (assign, ":camera_pitch", -18),
       (assign, ":animation", anim_stand_man),
       
       (position_set_z, pos5, ":cam_height"),

       # camera looks towards -z axis
       (position_rotate_x, pos5, -90),
       (position_rotate_z, pos5, 180),

       # now apply yaw and pitch
       (position_rotate_y, pos5, ":camera_yaw"),
       (position_rotate_x, pos5, ":camera_pitch"),
       (position_move_z, pos5, ":camera_distance", 0),
       (position_move_x, pos5, 5, 0),

       (profile_get_banner_id, ":profile_banner"),
#MOD begin
			 # (assign, reg1, ":profile_banner"),
			 # (display_message, "@{reg1}"),
       (try_begin),
         (is_between, ":profile_banner", 0, highest_clan_id),
				 (val_add, ":profile_banner", banner_meshes_begin),
			 (else_try),
				 (assign, ":profile_banner", banner_meshes_end_minus_one),
			 (try_end),
			 (init_position, pos2),
       (position_set_x, pos2, -175),
       (position_set_y, pos2, -300),
       (position_set_z, pos2, 180),
       (position_rotate_x, pos2, 90),
       (position_rotate_y, pos2, -15),
       (cur_tableau_add_mesh, ":profile_banner", pos2, 0, 0),
#MOD end       

       (init_position, pos2),
       (try_begin),
         (troop_is_hero, ":troop_no"),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
       (else_try),
         (store_mul, ":random_seed", ":troop_no", 126233),
         (val_mod, ":random_seed", 1000),
         (val_add, ":random_seed", 1),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
       (try_end),
       (cur_tableau_set_camera_position, pos5),

       (copy_position, pos8, pos5),
       (position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
       (position_rotate_z, pos8, 30), 
       (position_rotate_x, pos8, -60), 
       (cur_tableau_add_sun_light, pos8, 175,150,125),
     ]),
  
  #script_add_troop_to_cur_tableau_for_party
  # INPUT: troop_no
  # OUTPUT: none
  ("add_troop_to_cur_tableau_for_party",
    [
       (store_script_param, ":troop_no",1),
       (store_mod, ":hide_weapons", ":troop_no", 2), #hide_weapons flag is inside troop_no value
       (val_div, ":troop_no", 2), #removing the flag bit

       (set_fixed_point_multiplier, 100),

       (cur_tableau_clear_override_items),
       (try_begin),
         (eq, ":hide_weapons", 1),
         (cur_tableau_set_override_flags, af_override_fullhelm|af_override_head|af_override_weapons),
       (try_end),
       
       (init_position, pos2),
       (cur_tableau_set_camera_parameters, 1, 6, 6, 10, 10000),

       (init_position, pos5),
       (assign, ":cam_height", 105),
#       (val_mod, ":camera_distance", 5),
       (assign, ":camera_distance", 450),
       (assign, ":camera_yaw", 15),
       (assign, ":camera_pitch", -18),
       (assign, ":animation", anim_stand_man),
       
       (troop_get_inventory_slot, ":horse_item", ":troop_no", ek_horse),
       (try_begin),
         (gt, ":horse_item", 0),
         (eq, ":hide_weapons", 0),
         (cur_tableau_add_horse, ":horse_item", pos2, "anim_horse_stand", 0),
         (assign, ":animation", "anim_ride_0"),
         (assign, ":camera_yaw", 23),
         (assign, ":cam_height", 150),
         (assign, ":camera_distance", 550),
       (try_end),
       (position_set_z, pos5, ":cam_height"),

       # camera looks towards -z axis
       (position_rotate_x, pos5, -90),
       (position_rotate_z, pos5, 180),

       # now apply yaw and pitch
       (position_rotate_y, pos5, ":camera_yaw"),
       (position_rotate_x, pos5, ":camera_pitch"),
       (position_move_z, pos5, ":camera_distance", 0),
       (position_move_x, pos5, 5, 0),

       (try_begin),
         (troop_is_hero, ":troop_no"),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", -1),
       (else_try),
         (store_mul, ":random_seed", ":troop_no", 126233),
         (val_mod, ":random_seed", 1000),
         (val_add, ":random_seed", 1),
         (cur_tableau_add_troop, ":troop_no", pos2, ":animation", ":random_seed"),
       (try_end),
       (cur_tableau_set_camera_position, pos5),

       (copy_position, pos8, pos5),
       (position_rotate_x, pos8, -90), #y axis aligned with camera now. z is up
       (position_rotate_z, pos8, 30), 
       (position_rotate_x, pos8, -60), 
       (cur_tableau_add_sun_light, pos8, 175,150,125),
     ]),

#MOD begin
  # script_game_missile_launch
  # Input: arg1 = shooter_agent_id, arg2 = weapon_item_id, pos1 = weapon_item_position
  # Output: none 
  ("game_missile_launch",
  [
    (store_script_param, ":shooter_agent", 1),
    (store_script_param, ":item_id", 2),
		
		# (assign, reg1, ":shooter_agent"),
		# (assign, reg2, ":item_id"),
		# (display_message, "@agent: {reg1} item: {reg2}"),
		
		(try_begin),
			(gt, ":item_id", -1),
			(item_get_type, ":item_type", ":item_id"),
			
			#ignited missiles
			(try_begin),
				(this_or_next|multiplayer_is_server),
				(neg|game_in_multiplayer_mode),
				
				(neg|agent_is_non_player, ":shooter_agent"),
				#upkeep
				(try_begin),
					(multiplayer_is_dedicated_server),
					(neq, ":item_type", itp_type_thrown),
					(item_get_thrust_damage, ":usage", ":item_id"),
					(val_div, ":usage", 10),
					(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":shooter_agent", ":item_id", ":usage"),
				(try_end),
				
				(assign, ":value", 0),
				
				(try_begin),
					(eq, ":item_id", itm_missile_ignition_kit),
					(agent_slot_eq, ":shooter_agent", slot_agent_next_shot_is_alternative, 0),
					(agent_set_slot, ":shooter_agent", slot_agent_next_shot_is_alternative, 1),
					(assign, ":value", 1),
				(else_try),
					(is_between, ":item_type", itp_type_bow, itp_type_thrown),
					(agent_set_slot, ":shooter_agent", slot_agent_next_shot_is_alternative, 0),
					(assign, ":value", -1),
				(try_end),
				
				(neq, ":value", 0),
				(assign, ":continue", 0),
				(assign, ":end_item_slot", ek_head),
				(try_for_range, ":cur_item_slot", ek_item_0, ":end_item_slot"),
					(agent_get_item_slot, ":cur_item", ":shooter_agent", ":cur_item_slot"),
					(gt, ":cur_item", -1),
					(try_begin),
						(eq, ":value", 1),
						(item_get_type, ":cur_item_type", ":cur_item"),
						(is_between, ":cur_item_type", itp_type_arrows, itp_type_shield),
						(assign, ":continue", 1),
					(else_try),
						(eq, ":value", -1),
						(item_slot_ge, ":cur_item", slot_item_is_alt_mode, 1),
						(assign, ":continue", 1),
					(try_end),
					
					(eq, ":continue", 1),
					(agent_get_ammo_for_slot, ":last_ammo_amount", ":shooter_agent", ":cur_item_slot"),
					(try_begin),
						(gt, ":last_ammo_amount", 0),
						(store_add, ":next_item", ":cur_item", ":value"),
						(store_add, ":slot_no", ":cur_item_slot", 1),
						(agent_unequip_item, ":shooter_agent", ":cur_item", ":slot_no"),
						(agent_equip_item, ":shooter_agent", ":next_item", ":slot_no"),
						(agent_set_ammo, ":shooter_agent", ":next_item", ":last_ammo_amount"),
					(try_end),
					(assign, ":end_item_slot", -1),#break loop
				(try_end),
			(try_end),
			
			(try_begin),
				(neg|multiplayer_is_dedicated_server),#only client side
				(try_begin),
					(eq, "$g_show_shot_distance", 1),
					(call_script, "script_client_get_player_agent"),
					(assign, ":player_agent", reg0),
					(eq, ":player_agent", ":shooter_agent"),
					(set_fixed_point_multiplier, 100),
					(position_get_x, ":pos_x", pos1),
					(position_get_y, ":pos_y", pos1),
					(position_get_z, ":pos_z", pos1),
					(agent_set_slot, ":shooter_agent", slot_agent_shot_pos_x, ":pos_x"),
					(agent_set_slot, ":shooter_agent", slot_agent_shot_pos_y, ":pos_y"),
					(agent_set_slot, ":shooter_agent", slot_agent_shot_pos_z, ":pos_z"),
				(try_end),

				(try_begin),#stamina
					(this_or_next|is_between, ":item_type", itp_type_crossbow, itp_type_goods),
					(is_between, ":item_type", itp_type_pistol, itp_type_bullets),
					(call_script, "script_client_get_player_agent"),
					(eq, reg0, ":shooter_agent"),
					(call_script, "script_agent_lose_stamina", ":shooter_agent", stamina_attack),
				(try_end),
				(try_begin),
					(is_between, ":item_type", itp_type_pistol, itp_type_bullets),
					(try_begin),
						(this_or_next|is_between, ":item_id", itm_handgonne_a, itm_barbed_arrows +1),
						(this_or_next|is_between, ":item_id", itm_handgonne_a +loom_1_item, itm_barbed_arrows +loom_1_item +1),
						(this_or_next|is_between, ":item_id", itm_handgonne_a +loom_2_item, itm_barbed_arrows +loom_2_item +1),
						(this_or_next|is_between, ":item_id", itm_handgonne_a +loom_3_item, itm_barbed_arrows +loom_3_item +1),
						(eq, ":item_id", itm_autofire_musket),

						(assign, ":flashpan_pos_x", 0),
						(assign, ":flashpan_pos_y", 0),
						(assign, ":flashpan_pos_z", 0),
						(assign, ":muzzle_pos_x", 0),
						(assign, ":muzzle_pos_y", 0),
						(assign, ":muzzle_pos_z", 0),
						(try_begin),
							(this_or_next|eq, ":item_id", itm_handgonne_a),
							(this_or_next|eq, ":item_id", itm_handgonne_a +loom_1_item),
							(this_or_next|eq, ":item_id", itm_handgonne_a +loom_2_item),
							(eq, ":item_id", itm_handgonne_a +loom_3_item),
							(assign, ":flashpan_pos_x", -5),
							(assign, ":flashpan_pos_y", 39),
							(assign, ":muzzle_pos_x", -3),
							(assign, ":muzzle_pos_y", 61),
						(else_try),
							(this_or_next|eq, ":item_id", itm_handgonne_b),
							(this_or_next|eq, ":item_id", itm_handgonne_b +loom_1_item),
							(this_or_next|eq, ":item_id", itm_handgonne_b +loom_2_item),
							(eq, ":item_id", itm_handgonne_b +loom_3_item),
							(assign, ":flashpan_pos_x", -1),
							(assign, ":flashpan_pos_y", 61),
							(assign, ":muzzle_pos_x", 0),
							(assign, ":muzzle_pos_y", 80),
						(else_try),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_1),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_1 +loom_1_item),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_1 +loom_2_item),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_1 +loom_3_item),
							(eq, ":item_id", itm_autofire_musket),
							(assign, ":flashpan_pos_x", -5),
							(assign, ":flashpan_pos_y", 12),
							(assign, ":muzzle_pos_x", -4),
							(assign, ":muzzle_pos_y", 100),
						(else_try),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_2),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_2 +loom_1_item),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_2 +loom_2_item),
							(eq, ":item_id", itm_matchlock_arquebus_2 +loom_3_item),
							(assign, ":flashpan_pos_x", -5),
							(assign, ":flashpan_pos_y", 15),
							(assign, ":muzzle_pos_x", -4),
							(assign, ":muzzle_pos_y", 106),
						(else_try),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_3),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_3 +loom_1_item),
							(this_or_next|eq, ":item_id", itm_matchlock_arquebus_3 +loom_2_item),
							(eq, ":item_id", itm_matchlock_arquebus_3 +loom_3_item),
							(assign, ":flashpan_pos_x", -6),
							(assign, ":flashpan_pos_y", 12),
							(assign, ":muzzle_pos_x", -4),
							(assign, ":muzzle_pos_y", 110),
					
						(else_try),
							(this_or_next|eq, ":item_id", itm_wheellock_pistol_1),
							(this_or_next|eq, ":item_id", itm_wheellock_pistol_1 +loom_1_item),
							(this_or_next|eq, ":item_id", itm_wheellock_pistol_1 +loom_2_item),
							(eq, ":item_id", itm_wheellock_pistol_1 +loom_3_item),
							(assign, ":flashpan_pos_x", 5),
							(assign, ":flashpan_pos_y", 12),
							(assign, ":muzzle_pos_x", 34),
							(assign, ":muzzle_pos_y", 40),
						(try_end),
					
						(try_begin),
							(eq, ":item_type", itp_type_musket),		  
							(val_sub, ":muzzle_pos_x", 8),#this is needed, because the particles dont appear at the correct position
							(val_sub, ":flashpan_pos_x", 8),#this is needed, because the particles dont appear at the correct position
							(val_sub, ":muzzle_pos_z", 3),#this is needed, because the particles dont appear at the correct position
							(val_sub, ":flashpan_pos_z", 3),#this is needed, because the particles dont appear at the correct position
						(try_end),
					
						(set_fixed_point_multiplier, 100),
						(copy_position, pos2, pos1),
				
						(position_move_x, pos1, ":flashpan_pos_x"),
						(position_move_y, pos1, ":flashpan_pos_y"),
						(position_move_z, pos1, ":flashpan_pos_z"),

						(position_move_x, pos2, ":muzzle_pos_x"),
						(position_move_y, pos2, ":muzzle_pos_y"),
						(position_move_z, pos2, ":muzzle_pos_z"),

						(particle_system_burst_no_sync, "psys_firearm_flashpan_fire", pos1, 1),
						(particle_system_burst_no_sync, "psys_firearm_flashpan_smoke", pos1, 10),
						(try_begin),#pistol/musket
							(eq, ":item_type", itp_type_pistol),
							(position_rotate_z, pos1, -45),
							(position_rotate_z, pos2, -45),
							(particle_system_burst_no_sync, "psys_pistol_muzzle_fire", pos2, 1),
							(particle_system_burst_no_sync, "psys_pistol_muzzle_smoke", pos2, 25),
						(else_try),
							(eq, ":item_type", itp_type_musket),
							(particle_system_burst_no_sync, "psys_musket_muzzle_fire", pos2, 1),
							(particle_system_burst_no_sync, "psys_musket_muzzle_smoke", pos2, 25),
						(try_end),
						#(play_sound_at_position, "snd_pistol_shot", pos2),
						(copy_position, pos_calc, pos2),
						(call_script, "script_reserve_sound_channel", "snd_release_gun"),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),

  # script_game_missile_dives_into_water
  # Input: arg1 = missile_item_id, pos1 = missile_position_on_water
  # Output: none 
  ("game_missile_dives_into_water",
  [
		(store_script_param, ":missile_item_id", 1),
		(try_begin),
			(neg|multiplayer_is_dedicated_server),#only client side
			(particle_system_burst_no_sync, "psys_water_hit_a", pos1, 4),
			(particle_system_burst_no_sync, "psys_water_hit_b", pos1, 2),
			(try_begin),
				(gt, ":missile_item_id", -1),
				(item_get_type, ":missile_type", ":missile_item_id"),
				(try_begin),
					(this_or_next|eq, ":missile_type", itp_type_arrows),
					(eq, ":missile_type", itp_type_bolts),
					(play_sound_at_position, "snd_watersplash_sml", pos1),
				(else_try),
					(play_sound_at_position, "snd_watersplash_med", pos1),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("prop_missile_init",
  [
    (store_script_param, ":instance_id", 1),
		(store_script_param, ":init_y_vel", 2),
		(store_script_param, ":init_z_vel", 3),
		(store_script_param, ":agent_id", 4),
		
		(scene_prop_set_slot, ":instance_id", scene_prop_time, 0),
		(scene_prop_set_slot, ":instance_id", scene_prop_move_y, ":init_y_vel"),
		(scene_prop_set_slot, ":instance_id", scene_prop_move_z, ":init_z_vel"),
		(scene_prop_set_slot, ":instance_id", scene_prop_user_agent, ":agent_id"),
		
		(try_begin),
			(multiplayer_is_server),#server side only
			(get_max_players, ":max_players"),
			(try_for_range, ":cur_player", 1, ":max_players"),
				(player_is_active, ":cur_player"),
				(multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_client_prop_missile_init, ":instance_id", ":init_y_vel", ":init_z_vel"),
			(try_end),
		(try_end),
		
		# (assign,reg22,":init_y_vel"),
		# (assign,reg23,":init_z_vel"),
		# (display_message,"@cos:{reg22} sin:{reg23}"),
  ]),
	
    #hit types
    # 0 = world
    # 1 = agent
    # 2 = dynamic prop
    # 3 = world
    # 4 = mission object
    # 8 = friend
    # 9 = neutral agent
    # 10 = under water
  ("item_missile_hit",
  [
		# (store_script_param, ":shooter_agent_no", 1),
		# (store_script_param, ":collision_type", 2),
		
		(store_trigger_param_1, ":shooter_agent_no"),
	  (store_trigger_param_2, ":collision_type"),
		(store_script_param, ":missile_type", 1),
		
		(set_fixed_point_multiplier, 100),
	
		#pos1 killes the shooter
		(copy_position, pos60, pos1),#for sounds and hit detection
		(try_begin),
			(neg|multiplayer_is_dedicated_server),#only client side
			(eq, "$g_show_shot_distance", 1),#shot distance
			(call_script, "script_client_get_player_agent"),
			(eq, reg0, ":shooter_agent_no"),
			(call_script, "script_shot_distance", ":shooter_agent_no"),
		(try_end),
		
		(try_begin),
			(eq, ":missile_type", missile_bullet),
			(try_begin),
				(neg|multiplayer_is_dedicated_server),#only client side
				(try_begin),
					(this_or_next|eq, ":collision_type", 0),
					(eq, ":collision_type", 3),
					(particle_system_burst_no_sync, "psys_bullet_hit", pos60, 2),
					(particle_system_burst_no_sync, "psys_bullet_hit_particle", pos60, 1),
				(else_try),
					(this_or_next|eq, ":collision_type", 2),
					(eq, ":collision_type", 4),
					(particle_system_burst_no_sync, "psys_bullet_hit_objects", pos60, 2),
				(try_end),
			(try_end),
		(else_try),
			(this_or_next|eq, ":missile_type", missile_bomb),
			(eq, ":missile_type", missile_firepot),
			(try_begin),
				(neq, ":collision_type", 10),#not underwater
				(try_begin),
					(this_or_next|multiplayer_is_server),
					(neg|game_in_multiplayer_mode),
					(init_position,	pos_spawn),
					(position_copy_origin, pos_spawn, pos1),
					(position_set_z_to_ground_level, pos_spawn),
					(position_move_z, pos_spawn, 5),
					(store_mission_timer_a, ":prune_time"),
					(try_begin),
						(eq, ":missile_type", missile_bomb),
						(assign, ":prop", "spr_grenade"),
						(val_add, ":prune_time", 2),
					(else_try),
						(assign, ":prop", "spr_fire_xl"),
						(val_add, ":prune_time", 30),
					(try_end),
					(call_script, "script_spawn_new_or_get_free_scene_prop", ":prop"),
					(scene_prop_set_slot, reg0, scene_prop_prune_time, ":prune_time"),
					(scene_prop_set_slot, reg0, scene_prop_user_agent, ":shooter_agent_no"),
				(try_end),
				(try_begin),
					(neg|multiplayer_is_dedicated_server),#only client side
					(try_begin),
						(eq, ":missile_type", missile_bomb),
						(copy_position, pos_calc, pos1),
						(call_script, "script_reserve_sound_channel", "snd_fuse"),
					(else_try),
						(particle_system_burst_no_sync, "psys_fire_m", pos60, 16),
						(particle_system_burst_no_sync, "psys_smoke_m", pos60, 16),
					(try_end),
				(try_end),
			(try_end),
		(else_try),
			(eq, ":missile_type", missile_fire),
			(try_begin),
				(neg|multiplayer_is_dedicated_server),#only client side
				(neq, ":collision_type", 10),
				(particle_system_burst_no_sync, "psys_fire_xs", pos60, 4),
				(particle_system_burst_no_sync, "psys_smoke_xs", pos60, 4),
			(try_end),
		(try_end),
  ]),
	
  ("server_missile_hit",
  [
		#pos_hit = hit pos
    (store_script_param, ":shooter_agent_no", 1),
		(store_script_param, ":missile_type", 2),
    (store_script_param, ":damage", 3),
		(store_script_param, ":range", 4),

		(try_begin),
			(eq, ":missile_type", missile_roundshot),
			(assign, ":weapon_no", itm_weapon_round_shot),
		(else_try),
			(eq, ":missile_type", missile_bomb),
			(assign, ":weapon_no", itm_weapon_explosion),
		(else_try),
			(eq, ":missile_type", missile_fire),
			(assign, ":weapon_no", itm_weapon_fire),
		(try_end),
	  
		(copy_position, pos61, pos_hit),
		(set_fixed_point_multiplier, 100),
		
		#prop_instance_receive_damage only for multiplayer.
		(try_for_range, ":scene_props", destructible_props_begin, destructible_props_end),
			(troop_slot_eq, "trp_prop_destructible", ":scene_props", 1),
			(scene_prop_get_num_instances, ":num_instances_of_scene_prop", ":scene_props"),
			(try_for_range, ":cur_instance", 0, ":num_instances_of_scene_prop"),
				(scene_prop_get_instance, ":cur_instance_id", ":scene_props", ":cur_instance"),
				(prop_instance_get_position, pos62, ":cur_instance_id"),
				(get_distance_between_positions, ":cur_dist", pos61, pos62),
				(lt, ":cur_dist", ":range"),
				(store_mul, ":final_damage", ":range", 1000),
				(val_div, ":final_damage", ":cur_dist"),
				(val_mul, ":final_damage", ":damage"),
				(val_div, ":final_damage", 10000),
				(scene_prop_get_slot, ":resistance", ":cur_instance_id", scene_prop_resistance),
				(val_sub, ":final_damage", ":resistance"),
				(try_begin),
					(eq, ":missile_type", missile_fire),
					(scene_prop_get_slot, ":material", ":cur_instance_id", scene_prop_material),
					(this_or_next|eq, ":material", material_earth),
					(eq, ":material", material_water),
					(assign, ":final_damage", 0),
				(try_end),
				(gt, ":final_damage", 0),
				(prop_instance_receive_damage, ":cur_instance_id", ":shooter_agent_no", ":final_damage"),
			(try_end),
		(try_end),
		
		(position_move_z, pos61, -90, 1),#center of body adjustment
		(try_for_agents, ":agent_no", pos61, ":range"),
			(agent_is_active, ":agent_no"),
			(agent_is_alive, ":agent_no"),
			(agent_get_position, pos62, ":agent_no"),
			(get_distance_between_positions, ":cur_dist", pos61, pos62),
			(le, ":cur_dist", ":range"),
			(store_mul, ":final_damage", ":range", 1000),
			(val_div, ":final_damage", ":cur_dist"),
			(val_mul, ":final_damage", ":damage"),
			(val_div, ":final_damage", 10000),
			(gt, ":final_damage", 0),
			(agent_deliver_damage_to_agent_advanced, ":delivered_damage", ":shooter_agent_no", ":agent_no", ":final_damage", ":weapon_no"),
			(gt, ":delivered_damage", 0),
			(try_begin),
				(agent_is_alive,":agent_no"), # still alive? if not then dont do all this animation and sounds cause the death will trigger those.
				(try_begin),
					(agent_is_human, ":agent_no"),
					(agent_get_horse, ":horse", ":agent_no"),
					(try_begin),
						(agent_slot_eq, ":agent_no", slot_agent_gender, tf_male),
						(agent_play_sound, ":agent_no", "snd_man_hit"),
					(else_try),
						(agent_play_sound, ":agent_no", "snd_woman_hit"),
					(try_end),
					(try_begin),
						(le, ":horse", 0), # No horse so play the fall animation
						(neq, ":missile_type", missile_fire),
						(agent_set_animation, ":agent_no", "anim_strike_fall_back_rise_upper"),
					(try_end),
				(else_try),
					(neq, ":missile_type", missile_fire),
					(agent_set_animation, ":agent_no", "anim_horse_rear"),
				(try_end),
			(else_try),#else dying
				(agent_is_human, ":agent_no"),
				(neg|multiplayer_is_dedicated_server),#only on local server
				(try_begin),
					(agent_slot_eq, ":agent_no", slot_agent_gender, tf_male),
					(agent_play_sound, ":agent_no", "snd_man_die"),
				(else_try),
					(agent_play_sound, ":agent_no", "snd_woman_die"),
				(try_end),
			(try_end),
		(try_end),
  ]),
	
  ("server_missile_hit_scene_prop",
  [
    (store_script_param, ":hitted_instance", 1),
		(store_script_param, ":final_damage", 2),
		#pos_hit = hit pos
		(set_fixed_point_multiplier, 100),
		(prop_instance_get_position, pos0, ":hitted_instance"),
		(prop_instance_get_scene_prop_kind, ":instance_kind", ":hitted_instance"),
		
		(scene_prop_get_hit_points, ":cur_hp", ":hitted_instance"),
		
		(try_begin),#first time hit, spawns child props
			(is_between, ":instance_kind", "spr_wall_stone_simple_1a", "spr_wall_stone_simple_1a_p1"),
			(copy_position, pos_spawn, pos0),
			(position_move_x, pos_spawn, 100),
			(position_move_z, pos_spawn, 100),
			(try_begin),
				(is_between, ":instance_kind", "spr_wall_stone_simple_1a", "spr_wall_stone_entrance_1a"),
				(assign, ":num_childs", 9),
				(assign, ":start_prop", "spr_wall_stone_simple_1a_p1"),
				(store_sub, ":offset", ":instance_kind", "spr_wall_stone_simple_1a"),
			(else_try),
				(assign, ":num_childs", 7),
				(assign, ":start_prop", "spr_wall_stone_entrance_1a_p1"),
				(store_sub, ":offset", ":instance_kind", "spr_wall_stone_entrance_1a"),
			(try_end),
			
			(val_mul, ":offset", ":num_childs"),
			(val_add, ":start_prop", ":offset"),
			(store_add, ":end_prop", ":start_prop", ":num_childs"),
			
			(assign, ":closest_child", -1),
			(assign, ":closest_dist", 10000),
			(assign, ":cur_number", 0),
			
			(try_for_range, ":cur_prop", ":start_prop", ":end_prop"),
				(call_script, "script_spawn_new_or_get_free_scene_prop", ":cur_prop"),
				(get_distance_between_positions, ":cur_dist", pos_hit, pos_spawn),
				(try_begin),
					(lt, ":cur_dist", ":closest_dist"),
					(assign, ":closest_dist", ":cur_dist"),
					(assign, ":closest_child", ":cur_prop"),
				(try_end),
				
				(val_add, ":cur_number", 1),
				(try_begin),
					(eq, ":num_childs", 9),
					(try_begin),
						(this_or_next|eq, ":cur_number", 3),
						(eq, ":cur_number", 6),
						(position_move_x, pos_spawn, 200),
						(position_move_z, pos_spawn, -100),
					(else_try),
						(neq, ":cur_number", ":num_childs"),
						(position_move_x, pos_spawn, -100),
					(try_end),
				(else_try),
					(try_begin),
						(this_or_next|eq, ":cur_number", 3),
						(eq, ":cur_number", 5),
						(position_move_x, pos_spawn, 200),
						(position_move_z, pos_spawn, -100),
					(else_try),
						(this_or_next|eq, ":cur_number", 4),
						(eq, ":cur_number", 6),
						(position_move_x, pos_spawn, -200),
					(else_try),
						(neq, ":cur_number", ":num_childs"),
						(position_move_x, pos_spawn, -100),
					(try_end),											
				(try_end),
			(try_end),
			(call_script, "script_move_scene_prop_under_ground", ":hitted_instance", 0),
			(scene_prop_set_slot, ":hitted_instance", scene_prop_usage_state, 0),
			
			(try_begin),#apply damage to closest child prop
				(gt, ":closest_child", -1),
				(scene_prop_get_num_instances, ":num_instances", ":closest_child"),
				(try_for_range, ":cur_instance", 0, ":num_instances"),
					(scene_prop_get_instance, ":cur_instance_id", ":closest_child", ":cur_instance"),
					(prop_instance_get_position, pos0, ":cur_instance_id"),
					(get_distance_between_positions, ":cur_dist", pos_hit, pos0),
					(eq, ":cur_dist", ":closest_dist"),#closed spawned prop found
					(call_script, "script_server_missile_hit_scene_prop", ":cur_instance_id", ":final_damage"),
					(assign, ":num_instances", -1),#break loop
				(try_end),
			(try_end),
		
		(else_try),
			(is_between, ":instance_kind", "spr_wall_stone_simple_1a_p1", "spr_scene_props_end"),
			(try_begin),
				(gt, ":final_damage", ":cur_hp"),
				(call_script, "script_move_scene_prop_under_ground", ":hitted_instance", 0),
			(else_try),
				(position_copy_origin, pos_calc, pos0),
				(position_copy_rotation, pos_calc, pos_hit),#apply direction of the hit
				
				(store_div, ":rot_max", ":final_damage", 10),
				(val_max, ":rot_max", 1),
				(store_mul, ":rot_min", ":rot_max", -1),
				(val_add, ":rot_max", 1),
				
				(scene_prop_get_max_hit_points, ":max_hp", ":hitted_instance"),
				(store_div, ":move_max", 25000000, ":max_hp"),
				(val_mul, ":move_max", ":final_damage"),
				(val_div, ":move_max", 1000000),
				(store_div, ":move_min", ":move_max", 2),
				
				#randomize movement
				(store_random_in_range, ":move_val", ":move_min", ":move_max"),
				(position_move_y, pos_calc, ":move_val"),
				(position_copy_rotation, pos_calc, pos0),#apply original rotation
				
				#randomize rotation
				(store_random_in_range, ":rot_x_rand", ":rot_min", ":rot_max"),
				(position_rotate_x_floating, pos_calc, ":rot_x_rand"),
				(store_random_in_range, ":rot_y_rand", ":rot_min", ":rot_max"),
				(position_rotate_y_floating, pos_calc, ":rot_y_rand"),
				(store_random_in_range, ":rot_z_rand", ":rot_min", ":rot_max"),
				(position_rotate_z_floating, pos_calc, ":rot_z_rand"),
				
				(prop_instance_set_position, ":hitted_instance", pos_calc, 0),
			(try_end),
			
		(else_try),
			(gt, ":final_damage", ":cur_hp"),
			(call_script, "script_move_scene_prop_under_ground", ":hitted_instance", 0),
			(try_begin),
				(eq, ":instance_kind", "spr_cannon_base_1"),
				(try_for_range, ":cur_slot_prop", scene_prop_child_1, scene_prop_child_5 +1),
					(scene_prop_get_slot, ":cur_instance_id", ":hitted_instance", ":cur_slot_prop"),
					(prop_instance_is_valid, ":cur_instance_id"),
					(try_begin),
						(eq, ":cur_slot_prop", scene_prop_child_1),
						(scene_prop_slot_eq, ":cur_instance_id", scene_prop_usage_state, 6),#aim
						
						(call_script, "script_control_scene_prop", ":cur_instance_id", prop_action_2),
						(multiplayer_is_server),
						(get_max_players, ":num_players"),
						(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
							(player_is_active, ":cur_player_no"),
							(multiplayer_send_2_int_to_player, ":cur_player_no", multiplayer_event_client_control_scene_prop, ":cur_instance_id", prop_action_2), #CANCEL#
						(try_end),
					(try_end),
					(call_script, "script_move_scene_prop_under_ground", ":cur_instance_id", 0),
				(try_end),
			(else_try),
				(eq, ":instance_kind", "spr_cannon_barrel_1_32lb"),
				(scene_prop_get_slot, ":parent_instance_id", ":hitted_instance", scene_prop_parent),
				(prop_instance_is_valid, ":parent_instance_id"),
				(try_begin),
					(scene_prop_slot_eq, ":hitted_instance", scene_prop_usage_state, 6),#aim
					(call_script, "script_control_scene_prop", ":parent_instance_id", prop_action_2),
					(multiplayer_is_server),
					(get_max_players, ":num_players"),
					(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
						(player_is_active, ":cur_player_no"),
						(multiplayer_send_2_int_to_player, ":cur_player_no", multiplayer_event_client_control_scene_prop, ":parent_instance_id", prop_action_2), #CANCEL#
					(try_end),
				(try_end),
				(call_script, "script_move_scene_prop_under_ground", ":parent_instance_id", 0),
				(try_for_range, ":cur_slot_prop", scene_prop_child_2, scene_prop_child_5 +1),
					(scene_prop_get_slot, ":cur_instance_id", ":parent_instance_id", ":cur_slot_prop"),
					(prop_instance_is_valid, ":cur_instance_id"),
					(call_script, "script_move_scene_prop_under_ground", ":cur_instance_id", 0),
				(try_end),
			(try_end),
		(try_end),
  ]),
	
("weapon_attack",
[
    (store_trigger_param_1, ":agent"),

    (agent_get_wielded_item, ":item", ":agent", 0),
    (item_get_thrust_damage, ":velocity", ":item"),
    (item_get_type, ":type", ":item"),

    (try_begin),
      (eq, ":type", itp_type_thrown),
      (assign, ":ammo", ":item"),
    (else_try),
      (assign, ":end_slot", ek_head),
      (try_for_range, reg0, ek_item_0, ":end_slot"),
        (agent_get_item_slot, ":slot_item", ":agent", reg0),
        (item_get_type, ":slot_type", ":slot_item"),
        (try_begin),
          (eq, ":type", itp_type_bow),
          (eq, ":slot_type", itp_type_arrows),
          (assign, ":ammo", ":slot_item"),
          (assign, ":end_slot", -1),
        (else_try),
          (eq, ":type", itp_type_crossbow),
          (eq, ":slot_type", itp_type_bolts),
          (assign, ":ammo", ":slot_item"),
          (assign, ":end_slot", -1),
        (else_try),
          (is_between, ":type", itp_type_pistol, itp_type_bullets),
          (eq, ":slot_type", itp_type_bullets),
          (assign, ":ammo", ":slot_item"),
          (assign, ":end_slot", -1),
        (try_end),
      (try_end),
    (try_end),

    (try_begin),
      (eq, ":type", itp_type_bow),
      (agent_get_bone_position, pos1, ":agent", hb_item_r, 1),
      (position_move_y, pos1, 55),

      # (position_move_y, pos1, 6),
      # (position_move_z, pos1, 2),
    (else_try),
      (eq, ":type", itp_type_crossbow),
      (position_move_x, pos1, -1),
      (position_move_y, pos1, 66),
      (position_move_z, pos1, -3),
    (else_try),
      (this_or_next|eq, ":item", itm_handgonne_a),
      (this_or_next|eq, ":item", itm_handgonne_a +loom_1_item),
      (this_or_next|eq, ":item", itm_handgonne_a +loom_2_item),
      (eq, ":item", itm_handgonne_a +loom_3_item),
      (position_move_x, pos1, -3),
      (position_move_y, pos1, 61),
    (else_try),
      (this_or_next|eq, ":item", itm_handgonne_b),
      (this_or_next|eq, ":item", itm_handgonne_b +loom_1_item),
      (this_or_next|eq, ":item", itm_handgonne_b +loom_2_item),
      (eq, ":item", itm_handgonne_b +loom_3_item),
      (position_move_y, pos1, 81),
    (else_try),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_1),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_1 +loom_1_item),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_1 +loom_2_item),
      (eq, ":item", itm_matchlock_arquebus_1 +loom_3_item),
      (position_move_x, pos1, -1),
      (position_move_y, pos1, 101),
    (else_try),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_2),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_2 +loom_1_item),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_2 +loom_2_item),
      (eq, ":item", itm_matchlock_arquebus_2 +loom_3_item),
      (position_move_x, pos1, -1),
      (position_move_y, pos1, 107),
    (else_try),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_3),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_3 +loom_1_item),
      (this_or_next|eq, ":item", itm_matchlock_arquebus_3 +loom_2_item),
      (eq, ":item", itm_matchlock_arquebus_3 +loom_3_item),
      (position_move_x, pos1, -1),
      (position_move_y, pos1, 111),
    (else_try),
      (eq, ":type", itp_type_pistol),
      (position_move_x, pos1, 34),
      (position_move_y, pos1, 40),
    (try_end),

    # (agent_get_look_position, pos0, ":agent"),
    # (position_copy_rotation, pos2, pos0),
    # (position_copy_origin, pos2, pos1),

    (set_fixed_point_multiplier, 1),
    (add_missile, ":agent", pos1, ":velocity", ":item", 0, ":ammo", 0),

    # (try_begin),
    #     (cast_ray, reg0, pos2, pos1, 10),
    #     (display_message, "@{reg0}"),
    #     (particle_system_burst_no_sync, "psys_hit_explosion_big", pos2, 1),
    # (try_end),

    # (copy_position, pos_spawn, pos1),
    # (call_script, "script_spawn_new_or_get_free_scene_prop", "spr_arrow_missile"),
    # (assign, ":prop_instance", reg0),

    # (store_mission_timer_a_msec, reg0),
    # (scene_prop_set_slot, ":prop_instance", scene_prop_last_pos_time, reg0),

    # (position_get_x, reg0, pos1),
    # (position_get_y, reg1, pos1),
    # (position_get_z, reg2, pos1),
    # (val_mul, reg0, 10),
    # (val_mul, reg1, 10),
    # (val_mul, reg2, 10),
    # (scene_prop_set_slot, ":prop_instance", scene_prop_pos_x, reg0),
    # (scene_prop_set_slot, ":prop_instance", scene_prop_pos_y, reg1),
    # (scene_prop_set_slot, ":prop_instance", scene_prop_pos_z, reg2),

    # (copy_position, pos2, pos1),
    # (val_mul, ":velocity", 1000),
    # (position_move_y, pos1, ":velocity"),

    # (init_position, pos0),
    # (position_copy_rotation, pos1, pos0),
    # (position_copy_rotation, pos2, pos0),
    # (position_transform_position_to_local, pos0, pos2, pos1),
    # (position_get_x, reg0, pos0),
    # (position_get_y, reg1, pos0),
    # (position_get_z, reg2, pos0),
    # (scene_prop_set_slot, ":prop_instance", scene_prop_move_x, reg0),
    # (scene_prop_set_slot, ":prop_instance", scene_prop_move_y, reg1),
    # (scene_prop_set_slot, ":prop_instance", scene_prop_move_z, reg2),
    # (display_message, "@x:{reg0} y:{reg1} z:{reg2}"),

    # (assign, "$frist_time", 1),
  ]),

  ("shot_distance",
  [
    (store_script_param, ":shooter_agent", 1),
    
    (agent_get_slot, ":pos_x", ":shooter_agent", slot_agent_shot_pos_x),
    (agent_get_slot, ":pos_y", ":shooter_agent", slot_agent_shot_pos_y),
    (agent_get_slot, ":pos_z", ":shooter_agent", slot_agent_shot_pos_z),

    (set_fixed_point_multiplier, 100),
    (init_position, pos2),
    (position_set_x, pos2, ":pos_x"),
    (position_set_y, pos2, ":pos_y"),
    (position_set_z, pos2, ":pos_z"),
    
    (get_distance_between_positions, ":distance_meter", pos1, pos2),
    (store_div, reg61, ":distance_meter", 100),
    (store_mod, reg62, ":distance_meter", 100),
    (try_begin),
      (lt, reg62, 10),
      (str_store_string, s1, "@{reg61}.0{reg62}"),
    (else_try),
      (str_store_string, s1, "@{reg61}.{reg62}"),
    (try_end),

    (val_mul, ":distance_meter", 10000),   
    (store_div, ":distance_yard", ":distance_meter", 9144),#0,9144
    (store_div, reg59, ":distance_yard", 100),
    (store_mod, reg60, ":distance_yard", 100),
    (try_begin),
      (lt, reg60, 10),
      (str_store_string, s2, "@{reg59}.0{reg60}"),
    (else_try),
      (str_store_string, s2, "@{reg59}.{reg60}"),
    (try_end),
    
    (display_message, "@Distance: {s1} meter ^             {s2} yard ", 0x606880),#the space is needed to display it correctly
  ]),
  
  ("get_angle_of_ground_at_pos",
  [
		(store_script_param, ":terrain_check", 1),
		(store_script_param, ":multiplier", 2),
		(store_script_param, ":x_dist_left", 3),
		(store_script_param, ":x_dist_right", 4),
		(store_script_param, ":y_dist_front", 5),
		(store_script_param, ":y_dist_back", 6),

		(val_mul, ":x_dist_left", -1),
		(val_mul, ":y_dist_back", -1),

		(assign, ":x_rot", 0),
		(assign, ":y_rot", 0),
		
		(set_fixed_point_multiplier, 100),
		(try_begin),
			(eq, ":terrain_check", 1),
			(position_get_distance_to_terrain, ":move_z", pos_calc),
			(val_mul, ":move_z", -1),
			(position_move_z, pos_calc, ":move_z"),
		(else_try),
			(position_set_z_to_ground_level, pos_calc),
		(try_end),
		
		(position_get_z, ":base_z", pos51),
		
		(try_for_range, ":i", 0, 2),
			(try_begin),
				(eq, ":i", 0),
				(assign, ":dist_1", ":x_dist_left"),
				(assign, ":dist_2", ":x_dist_right"),
			(else_try),
				(assign, ":dist_1", ":y_dist_back"),
				(assign, ":dist_2", ":y_dist_front"),
			(try_end),
			
      (this_or_next|lt, ":dist_1", 0),
			(gt, ":dist_2", 0),
			
			(set_fixed_point_multiplier, 100000),
			(copy_position, pos51, pos_calc),
			(copy_position, pos52, pos_calc),
      
			(try_begin),
				(eq, ":i", 0),
				(position_move_x, pos51, ":dist_1"),
				(position_move_x, pos52, ":dist_2"),
			(else_try),
				(position_move_y, pos51, ":dist_1"),
				(position_move_y, pos52, ":dist_2"),
			(try_end),
			(position_set_z, pos51, ":base_z"),
			(position_set_z, pos52, ":base_z"),
			
			(try_begin),
				(eq, ":terrain_check", 1),
				(position_get_distance_to_terrain, ":height_1", pos51),
				(position_get_distance_to_terrain, ":height_2", pos52),
			(else_try),
				(position_get_distance_to_ground_level, ":height_1", pos51),
				(position_get_distance_to_ground_level, ":height_2", pos52),
			(try_end),
			
			(try_begin),
				(eq, ":i", 0),
				(store_sub, ":height_difference", ":height_2", ":height_1"),
			(else_try),
				(store_sub, ":height_difference", ":height_1", ":height_2"),
			(try_end),
			(get_distance_between_positions, ":dist_global", pos51,  pos52),
			(val_max, ":dist_global", 1),#fix
      (store_div, ":rotation", ":height_difference", ":dist_global"),
			
      (set_fixed_point_multiplier, 1000),
      (store_atan, ":rotation", ":rotation"),
			(try_begin),
				(eq, ":multiplier", 1),
				(val_div, ":rotation", 1000),
			(else_try),
				(eq, ":multiplier", 10),
				(val_div, ":rotation", 100),
			(else_try),
				(eq, ":multiplier", 100),
				(val_div, ":rotation", 10),
			(try_end),
			
			(try_begin),
				(eq, ":i", 0),
				(assign, ":y_rot", ":rotation"),
			(else_try),
				(assign, ":x_rot", ":rotation"),
			(try_end),
    (try_end),
    
    (set_fixed_point_multiplier, 100),#reset multiplier
    (assign, reg0, ":x_rot"),
    (assign, reg1, ":y_rot"),
	  # (display_message, "@xRot = {reg0} yRot = {reg1}"),
  ]),  
  
  ("agent_walk",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":walk_mode", 2),

    (try_begin),
      (this_or_next|multiplayer_is_server),
      (neg|game_in_multiplayer_mode),

      (try_begin),
        (eq, ":walk_mode", walk_by_key),
        (try_begin),
          (agent_slot_eq, ":agent_id", slot_agent_key_walk, 0),
          (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_key_walk, 1),
        (else_try),
          (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_key_walk, 0),
        (try_end),
		
      (else_try),
        (eq, ":walk_mode", walk_by_stamina),
        # (assign, ":stamina_value", - stamina_wpt_reduction),
				(try_begin),
          (agent_slot_eq, ":agent_id", slot_agent_stamina_walk, 0),
          (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_stamina_walk, 1),
				(else_try),
          (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_stamina_walk, 0),
					# (assign, ":stamina_value", stamina_wpt_reduction),
        (try_end),	
        # (agent_get_troop_id, ":player_troop", ":agent_id"),
				# (try_begin),#stamina wpt reduction
					# (gt, ":player_troop", -1),
					# (call_script, "script_server_set_troop_value", ":player_troop", multiplayer_event_client_raise_all_proficiencies, 0, ":stamina_value"),
					
					# (troop_get_slot, ":wpt_reduction_stamina", ":player_troop", slot_troop_wpt_reduction_stamina),
					# (val_add, ":stamina_value", ":wpt_reduction_stamina"),
					# (troop_set_slot, ":player_troop", slot_troop_wpt_reduction_stamina, ":stamina_value"),
					
					# #(assign, reg61, ":player_troop"),
					# #(assign, reg62, ":stamina_value"),
					# #(display_message, "@troop {reg61} value{reg62}", 0xCCCCCC),
				# (try_end),
		
			(else_try),
        (eq, ":walk_mode", walk_by_limiter),
				(try_begin),
          (agent_slot_eq, ":agent_id", slot_agent_limiter_walk, 0),
          (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_limiter_walk, 1),
				(else_try),
					(call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_limiter_walk, 0),
				(try_end),
      (try_end),
    
			(try_begin),
				(agent_slot_eq, ":agent_id", slot_agent_is_walking, 0),
				(call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_is_walking, 1),
          
				(assign, ":speed", 64),
				(agent_get_troop_id, ":troop_id", ":agent_id"),
				(try_begin),
					(gt, ":troop_id", -1),
					(store_attribute_level, ":agility", ":troop_id", ca_agility),
					(try_begin),
						(ge, ":agility", 3),
						(store_div, ":att_speed", ":agility", 3),
						(val_sub, ":speed", ":att_speed"),
					(try_end),
					(store_skill_level, ":athletics", "skl_athletics", ":troop_id"),
					(try_begin),
						(gt, ":athletics", 0),
						(val_sub, ":speed", ":athletics"),
					(try_end),
    
					(agent_set_speed_modifier, ":agent_id", ":speed"),
					(try_begin),
						(neq, ":walk_mode", walk_by_limiter),
						(assign, ":horse_speed", 30),
						(store_skill_level, ":riding", "skl_riding", ":troop_id"),
						(try_begin),
							(gt, ":riding", 0),
							(val_sub, ":horse_speed", ":riding"),
						(try_end),
						(call_script, "script_multiplayer_server_set_horse_speed", ":agent_id", ":horse_speed"),
					(try_end),
				(try_end),
			(else_try),
				(agent_slot_eq, ":agent_id", slot_agent_key_walk, 0),
				(agent_slot_eq, ":agent_id", slot_agent_stamina_walk, 0),
				(agent_slot_eq, ":agent_id", slot_agent_limiter_walk, 0),
				(call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_is_walking, 0),
				(call_script, "script_agent_reset_speed", ":agent_id"),
			(try_end),
		(try_end),
  ]),

  ("agent_sprint_limiter",
  [
    (store_script_param, ":agent_id", 1),

		(agent_get_troop_id, ":sprint_troop", ":agent_id"),
    (try_begin),
      (gt, ":sprint_troop", -1),
			(call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_is_sprinting, 1),
			
			(agent_get_slot, ":carry_weight", ":agent_id", slot_agent_carry_weight),
      (agent_get_horse, ":horse", ":agent_id"),
      (try_begin),# unmounted agent
        (le, ":horse", 0),
        (assign, ":speed", 200),
        (store_skill_level, ":skl", "skl_athletics", ":sprint_troop"),
        (try_begin),
          (gt, ":skl", 0),
          (store_mul, ":skl_speed", ":skl", 2),
          (val_sub, ":speed", ":skl_speed"),
        (try_end),
				(try_begin),
					(gt, ":carry_weight", 0),
					(val_div, ":carry_weight", weight_divider_sprint),
					(val_sub, ":speed", ":carry_weight"),
				(try_end),
        (agent_set_speed_modifier, ":agent_id", ":speed"),
      (else_try),# mounted agent
        (assign, ":speed", 175),
        (store_skill_level, ":skl", "skl_riding", ":sprint_troop"),
        (try_begin),
          (gt, ":skl", 0),
          (val_sub, ":speed", ":skl"),
        (try_end),
				(try_begin),
					(gt, ":carry_weight", 0),
					(val_div, ":carry_weight", weight_divider_sprint_horse),
					(val_sub, ":speed", ":carry_weight"),
				(try_end),
				(call_script, "script_multiplayer_server_set_horse_speed", ":agent_id", ":speed"),
      (try_end),
    (try_end),
  ]),
  
  ("agent_sprint_stop",
  [
    (store_script_param, ":agent_id", 1),
		
    (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_is_sprinting, 0),
    (call_script, "script_agent_reset_speed", ":agent_id"),
  ]),

  ("agent_reset_speed",
  [
    (store_script_param, ":agent_id", 1),
		
		(try_begin),
			(agent_slot_eq, ":agent_id", slot_agent_is_walking, 0),
			(agent_slot_eq, ":agent_id", slot_agent_is_sprinting, 0),
			
			(assign, ":human_speed_modifier", 100),
			(assign, ":horse_speed_modifier", 100),
			(agent_get_slot, ":carry_weight", ":agent_id", slot_agent_carry_weight),
			(try_begin),
				(gt, ":carry_weight", 0),
				# (store_div, ":human_mod", ":carry_weight", weight_divider_run),
				# (val_sub, ":human_speed_modifier", ":human_mod"),
				(store_div, ":horse_mod", ":carry_weight", weight_divider_run_horse),
				(val_sub, ":horse_speed_modifier", ":horse_mod"),
			(try_end),
			(agent_set_speed_modifier, ":agent_id", ":human_speed_modifier"),
			(call_script, "script_multiplayer_server_set_horse_speed", ":agent_id", ":horse_speed_modifier"),
		(try_end),
  ]),

  ("shield_bash",
  [
    (store_script_param, ":agent_id", 1),
    (try_begin),
      (this_or_next|multiplayer_is_server),
      (neg|game_in_multiplayer_mode),

      (agent_get_wielded_item, ":item_id", ":agent_id", 1),
      (gt, ":item_id", -1),

			(agent_get_crouch_mode, ":crouch", ":agent_id"),
			(eq, ":crouch", 0),#no crouching
			(try_begin),
				(agent_slot_eq, ":agent_id", slot_agent_gender, tf_male),
				(agent_play_sound, ":agent_id", "snd_man_grunt"),
			(else_try),
				(agent_play_sound, ":agent_id", "snd_woman_yell"),
			(try_end),
			(agent_set_animation, ":agent_id", "anim_shield_bash"),
			(store_mission_timer_a_msec, ":bash_time"),
			(val_add, ":bash_time", 250),
			(agent_set_slot, ":agent_id", slot_agent_nudge_time, ":bash_time"),
    (try_end),
  ]),

  ("switch_weapon_mode",
  [
    (store_script_param, ":agent", 1),
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),

			(agent_get_wielded_item, ":cur_item", ":agent", 0),
			(gt, ":cur_item", -1),
			
			(try_begin),
				(item_slot_eq, ":cur_item", slot_item_is_alt_mode, 2),
				(store_sub, ":equip_item", ":cur_item", 1),
			(else_try),
				(store_add, ":next_item", ":cur_item", 1),
				(item_slot_eq, ":next_item", slot_item_is_alt_mode, 2),
				(assign, ":equip_item", ":next_item", 1),
			(else_try),
				(assign, ":equip_item", -1),
			(try_end),
			
			(gt, ":equip_item", -1),
			(agent_unequip_item, ":agent", ":cur_item", 0),
			(agent_equip_item, ":agent", ":equip_item", 0),
			(agent_set_wielded_item, ":agent", ":equip_item"),
		(try_end),
  ]),
  
  ("set_base_stamina",
  [
    (store_script_param, ":troop_id", 1),
    (try_begin),
      (gt, ":troop_id", -1),
			(assign, ":base_stamina", agent_base_stamina),
			(store_attribute_level, ":att_str", ":troop_id", ca_strength),
			(store_attribute_level, ":att_agi", ":troop_id", ca_agility),
			(store_div, ":str_bonus", agent_base_stamina, stamina_str_divider),
			(store_div, ":agi_bonus", agent_base_stamina, stamina_agi_divider),
			(val_mul, ":att_str", ":str_bonus"),
			(val_mul, ":att_agi", ":agi_bonus"),
			(val_add, ":base_stamina", ":att_str"),
			(val_add, ":base_stamina", ":att_agi"),
			(troop_set_slot, ":troop_id", slot_troop_base_stamina, ":base_stamina"),
    (try_end),
  ]),

  ("agent_lose_stamina",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":type", 2),
		(try_begin),
			(agent_is_active, ":agent_id"),
			(agent_is_alive, ":agent_id"),
			(try_begin),
				(agent_slot_eq, ":agent_id", slot_agent_lose_stamina, 0),
				(agent_set_slot, ":agent_id", slot_agent_lose_stamina, 1),
			(try_end),

			(agent_get_slot, ":cur_stamina", ":agent_id", slot_agent_cur_stamina),
			(try_begin),
				(gt, ":cur_stamina", 0),
				(assign, ":value", 0),
				(try_begin),
					(this_or_next|eq, ":type", stamina_attack),
					(this_or_next|eq, ":type", stamina_defend),
					(eq, ":type", stamina_attack_shield),

					(agent_get_wielded_item, ":item_r", ":agent_id", 0),
					(agent_get_wielded_item, ":item_l", ":agent_id", 1),
					(try_begin),
						(this_or_next|eq, ":type", stamina_attack_shield),
						(eq, ":type", stamina_defend),
						(try_begin),
							(eq, ":type", stamina_attack_shield),
							(assign, ":item", ":item_l"),
						(else_try),
							(eq, ":type", stamina_defend),
							(try_begin),
								(gt, ":item_l", -1),#has shield
								(assign, ":item", ":item_l"),
							(else_try),
								(assign, ":item", ":item_r"),
							(try_end),
						(try_end),
					(else_try),
						(assign, ":item", ":item_r"),
					(try_end),
			
					(gt, ":item", -1),
					(set_fixed_point_multiplier, 10),
					(item_get_weight, ":value", ":item"),
					(set_fixed_point_multiplier, 100),
			
					#item weight div. item ammo for thrown
					(item_get_type, ":item_type", ":item"),
					(try_begin),
						(eq, ":item_type", itp_type_thrown),
						(item_get_max_ammo, ":item_ammo", ":item"),
						(val_div, ":value", ":item_ammo"),
					(try_end),

				(else_try),
					(agent_get_slot, ":value", ":agent_id", slot_agent_carry_weight),
					(val_div, ":value", 10),#set to 1 kg steps
					(try_begin),
						(eq, ":type", stamina_run_horse),#looses less weight-stamina on horseback
						(val_div, ":value", stamina_mounted_weight_divider),
					(try_end),
				(try_end),
				
				(val_add, ":value", ":type"),
				(val_sub, ":cur_stamina", ":value"),
				(val_max, ":cur_stamina", 0),
				(agent_set_slot, ":agent_id", slot_agent_cur_stamina, ":cur_stamina"),
				(try_begin),
					(lt, ":cur_stamina", 1),
					(agent_slot_eq, ":agent_id", slot_agent_stamina_walk, 0),
					(try_begin),
						(game_in_multiplayer_mode),
						(neg|multiplayer_is_server),
						(multiplayer_send_int_to_server, multiplayer_event_agent_walk, walk_by_stamina),
					(else_try),
						(call_script, "script_agent_walk", ":agent_id", walk_by_stamina),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("agent_recover_stamina",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":type", 2),
		(try_begin),
			(agent_is_active, ":agent_id"),
			(agent_is_alive, ":agent_id"),
			(try_begin),
				(agent_slot_eq, ":agent_id", slot_agent_lose_stamina, 1),
				(agent_set_slot, ":agent_id", slot_agent_lose_stamina, 0),
			(try_end),

			(agent_get_slot, ":max_stamina", ":agent_id", slot_agent_max_stamina),
#workaround
      (agent_get_troop_id, ":agent_troop", ":agent_id"),
      (gt, ":agent_troop", -1),
      (troop_get_slot, ":base_stamina", ":agent_troop", slot_troop_base_stamina),
      (try_begin),
        (lt, ":base_stamina", ":max_stamina"),#max stamina is sometimes higher than base stamina, no idea why atm
        (assign, ":max_stamina", ":base_stamina"),
      (try_end),
#workaround
			(agent_get_slot, ":cur_stamina", ":agent_id", slot_agent_cur_stamina),
			(try_begin),
				(lt, ":cur_stamina", ":max_stamina"),
				(val_add, ":cur_stamina", ":type"),
				(val_min, ":cur_stamina", ":max_stamina"),
				(agent_set_slot, ":agent_id", slot_agent_cur_stamina, ":cur_stamina"),
				(try_begin),
					(gt, ":cur_stamina", agent_run_stamina),
					(agent_slot_eq, ":agent_id", slot_agent_is_walking, 1),
					(agent_slot_eq, ":agent_id", slot_agent_key_walk, 0),
					(agent_slot_eq, ":agent_id", slot_agent_limiter_walk, 0),
					(try_begin),
						(game_in_multiplayer_mode),
						(neg|multiplayer_is_server),
						(multiplayer_send_int_to_server, multiplayer_event_agent_walk, walk_by_stamina),
					(else_try),
						(call_script, "script_agent_walk", ":agent_id", walk_by_stamina),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("agent_set_weight",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":item_id", 2),
    (store_script_param, ":command", 3),
		
		(try_begin),
			(agent_is_active, ":agent_id"),
			(agent_is_alive, ":agent_id"),

			(set_fixed_point_multiplier, 10),
			(item_get_weight, ":item_weight", ":item_id"),
			(set_fixed_point_multiplier, 100),
			(try_begin),
				(gt, ":item_weight", 0),
				(agent_get_slot, ":carry_weight", ":agent_id", slot_agent_carry_weight),
				(try_begin),
					(this_or_next|eq, ":command", agent_add_carry_weight),
					(eq, ":command", agent_add_armor_weight),
					(val_add, ":carry_weight", ":item_weight"),
					(agent_set_slot, ":agent_id", slot_agent_carry_weight, ":carry_weight"),
					(try_begin),
						(eq, ":command", agent_add_armor_weight),
						(agent_get_slot, ":armor_weight", ":agent_id", slot_agent_armor_weight),
						(val_add, ":armor_weight", ":item_weight"),
						(agent_set_slot, ":agent_id", slot_agent_armor_weight, ":armor_weight"),
					(try_end),
				(else_try),
					(this_or_next|eq, ":command", agent_sub_carry_weight),
					(eq, ":command", agent_sub_armor_weight),
					(val_sub, ":carry_weight", ":item_weight"),
					(agent_set_slot, ":agent_id", slot_agent_carry_weight, ":carry_weight"),
					(try_begin),
						(eq, ":command", agent_sub_armor_weight),
						(agent_get_slot, ":armor_weight", ":agent_id", slot_agent_armor_weight),
						(val_sub, ":armor_weight", ":item_weight"),
						(agent_set_slot, ":agent_id", slot_agent_armor_weight, ":armor_weight"),
					(try_end),
				(try_end),
				(call_script, "script_agent_reset_speed", ":agent_id"),#set base speed
			(try_end),
		(try_end),
  ]),
  
  ("agent_spawn_item",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":item_id", 2),
		(store_script_param, ":without_refill", 3),
		
		# (assign, reg1, ":without_refill"),
		# (display_message, "@{reg1}"),
		
		(try_begin),
			(agent_is_active, ":agent_id"),
			(agent_is_alive, ":agent_id"),

			(agent_get_position, pos_spawn, ":agent_id"),
			# (store_random_in_range,":pos_x", -50, 51),
			# (position_move_x, pos_spawn, ":pos_x"),
			# (store_random_in_range,":pos_y", -50, 51),
			# (position_move_y, pos_spawn, ":pos_y"),
			(set_spawn_position, pos_spawn),
			
			(item_get_type, ":item_type", ":item_id"),
			(try_begin),
				(eq, ":item_type", itp_type_horse),
				(spawn_horse, ":item_id", 0),
			(else_try),
				(try_begin),
					(eq, ":without_refill", 0),
					(spawn_item, ":item_id", 0, item_prune_time),
				(else_try),
					(spawn_item_without_refill, ":item_id", 0, item_prune_time),
				(try_end),
			(try_end),
		(try_end),
  ]),
  
  ("agent_spawn_scene_prop",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":instance_kind", 2),
		(try_begin),
			(agent_is_active, ":agent_id"),
			(agent_is_alive, ":agent_id"),

			(assign, ":end_slot", 4),
			(try_for_range, ":cur_slot", 0, ":end_slot"),
				(agent_get_item_slot, ":item", ":agent_id", ":cur_slot"),
				(eq, ":item", itm_engineer_material),
				(agent_get_ammo_for_slot, ":cur_build_points", ":agent_id", ":cur_slot"),
				(assign, ":end_slot", -1),#break loop
			(try_end),
			
			(try_begin),
				(eq, ":instance_kind", "spr_construct_box_a"),
				(assign, ":only_on_terrain", 0),
				(assign, ":offset_y", 100),
				(assign, ":x_dist_left", 20),
				(assign, ":x_dist_right", 20),
				(assign, ":y_dist_front", 15),
				(assign, ":y_dist_back", 15),
				(assign, ":x_rot_min", -30),
				(assign, ":x_rot_max", 31),
				(assign, ":y_rot_min", -30),
				(assign, ":y_rot_max", 31),
			(else_try),
				(eq, ":instance_kind", "spr_construct_siege_large_shield_a"),
				(assign, ":only_on_terrain", 0),
				(assign, ":offset_y", 100),
				(assign, ":x_dist_left", 40),
				(assign, ":x_dist_right", 40),
				(assign, ":y_dist_front", 45),
				(assign, ":y_dist_back", 45),
				(assign, ":x_rot_min", -30),
				(assign, ":x_rot_max", 31),
				(assign, ":y_rot_min", -30),
				(assign, ":y_rot_max", 31),
			(else_try),
				(eq, ":instance_kind", "spr_construct_spike_group_a"),
				(assign, ":only_on_terrain", 1),
				(assign, ":offset_y", 100),
				(assign, ":x_dist_left", 55),
				(assign, ":x_dist_right", 55),
				(assign, ":y_dist_front", 35),
				(assign, ":y_dist_back", 35),
				(assign, ":x_rot_min", -30),
				(assign, ":x_rot_max", 31),
				(assign, ":y_rot_min", -30),
				(assign, ":y_rot_max", 31),
			(else_try),
				(eq, ":instance_kind", "spr_construct_plank"),
				(assign, ":only_on_terrain", 0),
				(assign, ":offset_y", 0),
				(assign, ":x_dist_left", 0),
				(assign, ":x_dist_right", 0),
				(assign, ":y_dist_front", 400),
				(assign, ":y_dist_back", 0),
				(assign, ":x_rot_min", -45),
				(assign, ":x_rot_max", 46),
				(assign, ":y_rot_min", -45),
				(assign, ":y_rot_max", 46),
			(else_try),
				(eq, ":instance_kind", "spr_construct_earthwork"),
				(assign, ":only_on_terrain", 1),
				(assign, ":offset_y", 100),
				(assign, ":x_dist_left", 100),
				(assign, ":x_dist_right", 100),
				(assign, ":y_dist_front", 40),
				(assign, ":y_dist_back", 40),
				(assign, ":x_rot_min", -40),
				(assign, ":x_rot_max", 41),
				(assign, ":y_rot_min", -40),
				(assign, ":y_rot_max", 41),
			(else_try),
				(eq, ":instance_kind", "spr_construct_gabion"),
				(assign, ":only_on_terrain", 0),
				(assign, ":offset_y", 100),
				(assign, ":x_dist_left", 25),
				(assign, ":x_dist_right", 25),
				(assign, ":y_dist_front", 25),
				(assign, ":y_dist_back", 25),
				(assign, ":x_rot_min", -30),
				(assign, ":x_rot_max", 31),
				(assign, ":y_rot_min", -30),
				(assign, ":y_rot_max", 31),
			(try_end),
			
			(set_fixed_point_multiplier, 100),
			(agent_get_position, pos20, ":agent_id"),
			(position_move_y, pos20, ":offset_y"),
			
			(copy_position, pos_spawn, pos20),
			(position_set_z_to_ground_level, pos_spawn),
			
			(get_distance_between_positions, ":distance", pos20, pos_spawn),
			(le, ":distance", 100),
			
			#randomize rotation
			(store_random_in_range, ":rot_z_rand", -5, 6),
			(position_rotate_z, pos_spawn, ":rot_z_rand"),
			
			(assign, ":continue", 0),
			(assign, ":rot_x", 0),
			(assign, ":rot_y", 0),
			
			(try_begin),
				(eq, ":instance_kind", "spr_construct_plank"),
				(position_rotate_x, pos_spawn, 45),
				(assign, ":move_step", 10),
				(assign, ":end", 90),
				(try_for_range, ":cur_angle", 0, ":end"),
					(copy_position, pos_calc, pos_spawn),
					(store_mul, ":rot_x", ":cur_angle", -1),
					(position_rotate_x, pos_calc, ":rot_x"),
					(position_move_y, pos_calc, 90),
					(store_div, ":range_end", ":y_dist_front", ":move_step"),
					(try_for_range, ":unused", 9, ":range_end"),
						(position_move_y, pos_calc, ":move_step"),
						(position_get_distance_to_ground_level, ":dist_to_ground", pos_calc),
						(is_between, ":dist_to_ground", -25, 0),
						(assign, ":end", -1),
						(assign, ":range_end", -1),
					(try_end),
				(try_end),
				
				(try_begin),
					(eq, ":end", -1),
					# (agent_get_ground_scene_prop, ":ground_prop_id", ":agent_id"),
					# (try_begin),
						# (prop_instance_is_valid, ":ground_prop_id"),
						# (prop_instance_get_scene_prop_kind, ":ground_prop_kind", ":ground_prop_id"),
						# (try_begin),
							# (neq, ":ground_prop_kind", "spr_construct_plank"),
							# (assign, ":continue", 1),
						# (try_end),
					# (else_try),
						(assign, ":continue", 1),
					# (try_end),
					#REWORK
					
					# (assign, reg0, ":dist_to_ground"),
					# (display_message, "@highest: {reg0}"),
				(try_end),
				
			(else_try),
				(copy_position, pos_calc, pos_spawn),
				(call_script, "script_get_angle_of_ground_at_pos", ":only_on_terrain", 1, ":x_dist_left", ":x_dist_right", ":y_dist_front", ":y_dist_back"),
				(is_between, reg0, ":x_rot_min", ":x_rot_max"),
				(is_between, reg1, ":y_rot_min", ":y_rot_max"),
				(assign, ":continue", 1),
				(assign, ":rot_x", reg0),
				(assign, ":rot_y", reg1),
				
				(eq, ":instance_kind", "spr_construct_spike_group_a"),
				(position_rotate_z, pos_spawn, 180),
			(try_end),
				
			(try_begin),
				(eq, ":only_on_terrain", 1),
				
				(assign, ":continue", 0),
				(position_get_distance_to_terrain, ":dist_to_ground", pos_spawn),
				# (assign, reg1, ":dist_to_ground"),
				# (display_message, "@dist to ground: {reg1}"),
				(is_between, ":dist_to_ground", -50, 51),
				(assign, ":continue", 1),
				
				(eq, ":instance_kind", "spr_construct_earthwork"),
				(position_move_z, pos_spawn, -100),
			(try_end),
			
			#(position_get_rotation_around_z, ":rot_z", pos_spawn),
			#(assign, reg1, ":rot_z"),
			#(display_message, "@rot z: {reg1}"),
			
			(eq, ":continue", 1),
			
			(troop_get_slot, ":cost", "trp_construct_prop_cost", ":instance_kind"),
			(try_begin),
				(gt, ":cost", 0),
				(val_sub, ":cur_build_points", ":cost"),
				(agent_set_ammo, ":agent_id", ":item", ":cur_build_points"),
			(try_end),
			(try_begin),
				(eq, ":instance_kind", "spr_construct_earthwork"),
				(assign, "$next_spawn_prop_hp", 1),
			(try_end),

			(position_rotate_x, pos_spawn, ":rot_x"),
			(position_rotate_y, pos_spawn, ":rot_y"),
			(call_script, "script_spawn_new_or_get_free_scene_prop", ":instance_kind"),
		(try_end),
  ]),

  ("agent_get_gender",
  [
    (store_script_param, ":agent_id", 1),
		(try_begin),
			(agent_is_active, ":agent_id"),
			(agent_is_alive, ":agent_id"),
			(try_begin),
				(this_or_next|agent_is_non_player, ":agent_id"),
				(neg|game_in_multiplayer_mode),
				(agent_get_troop_id, ":agent_troop", ":agent_id"),
				(try_begin),
					(gt, ":agent_troop", -1),
					(troop_get_type, ":gender", ":agent_troop"),
				(try_end),
			(else_try),
				(game_in_multiplayer_mode),
				(agent_get_player_id, ":agent_player", ":agent_id"),
			(player_is_active, ":agent_player"),
				(player_get_gender, ":gender", ":agent_player"),
			(try_end),
			(agent_set_slot, ":agent_id", slot_agent_gender, ":gender"),
		(try_end),
  ]),
  
  #script_control_scene_prop
  ("control_scene_prop",
  [ 
    (store_script_param, ":used_instance_id", 1),
    (store_script_param, ":command", 2),
		
    (try_begin),
  		# (prop_instance_is_valid, ":used_instance_id"),
      (scene_prop_slot_eq, ":used_instance_id", scene_prop_usage_state, 6),#aim
      (scene_prop_get_slot, ":parent_instance_id", ":used_instance_id", scene_prop_parent),
      (prop_instance_is_valid, ":parent_instance_id"),
			
			(scene_prop_get_slot, ":agent_id", ":used_instance_id", scene_prop_user_agent),
			
			(set_fixed_point_multiplier, 100),
			(prop_instance_is_animating, ":animating", ":parent_instance_id"),
			(try_begin),#stop previous animations
				(eq, ":animating", 1),
				(prop_instance_stop_animating, ":parent_instance_id"),
				(prop_instance_get_animation_target_position, pos0, ":parent_instance_id"),
			(else_try),
				(prop_instance_get_position, pos0, ":parent_instance_id"),
			(try_end),
			
			(assign, ":animation_time", 25),
			(assign, ":pos_y_base", 0),
			(assign, ":rot_x_wheel", 0),
			(assign, ":rot_x_barrel", 0),
			(assign, ":rot_z_base", 0),
			
			(try_begin),
				(this_or_next|eq, ":command", prop_move_forward), #UP#
				(this_or_next|eq, ":command", prop_move_left_forward),
				(eq, ":command", prop_move_right_forward),
				(assign, ":rot_x_barrel", 75),
			(else_try),
				(this_or_next|eq, ":command", prop_move_backward), #DOWN#
				(this_or_next|eq, ":command", prop_move_left_backward),
				(eq, ":command", prop_move_right_backward),
				(assign, ":rot_x_barrel", -75),
			(else_try),
				(this_or_next|eq, ":command", prop_move_forward_alt), #UP SLOW#
				(this_or_next|eq, ":command", prop_move_left_forward_alt),
				(eq, ":command", prop_move_right_forward_alt),
				(assign, ":rot_x_barrel", 25),
			(else_try),
				(this_or_next|eq, ":command", prop_move_backward_alt), #DOWN SLOW#
				(this_or_next|eq, ":command", prop_move_left_backward_alt),
				(eq, ":command", prop_move_right_backward_alt),
				(assign, ":rot_x_barrel", -25),
			(try_end),
			
			(try_begin),
				(this_or_next|eq, ":command", prop_move_left), #LEFT#
				(this_or_next|eq, ":command", prop_move_left_forward),
				(eq, ":command", prop_move_left_backward),
				(assign, ":rot_z_base", 75),
				(assign, ":rot_x_wheel", 300),
			(else_try),
				(this_or_next|eq, ":command", prop_move_right), #RIGHT#
				(this_or_next|eq, ":command", prop_move_right_forward),
				(eq, ":command", prop_move_right_backward),
				(assign, ":rot_z_base", -75),
				(assign, ":rot_x_wheel", -300),
			(else_try),
				(this_or_next|eq, ":command", prop_move_left_alt), #LEFT SLOW#
				(this_or_next|eq, ":command", prop_move_left_forward_alt),
				(eq, ":command", prop_move_left_backward_alt),
				(assign, ":rot_z_base", 25),
				(assign, ":rot_x_wheel", 100),
			(else_try),
				(this_or_next|eq, ":command", prop_move_right_alt), #RIGHT SLOW#
				(this_or_next|eq, ":command", prop_move_right_forward_alt),
				(eq, ":command", prop_move_right_backward_alt),
				(assign, ":rot_z_base", -25),
				(assign, ":rot_x_wheel", -100),
			(else_try),
				(is_between, ":command", prop_action_1, prop_action_3),#IGNITE# #CANCEL#
				(try_begin),
					(agent_is_active, ":agent_id"),
					(agent_is_alive, ":agent_id"),
					(agent_set_slot, ":agent_id", slot_agent_use_scene_prop, -1),
					
					(try_begin),
						(this_or_next|multiplayer_is_server),
						(neg|game_in_multiplayer_mode),
						(agent_set_animation, ":agent_id", "anim_crouch_to_stand_keep"),
					(try_end),
					
					(try_begin),#close custom camera and hud
						(neg|multiplayer_is_dedicated_server),
						(call_script, "script_client_get_player_agent"),
						(eq, ":agent_id", reg0),
						(call_script, "script_client_custom_camera", 0),
						(assign, "$g_close_artillery_hud", 1),
					(try_end),
					
					(try_begin), #IGNITE#
						(eq, ":command", prop_action_1),
						(agent_get_wielded_item, ":item", ":agent_id", 0),
						(try_begin),#only with linstock
							(eq, ":item", itm_linstock),
							(store_mission_timer_a_msec, ":cur_time"),
							(val_add, ":cur_time", 2000),
							(scene_prop_set_slot, ":used_instance_id", scene_prop_next_action_time, ":cur_time"),
							(try_begin),
								(neg|multiplayer_is_dedicated_server),
								(prop_instance_get_position, pos12, ":used_instance_id"),
								(position_move_y, pos12, -63),
								(particle_system_burst_no_sync, "psys_cannon_flashpan_smoke", pos12, 25),
								(copy_position, pos_calc, pos12),
								(call_script, "script_reserve_sound_channel", "snd_fuse"),
							(try_end),
						(else_try),
							(scene_prop_set_slot, ":used_instance_id", scene_prop_usage_state, 5),
						(try_end),
					(else_try), #CANCEL#
						(scene_prop_set_slot, ":used_instance_id", scene_prop_usage_state, 5),
					(try_end),
				(try_end),
				
			(else_try),
				(eq, ":command", prop_action_3), #FIRE#

				(assign, ":animation_time", cannon_1_push_back_time),
				(assign, ":pos_y_base", - cannon_1_push_distance),
				(store_div, ":rot_x_wheel", -36000, cannon_1_wheel_circumference),
				(val_mul, ":rot_x_wheel", cannon_1_push_distance),

				#(position_get_rotation_around_x, reg30, pos0),
				#(position_get_rotation_around_y, reg31, pos0),
				#(position_get_rotation_around_z, reg32, pos0),
				#(display_message, "@angles cannon x={reg30} y={reg31} z={reg32}"),
						
				(prop_instance_enable_physics, ":parent_instance_id", 0),#dont use the prop as ground
				(prop_instance_enable_physics, ":used_instance_id", 0),#dont use the prop as ground			
					
				(position_set_z_to_ground_level, pos0),
				(copy_position, pos_calc, pos0),
				(call_script, "script_get_angle_of_ground_at_pos", 0, 1, cannon_1_wheel_right_x, cannon_1_wheel_right_x, cannon_1_wheel_front_y, cannon_1_wheel_front_y),

				(position_get_rotation_around_x, ":rot_x", pos0),
				(position_get_rotation_around_y, ":rot_y", pos0),
				(val_sub, reg0, ":rot_x"),
				(val_sub, reg1, ":rot_y"),
				(position_rotate_x, pos0, reg0),
				(position_rotate_y, pos0, reg1),

				(prop_instance_get_position, pos11, ":used_instance_id"),
				(position_move_y, pos11, 125),
				(try_begin),
					(this_or_next|multiplayer_is_server),#server side only
					(neg|game_in_multiplayer_mode),
					(scene_prop_get_slot, ":ammo_type", ":used_instance_id", scene_prop_ammo_type),#ammo type
					(item_get_max_ammo, ":max_ammo", ":ammo_type"),
						
					(store_sub, ":dummy_barrel", ":ammo_type", itm_cannon_missile_begin),
					(val_add, ":dummy_barrel", itm_cannon_barrel_1_begin),#dummy barrel for the shot

					(item_get_speed_rating, ":min_v0", ":dummy_barrel"),
					(item_get_missile_speed, ":max_v0", ":dummy_barrel"),
					(val_mul, ":max_v0", 100),#set to cm/s

					(val_mul, ":min_v0", ":max_v0"),
					(val_div, ":min_v0", 100),
					(val_add, ":max_v0", 1),

					(item_get_accuracy, ":rotation", ":dummy_barrel"),
					(store_mul, ":rot_max", ":rotation", 50),
					(val_add, ":rot_max", 1),

					(try_for_range, ":unused", 0, ":max_ammo"),
						(copy_position, pos12, pos11),
						(store_random_in_range, ":shot_rot_y", 0, 36000),
						(store_random_in_range, ":shot_rot_x", 0, ":rot_max"),
						
						(set_fixed_point_multiplier, 100),
						(position_rotate_y_floating, pos12, ":shot_rot_y"),
						(position_rotate_x_floating, pos12, ":shot_rot_x"),
						(store_random_in_range, ":shot_v0", ":min_v0", ":max_v0"),#random v0
						
						(try_begin),
							(eq, ":ammo_type", itm_round_shot_32lb_missile),
							(copy_position, pos14, pos12),
							(copy_position, pos15, pos12),
							(position_move_y, pos15, 1000),
							
							(position_get_x, ":pos_x_1", pos14),
							(position_get_x, ":pos_x_2", pos15),
							(position_get_y, ":pos_y_1", pos14),
							(position_get_y, ":pos_y_2", pos15),
							(position_get_z, ":pos_z_1", pos14),
							(position_get_z, ":pos_z_2", pos15),
							(store_sub, ":dist_x", ":pos_x_1", ":pos_x_2"),
							(store_sub, ":dist_y", ":pos_y_2", ":pos_y_1"),
							(store_sub, ":dist_z", ":pos_z_2", ":pos_z_1"),
							
							(position_set_z, pos14, 0),
							(position_set_z, pos15, 0),
							(get_distance_between_positions, ":dist_diagonal", pos14, pos15),
									
							(try_begin),
								(neq, ":dist_x", 0),
								(neq, ":dist_y", 0),
								(store_mul, ":calc_x", ":dist_x", 100),
								(store_div, ":div_value", ":calc_x", ":dist_y"),
								(store_atan, ":missile_rot_z", ":div_value"),
								(try_begin),
									(lt, ":dist_y", 0),
									(val_sub, ":missile_rot_z", 18000),
								(try_end),

							(else_try),
								(eq, ":dist_y", 0),
								(try_begin),
									(ge, ":dist_x", 0),
									(assign, ":missile_rot_z", 9000),
								(else_try),
									(assign, ":missile_rot_z", 27000),
								(try_end),
							(else_try),
								(assign, ":missile_rot_z", 0),
							(try_end),
									
							(try_begin),
								(neq, ":dist_diagonal", 0),
								(neq, ":dist_z", 0),
								(store_mul, ":calc_z", ":dist_z", 100),
								(store_div, ":div_value", ":calc_z", ":dist_diagonal"),
								(store_atan, ":missile_rot_x", ":div_value"),
							(else_try),
								(eq, ":dist_diagonal", 0),
								(try_begin),
									(ge, ":dist_z", 0),
									(assign, ":missile_rot_x", 9000),
								(else_try),
									(assign, ":missile_rot_x", 27000),
								(try_end),
							(else_try),
								(assign, ":missile_rot_x", 0),
							(try_end),
								
							# (position_get_rotation_around_z, reg1, pos12),
							# (assign, reg2, ":missile_rot_z"),
							# (position_get_rotation_around_x, reg3, pos12),
							# (assign, reg4, ":missile_rot_x"),
							# (assign, reg5, ":dist_x"),
							# (assign, reg6, ":dist_y"),
							# (assign, reg7, ":dist_z"),
							# (display_message, "@[Z] op:{reg1} calc:{reg2} [Y] op:{reg3} calc:{reg4} ... x: {reg5} y: {reg6} z: {reg7}"),
									
							(init_position,	pos13),
							(position_copy_origin, pos13,	pos12),
							(position_rotate_z_floating, pos13, ":missile_rot_z"),
									
							(store_cos, ":cos_of_angle", ":missile_rot_x"),
							(store_mul,	":init_y_vel", ":cos_of_angle",	":shot_v0"),
							(val_div,	":init_y_vel", 100),
							
							(store_sin, ":sin_of_angle", ":missile_rot_x"),
							(store_mul,	":init_z_vel",":sin_of_angle", ":shot_v0"),
							(val_div,	":init_z_vel", 100),

							(copy_position, pos_spawn, pos13),
              (call_script, "script_spawn_new_or_get_free_scene_prop", "spr_round_shot_missile"),
							(call_script, "script_prop_missile_init", reg0, ":init_y_vel", ":init_z_vel", ":agent_id"),
						(else_try),
							(add_missile, ":agent_id", pos12, ":shot_v0", ":dummy_barrel", 0, ":ammo_type", 0),
						(try_end),
					(try_end),
				(try_end),
			  
				(try_begin),#particles and sound client side
					(neg|multiplayer_is_dedicated_server),
					(copy_position, pos12, pos11),
					(position_move_y, pos12, -63),
					(position_move_z, pos12, 15),
					
					(particle_system_burst_no_sync, "psys_cannon_flashpan_fire", pos12, 1),
					(particle_system_burst_no_sync, "psys_cannon_flashpan_smoke", pos12, 25),
					
					(particle_system_burst_no_sync, "psys_cannon_muzzle_fire", pos11, 1),
					(particle_system_burst_no_sync, "psys_cannon_muzzle_smoke", pos11, 100),
					
					(copy_position, pos_calc, pos11),
					(call_script, "script_reserve_sound_channel", "snd_cannon_fire"),
					(try_begin),
						(eq, "$g_show_shot_distance", 1),
						(call_script, "script_client_get_player_agent"),
						(eq, reg0, ":agent_id"),
						(set_fixed_point_multiplier, 100),
						(position_get_x, ":pos_x", pos11),
						(position_get_y, ":pos_y", pos11),
						(position_get_z, ":pos_z", pos11),
						(agent_set_slot, ":agent_id", slot_agent_shot_pos_x, ":pos_x"),
						(agent_set_slot, ":agent_id", slot_agent_shot_pos_y, ":pos_y"),
						(agent_set_slot, ":agent_id", slot_agent_shot_pos_z, ":pos_z"),
					(try_end),
				(try_end),							
		
				(try_begin),
					(eq, "$g_quick_artillery", 1),
					(scene_prop_set_slot, ":used_instance_id", scene_prop_usage_state, 2),
				(else_try),
					(scene_prop_set_slot, ":used_instance_id", scene_prop_usage_state, 0),
				(try_end),
				#reenable physics
				(prop_instance_enable_physics, ":parent_instance_id", 1),
				(prop_instance_enable_physics, ":used_instance_id", 1),
			(try_end),
      
			(try_begin),
				(this_or_next|neq, ":pos_y_base", 0),
				(neq, ":rot_z_base", 0),
				(try_begin),
					(neq, ":pos_y_base", 0),
					(position_move_y, pos0, ":pos_y_base"),
				(else_try),
					(position_rotate_z_floating, pos0, ":rot_z_base"),
					(scene_prop_get_slot, ":rot_z", ":parent_instance_id", scene_prop_rot_z),
					(val_add, ":rot_z", ":rot_z_base"),
					(try_begin),
						(ge, ":rot_z", 36000),
						(val_sub, ":rot_z", 36000),
					(else_try),
						(lt, ":rot_z", 0),
						(val_add, ":rot_z", 36000),
					(try_end),
					(scene_prop_set_slot, ":parent_instance_id", scene_prop_rot_z, ":rot_z"),
				(try_end),
				(try_begin),#only animate clientside, reduces network traffic
					(neg|multiplayer_is_dedicated_server),
					(prop_instance_animate_to_position, ":parent_instance_id", pos0, ":animation_time"),
				(else_try),#serverside
					(prop_instance_set_position, ":parent_instance_id", pos0, 1),
				(try_end),
			(try_end),

			(try_begin),
				(neg|is_between, ":command", prop_action_1, prop_action_3),
				(prop_instance_get_position, pos_move, ":used_instance_id"),
				# (position_get_rotation_around_x, reg30, pos_move),
				# (position_get_rotation_around_y, reg31, pos_move),
				# (position_get_rotation_around_z, reg32, pos_move),
				# (display_message, "@angles cannon x={reg30} y={reg31} z={reg32}"),
				(try_for_range, ":cur_slot_prop", scene_prop_child_1, scene_prop_child_5 +1),
					(assign, ":rot_x_new", 0),
					(try_begin),
						(eq, ":cur_slot_prop", scene_prop_child_1),
						(assign, ":cur_instance_id", ":used_instance_id"),
						(assign, ":rot_x_new", ":rot_x_barrel"),
					(else_try),
						(scene_prop_get_slot, ":cur_instance_id", ":parent_instance_id", ":cur_slot_prop"),
						(assign, ":rot_x_new", ":rot_x_wheel"),
						(try_begin),
							(neq, ":command", prop_action_3),
							(this_or_next|eq, ":cur_slot_prop", scene_prop_child_3),
							(eq, ":cur_slot_prop", scene_prop_child_5),
							(val_mul, ":rot_x_new", -1),
						(try_end),
					(try_end),
					
					(prop_instance_is_valid, ":cur_instance_id"),
					(try_begin),
						(this_or_next|neq, ":rot_x_new", 0),
						(this_or_next|neq, ":pos_y_base", 0),
						(neq, ":rot_z_base", 0),
						
						(store_sub, ":pos_offset_array", ":cur_slot_prop", scene_prop_child_1),
						(val_add, ":pos_offset_array", "trp_cannon_1_child_1_pos"),
						(troop_get_slot, ":pos_x", ":pos_offset_array", pos_x),
						(troop_get_slot, ":pos_y", ":pos_offset_array", pos_y),
						(troop_get_slot, ":pos_z", ":pos_offset_array", pos_z),
						(copy_position, pos_move, pos0),
						(position_move_x, pos_move, ":pos_x"),
						(position_move_y, pos_move, ":pos_y"),
						(position_move_z, pos_move, ":pos_z"),
						
						(scene_prop_get_slot, ":rot_x", ":cur_instance_id", scene_prop_rot_x),
						(val_add, ":rot_x", ":rot_x_new"),
						(position_rotate_x_floating, pos_move, ":rot_x"),
						(try_begin),
							(neq, ":rot_x_new", 0),
							(try_begin),
								(is_between, ":cur_slot_prop", scene_prop_child_2, scene_prop_child_5 +1),
								(try_begin),
									(ge, ":rot_x", 36000),
									(val_sub, ":rot_x", 36000),
								(else_try),
									(lt, ":rot_x", 0),
									(val_add, ":rot_x", 36000),
								(try_end),
							(try_end),
							(scene_prop_set_slot, ":cur_instance_id", scene_prop_rot_x, ":rot_x"),
						(try_end),
						
						(try_begin),#only animate clientside, reduces network traffic
							(neg|multiplayer_is_dedicated_server),
							(prop_instance_is_animating, ":animating", ":cur_instance_id"),
							(try_begin),#stop previous animations
								(eq, ":animating", 1),
								(prop_instance_stop_animating, ":cur_instance_id"),
							(try_end),
							(prop_instance_animate_to_position, ":cur_instance_id", pos_move, ":animation_time"),
						(else_try),#serverside
							(prop_instance_set_position, ":cur_instance_id", pos_move, 1),
						(try_end),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("clear_scene_prop",
  [
    (store_script_param, ":instance_id", 1),
    (prop_instance_clear_attached_missiles, ":instance_id"),
		(prop_instance_refill_hit_points, ":instance_id"),
		
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			(try_begin),
				(gt, "$next_spawn_prop_hp", -1),
				#(assign, reg1, "$next_spawn_prop_hp"),
			#(display_message, "@hp {reg1}"),
				(scene_prop_get_max_hit_points, ":damage_hp", ":instance_id"),
				(val_sub, ":damage_hp", "$next_spawn_prop_hp"),
			(try_begin),
				(gt, ":damage_hp", 1),
				(prop_instance_receive_damage, ":instance_id", "$agent_deployed_shield", ":damage_hp"),
			(try_end),
				(assign, "$next_spawn_prop_hp", -1),
			(try_end),
		(try_end),
  ]),
	
  ("init_scene_prop",
	[
		(store_trigger_param_1, ":instance_id"),
		(store_script_param, ":material", 1),
		
		(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
		(store_mission_timer_a, ":mission_time"),
		(scene_prop_set_slot, ":instance_id", scene_prop_spawn_time, ":mission_time"),
		
		(scene_prop_set_slot, ":instance_id", scene_prop_material, ":material"),
		(try_begin),
			(eq, ":material", material_stone),
			(scene_prop_set_slot, ":instance_id", scene_prop_resistance, resistance_stone),
		(else_try),
			(eq, ":material", material_wood),
			(scene_prop_set_slot, ":instance_id", scene_prop_resistance, resistance_wood),
		(else_try),
			(eq, ":material", material_metal),
			(scene_prop_set_slot, ":instance_id", scene_prop_resistance, resistance_metal),
		(try_end),
	]),

  ("init_game_data", init_game_data()),
	
  ("spawn_new_or_get_free_scene_prop",
  [
    (store_script_param, ":scene_prop_id", 1),
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),

			(assign, ":free_prop_instance", -1),#try to get an unused prop
			(scene_prop_get_num_instances, ":num_instances", ":scene_prop_id"),
			(try_for_range, ":cur_instance", 0, ":num_instances"),
				(scene_prop_get_instance, ":cur_instance_id", ":scene_prop_id", ":cur_instance"),
				(scene_prop_slot_eq, ":cur_instance_id", scene_prop_is_used, 0),#free
				(assign, ":free_prop_instance", ":cur_instance_id"),
				(assign, ":num_instances", -1),#stop loop
			(try_end),

			(try_begin),
				(gt, ":free_prop_instance", -1),#free prop found
				(prop_instance_set_position, ":free_prop_instance", pos_spawn, 0),
				(prop_instance_enable_physics, ":free_prop_instance", 1),
				(call_script, "script_multiplayer_server_set_scene_prop_slot", ":free_prop_instance", scene_prop_is_used, 1),
				(call_script, "script_clear_scene_prop", ":free_prop_instance"),
				(assign, reg0, ":free_prop_instance"),
			(else_try),
				(set_spawn_position, pos_spawn),
				(spawn_scene_prop, ":scene_prop_id"),
			(try_end),
		(try_end),
  ]),

  ("move_scene_prop_under_ground",
  [
    (store_script_param, ":instance_id", 1),
		(store_script_param, ":dont_send_to_clients", 2),

		(prop_instance_enable_physics, ":instance_id", 0),
		
		#stop previous animations
		(prop_instance_is_animating, ":animating", ":instance_id"),
		(try_begin),
			(eq, ":animating", 1),
			(prop_instance_stop_animating, ":instance_id"),
		(try_end),
		
    (set_fixed_point_multiplier, 100),
		(prop_instance_get_position, pos_move, ":instance_id"),
		(position_move_z, pos_move, -10000, 1),#global
    (prop_instance_set_position, ":instance_id", pos_move, ":dont_send_to_clients"),
		(try_begin),
			(eq, ":dont_send_to_clients", 0),
      (call_script, "script_multiplayer_server_set_scene_prop_slot", ":instance_id", scene_prop_is_used, 0),
		(else_try),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 0),
		(try_end),
  ]),
	
  ("reset_scene_prop_position",
  [
    (store_script_param, ":instance_id", 1),
		(store_script_param, ":dont_send_to_clients", 2),

		#stop previous animations
		(prop_instance_is_animating, ":animating", ":instance_id"),
		(try_begin),
			(eq, ":animating", 1),
			(prop_instance_stop_animating, ":instance_id"),
		(try_end),
		
    #(set_fixed_point_multiplier, 100),
		(prop_instance_get_starting_position, pos_move, ":instance_id"),
		#(prop_instance_get_position, pos_move, ":instance_id"),
		#(position_move_z, pos_move, 10000, 1),#global
    (prop_instance_set_position, ":instance_id", pos_move, ":dont_send_to_clients"),
		(prop_instance_enable_physics, ":instance_id", 1),
		(try_begin),
			(eq, ":dont_send_to_clients", 0),
      (call_script, "script_multiplayer_server_set_scene_prop_slot", ":instance_id", scene_prop_is_used, 1),
		(else_try),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
			(call_script, "script_clear_scene_prop", ":instance_id"),
		(try_end),
  ]),

  ("agent_drowning",
  [
    (store_script_param, ":agent_id", 1),
	
    (assign, ":server_drowning", 1),
		(assign, ":continue", 0),
	
		(try_begin),
			(eq, ":agent_id", -1),
			(call_script, "script_client_get_player_agent"),
			(assign, ":agent_id", reg0),
			(try_begin),
        (agent_is_active, ":agent_id"),
        (agent_is_alive, ":agent_id"),
				(assign, ":server_drowning", 0),
				(assign, ":continue", 1),
			(try_end),
		(else_try),
			(assign, ":continue", 1),
		(try_end),
    
		(try_begin),
      (eq, ":continue", 1),
      (assign, ":drowning", 0),
      (set_fixed_point_multiplier, 100),#set to cm #fix for drowning
      (agent_get_position, pos1, ":agent_id"),
      (position_get_z,":cur_z_position", pos1),
    
			(try_begin),#human agent
        (agent_is_human, ":agent_id"),
        (try_begin),
					(neg|agent_is_non_player, ":agent_id"),
					(agent_get_troop_id, ":troop_id", ":agent_id"),
					(gt, ":troop_id", -1), 
					(troop_get_slot, ":max_time", ":troop_id", slot_troop_base_stamina),
					(val_div, ":max_time", drown_stamina_time),
        (else_try),
					(assign, ":max_time", drown_time_human),
        (try_end),
				(agent_get_horse, ":horse", ":agent_id"),
				(try_begin),#unmounted agent
          (eq, ":horse", -1),
          (agent_get_crouch_mode, ":crouch", ":agent_id"),
          (try_begin),
            (eq, ":crouch", 1),
            (le,":cur_z_position",-140),
            (assign, ":drowning", 5),
          (else_try),
            (le,":cur_z_position",-200),
            (assign, ":drowning", 5),
          (try_end),
        (else_try),#mounted agent
          (le,":cur_z_position",-280),
          (assign, ":drowning", 5),
        (try_end),
      (else_try),#horse agent
        (assign, ":max_time", drown_time_horse),
				(le,":cur_z_position",-210),
        (assign, ":drowning", 10),#X damage for humans, 2X for horses
      (try_end),
    
      (agent_get_slot, ":timer", ":agent_id", slot_agent_drown_time),
      (try_begin),
        (ge, ":drowning", 1),
        (val_add, ":timer", 1),
        (val_min, ":timer", ":max_time"),
		
				(try_begin),#only serverside
					(eq, ":server_drowning", 1),
					(store_sub, ":max_time_sub", ":max_time", 1),
          (try_begin),
            # (eq, ":timer", ":max_time_sub"),
            # (agent_play_sound, ":agent_id", "snd_drowning"),
          # (else_try),
            (gt, ":timer", ":max_time_sub"),
						(agent_get_slot, ":damage", ":agent_id", slot_agent_interval_damage),
            (val_add, ":damage", ":drowning"),
						(agent_set_slot, ":agent_id", slot_agent_interval_damage, ":damage"),
						(agent_set_slot, ":agent_id", slot_agent_interval_weapon, itm_weapon_drown),
          (try_end),		
				(try_end),
      (else_try),#above waterlevel
        (gt, ":timer", 0),
        (val_sub, ":timer", 2),
        (val_max, ":timer", 0),
      (try_end),

      (try_begin),
        (neg|agent_slot_eq, ":agent_id", slot_agent_drown_time, ":timer"),
        (agent_set_slot, ":agent_id", slot_agent_drown_time, ":timer"),
      (try_end),
    (try_end),
  ]),

	("server_set_bleed_interval",
  [
    (store_script_param, ":agent", 1),
		(store_script_param, ":value", 2),

		(try_begin),
			(neg|agent_slot_eq, ":agent", slot_agent_bleed_interval, ":value"),
			# (assign, reg1, ":value"),
			# (display_message, "@2: {reg1}"),
			(try_begin),
				(agent_slot_ge, ":agent", slot_agent_bleed_interval, 1),
				(ge, ":value", 1),
				(assign, ":synce", 0),
			(else_try),#only synce if its 0 or changed from 0 to somewhat
				(assign, ":synce", 1),
			(try_end),
			
			(agent_set_slot, ":agent", slot_agent_bleed_interval, ":value"),#set slot on server
			(try_begin),
				(eq, ":synce", 1),
				(multiplayer_is_server), # If this is a server broadcast to player
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
				(agent_is_human, ":agent"),
				(agent_get_team, ":agent_team", ":agent"),
				(try_begin),
					(neg|agent_is_non_player, ":agent"),
					(agent_get_player_id, ":player", ":agent"),
				(else_try),
					(assign, ":player", -1),
				(try_end),
				
				(get_max_players, ":num_players"),
				(try_for_range, ":cur_player", 1, ":num_players"),
					(player_is_active, ":cur_player"),
					(neq, ":player", ":cur_player"),
					(player_get_agent_id, ":cur_agent", ":cur_player"),
					(agent_is_active, ":cur_agent"),
					(agent_is_alive, ":cur_agent"),
					(agent_get_team, ":cur_agent_team", ":cur_agent"),
					(eq, ":agent_team", ":cur_agent_team"),
					(agent_has_item_equipped, ":cur_agent", itm_surgeon_kit),
					(multiplayer_send_2_int_to_player, ":cur_player", multiplayer_event_client_agent_set_bleed_interval, ":agent", ":value"),
				(try_end),
			(try_end),
		(try_end),
	]),
	
  ("agent_bleeding",
  [
    (store_script_param, ":agent_id", 1),
		
		(agent_set_slot, ":agent_id", slot_agent_interval_weapon, itm_weapon_bleed),
		# (agent_set_slot, ":agent_id", slot_agent_interval_sound, 1),
		
    (agent_get_slot, ":bleed_time", ":agent_id", slot_agent_bleed_time),
		(agent_get_slot, ":bleed_interval", ":agent_id", slot_agent_bleed_interval),
		(val_add, ":bleed_time", ":bleed_interval"),
		(agent_set_slot, ":agent_id", slot_agent_bleed_time, ":bleed_time"),
		
		(agent_get_slot, ":damage", ":agent_id", slot_agent_interval_damage),
		(val_add, ":damage", 1),
		(agent_set_slot, ":agent_id", slot_agent_interval_damage, ":damage"),
    
		(agent_get_slot, ":hp_lost", ":agent_id", slot_agent_bleed_hp_lost),
		(val_add, ":hp_lost", 1),
		(agent_set_slot, ":agent_id", slot_agent_bleed_hp_lost, ":hp_lost"),
  ]),
  
  ("agent_interval_damage",
  [
    (store_script_param, ":agent_id", 1),
	
		(agent_get_slot, ":damage", ":agent_id", slot_agent_interval_damage),
    (store_agent_hit_points, ":agent_hp", ":agent_id", 1),
    (val_sub, ":agent_hp", ":damage"),
    (agent_set_hit_points, ":agent_id", ":agent_hp", 1),

    (try_begin),#more than 0 hp
      (gt, ":agent_hp", 0),
      (try_begin),
        (agent_slot_eq, ":agent_id", slot_agent_interval_sound, 1),
				(try_begin),
					(agent_is_human, ":agent_id"),
					(try_begin),
						(agent_slot_eq, ":agent_id", slot_agent_gender, tf_male),
						(agent_play_sound, ":agent_id", "snd_man_hit"),
					(else_try),
						(agent_play_sound, ":agent_id", "snd_woman_hit"),
					(try_end),
				(else_try),	
					(agent_play_sound, ":agent_id", "snd_horse_low_whinny"),
				(try_end),
      (try_end),
    (else_try),#less than 1 hp
      (agent_get_slot, ":last_attacker", ":agent_id", slot_agent_last_attacker_agent),
			(agent_get_slot, ":weapon_no", ":agent_id", slot_agent_interval_weapon),
      (try_begin),
        (agent_is_active, ":last_attacker"),
        (agent_deliver_damage_to_agent, ":last_attacker", ":agent_id", 1, ":weapon_no"),
      (else_try),
        (agent_deliver_damage_to_agent, ":agent_id", ":agent_id", 1, ":weapon_no"),
      (try_end),
      (try_begin),
        (agent_slot_eq, ":agent_id", slot_agent_interval_sound, 1),
				(agent_is_human, ":agent_id"),
        (neg|multiplayer_is_dedicated_server),#only on local server
        (try_begin),
          (agent_slot_eq, ":agent_id", slot_agent_gender, tf_male),
          (agent_play_sound, ":agent_id", "snd_man_die"),
        (else_try),
          (agent_play_sound, ":agent_id", "snd_woman_die"),
        (try_end),
      (try_end),
    (try_end),
	
	#reset slots
		(agent_set_slot, ":agent_id", slot_agent_interval_damage, 0),
    (agent_set_slot, ":agent_id", slot_agent_interval_sound, 0),
  ]),
  
  ("agent_speed_limiter",
  [
		(store_script_param, ":agent_id", 1),
	
		(try_begin),
			(agent_slot_eq, ":agent_id", slot_agent_is_sprinting, 0),
    
			(agent_get_horse, ":horse", ":agent_id"),
			(le, ":horse", 0),
			(assign, ":walk", 0),
			# (assign, ":stop_crouch", 0),
			(agent_get_wielded_item, ":weapon", ":agent_id", 0),
    
			(try_begin),
				(gt, ":weapon", 0),
				(item_get_type, ":weapon_type", ":weapon"),
				(try_begin),
					(this_or_next|eq, ":weapon_type", itp_type_bow),
					(this_or_next|eq, ":weapon_type", itp_type_crossbow),
					(this_or_next|eq, ":weapon_type", itp_type_musket),
					(eq, ":weapon_type", itp_type_pistol),
					#(agent_get_attack_action, ":attack_action", ":agent_id"),
					(call_script, "script_workaround_agent_attack_action", ":agent_id"),
					(try_begin),
						(this_or_next|eq, reg0, 1),#readying_attack
						(this_or_next|eq, reg0, 2),#releasing_attack
						(eq, reg0, 5),#reloading
						(assign, ":walk", 1),
					(try_end),
					# (try_begin),
						# (eq, ":weapon_type", itp_type_crossbow),
						# (eq, reg0, 5),#reloading
						# (assign, ":stop_crouch", 1),
					# (else_try),
						# (this_or_next|eq, ":weapon", itm_flat_bow),
						# (this_or_next|eq, ":weapon", itm_reflex_bow),
						# (this_or_next|eq, ":weapon", itm_long_bow),
			
						# (this_or_next|eq, ":weapon", itm_flat_bow +loom_1_item),
						# (this_or_next|eq, ":weapon", itm_reflex_bow +loom_1_item),
						# (this_or_next|eq, ":weapon", itm_long_bow +loom_1_item),
			
						# (this_or_next|eq, ":weapon", itm_flat_bow +loom_2_item),
						# (this_or_next|eq, ":weapon", itm_reflex_bow +loom_2_item),
						# (this_or_next|eq, ":weapon", itm_long_bow +loom_2_item),
			
						# (this_or_next|eq, ":weapon", itm_flat_bow +loom_3_item),
						# (this_or_next|eq, ":weapon", itm_reflex_bow +loom_3_item),
						# (eq, ":weapon", itm_long_bow +loom_3_item),
						# (try_begin),
							# (this_or_next|eq, reg0, 1),#readying_attack
							# (eq, reg0, 2),#releasing_attack
							# (assign, ":stop_crouch", 1),
						# (try_end),
					# (try_end),
				(try_end),
			(try_end),
 
			(try_begin),
				(eq, ":walk", 1),
				(agent_slot_eq, ":agent_id", slot_agent_limiter_walk, 0),
				(call_script, "script_agent_walk", ":agent_id", walk_by_limiter),
			(else_try),
				(eq, ":walk", 0),
#        (try_begin),
				(agent_slot_eq, ":agent_id", slot_agent_limiter_walk, 1),
				(call_script, "script_agent_walk", ":agent_id", walk_by_limiter),
#          (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_limiter_walk, 0),
#        (try_end),
#        (agent_slot_eq, ":agent_id", slot_agent_key_walk, 0),
#        (agent_slot_eq, ":agent_id", slot_agent_stamina_walk, 0),
#        (agent_slot_eq, ":agent_id", slot_agent_is_walking, 1),
#        (call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_is_walking, 0),
#        (call_script, "script_agent_reset_speed", ":agent_id"),
			(try_end),
    
			# (try_begin),#stop crouching
				# (eq, ":stop_crouch", 1),
				# (agent_get_crouch_mode, ":crouch", ":agent_id"),
				# (eq, ":crouch", 1),
				# (agent_set_crouch_mode, ":agent_id", 0),
			# (try_end),
		(try_end),
  ]),
	
  ("restore_agent",
  [	
		(store_script_param, ":agent_id", 1),
		
		(agent_set_hit_points, ":agent_id", 100, 0), #100%
		(agent_refill_ammo, ":agent_id"),
		(agent_refill_wielded_shield_hit_points, ":agent_id"),
		(agent_set_slot, ":agent_id", slot_agent_bleed_time, 0),
		(call_script, "script_server_set_bleed_interval", ":agent_id", 0),
		(agent_get_troop_id, ":agent_troop", ":agent_id"),
		(troop_get_slot, ":base_stamina", ":agent_troop", slot_troop_base_stamina),
		(call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_cur_stamina, ":base_stamina"),
		(call_script, "script_multiplayer_server_set_agent_slot", ":agent_id", slot_agent_max_stamina, ":base_stamina"),
		(agent_get_horse, ":horse", ":agent_id"),
		(try_begin),
			(ge, ":horse", 0),
			(agent_set_hit_points, ":horse", 100, 0), #100%
			(agent_set_slot, ":horse", slot_agent_bleed_time, 0),
			(call_script, "script_server_set_bleed_interval", ":horse", 0),
		(try_end),
  ]),
  
  ("ai_set_advanced_order",
  [
	  (store_script_param, ":team_no", 1),
		(store_script_param, ":group_no", 2),
		(store_script_param, ":order_type", 3),
		
		(try_begin),
		  (this_or_next|eq, ":order_type", "str_order_movement_run"),
			(eq, ":order_type", "str_order_movement_walk"),
			(assign, ":offset", "trp_group_0_order_run"),
			(try_begin),
				(eq, ":order_type", "str_order_movement_run"),
				(assign, ":value", 1),
			(else_try),
				(assign, ":value", 0),
			(try_end),
			
		(else_try),
		  (this_or_next|eq, ":order_type", "str_order_shot_volley"),
			(eq, ":order_type", 0),
			(assign, ":offset", "trp_group_0_order_volley"),
			(try_begin),
				(eq, ":order_type", "str_order_shot_volley"),
				(assign, ":value", 1),
				(try_begin),
					(this_or_next|multiplayer_is_server),
					(neg|game_in_multiplayer_mode),
					(store_mission_timer_a, ":cur_time"),
					(val_add, ":cur_time", 1),
					(try_for_range, ":cur_array", "trp_team_volley_check_time_bow", "trp_team_volley_shot_time_bow"),
						(troop_set_slot, ":cur_array", ":team_no", ":cur_time"),
					(try_end),
				(try_end),
			(else_try),
				(assign, ":value", 0),
			(try_end),
			
		(else_try),
		  (this_or_next|eq, ":order_type", "str_order_shot_fire"),
			(eq, ":order_type", "str_order_shot_normal"),
			(assign, ":offset", "trp_group_0_order_fire"),
			(try_begin),
				(eq, ":order_type", "str_order_shot_fire"),
				(assign, ":value", 1),
			(else_try),
				(assign, ":value", 0),
			(try_end),
    (try_end),
		
		(store_add, ":cur_array", ":offset", ":group_no"),
		(troop_set_slot, ":cur_array", ":team_no", ":value"),
		(try_begin),
			(eq, ":group_no", grc_everyone),
			(try_for_range, ":array", ":offset", ":cur_array"),
				(troop_set_slot, ":array", ":team_no", ":value"),
			(try_end),
	  (try_end),
		
		(try_begin),
			(neg|multiplayer_is_dedicated_server),
			(gt, ":order_type", 0),
			(try_begin),
				(game_in_multiplayer_mode),
				(multiplayer_get_my_player, ":my_player"),
				(str_store_player_username, s1, ":my_player"),
			(else_try),
				(get_player_agent_no, ":player_agent"),
				(agent_is_active, ":player_agent"),
				(agent_is_alive, ":player_agent"),
				(str_store_agent_name, s1, ":player_agent"),
			(try_end),
			(try_begin),
				(neq, ":group_no", grc_everyone),
				(str_store_class_name, s2, ":group_no"),
			(else_try),
				(str_store_string, s2, "@Everyone"),
			(try_end),
			(store_add, ":string", ":order_type", 5),
			(display_message, ":string", 0xbfcd7e),
		(try_end),
		#message
		###
		(try_begin),
			(eq, "$get_url_response", 0),
			(eq, "$g_show_debug_messages", 1),
			(assign, reg50, ":team_no"),
			(assign, reg51, ":group_no"),
			(assign, reg52, ":order_type"),
			(assign, reg53, ":value"),
			(display_message, "@team:{reg50} group:{reg51} order:{reg52} value:{reg53}"),
		(try_end),
		###
  ]),
 
  ("client_stamina_change_1",
  [
		(store_script_param, ":full_second", 1),
		
    (call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
		(try_begin),
			(agent_is_active, ":player_agent"),
      (agent_is_alive, ":player_agent"),
      
			(try_begin),
				(eq, ":full_second", 1),
				#limit max stamina due low hp
				(store_agent_hit_points, ":agent_hp", ":player_agent", 1),
				(try_begin),
					(le, ":agent_hp", agent_hp_cant_run),
					(try_begin),
						(neg|agent_slot_eq, ":player_agent", slot_agent_max_stamina, agent_run_stamina),
						(agent_set_slot, ":player_agent", slot_agent_max_stamina, agent_run_stamina),
						(agent_set_slot, ":player_agent", slot_agent_cur_stamina, 0),
					
						(agent_slot_eq, ":player_agent", slot_agent_stamina_walk, 0),
						(try_begin),
							(game_in_multiplayer_mode),
							(neg|multiplayer_is_server),
							(multiplayer_send_int_to_server, multiplayer_event_agent_walk, walk_by_stamina),
						(else_try),
							(call_script, "script_agent_walk", ":player_agent", walk_by_stamina),
						(try_end),
					(try_end),
					
				(else_try),
					(le, ":agent_hp", agent_hp_cant_sprint),
					(try_begin),
						(neg|agent_slot_eq, ":player_agent", slot_agent_max_stamina, agent_sprint_stamina),
						(agent_set_slot, ":player_agent", slot_agent_max_stamina, agent_sprint_stamina),
						(agent_slot_ge, ":player_agent", slot_agent_cur_stamina, agent_sprint_stamina),
						(agent_set_slot, ":player_agent", slot_agent_cur_stamina, agent_sprint_stamina),
					(try_end),
					
				(else_try),
					(agent_get_troop_id, ":player_troop", ":player_agent"),
					(gt, ":player_troop", -1),
					(troop_get_slot, ":base_stamina", ":player_troop", slot_troop_base_stamina),
					(neg|agent_slot_eq, ":player_agent", slot_agent_max_stamina, ":base_stamina"),
					(agent_set_slot, ":player_agent", slot_agent_max_stamina, ":base_stamina"),
				(try_end),
			(try_end),
			
			#(agent_get_attack_action, ":agent_attack_action", ":player_agent"),
			(call_script, "script_workaround_agent_attack_action", ":player_agent"),
			#
			#(display_message, "@attack: {reg0}"),
			#
			# (try_begin),
			# 	(eq, reg0, 1),#readying_attack
			# 	(assign, ":ready_action", 1),
			# (else_try),
			# 	(assign, ":ready_action", 0),
			# (try_end),
			
			(try_begin),
				(this_or_next|eq, reg0, 2),#releasing_attack
				(eq, reg0, 5),#reloading
				(assign, ":attack_action", 1),
			(else_try),
				(assign, ":attack_action", 0),
			(try_end),

			# (agent_get_defend_action, ":agent_defend_action", ":player_agent"),
			# (try_begin),
			# 	(this_or_next|eq, ":agent_defend_action", 1),#parrying
			# 	(eq, ":agent_defend_action", 2),#blocking
			# 	(assign, ":defend_action", 1),
			# (else_try),
			# 	(assign, ":defend_action", 0),
			# (try_end),
			
			(agent_get_wielded_item, ":item", ":player_agent", 0),
			(try_begin),
				(gt, ":item", -1),
				(item_get_type, ":item_type", ":item"),
				(eq, ":item_type", itp_type_bow),
				(eq, reg0, 1),#readying_attack
				(assign, ":bow_draw", 1),
			(else_try),
				(assign, ":bow_draw", 0),
			(try_end),

			(agent_get_animation, ":anim", ":player_agent", 0),
			
			(try_begin),
				(this_or_next|is_between, ":anim", "anim_jump", "anim_stand_unarmed"),#no jumping
				(this_or_next|eq, ":anim", "anim_kick_right_leg"),#no kicking
				(this_or_next|eq, ":anim", "anim_shield_bash"),#no shield bash
				(eq, ":anim", "anim_crouch_to_stand"),#no crouching
				(assign, ":movement_action", 1),
			(else_try),
				(assign, ":movement_action", 0),
			(try_end),
				
			(try_begin),
				(is_between, ":anim", "anim_run_forward", "anim_walk_forward"),#running ..buggy
				(assign, ":run_movement", 1),
			(else_try),
				(assign, ":run_movement", 0),
			(try_end),

			(agent_get_horse, ":horse", ":player_agent"),
			(try_begin),
				(eq, ":movement_action", 0),
				(gt, ":horse", -1),
				(agent_get_animation, ":horse_anim", ":horse", 0),
				(is_between, ":horse_anim", "anim_horse_pace_1", "anim_horse_rear"),
				(assign, ":horse_movement", 1),
			(else_try),
				(assign, ":horse_movement", 0),
			(try_end),
			
			(assign, ":script_id", -1),
			(assign, ":message", -1),
			#no sprinting
			(try_begin),
				(agent_slot_eq, ":player_agent", slot_agent_is_sprinting, 0),
				(try_begin),
					(eq, ":full_second", 1),
					(try_begin),#keys pressed
						(this_or_next|eq, ":run_movement", 1),
						(eq, ":horse_movement", 1),
						(try_begin),
							(agent_slot_eq, ":player_agent", slot_agent_is_walking, 0),
							(agent_slot_eq, ":player_agent", slot_agent_use_scene_prop, -1),
							(assign, ":script_id", "script_agent_lose_stamina"),
							(try_begin),
								(eq, ":horse_movement", 1),
								(assign, ":message", stamina_run_horse),
							(else_try),
								(assign, ":message", stamina_run),
							(try_end),
						(else_try),
							(eq, ":bow_draw", 0),#pulling a bow consumes stamina
							(eq, ":attack_action", 0),
							(eq, ":movement_action", 0),
							(this_or_next|agent_slot_eq, ":player_agent", slot_agent_is_walking, 1),
							(agent_slot_ge, ":player_agent", slot_agent_use_scene_prop, 0),
							(agent_slot_eq, ":player_agent", slot_agent_drown_time, 0),
							(assign, ":script_id", "script_agent_recover_stamina"),
							(assign, ":message", stamina_recover_walk),
						(try_end),
					(else_try),#keys losed
						(eq, ":bow_draw", 0),#pulling a bow consumes stamina
						(eq, ":attack_action", 0),
						(eq, ":movement_action", 0),
						(agent_slot_eq, ":player_agent", slot_agent_drown_time, 0),#recover stamina only above water
						(assign, ":script_id", "script_agent_recover_stamina"),
						(assign, ":message", stamina_recover_stand),
					(try_end),
				(try_end),
			
			#sprinting
			(else_try),
				(agent_slot_eq, ":player_agent", slot_agent_is_walking, 0),
				(try_begin),
					(game_key_is_down, gk_crouch),
					(this_or_next|eq, ":run_movement", 1),
					(eq, ":horse_movement", 1),
					# (eq, ":attack_action", 0),
					# (eq, ":ready_action", 0),
					# (eq, ":defend_action", 0),
					(agent_slot_ge, ":player_agent", slot_agent_cur_stamina, agent_run_stamina),
					(try_begin),
						(eq, ":full_second", 1),
						(assign, ":script_id", "script_agent_lose_stamina"),
						(try_begin),# unmounted agent
							(le, ":horse", 0),
							(assign, ":message", stamina_sprint),
						(else_try),# mounted agent
							(assign, ":message", stamina_sprint_horse),
						(try_end),
					(try_end),
				(else_try),
					(try_begin),
						(game_in_multiplayer_mode),
						(neg|multiplayer_is_server),
						(multiplayer_send_message_to_server, multiplayer_event_agent_sprint_stop),
					(else_try),
						(call_script, "script_agent_sprint_stop", ":player_agent"),
					(try_end),
				(try_end),
			(try_end),

			(gt, ":script_id", -1),
			(gt, ":message", -1),
			(call_script, ":script_id", ":player_agent", ":message"),
		(try_end),
  ]),
  
  ("client_speed_calculation",
  [ 
    (call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
		(try_begin),
			(agent_is_active, ":player_agent"),
      (agent_is_alive, ":player_agent"),
  
      (agent_get_speed, pos6, ":player_agent"),
    
      (init_position, pos7),
      (get_distance_between_positions, ":distance",pos6, pos7),
	  
			(gt, ":distance", 0),

      (store_div, reg61, ":distance", 100),
      (store_mod, reg62, ":distance", 100),
      (try_begin),
        (lt, reg62, 10),
        (str_store_string, s1, "@{reg61}.0{reg62}"),
      (else_try),
        (str_store_string, s1, "@{reg61}.{reg62}"),
      (try_end),

      #set m/s to km/h
      (store_mul, reg1, ":distance", 36),
      (val_div, reg1, 10),
      (store_div, reg61, reg1, 100),
      (store_mod, reg62, reg1, 100),
      (try_begin),
        (lt, reg62, 10),
        (str_store_string, s2, "@{reg61}.0{reg62}"),
      (else_try),
        (str_store_string, s2, "@{reg61}.{reg62}"),
      (try_end),
#      (neq, reg1, 0),
      (display_message, "@{s1} m/s   {s2} km/h", 0xCCCCCC),
		(try_end),
  ]),

  ("client_custom_camera",
  [
    (store_script_param, ":mode", 1),
		(try_begin),
			(neq, "$custom_camera", ":mode"),
      (assign, "$custom_camera", ":mode"),
			(try_begin),
				(gt, ":mode", 0),
				(mission_cam_set_mode, 1, 250, 1),
				(try_begin),
					(eq, "$g_show_debug_messages", 1),
					(display_message, "@[DEBUG] custom camera enabled", 0xCCCCCC),
				(try_end),
			(else_try),
				(mission_cam_set_mode, 0, 250, 1),
				(call_script, "script_client_get_player_agent"),
				(assign, ":player_agent", reg0),
				(agent_is_active, ":player_agent"),
        (agent_is_alive, ":player_agent"),
				(agent_set_visibility, ":player_agent", 1),
				(try_begin),
					(eq, "$g_show_debug_messages", 1),
					(display_message, "@[DEBUG] custom camera disabled", 0xCCCCCC),
				(try_end),
			(try_end),
		(try_end),
	
		(try_begin),
			(eq, ":mode", 1),
			(call_script, "script_client_get_player_agent"),
			(assign, ":player_agent", reg0),
			(agent_is_active, ":player_agent"),
      (agent_is_alive, ":player_agent"),
			(agent_get_slot, ":prop_instance", ":player_agent", slot_agent_use_scene_prop),
			(prop_instance_is_valid, ":prop_instance"),
	  
			(agent_set_visibility, ":player_agent", 0),
			(set_fixed_point_multiplier, 100),
			(prop_instance_get_position, pos40, ":prop_instance"),
			# (try_begin),
				# (camera_in_first_person),#WSE
				# (position_move_y, pos40, 100, 0),
			# (else_try),
#REWORK
				(position_move_y, pos40, -100, 0),
				(position_move_z, pos40, 25, 0),
				(position_rotate_x, pos40, -1),
			# (try_end),
			#fov 75 is default
			(assign, "$custom_camera_fov", 75),
			(try_begin),
				(game_key_is_down, gk_zoom),
				(assign, "$custom_camera_fov", 37),
			(try_end),
			#(mission_cam_animate_to_position, pos40, 100, 0),
			(mission_cam_animate_to_position_and_aperture, pos40, "$custom_camera_fov", 100, 0),
		(try_end),
  ]),
	
  ("client_get_player_agent",
  [ 
		(try_begin),
			(game_in_multiplayer_mode),
			(multiplayer_get_my_player, ":my_player"),
			(try_begin),
				(player_is_active, ":my_player"),
				(player_get_agent_id, reg0, ":my_player"),
			(else_try),
				(assign, reg0, -1),
			(try_end),
		(else_try),
			(get_player_agent_no, reg0),
		(try_end),
  ]),
	
  ("cf_no_action_menus_active",
  [
		(assign, ":menu_open", 0),
		(assign, ":end", "prsnt_ammo_box_hud"),
		(try_for_range, ":cur_presentation", "prsnt_game_multiplayer_admin_panel", ":end"),
			(neg|is_between, ":cur_presentation", "prsnt_multiplayer_message_1", "prsnt_multiplayer_escape_menu"),
			(neq, ":cur_presentation", "prsnt_mod_hud"),
			(neq, ":cur_presentation", "prsnt_multiplayer_duel_start_counter"),
			(neq, ":cur_presentation", "prsnt_surgeon_hud"),
			(is_presentation_active, ":cur_presentation"),
			(assign, ":end", -1),
			(assign, ":menu_open", 1),
		(try_end),
		(try_begin),
			(eq, ":menu_open", 0),
			(game_in_multiplayer_mode),
			(neg|multiplayer_is_dedicated_server),
			(multiplayer_get_my_player, ":player_no"),
			(player_is_active, ":player_no"),
			(player_is_busy_with_menus, ":player_no"),
			(assign, ":menu_open", 1),
		(try_end),
		(eq, ":menu_open", 0),
	]),
	
  ("thunderbolt_hit",
  [
		(init_position, pos53),
		(set_fixed_point_multiplier, 100),
		(position_set_x, pos53, "$g_thunderbolt_x"),
		(position_set_y, pos53, "$g_thunderbolt_y"),
		(position_set_z, pos53, "$g_thunderbolt_z"),
		
		(try_begin),
			(neg|multiplayer_is_dedicated_server),
			(try_begin),
				(lt, "$g_thunderbolt_z", 50000),
				(copy_position, pos54, pos53),
				(position_move_z, pos54, 50000),
				(particle_system_burst_no_sync, "psys_upper_lightning", pos54, 1),
				(store_random_in_range, ":particle", "psys_lightning_1", "psys_upper_lightning"),
				(particle_system_burst_no_sync, ":particle", pos53, 1),
			(try_end),
			(copy_position, pos_calc, pos53),
			(call_script, "script_reserve_sound_channel", "snd_thunder_close"),
			(store_mission_timer_a_msec, "$g_thunderbolt_next_time"),
			(store_random_in_range, ":duration", 250, 1001),
			(store_add, "$g_thunderbolt_end_time", "$g_thunderbolt_next_time", ":duration"),
			(assign, "$g_thunderbolt_state", 1),
		(try_end),
		
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			(is_between, "$g_thunderbolt_x", 0, "$g_scene_max_x"),
			(is_between, "$g_thunderbolt_y", 0, "$g_scene_max_y"),
			(try_for_agents, ":cur_agent"),
				(agent_is_active, ":cur_agent"),
				(agent_is_alive,":cur_agent"),
				(agent_get_position, pos54, ":cur_agent"),
				(position_move_z, pos54, 90),
				
				(get_distance_between_positions,":distance", pos53, pos54),
				(lt, ":distance", 2000),
				(store_sub, ":damage", 2000, ":distance"),
				(gt, ":damage", 0),
				(agent_deliver_damage_to_agent, ":cur_agent", ":cur_agent", ":damage", 0),
			(try_end),
		(try_end),
  ]),
	
    ("apply_weather",
    [
		(set_fixed_point_multiplier, 100),
		(set_shader_param_float, "@vCloudAmount", "$g_cloud_amount"),
		(set_fog_distance, "$g_fog_distance", "$g_fog_color"),
		(call_script, "script_calculate_wind_axis_speed"),
		(try_begin),
			(gt, "$g_precipitation_strength", 0),
			(neq, "$g_cur_scene_type", scene_type_snow),
			(neq, "$g_cur_scene_type", scene_type_snow_forest),
			(play_sound, "snd_rain"),
		(try_end),
	]),
	
    ("calculate_wind_axis_speed",
    [
		(store_mul, ":total_wind_mm", "$g_wind_strength", 1000),
		(store_mul, ":angle", "$g_wind_direction", 1000),
		(set_fixed_point_multiplier, 1000),
		(store_sin, "$g_wind_x_mm", ":angle"),#X
		(store_cos, "$g_wind_y_mm", ":angle"),#Y
		(val_mul, "$g_wind_x_mm", "$g_wind_strength"),
		(val_mul, "$g_wind_y_mm", "$g_wind_strength"),
		
		(set_shader_param_float4, "@vWindVector", "$g_wind_x_mm", "$g_wind_y_mm", 0, ":total_wind_mm"),
		# (set_shader_param_float, "@vWindSpeedX", "$g_wind_x_mm"),
		# (set_shader_param_float, "@vWindSpeedY", "$g_wind_y_mm"),
		
		# (assign, reg30, "$g_wind_x_mm"),
		# (assign, reg31, "$g_wind_y_mm"),
		# (display_message, "@X:{reg30}mm Y:{reg31}mm"),
	]),
	
    ("reserve_sound_channel",
    [
		(store_script_param, ":sound", 1),
		
		(set_fixed_point_multiplier, 100),
		(position_get_x, ":pos_x", pos_calc),
		(position_get_y, ":pos_y", pos_calc),
		(position_get_z, ":pos_z", pos_calc),
		
		(store_mission_timer_a_msec, ":cur_time"),
		(assign, ":end_channel", "trp_sound_channels_end"),
		(try_for_range, ":cur_sound_channel", "trp_sound_channel_0", ":end_channel"),
			(troop_slot_eq, ":cur_sound_channel", sound_slot_id, -1),
			(troop_set_slot, ":cur_sound_channel", sound_slot_id, ":sound"),
			(troop_set_slot, ":cur_sound_channel", sound_slot_time, ":cur_time"),
			(troop_set_slot, ":cur_sound_channel", sound_slot_pos_x, ":pos_x"),
			(troop_set_slot, ":cur_sound_channel", sound_slot_pos_y, ":pos_y"),
			(troop_set_slot, ":cur_sound_channel", sound_slot_pos_z, ":pos_z"),
			(assign, ":end_channel", -1),#break loop
		(try_end),
		(try_begin),
			(neq, ":end_channel", -1),
			(display_message, "@ERROR: no free sound channel found", 0xff0000),
		(try_end),
    ]),
	
    ("init_on_round_start",
    [
		(assign, "$g_order_menu_open", -1),
		(assign, "$g_order_selected_group", grc_everyone),
		# (assign, "$g_order_flag_was_active", 0),
		
		#reset advanced orders
		(try_begin),
			(game_in_multiplayer_mode),
			(get_max_players, ":end_team"),
		(else_try),
			(assign, ":end", 8),
			(try_for_range, ":end_team", 0, ":end"),
				(num_active_teams_le, ":end_team"),
				(assign, ":end", -1),#break loop
			(try_end),
		(try_end),
		
		(get_max_players, ":end_team"),
		(try_for_range, ":cur_team", 0, ":end_team"),
			(try_for_range, ":cur_array", advanced_order_arrays_begin, advanced_order_arrays_end),
				(troop_set_slot, ":cur_array", ":cur_team", 0),
			(try_end),
		(try_end),
		
		(try_begin),
        (neg|multiplayer_is_dedicated_server),
			(try_begin),
				(neq, "$custom_camera", 0),
        (call_script, "script_client_custom_camera", 0),
				(assign, "$g_close_artillery_hud", 1),
			(try_end),
		(try_end),

		(try_begin),
			(eq, "$g_show_debug_messages", 1),
			(display_message, "@[DEBUG] round starts", 0xCCCCCC),
		(try_end),
    ]),

  ("multiplayer_server_explosion_at_position",
  [
		(try_begin),
			(neg|multiplayer_is_dedicated_server), # If a client and not a dedicated server that calls then play locally.
			(position_get_distance_to_terrain, ":terrain_dist", pos_hit), 
			# (assign, reg1, ":terrain_dist"),
			# (display_message, "@{reg1}"),
			(try_begin),
				(lt, ":terrain_dist", 100),#sometimes buggy.. so a bigger value
				(particle_system_burst_no_sync, "psys_hit_explosion_big", pos_hit, 1),
				(particle_system_burst_no_sync, "psys_hit_ground_dust_big", pos_hit, 10),
				(particle_system_burst_no_sync, "psys_hit_ground_big", pos_hit, 100),
			(else_try),
				(particle_system_burst_no_sync, "psys_hit_explosion_big", pos_hit, 1),
				(particle_system_burst_no_sync, "psys_hit_object_dust_big", pos_hit, 10),
			(try_end),
			(copy_position, pos_calc, pos_hit),
			(call_script, "script_reserve_sound_channel", "snd_explosion"),
		(try_end),
		(try_begin),
			(multiplayer_is_server), # If this is a server broadcast the sound to all players 
			(set_fixed_point_multiplier, 100),
			(position_get_x, ":pos_x", pos_hit),
			(position_get_y, ":pos_y", pos_hit),
			(position_get_z, ":pos_z", pos_hit),
			(get_max_players, ":num_players"),
			(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
				(player_is_active, ":cur_player"),
				(multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_explosion_at_pos,":pos_x",":pos_y",":pos_z"),
			(try_end),
		(try_end),
  ]),

  ("multiplayer_server_set_scene_prop_slot",
  [
    (store_script_param, ":instance_id", 1),
    (store_script_param, ":slot_no", 2),
    (store_script_param, ":value", 3),
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),

			(try_begin),
				(neg|scene_prop_slot_eq, ":instance_id", ":slot_no", ":value"),
				(scene_prop_set_slot, ":instance_id", ":slot_no", ":value"),#set slot on server
				(multiplayer_is_server), # If this is a server broadcast to all players
				(get_max_players, ":num_players"),
				(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
					(player_is_active, ":cur_player"),
					(multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_client_set_prop_slot, ":instance_id", ":slot_no", ":value"),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("multiplayer_server_set_troop_slot",
  [
    (store_script_param, ":troop_id", 1),
    (store_script_param, ":slot_no", 2),
    (store_script_param, ":value", 3),
    (store_script_param, ":type", 4),
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),

			(try_begin),
				(neg|troop_slot_eq, ":troop_id", ":slot_no", ":value"),
				(troop_set_slot, ":troop_id", ":slot_no", ":value"),#set slot on server
				(multiplayer_is_server), # If this is a server broadcast to all players
				(assign, ":player_id", -1),
				(get_max_players, ":num_players"),#get the player id for the troop
				(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
					(player_is_active, ":cur_player"),
					(player_get_troop_id, ":cur_troop", ":cur_player"),
					(eq, ":cur_troop", ":troop_id"),
					(assign, ":player_id", ":cur_player"),
					(assign, ":num_players", -1),#break loop
				(try_end),
			
				(gt, ":player_id", -1),
				(try_begin),
					(eq, ":type", to_player),
					(gt, ":player_id", 0), #dont sent to server
					(player_is_active, ":player_id"),
					(multiplayer_send_3_int_to_player, ":player_id", multiplayer_event_client_set_troop_slot, ":troop_id", ":slot_no", ":value"),
				(else_try),
					(eq, ":type", to_all),
					(get_max_players, ":num_players"),
					(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
						(player_is_active, ":cur_player"),
						(multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_client_set_troop_slot, ":troop_id", ":slot_no", ":value"),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),  
  
  ("multiplayer_server_set_player_slot",
  [
    (store_script_param, ":player_id", 1),
    (store_script_param, ":slot_no", 2),
    (store_script_param, ":value", 3),
    (store_script_param, ":type", 4),

		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),

			(try_begin),
				(neg|player_slot_eq, ":player_id", ":slot_no", ":value"),
				(player_set_slot, ":player_id", ":slot_no", ":value"),#set slot on server
				(multiplayer_is_server), # If this is a server broadcast to all players
				(try_begin),
					(eq, ":type", to_player),
					(gt, ":player_id", 0), #dont sent to server
					(player_is_active, ":player_id"),
					(multiplayer_send_2_int_to_player, ":player_id", multiplayer_event_client_set_player_slot, ":slot_no", ":value"),
				(else_try),
					(eq, ":type", to_all),
					(get_max_players, ":num_players"),
					(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
						(player_is_active, ":cur_player"),
						(multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_client_set_player_slot, ":slot_no", ":value", ":player_id"),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("multiplayer_server_set_agent_slot",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":slot_no", 2),
    (store_script_param, ":value", 3),
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),

	#    (assign, reg1, ":slot_no"),
	#    (display_message, "@{reg1}", 0xCCCCCC),

			(try_begin),
				(this_or_next|is_between, ":slot_no", slot_agent_cur_stamina, slot_agent_max_stamina +1),
				(neg|agent_slot_eq, ":agent_id", ":slot_no", ":value"),
				(agent_set_slot, ":agent_id", ":slot_no", ":value"),#set slot on server
				(try_begin),
					(multiplayer_is_server), # If this is a server broadcast to player
					(neg|agent_is_non_player, ":agent_id"),
					(agent_get_player_id, ":player_id", ":agent_id"),
					(gt, ":player_id", 0), #dont sent to server
					(player_is_active, ":player_id"),
					(multiplayer_send_2_int_to_player, ":player_id", multiplayer_event_client_set_agent_slot, ":slot_no", ":value"),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("multiplayer_server_set_troop_id",
  [
    (store_script_param, ":player_id", 1),
    (store_script_param, ":troop_id", 2),
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			(player_set_troop_id, ":player_id", ":troop_id"),#set troop on server
		(try_end),
  ]),

  ("multiplayer_set_troop_value",
  [
    (store_script_param, ":troop_id", 1),
    (store_script_param, ":event_type", 2),
    (store_script_param, ":value_type", 3),
    (store_script_param, ":value", 4),

		#set troop value on server
		(try_begin),
			(eq, ":event_type", multiplayer_event_client_set_attribute),
			(try_begin),
				(eq, "$g_show_debug_messages", 1),
				(eq, "$get_url_response", 0),
				(assign, reg50, ":troop_id"),
				(assign, reg51, ":value_type"),
				(assign, reg52, ":value"),
				(display_message, "@[DEBUG] troop {reg50} attribute {reg51} is set to {reg52}", 0xCCCCCC),
			(try_end),
			(store_attribute_level, ":last_value", ":troop_id", ":value_type"),
			(store_sub, ":calc_value", ":value", ":last_value"),
			(troop_raise_attribute, ":troop_id", ":value_type", ":calc_value"),
			#set stamina
			(try_begin),
				(lt, ":value_type", ca_intelligence),
				(call_script, "script_set_base_stamina", ":troop_id"),
			(try_end),
		(else_try),
			(eq, ":event_type", multiplayer_event_client_set_proficiency),
			(try_begin),
				(eq, "$g_show_debug_messages", 1),
				(eq, "$get_url_response", 0),
				(assign, reg50, ":troop_id"),
				(assign, reg51, ":value_type"),
				(assign, reg52, ":value"),
				(display_message, "@[DEBUG] troop {reg50} proficiency {reg51} is set to {reg52}", 0xCCCCCC),
			(try_end),
			(store_proficiency_level, ":last_value", ":troop_id", ":value_type"),
			(store_sub, ":calc_value", ":value", ":last_value"),
			(troop_raise_proficiency_linear, ":troop_id", ":value_type", ":calc_value"),
		(else_try),
			(eq, ":event_type", multiplayer_event_client_set_skill),
			(try_begin),
				(eq, "$g_show_debug_messages", 1),
				(eq, "$get_url_response", 0),
				(assign, reg50, ":troop_id"),
				(assign, reg51, ":value_type"),
				(assign, reg52, ":value"),
				(display_message, "@[DEBUG] troop {reg50} skill {reg51} is set to {reg52}", 0xCCCCCC),
			(try_end),
			(store_skill_level, ":last_value", ":value_type", ":troop_id"),
			(store_sub, ":calc_value", ":value", ":last_value"),
			(troop_raise_skill, ":troop_id", ":value_type", ":calc_value"),
		(else_try),
			(eq, ":event_type", multiplayer_event_client_raise_all_proficiencies),
			(try_for_range, ":cur_wpt", wpt_one_handed_weapon, wpt_firearm + 1),
				(troop_raise_proficiency_linear, ":troop_id", ":cur_wpt", ":value"),
			(try_end),
			(try_begin),
				(eq, "$g_show_debug_messages", 1),
				(eq, "$get_url_response", 0),
				(assign, reg50, ":troop_id"),
				(assign, reg51, ":value"),
				(display_message, "@[DEBUG] troop {reg50} proficiencies are changed by {reg51}", 0xCCCCCC),
			(try_end),
		(try_end),
			
		(try_begin),
			(multiplayer_is_server), # If this is a server broadcast to all players
			(get_max_players, ":num_players"),
			(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
				(player_is_active, ":cur_player"),
				(try_begin),
					(eq, ":event_type", multiplayer_event_client_raise_all_proficiencies),
					(multiplayer_send_2_int_to_player, ":cur_player", ":event_type", ":troop_id", ":value"),
				(else_try),
					(store_mul, ":send_type", ":value_type", 1000),
					(store_add, ":send_value", ":value", ":send_type"),
					(multiplayer_send_2_int_to_player, ":cur_player", ":event_type", ":troop_id", ":send_value"),
				(try_end),
			(try_end),
		(try_end),
  ]),
  
  ("multiplayer_server_set_horse_speed",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":speed", 2),
		
		(agent_set_horse_speed_factor, ":agent_id", ":speed"),
		(try_begin),
			(multiplayer_is_server), # If this is a server broadcast to all players
			(get_max_players, ":num_players"),
			(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
				(player_is_active, ":cur_player"),
				(multiplayer_send_2_int_to_player, ":cur_player", multiplayer_event_client_agent_set_horse_speed, ":agent_id", ":speed"),
			(try_end),
		(try_end),
	]),
	
  ("multiplayer_server_ammo_box",
  [
    (store_script_param, ":agent_id", 1),
    (store_script_param, ":item_no", 2),

		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
		
			(agent_get_slot, ":instance_id", ":agent_id", slot_agent_use_scene_prop),
			(agent_set_slot, ":agent_id", slot_agent_use_scene_prop, -1),#set slot on server
			(try_begin),
				(prop_instance_is_valid, ":instance_id"),
				(assign, ":slot_1_begin", scene_prop_item_1),
				(assign, ":slot_1_end", scene_prop_item_2 +1),
				(try_for_range, ":cur_slot", ":slot_1_begin", ":slot_1_end"),
					(scene_prop_get_slot, ":cur_item", ":instance_id", ":cur_slot"),
					(eq, ":cur_item", ":item_no"),
					(try_begin),
						(eq, ":cur_slot", scene_prop_item_1),
						(assign, ":slot_2", scene_prop_item_1_cur_ammo),
					(else_try),
						(assign, ":slot_2", scene_prop_item_2_cur_ammo),
					(try_end),
					(scene_prop_get_slot, ":cur_ammo", ":instance_id", ":slot_2"),
					(val_sub, ":cur_ammo", 1),
					(scene_prop_set_slot, ":instance_id", ":slot_2", ":cur_ammo"),#set slot on server
					(assign, ":slot_1_end", -1),#stop loop
				(try_end),
				(prop_instance_get_position, pos_spawn, ":instance_id"),
				(set_spawn_position, pos_spawn),
				(spawn_item, ":item_no", 0, item_prune_time),
			
				(try_begin),
					(multiplayer_is_server), # If this is a server broadcast to all players
					(agent_get_player_id, ":player_id", ":agent_id"),
					(get_max_players, ":num_players"),
					(try_for_range, ":cur_player", 1, ":num_players"), #0 is server so starting from 1
						(player_is_active, ":cur_player"),
						(neq, ":player_id", ":cur_player"),#do not send to the player who uses the ammo box, slots are set clientside
						(multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_client_set_prop_slot, ":instance_id", ":slot_2", ":cur_ammo"),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),

  ("multiplayer_set_server_settings",[
    (store_script_param, ":player_no", 1),
    (store_script_param, ":sub_type", 2),
    (store_script_param, ":value", 3),
		(store_script_param, ":value_2", 4),
		
		(try_begin),
			(this_or_next|player_is_active, ":player_no"),
			(lt, ":player_no", 1),
			
			# (assign, reg0, ":player_no"),
			# (assign, reg1, ":sub_type"),
			# (assign, reg2, ":value"),
			# (assign, reg3, ":value_2"),
			# (display_message, "@(script) player:{reg0} type:{reg1} value1:{reg2} value2:{reg3}"),
			
			(str_clear, s0),
			(multiplayer_get_my_player, ":my_player_no"),
			(try_begin),
				(this_or_next|player_is_active, ":player_no"),
				(player_is_active, ":my_player_no"),
				(try_begin),
					(player_is_active, ":player_no"),
					(str_store_player_username, s1, ":player_no"),
				(else_try),
					(str_store_player_username, s1, ":my_player_no"),
				(try_end),
				(assign, reg0, ":value"),
				(assign, reg1, ":value_2"),
			(try_end),
		
			(try_begin),
				(eq, ":sub_type", sub_event_max_num_players),
				(server_set_max_num_players, ":value"),
				(str_store_string, s0, "@Maximum number of players set to {reg0} by {s1}."),

			(else_try),
				(eq, ":sub_type", sub_event_num_bots_in_team),
				(try_begin),
					(eq, ":value", 1),
					(assign, "$g_multiplayer_num_bots_team_1", ":value_2"),
				(else_try),
					(assign, "$g_multiplayer_num_bots_team_2", ":value_2"),
				(try_end),
				(this_or_next|player_is_active, ":player_no"),
				(this_or_next|eq, ":player_no", 0),#Server
				(player_is_active, ":my_player_no"),
				(store_sub, ":check_value", ":value", 1),
				(team_get_faction, ":faction", ":check_value"),
				(str_store_faction_name, s3, ":faction"),
				(str_store_string, s0, "@Number of bots in {s3} set to {reg1} by {s1}."),

			# (else_try),
				# (eq, ":sub_type", sub_event_anti_cheat),
				# (server_set_anti_cheat, ":value"),
				# (str_store_string, s0, "@Valve Anti-Cheat {reg0?enabled:disabled} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_friendly_fire),
				# (server_set_friendly_fire, ":value"),
				# (str_store_string, s0, "@Ranged friendly fire {reg0?enabled:disabled} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_melee_friendly_fire),
				# (server_set_melee_friendly_fire, ":value"),
				# (str_store_string, s0, "@Melee friendly fire {reg0?enabled:disabled} by {s1}."),
							
			# (else_try),
				# (eq, ":sub_type", sub_event_friendly_fire_damage_self_ratio),
				# (server_set_friendly_fire_damage_self_ratio, ":value"),
				# (str_store_string, s0, "@Friendly fire self damage {reg0?set to {reg0}%:disabled} by {s1}."),

			# (else_try),
				# (eq, ":sub_type", sub_event_friendly_fire_damage_friend_ratio),
				# (server_set_friendly_fire_damage_friend_ratio, ":value"),
				# (str_store_string, s0, "@Friendly fire damage to friend {reg0?set to {reg0}%:disabled} by {s1}."),

			(else_try),
				(eq, ":sub_type", sub_event_ghost_mode),
				(server_set_ghost_mode, ":value"),
				(store_add, ":ghost_mode", ":value", "str_free"),
				(str_store_string, s2, ":ghost_mode"),
				(str_store_string, s0, "@Spectator camera set to {s2} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_control_block_dir),
				(server_set_control_block_dir, ":value"),
				(str_store_string, s0, "@Control block direction set to {reg0?mouse movement:automatic} by {s1}."),

			(else_try),
				(eq, ":sub_type", sub_event_combat_speed),
				(server_set_combat_speed, ":value"),
				(store_add, ":combat_speed", ":value", "str_combat_speed_0"),
				(str_store_string, s2, ":combat_speed"),
				(str_store_string, s0, "@Combat speed set to {s2} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_respawn_count),   
				(assign, "$g_multiplayer_number_of_respawn_count", ":value"),
				(str_store_string, s0, "@Defender respawn count set to {reg0?{reg0}:unlimited} by {s1}."),

			# (else_try),
				# (eq, ":sub_type", sub_event_add_to_servers_list),
				# (server_set_add_to_game_servers_list, ":value"),
				# (str_store_string, s0, "@Server {reg0?added to:removed from} the official servers list by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_respawn_period), 
				(assign, "$g_multiplayer_respawn_period", ":value"),
				(str_store_string, s0, "@Respawn period set to {reg0} seconds by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_game_max_minutes),
				# (assign, "$g_multiplayer_game_max_minutes", ":value"),
				# (str_store_string, s0, "@Map time limit set to {reg0} minutes by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_round_max_seconds),
				(assign, "$g_multiplayer_round_max_seconds", ":value"),
				(str_store_string, s0, "@Round time limit set to {reg0} seconds by {s1}."),
							
			(else_try),
				(eq, ":sub_type", sub_event_player_respawn_as_bot),
				(assign, "$g_multiplayer_player_respawn_as_bot", ":value"),
				(str_store_string, s0, "@Switch to bot on death {reg0?enabled:disabled} by {s1}."),	
			
			(else_try),
				(eq, ":sub_type", sub_event_game_max_points),
				(assign, "$g_multiplayer_game_max_points", ":value"),
				(str_store_string, s0, "@Team point limit set to {reg0} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_point_gained_from_flags),
				# (assign, "$g_multiplayer_point_gained_from_flags", ":value"),
				# (str_store_string, s0, "@Team points gained from flags set to {reg0}% by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_point_gained_from_capturing_flag),
				# (assign, "$g_multiplayer_point_gained_from_capturing_flag", ":value"),
				# (str_store_string, s0, "@Points gained for capturing flags {reg0?set to {reg0}:disabled} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_initial_gold_multiplier),
				# (assign, "$g_multiplayer_initial_gold_multiplier", ":value"),
				# (str_store_string, s0, "@Starting gold {reg0?set to {reg0}%:disabled} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_battle_earnings_multiplier),
				# (assign, "$g_multiplayer_battle_earnings_multiplier", ":value"),
				# (str_store_string, s0, "@Combat gold bonus {reg0?set to {reg0}%:disabled} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_round_earnings_multiplier),
				# (assign, "$g_multiplayer_round_earnings_multiplier", ":value"),
				# (str_store_string, s0, "@Round gold bonus {reg0?set to {reg0}%:disabled} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_next_team_faction),
				(try_begin),
					(eq, ":value", 1),
					(assign, "$g_multiplayer_next_team_1_faction", ":value_2"),
				(else_try),
					(assign, "$g_multiplayer_next_team_2_faction", ":value_2"),
				(try_end),
				(this_or_next|player_is_active, ":player_no"),
				(this_or_next|eq, ":player_no", 0),#Server
				(player_is_active, ":my_player_no"),
				(str_store_faction_name, s2, ":value_2"),
				(str_store_string, s0, "@next team {reg0} faction changed to {s2} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_valid_vote_ratio),
				# (assign, "$g_multiplayer_valid_vote_ratio", ":value"),
				# (str_store_string, s0, "@Poll accept threshold set to {reg0}% by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_auto_team_balance_limit),
				(assign, "$g_multiplayer_auto_team_balance_limit", ":value"),
				(val_mod, reg0, 1000),
				(str_store_string, s0, "@Auto team balance threshold set to {reg0?{reg0}:unlimited} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_num_bots_voteable),
				(assign, "$g_multiplayer_num_bots_voteable", ":value"),
				(str_store_string, s0, "@Bot count limit for polls {reg0?set to {reg0}:disabled} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_factions_voteable),
				(assign, "$g_multiplayer_factions_voteable", ":value"),
				(str_store_string, s0, "@Polls to change factions {reg0?enabled:disabled} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_maps_voteable),
				(assign, "$g_multiplayer_maps_voteable", ":value"),
				(str_store_string, s0, "@Polls to change maps {reg0?enabled:disabled} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_kick_voteable),
				(assign, "$g_multiplayer_kick_voteable", ":value"),
				(str_store_string, s0, "@Polls to kick players {reg0?enabled:disabled} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_ban_voteable),
				(assign, "$g_multiplayer_ban_voteable", ":value"),
				(str_store_string, s0, "@Polls to ban players {reg0?enabled:disabled} by {s1}."),
			
			(else_try),
				(eq, ":sub_type", sub_event_allow_player_banners),
				(assign, "$g_multiplayer_allow_player_banners", ":value"),
				(str_store_string, s0, "@Individual banners {reg0?enabled:disabled} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_force_default_armor),
				# (assign, "$g_multiplayer_force_default_armor", ":value"),
				# (str_store_string, s0, "@Force minimum armor {reg0?enabled:disabled} by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_disallow_ranged_weapons),
				# (assign, "$g_multiplayer_disallow_ranged_weapons", ":value"),
				# (str_store_string, s0, "@{reg0?Disallowed:Allowed} ranged weapons by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_disallow_melee_weapons),
				# (assign, "$g_multiplayer_disallow_melee_weapons", ":value"),
				# (str_store_string, s0, "@{reg0?Disallowed:Allowed} melee weapons by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_disallow_armors),
				# (assign, "$g_multiplayer_disallow_armors", ":value"),
				# (str_store_string, s0, "@{reg0?Disallowed:Allowed} armors by {s1}."),
			
			# (else_try),
				# (eq, ":sub_type", sub_event_disallow_horses),
				# (assign, "$g_multiplayer_disallow_horses", ":value"),
				# (str_store_string, s0, "@{reg0?Disallowed:Allowed} horses by {s1}."),

			(else_try),
				(eq, ":sub_type", sub_event_quick_artillery),
				(assign, "$g_quick_artillery", ":value"),
				(str_store_string, s0, "@{reg0?Allowed:Disallowed} quick artillery by {s1}."),
				
			(else_try),
				(eq, ":sub_type", sub_event_server_announcements),
				(assign, "$g_server_announcements", ":value"),
				(str_store_string, s0, "@{reg0?Enabled:Disabled} server announcements by {s1}."),
				
			(else_try),
				(eq, ":sub_type", sub_event_all_items),
				(assign, "$g_inventory_all_items", ":value"),
				(str_store_string, s0, "@{reg0?Enabled:Disabled} all inventory items by {s1}."),
				
			(else_try),
				(eq, ":sub_type", sub_event_allow_online_chars),
				(assign, "$g_allow_online_chars", ":value"),
				(str_store_string, s0, "@{reg0?Enabled:Disabled} Online Characters by {s1}."),
				
			(else_try),
				(eq, ":sub_type", sub_event_allow_faction_troops),
				(assign, "$g_allow_faction_troops", ":value"),
				(str_store_string, s0, "@{reg0?Enabled:Disabled} Faction Troops by {s1}."),
				
			(else_try),
				(eq, ":sub_type", sub_event_cloud_amount),
				(assign, "$g_multiplayer_cloud_amount", ":value"),
				(try_begin),
					(multiplayer_is_server),
					(call_script, "script_multiplayer_server_set_weather_time"),
				(try_end),
				
			(else_try),
				(eq, ":sub_type", sub_event_precipitation_strength),
				(assign, "$g_multiplayer_precipitation_strength", ":value"),
				(try_begin),
					(multiplayer_is_server),
					(call_script, "script_multiplayer_server_set_weather_time"),
				(try_end),
				
			(else_try),
				(eq, ":sub_type", sub_event_fog_distance),
				(assign, "$g_multiplayer_fog_distance", ":value"),
				(try_begin),
					(multiplayer_is_server),
					(call_script, "script_multiplayer_server_set_weather_time"),
				(try_end),
				
			(else_try),
				(eq, ":sub_type", sub_event_thunderstorm),
				(assign, "$g_multiplayer_thunderstorm", ":value"),
				(try_begin),
					(multiplayer_is_server),
					(call_script, "script_multiplayer_server_set_weather_time"),
				(try_end),
				
			(else_try),
				(eq, ":sub_type", sub_event_wind_strength),
				(assign, "$g_multiplayer_wind_strength", ":value"),
				(try_begin),
					(multiplayer_is_server),
					(call_script, "script_multiplayer_server_set_weather_time"),
				(try_end),
				
			(else_try),
				(eq, ":sub_type", sub_event_wind_direction),
				(assign, "$g_multiplayer_wind_direction", ":value"),
				(try_begin),
					(multiplayer_is_server),
					(call_script, "script_multiplayer_server_set_weather_time"),
				(try_end),
				
			(else_try),
				(eq, ":sub_type", sub_event_day_time),
				(assign, "$g_multiplayer_day_time", ":value"),
				(try_begin),
					(multiplayer_is_server),
					(call_script, "script_multiplayer_server_set_weather_time"),
				(try_end),
			
			#client only
			(else_try),
				(eq, ":sub_type", sub_event_game_type),
				(assign, "$g_multiplayer_game_type", ":value"),	

			(else_try),      
				(eq, ":sub_type", sub_event_renaming_server_allowed),
				(assign, "$g_multiplayer_renaming_server_allowed", ":value"),
			
			(else_try),
				(eq, ":sub_type", sub_event_changing_game_type_allowed),
				(assign, "$g_multiplayer_changing_game_type_allowed", ":value"),

			(else_try),
				(eq, ":sub_type", sub_event_max_num_bots),
				(assign, "$g_multiplayer_max_num_bots", ":value"),
			(try_end),
			
			(try_begin),
				(neq, ":player_no", 0),#not if server sets values
				(this_or_next|player_is_active, ":player_no"),
				(player_is_active, ":my_player_no"),
				(neg|str_is_empty, s0),
				(display_message, "str_server_s0", 0xff6666),
			(try_end),        
		
			(try_begin),#send settings to server
				(neg|multiplayer_is_dedicated_server),
				(try_begin),
					(eq, ":player_no", -1),#-1 means called from admin pannel
					(player_is_active, ":my_player_no"),
					(neq, ":my_player_no", 0),#dont send to server if this is the server
					(try_begin),
						(eq, ":value_2", -1),
						(multiplayer_send_2_int_to_server, multiplayer_event_set_admin_settings, ":sub_type", ":value"),
					(else_try),
						(multiplayer_send_3_int_to_server, multiplayer_event_set_admin_settings, ":sub_type", ":value", ":value_2"),
					(try_end),
				(try_end),
			(else_try),#send settings to clients
				(get_max_players, ":num_players"),
				(try_for_range, ":cur_player", 1, ":num_players"),
					(player_is_active, ":cur_player"),
					(neq, ":cur_player", ":player_no"),#dont sent to player who made the changes
					(try_begin),
						(eq, ":value_2", -1),
						(multiplayer_send_3_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, ":player_no", ":sub_type", ":value"),
					(else_try),
						(multiplayer_send_4_int_to_player, ":cur_player", multiplayer_event_return_admin_settings, ":player_no", ":sub_type", ":value", ":value_2"),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),
	
  ("multiplayer_server_set_weather_time",
	[
		(try_begin),
			(eq, "$g_multiplayer_cloud_amount", -1),
			(store_random_in_range, "$g_cloud_amount", 0, 101),
		(else_try),
			(assign, "$g_cloud_amount", "$g_multiplayer_cloud_amount"),
		(try_end),
		
		(try_begin),
			(eq, "$g_multiplayer_precipitation_strength", -1),
			(try_begin),
				(ge, "$g_cloud_amount", 75),
				(store_random_in_range, "$g_precipitation_strength", 0, 201),
				(try_begin),
					(gt, "$g_precipitation_strength", 100),
					(assign, "$g_precipitation_strength", 0),
				(try_end),
			(else_try),
				(assign, "$g_precipitation_strength", 0),
			(try_end),
		(else_try),
			(assign, "$g_precipitation_strength", "$g_multiplayer_precipitation_strength"),
		(try_end),
		
		(try_begin),
			(eq, "$g_multiplayer_fog_distance", -1),
			(try_begin),
				(gt, "$g_precipitation_strength", 0),
				(store_mul, ":distance_decrease", "$g_precipitation_strength", 4),
				(store_sub, ":max_fog_distance", 501, ":distance_decrease"),
			(else_try),
				(assign, ":max_fog_distance", 4001),
			(try_end),
			
			(store_random_in_range, "$g_fog_distance", 100, ":max_fog_distance"),
		(else_try),
			(assign, "$g_fog_distance", "$g_multiplayer_fog_distance"),
		(try_end),
		
		(try_begin),
			(eq, "$g_multiplayer_thunderstorm", -1),
			(try_begin),
				(gt, "$g_precipitation_strength", 0),
				(ge, "$g_cloud_amount", 75),
				(store_random_in_range, "$g_thunderstorm", 0, 2),
			(else_try),
				(assign, "$g_thunderstorm", 0),
			(try_end),
		(else_try),
			(assign, "$g_thunderstorm", "$g_multiplayer_thunderstorm"),
		(try_end),
		
		(try_begin),
			(eq, "$g_multiplayer_wind_strength", -1),
			(store_random_in_range, "$g_wind_strength", 1, 11),
		(else_try),
			(assign, "$g_wind_strength", "$g_multiplayer_wind_strength"),
		(try_end),
		
		(try_begin),
			(eq, "$g_multiplayer_wind_direction", -1),
			(store_random_in_range, "$g_wind_direction", 0, 360),
		(else_try),
			(assign, "$g_wind_direction", "$g_multiplayer_wind_direction"),
		(try_end),

		(try_begin),
			(eq, "$g_multiplayer_day_time", -1),
			(store_random_in_range, "$g_day_time", 0, 24),
		(else_try),
			(assign, "$g_day_time", "$g_multiplayer_day_time"),
		(try_end),
		
		#set bundled multiplayer values
		(store_mul, ":v1", "$g_cloud_amount", 2097152),
		(store_mul, ":v2", "$g_precipitation_strength", 16384),
		(store_add, "$g_multiplayer_wt_val_1", ":v1", ":v2"),
		(val_add, "$g_multiplayer_wt_val_1", "$g_fog_distance"),
		(store_mul, ":v1", "$g_wind_strength", 16384),
		(store_mul, ":v2", "$g_wind_direction", 32),
		(store_add, "$g_multiplayer_wt_val_2", ":v1", ":v2"),
		(val_add, "$g_multiplayer_wt_val_2", "$g_day_time"),
  ]),

  ("multiplayer_server_init_upkeep",
	[
		(store_script_param, ":cur_player", 1),
		#(display_message, "@init upkeep"),
		(assign, "$g_request_length", 0),
		(assign, "$g_upkeep_player_messages", 0),
		(try_begin),
			(eq, ":cur_player", -1),
			(get_max_players, ":num_players"),
			(try_for_range, ":cur_player", 1, ":num_players"),
				(player_is_active, ":cur_player"),
				(call_script, "script_multiplayer_server_player_pay_upkeep", ":cur_player", 0),
				(try_begin),#split the request to avoid server crashes if the string length limit is reached
					(gt, "$g_request_length", 1024),
					(call_script, "script_multiplayer_server_send_upkeep_request"),
					(assign, "$g_request_length", 0),#reset
					(assign, "$g_upkeep_player_messages", 0),#reset
				(try_end),
			(try_end),
		(else_try),
			(call_script, "script_multiplayer_server_player_pay_upkeep", ":cur_player", 1),
		(try_end),
		
		(try_begin),#send the rest
			(gt, "$g_upkeep_player_messages", 0),
			(call_script, "script_multiplayer_server_send_upkeep_request"),
		(try_end),
  ]),
	
  ("multiplayer_server_send_upkeep_request",
	[
    (try_begin),
      (eq, "$g_allow_online_chars", 1),
  		(str_store_string, s1, "str_server_security_token"),
  		(str_store_server_name, s2),
  		(assign, reg2, "str_service_action_4"),
  		(str_store_string, s10, "str_service_action_4"),
  		(str_store_string, s10, "str_service_url_1"),
  		# (str_encode_url, s10),
  		(send_message_to_url, s10),
  		(str_store_string, s0, "@[DEBUG]: Send http request: {s10}"),
  		(server_add_message_to_log, s0),
  		(display_message, s0),
    (try_end),
  ]),
	
  ("multiplayer_server_player_pay_upkeep",
	[
		(store_script_param, ":cur_player", 1),
		(store_script_param, ":leaves", 2),
		
		(call_script, "script_multiplayer_server_player_set_final_item_usage", ":cur_player"),#if alive
		(assign, ":usage_item_count", 0),
		(assign, ":upkeep_item_count", 0),
		(assign, ":full_upkeep", 0),
		
		(store_mul, ":array_offset", ":cur_player", 9),
		(player_get_slot, ":spawns", ":cur_player", slot_player_round_spawn_count),
		(try_begin),
			(eq, ":leaves", 0),
			(player_get_gold, ":gold", ":cur_player"),
			(player_set_slot, ":cur_player", slot_player_round_spawn_count, 0),#reset
		(try_end),

		(store_add, ":spawn_count", ":spawns", 1),
		(try_for_range, ":cur_life", 1, ":spawn_count"),		
			(try_for_range, ":cur_item_slot", ek_item_0, ek_food),
				(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_item", ":array_offset"),
				(val_add, ":cur_array", ":cur_item_slot"),
				(troop_get_slot, ":spawn_item", ":cur_array", ":cur_life"),
							
				(gt, ":spawn_item", -1),#item found
				(troop_set_slot, ":cur_array", ":cur_life", -1),#reset
					
				# (assign, reg1, ":cur_array"),###########################
				
				(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_usage", ":array_offset"),
				(val_add, ":cur_array", ":cur_item_slot"),
				(troop_get_slot, ":item_usage", ":cur_array", ":cur_life"),

				(troop_get_slot, ":cur_usage", "trp_player_item_total_usage", ":spawn_item"),
				(val_add, ":cur_usage", ":item_usage"),
				(troop_set_slot, "trp_player_item_total_usage", ":spawn_item", ":cur_usage"),
					
				# (assign, reg2, ":cur_usage"),###########################
				# (str_store_item_name, s30, ":spawn_item"),
				# (str_store_string, s21, "@{s21},{s30} array:{reg1} usage:{reg2}"),
			(try_end),
				
			# (assign, reg1, ":cur_life"),###########################
			# (str_store_string, s20, "@LIFE {reg1} ITEMS: {s21}"),
		(try_end),
			
		# (str_store_player_username, s29, ":cur_player"),###########################
		# (assign, reg0, ":cur_player"),###########################
		# (str_store_string, s20, "@{s29} ({reg1}) = {s20}"),
		# (server_add_message_to_log, s20),
		# (display_message, s20),
		
		(player_get_slot, ":ticks_since_upkeep", ":cur_player", slot_player_ticks_since_upkeep),
		
		(try_for_range, ":cur_item", itm_regular_items_begin +1, itm_admin_items_begin),
			(troop_get_slot, ":item_usage", "trp_player_item_total_usage", ":cur_item"),
			(gt, ":item_usage", 0),
			(troop_set_slot, "trp_player_item_total_usage", ":cur_item", 0),#reset
			(val_add, ":usage_item_count", 1),
				
			(assign, reg1, ":cur_item"),
			(assign, reg2, ":item_usage"),
			(try_begin),
				(eq, ":usage_item_count", 1),
				(str_store_string, s4, "@{reg1},{reg2}"),
				(val_add, "$g_request_length", 1),
			(else_try),
				(str_store_string, s4, "@{s4},{reg1},{reg2}"),
				(val_add, "$g_request_length", 2),
			(try_end),
			
			(try_for_range, ":i", 0, 2),
				(try_begin),
					(eq, ":i", 0),
					(assign, ":reg", reg1),
				(else_try),
					(assign, ":reg", reg2),
				(try_end),
				(try_begin),
					(lt, ":reg", 10),
					(val_add, "$g_request_length", 1),
				(else_try),
					(lt, ":reg", 100),
					(val_add, "$g_request_length", 2),
				(else_try),
					(lt, ":reg", 1000),
					(val_add, "$g_request_length", 3),
				(else_try),
					(lt, ":reg", 10000),
					(val_add, "$g_request_length", 4),
				(else_try),
					(val_add, "$g_request_length", 5),
				(try_end),
			(try_end),
				
			(item_get_type, ":item_type", ":cur_item"),
			(try_begin),
				(this_or_next|eq, ":item_type", itp_type_arrows),
				(this_or_next|eq, ":item_type", itp_type_bolts),
				(this_or_next|eq, ":item_type", itp_type_thrown),
				(eq, ":item_type", itp_type_bullets),
				(item_get_max_ammo, ":max_usage", ":cur_item"),
				(try_begin),
					(eq, ":item_type", itp_type_thrown),
					(item_get_hit_points, ":max_hp", ":cur_item"),
					(val_mul, ":max_usage", ":max_hp"),
				(try_end),
				(val_mul, ":max_usage", max_usage_multi_missiles),
			(else_try),
				(item_get_hit_points, ":max_usage", ":cur_item"),
				(try_begin),
					(eq, ":item_type", itp_type_shield),
					(val_mul, ":max_usage", max_usage_multi_shield),
				(else_try),
					(eq, ":item_type", itp_type_horse),
					(val_mul, ":max_usage", max_usage_multi_horse),
				(try_end),
			(try_end),
				
			(item_get_value, ":item_upkeep", ":cur_item"),
			(val_mul, ":item_upkeep", 1000000),
			(val_div, ":item_upkeep", ":max_usage"),
			(val_mul, ":item_upkeep", ":item_usage"),
			
			#modifier
			(val_mul, ":item_upkeep", ":ticks_since_upkeep"),
			(val_div, ":item_upkeep", 5),
			(val_div, ":item_upkeep", ":spawns"),
			
			(val_div, ":item_upkeep", 1000000),
			
			# (store_mod, ":upkeep_comma", ":item_upkeep", 10),
			# (val_div, ":item_upkeep", 10),
			#
			# (assign, reg3, ":max_usage"),
			# (assign, reg14, ":item_upkeep"),
			# (assign, reg15, ":upkeep_comma"),
			# (display_message, "@item:{reg1} usage:{reg2} max:{reg3} upkeep:{reg14}.{reg15}"),
			#
			# (try_begin),#rounding
				# (ge, ":upkeep_comma", 5),
				# (val_add, ":item_upkeep", 1),
			# (try_end),
				
			(gt, ":item_upkeep", 0),
			(val_add, ":upkeep_item_count", 1),
			(val_add, ":full_upkeep", ":item_upkeep"),
			
			(eq, ":leaves", 0),
			(assign, reg4, ":item_upkeep"),
			(try_begin),
				(le, ":full_upkeep", ":gold"),
				(str_store_string, s5, "@{reg4}"),
			(else_try),#if player cant afford the upkeep, then remove the current item from the inventory
				(str_store_string, s5, "@{reg4}, cant afford"),
				(player_get_troop_id, ":troop_no", ":cur_player"),
				(troop_get_slot, ":backup_troop_no", ":troop_no", slot_troop_online_troop_backup),
				(call_script, "script_multiplayer_set_item_available_for_troop", ":cur_item", ":backup_troop_no", -1),
				(multiplayer_send_2_int_to_player, ":cur_player", multiplayer_event_client_item_availability, ":cur_item", -1),
				#remove from default items
				(assign, ":end_item_slot", slot_player_selected_item_indices_end),
				(try_for_range, ":cur_item_slot", slot_player_selected_item_indices_begin, ":end_item_slot"),
					(player_slot_eq, ":cur_player", ":cur_item_slot", ":cur_item"),
					(call_script, "script_multiplayer_server_set_player_slot", ":cur_player", ":cur_item_slot", -1, to_player),
					(assign, ":end_item_slot", -1),#break loop
				(try_end),
			(try_end),
				
			(str_store_item_name, s1, ":cur_item"),
			(try_begin),
				(eq, ":upkeep_item_count", 1),
				(str_store_string, s2, "@{s1}({s5})"),
			(else_try),
				(str_store_string, s2, "@{s2}, {s1}({s5})"),
			(try_end),
		(try_end),
		
		(player_get_unique_id, ":cur_unique_id", ":cur_player"),
		(assign, reg1, ":cur_unique_id"),
		
		(store_div, ":offset", ":cur_unique_id", 1048576),
		(store_mul, ":id_sub", ":offset", 1048576),
		(val_sub, ":cur_unique_id", ":id_sub"),
		
		(store_add, ":array", "trp_unquie_id_round_bonus_0", ":offset"),
		
		(try_begin),
			(eq, ":leaves", 0),
			(val_add, "$g_upkeep_player_messages", 1),
			(troop_get_slot, reg2, ":array", ":cur_unique_id"),
			(val_clamp, reg2, 0, 2),
		(else_try),
			(assign, "$g_upkeep_player_messages", 1),
			(assign, reg2, 2),#leaves server
		(try_end),
		(assign, reg3, ":spawns"),
		
		(val_add, "$g_request_length", 7),#unique_id
		(val_add, "$g_request_length", 1),#leaves?
		(try_begin),#spawns
			(lt, reg3, 10),
			(val_add, "$g_request_length", 1),
		(else_try),
			(lt, reg3, 100),
			(val_add, "$g_request_length", 2),
		(else_try),
			(val_add, "$g_request_length", 3),
		(try_end),
		
		(try_begin),
			(gt, ":usage_item_count", 0),
			(try_begin),
				(eq, "$g_upkeep_player_messages", 1),
				(str_store_string, s3, "@[{reg1},{reg2},{reg3},{s4}]"),
				(val_add, "$g_request_length", 5),
			(else_try),
				(str_store_string, s3, "@{s3},[{reg1},{reg2},{reg3},{s4}]"),
				(val_add, "$g_request_length", 6),
			(try_end),
		(else_try),
			(try_begin),
				(eq, "$g_upkeep_player_messages", 1),
				(str_store_string, s3, "@[{reg1},{reg2},{reg3}]"),
				(val_add, "$g_request_length", 4),
			(else_try),
				(str_store_string, s3, "@{s3},[{reg1},{reg2},{reg3}]"),
				(val_add, "$g_request_length", 5),
			(try_end),
		(try_end),
		
		(player_set_slot, ":cur_player", slot_player_ticks_since_upkeep, 0),#reset
			
		(try_begin),
			(eq, ":leaves", 0),
			(gt, ":full_upkeep", 0),

			(val_sub, ":gold", ":full_upkeep"),
			(player_set_gold, ":cur_player", ":gold", 0),
				
			(this_or_next|player_slot_eq, ":cur_player", slot_player_online_status, online_status_registered),
			(this_or_next|player_slot_eq, ":cur_player", slot_player_online_status, online_status_new_player),
			(player_slot_eq, ":cur_player", slot_player_online_status, online_status_admin),
				
			(assign, reg1, ":full_upkeep"),
			(player_get_slot, reg2, ":cur_player", slot_player_gold_earned_since_upkeep),
			(store_sub, reg3, reg2, reg1),#turnover
			(try_begin),
				(gt, reg3, 0),
				(str_store_string, s1, "@+{reg3}"),
			(else_try),
				(str_store_string, s1, "@{reg3}"),
			(try_end),
			
			(str_store_string, s0, "@Upkeep: {reg1}, Income since last upkeep: {reg2}, Turnover: {s1} ^{s2}"),
			(call_script, "script_multiplayer_send_message_to_player", ":cur_player", color_pay_upkeep),
				
			(player_set_slot, ":cur_player", slot_player_gold_earned_since_upkeep, 0),#reset
		(try_end),
	]),
	
  ("multiplayer_server_player_set_cur_item_usage",	
	[
		(store_script_param, ":agent_no", 1),
		(store_script_param, ":item_id", 2),
		(store_script_param, ":usage", 3),
		#
		# (assign, reg1, ":agent_no"),
		# (assign, reg2, ":item_id"),
		# (assign, reg3, ":usage"),
		# (display_message, "@IN agent:{reg1} item:{reg2} usage:{reg3}"),
		#
		(agent_get_player_id, ":player_no", ":agent_no"),
		(try_begin),
			(player_is_active, ":player_no"),
			(player_slot_eq, ":player_no", slot_player_use_troop, "trp_online_character"),
			
			(player_get_slot, ":spawn_count", ":player_no", slot_player_round_spawn_count),
			(store_mul, ":array_offset", ":player_no", 9),
			(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_item", ":array_offset"),

			(item_get_type, ":item_type", ":item_id"),
			(assign, ":spawn_inv_slot", -1),
			(assign, ":cur_inv_slot", -1),
			(try_begin),
				(this_or_next|is_between, ":item_type", itp_type_head_armor, itp_type_pistol),
				(eq, ":item_type", itp_type_horse),
				(try_begin),
					(eq, ":item_type", itp_type_head_armor),
					(assign, ":i", ek_head),
				(else_try),
					(eq, ":item_type", itp_type_body_armor),
					(assign, ":i", ek_body),
				(else_try),
					(eq, ":item_type", itp_type_foot_armor),
					(assign, ":i", ek_foot),
				(else_try),
					(eq, ":item_type", itp_type_hand_armor),
					(assign, ":i", ek_gloves),
				(else_try),
					(assign, ":i", ek_horse),
				(try_end),
				
				(val_add, ":cur_array", ":i"),
				(try_begin),
					(troop_slot_eq, ":cur_array", ":spawn_count", ":item_id"),
					(assign, ":spawn_inv_slot", ":i"),
				(try_end),

			(else_try),
				(try_begin),#check if this is alt mode
					(item_slot_ge, ":item_id", slot_item_is_alt_mode, 1),
					(val_sub, ":item_id", 1),
				(try_end),
				
				(assign, ":end_inv_slot", ek_head),
				(try_for_range, ":i", ek_item_0, ":end_inv_slot"),
					(try_begin),
						(this_or_next|is_between, ":item_type", itp_type_arrows, itp_type_bow),
						(this_or_next|eq, ":item_type", itp_type_thrown),
						(eq, ":item_type", itp_type_bullets),
						(agent_get_item_slot, ":cur_item", ":agent_no", ":i"),
						(eq, ":cur_item", ":item_id"),
						(assign, ":cur_inv_slot", ":i"),
					(try_end),
					(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_item", ":array_offset"),
					(val_add, ":cur_array", ":i"),
					(try_begin),
						(troop_slot_eq, ":cur_array", ":spawn_count", ":item_id"),
						(assign, ":spawn_inv_slot", ":i"),
						(this_or_next|is_between, ":item_type", itp_type_arrows, itp_type_bow),
						(this_or_next|eq, ":item_type", itp_type_thrown),
						(eq, ":item_type", itp_type_bullets),
						(assign, ":end_inv_slot", -1),#break loop
					(try_end),
					(gt, ":cur_inv_slot", -1),
					(gt, ":spawn_inv_slot", -1),
					(assign, ":end_inv_slot", -1),#break loop
				(try_end),
			(try_end),
			
			(try_begin),
				(gt, ":spawn_inv_slot", -1),
				(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_usage", ":array_offset"),
				(val_add, ":cur_array", ":spawn_inv_slot"),
				(try_begin),#reset usage
					(this_or_next|eq, ":item_type", itp_type_horse),
					(this_or_next|gt, ":cur_inv_slot", -1),
					(eq, ":usage", -1),
					(try_begin),
						(eq, ":item_type", itp_type_horse),
						(item_get_hit_points, ":max_usage", ":item_id"),
						(store_sub, ":item_usage", ":max_usage", ":usage"),
					(else_try),
						(gt, ":cur_inv_slot", -1),
						(neq, ":usage", -1),
						(try_begin),
							(this_or_next|is_between, ":item_type", itp_type_arrows, itp_type_shield),
							(this_or_next|eq, ":item_type", itp_type_thrown),
							(eq, ":item_type", itp_type_bullets),
							(item_get_max_ammo, ":max_usage", ":item_id"),
							(agent_get_ammo_for_slot, ":cur_usage", ":agent_no", ":cur_inv_slot"),
							(try_begin),
								(eq, ":item_type", itp_type_thrown),
								(item_get_hit_points, ":max_hp", ":item_id"),
								(val_mul, ":cur_usage", ":max_hp"),
								(val_mul, ":max_usage", ":max_hp"),
							(try_end),
						(else_try),
							(eq, ":item_type", itp_type_shield),
							(item_get_hit_points, ":max_usage", ":item_id"),
							(try_begin),
								(item_slot_eq, ":item_id", slot_item_carry_slot, 3),#on back
								(agent_get_slot, ":cur_usage", ":agent_no", slot_agent_cur_shield_hp),
							(else_try),
								(agent_get_slot, ":cur_usage", ":agent_no", slot_agent_cur_buckler_hp),
							(try_end),
							(try_begin),
								(eq, ":cur_usage", 0),
								(item_get_hit_points, ":cur_usage", ":item_id"),
							(try_end),
						(try_end),
						(store_sub, ":item_usage", ":max_usage", ":cur_usage"),
						(try_begin),#add usage of melee mode
							(eq, ":item_type", itp_type_thrown),
							(troop_get_slot, ":melee_usage", ":cur_array", ":spawn_count"),
							(val_add, ":item_usage", ":melee_usage"),
						(try_end),
					(else_try),
						(assign, ":item_usage", 0),
					(try_end),
				(else_try),
					(troop_get_slot, ":item_usage", ":cur_array", ":spawn_count"),
					(val_add, ":item_usage", ":usage"),
					(item_get_hit_points, ":max_usage", ":item_id"),
				(try_end),
				
				(val_add, ":max_usage", 1),
				(val_clamp, ":item_usage", 0, ":max_usage"),#no invalid values
				(troop_set_slot, ":cur_array", ":spawn_count", ":item_usage"),
				#
				# (assign, reg1, ":spawn_inv_slot"),
				# (assign, reg2, ":item_id"),
				# (assign, reg3, ":item_usage"),
				# (display_message, "@slot:{reg1} item:{reg2} usage:{reg3}"),
				#
			(try_end),
		(try_end),
	]),	
	
  ("multiplayer_server_player_set_final_item_usage",	
	[
		(store_script_param, ":player_no", 1),
			
		(player_get_agent_id, ":player_agent_no", ":player_no"),
		(try_begin),
			(agent_is_active, ":player_agent_no"),
			(player_get_slot, ":spawn_count", ":player_no", slot_player_round_spawn_count),
			(store_mul, ":array_offset", ":player_no", 9),
			
			(try_for_range, ":cur_item_slot", ek_item_0, ek_food),
				(neg|is_between, ":cur_item_slot", ek_head, ek_horse),
				
				(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_item", ":array_offset"),
				(val_add, ":cur_array", ":cur_item_slot"),
				(troop_get_slot, ":spawn_item", ":cur_array", ":spawn_count"),
				(gt, ":spawn_item", -1),
				
				(item_get_type, ":item_type", ":spawn_item"),
				(this_or_next|eq, ":item_type", itp_type_arrows),
				(this_or_next|eq, ":item_type", itp_type_bolts),
				(this_or_next|eq, ":item_type", itp_type_thrown),
				(this_or_next|eq, ":item_type", itp_type_bullets),
				(this_or_next|eq, ":item_type", itp_type_shield),
				(eq, ":item_type", itp_type_horse),
				
				(try_begin),#fix
					(lt, ":cur_item_slot", ek_horse),
					(agent_get_item_slot, ":cur_item", ":player_agent_no", ":cur_item_slot"),
				(else_try),
					(agent_get_horse, ":horse_agent_no", ":player_agent_no"),
					(try_begin),
						(agent_is_active, ":horse_agent_no"),
						(agent_get_item_id, ":cur_item", ":horse_agent_no"),
					(else_try),
						(assign, ":cur_item", -1),
					(try_end),
				(try_end),

				(assign, ":end_slot", ek_head),
				(try_begin),
					(eq, ":spawn_item", ":cur_item"),
					(assign, ":end_slot", -1),
					(assign, ":inv_slot", ":cur_item_slot"),
				(else_try),
					(is_between, ":cur_item_slot", ek_item_0, ":end_slot"),
					(try_for_range, ":inv_slot", ek_item_0, ":end_slot"),
						(agent_get_item_slot, ":cur_item", ":player_agent_no", ":inv_slot"),
						(eq, ":spawn_item", ":cur_item"),
						(assign, ":end_slot", -1),#break loop
					(try_end),
				(try_end),
				
				(eq, ":end_slot", -1),#item found
				(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_usage", ":array_offset"),
				(val_add, ":cur_array", ":cur_item_slot"),
				
				(try_begin),
					(this_or_next|eq, ":item_type", itp_type_arrows),
					(this_or_next|eq, ":item_type", itp_type_bolts),
					(this_or_next|eq, ":item_type", itp_type_thrown),
					(eq, ":item_type", itp_type_bullets),
					(item_get_max_ammo, ":max_usage", ":spawn_item"),
					(agent_get_ammo_for_slot, ":cur_usage", ":player_agent_no", ":inv_slot"),
					(try_begin),
						(eq, ":item_type", itp_type_thrown),
						(item_get_hit_points, ":max_hp", ":spawn_item"),
						(val_mul, ":cur_usage", ":max_hp"),
						(val_mul, ":max_usage", ":max_hp"),
					(try_end),
				(else_try),
					(this_or_next|eq, ":item_type", itp_type_shield),
					(eq, ":item_type", itp_type_horse),
					(item_get_hit_points, ":max_usage", ":spawn_item"),
					(try_begin),
						(eq, ":item_type", itp_type_shield),
						(try_begin),
							(item_slot_eq, ":spawn_item", slot_item_carry_slot, 3),#on back
							(agent_get_slot, ":cur_usage", ":player_agent_no", slot_agent_cur_shield_hp),
						(else_try),
							(agent_get_slot, ":cur_usage", ":player_agent_no", slot_agent_cur_buckler_hp),
						(try_end),
						(try_begin),
							(eq, ":cur_usage", 0),
							(item_get_hit_points, ":cur_usage", ":spawn_item"),
						(try_end),
					(else_try),
						(agent_is_active, ":horse_agent_no"),
						(store_agent_hit_points, ":cur_usage", ":horse_agent_no", 1),
					(try_end),
				(try_end),
				
				(store_sub, ":item_usage", ":max_usage", ":cur_usage"),
				(try_begin),#add usage of melee mode
					(eq, ":item_type", itp_type_thrown),
					(troop_get_slot, ":melee_usage", ":cur_array", ":spawn_count"),
					(val_add, ":item_usage", ":melee_usage"),
				(try_end),
				
				(val_add, ":max_usage", 1),
				(val_clamp, ":item_usage", 0, ":max_usage"),#no invalid values
				(troop_set_slot, ":cur_array", ":spawn_count", ":item_usage"),
				#
				# (assign, reg1, ":item_usage"),
				# (display_message, "@cur usage on death: {reg1}"),
				#
			(try_end),
		(try_end),
	]),
	
  ("multiplayer_broadcast_message",
  [
    (store_script_param_1, ":log"),
    (store_script_param_2, ":color"),
    
    (try_begin),
      (eq, ":log", 1),
      (server_add_message_to_log, s0),
    (try_end),
    (try_begin),
			(eq, ":color", color_server_message),
			(str_store_string, s0, "str_server_s0"),
		(try_end),
		
    (get_max_players, ":max_players"),
    (try_for_range, ":cur_player", 0, ":max_players"),
      (call_script, "script_multiplayer_send_message_to_player", ":cur_player", ":color"),
    (try_end),
		(str_clear, s0),#clear string
  ]),

  # Input: player, message_type
  # Output: none
  ("multiplayer_send_message_to_player",[
    (store_script_param_1, ":player"),
    (store_script_param_2, ":color"),
    (try_begin),
      (player_is_active, ":player"),
      #(player_get_team_no, ":team_no", ":player"), #dedicated server has -1 for team_no
      #(neq, ":team_no", -1),
      (try_begin),
        (eq, ":color", color_server_message),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0xff6666),
      (else_try),
        (eq, ":color", color_admin_message),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0xff00ff),
      (else_try),
        (eq, ":color", color_admin_chat),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0xfff600),
      (else_try),
        (eq, ":color", color_player_warning),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0xff0000),
      (else_try),
        (eq, ":color", color_player_info),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0xffffff),
      (else_try),
        (eq, ":color", color_victim_damage),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0x806060),
      (else_try),
        (eq, ":color", color_dealer_damage),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0x5f805f),
      (else_try),
        (eq, ":color", color_shot_distance),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0x606880),
      (else_try),
        (eq, ":color", color_gained_wage),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0x00ff00),
			(else_try),
        (eq, ":color", color_pay_upkeep),
        (multiplayer_send_int_to_player, ":player", multiplayer_event_return_message_color, 0xff3366),
      (try_end),
			(multiplayer_send_string_to_player, ":player", multiplayer_event_show_message, s0),
    (try_end),
  ]),

	("workaround_agent_attack_action",
	[
		(store_script_param, ":agent_id", 1),
    
		(assign, reg0, 0),
		(try_begin),
			(agent_is_active, ":agent_id"),
			(agent_is_alive, ":agent_id"),
			(assign, ":workaround", 0),
			(try_begin),#on decicated server
				(multiplayer_is_dedicated_server),
				(assign, ":workaround", 1),
			(else_try),#client side while on decicated server
				(game_in_multiplayer_mode),
				(neg|multiplayer_is_server),
				(assign, ":workaround", 1),
			(try_end),
		
			(try_begin),
				(eq, ":workaround", 1),
				(agent_get_animation, ":anim", ":agent_id", 1),#upper body animation
				(try_begin),
					(this_or_next|eq, ":anim", "anim_ready_bow"),
					(this_or_next|eq, ":anim", "anim_ready_crossbow"),
					(this_or_next|eq, ":anim", "anim_ready_javelin"),
					(this_or_next|eq, ":anim", "anim_ready_throwing_knife"),
					(this_or_next|eq, ":anim", "anim_ready_throwing_axe"),
					(this_or_next|eq, ":anim", "anim_ready_stone"),
					(this_or_next|eq, ":anim", "anim_ready_pistol"),
					(this_or_next|eq, ":anim", "anim_ready_musket"),
					(this_or_next|eq, ":anim", "anim_ready_swingright_fist"),
					(this_or_next|eq, ":anim", "anim_ready_swingleft_fist"),
					(this_or_next|eq, ":anim", "anim_ready_direct_fist"),
					(this_or_next|eq, ":anim", "anim_ready_uppercut_fist"),
					(this_or_next|eq, ":anim", "anim_ready_slashright_twohanded"),
					(this_or_next|eq, ":anim", "anim_ready_slashleft_twohanded"),
					(this_or_next|eq, ":anim", "anim_ready_thrust_twohanded"),
					(this_or_next|eq, ":anim", "anim_ready_overswing_twohanded"),
					(this_or_next|eq, ":anim", "anim_ready_thrust_onehanded"),
					(this_or_next|eq, ":anim", "anim_ready_thrust_onehanded_horseback"),
					(this_or_next|eq, ":anim", "anim_ready_thrust_onehanded_lance"),
					(this_or_next|eq, ":anim", "anim_ready_slashright_onehanded"),
					(this_or_next|eq, ":anim", "anim_ready_slashleft_onehanded"),
					(this_or_next|eq, ":anim", "anim_ready_overswing_onehanded"),
					(this_or_next|eq, ":anim", "anim_ready_slash_horseback_right"),
					(this_or_next|eq, ":anim", "anim_ready_slash_horseback_left"),
					(this_or_next|eq, ":anim", "anim_ready_slash_horseback_polearm_right"),
					(this_or_next|eq, ":anim", "anim_ready_slash_horseback_polearm_left"),
					(this_or_next|eq, ":anim", "anim_ready_overswing_staff"),
					(this_or_next|eq, ":anim", "anim_ready_thrust_staff"),
					(this_or_next|eq, ":anim", "anim_ready_slashleft_staff"),
					(this_or_next|eq, ":anim", "anim_ready_slashright_staff"),
					(this_or_next|eq, ":anim", "anim_crouch_ready_pistol"),
					(this_or_next|eq, ":anim", "anim_ready_overswing_spear"),
					(this_or_next|eq, ":anim", "anim_ready_overswing_musket"),
					(eq, ":anim", "anim_ready_thrust_musket"),
					(assign, reg0, 1),#readying_attack
				(else_try),
					(this_or_next|eq, ":anim", "anim_release_bow"),
					(this_or_next|eq, ":anim", "anim_release_crossbow"),
					(this_or_next|eq, ":anim", "anim_release_javelin"),
					(this_or_next|eq, ":anim", "anim_release_throwing_knife"),
					(this_or_next|eq, ":anim", "anim_release_throwing_axe"),
					(this_or_next|eq, ":anim", "anim_release_stone"),
					(this_or_next|eq, ":anim", "anim_release_pistol"),
					(this_or_next|eq, ":anim", "anim_release_musket"),
					(this_or_next|eq, ":anim", "anim_release_swingright_fist"),
					(this_or_next|eq, ":anim", "anim_release_swingleft_fist"),
					(this_or_next|eq, ":anim", "anim_release_direct_fist"),
					(this_or_next|eq, ":anim", "anim_release_uppercut_fist"),
					(this_or_next|eq, ":anim", "anim_release_slashright_twohanded"),
					(this_or_next|eq, ":anim", "anim_release_slashleft_twohanded"),
					(this_or_next|eq, ":anim", "anim_release_thrust_twohanded"),
					(this_or_next|eq, ":anim", "anim_release_overswing_twohanded"),
					(this_or_next|eq, ":anim", "anim_release_thrust_onehanded"),
					(this_or_next|eq, ":anim", "anim_release_thrust_onehanded_horseback"),
					(this_or_next|eq, ":anim", "anim_release_thrust_onehanded_lance"),
					(this_or_next|eq, ":anim", "anim_release_slashright_onehanded"),
					(this_or_next|eq, ":anim", "anim_release_slashleft_onehanded"),
					(this_or_next|eq, ":anim", "anim_release_overswing_onehanded"),
					(this_or_next|eq, ":anim", "anim_release_slash_horseback_right"),
					(this_or_next|eq, ":anim", "anim_release_slash_horseback_left"),
					(this_or_next|eq, ":anim", "anim_release_slash_horseback_polearm_right"),
					(this_or_next|eq, ":anim", "anim_release_slash_horseback_polearm_left"),
					(this_or_next|eq, ":anim", "anim_release_overswing_staff"),
					(this_or_next|eq, ":anim", "anim_release_thrust_staff"),
					(this_or_next|eq, ":anim", "anim_release_slashleft_staff"),
					(this_or_next|eq, ":anim", "anim_release_slashright_staff"),
					(this_or_next|eq, ":anim", "anim_crouch_release_pistol"),
					(this_or_next|eq, ":anim", "anim_release_overswing_spear"),
					(this_or_next|eq, ":anim", "anim_release_overswing_musket"),
					(eq, ":anim", "anim_release_thrust_musket"),
					(assign, reg0, 2),#releasing_attack
				(else_try),
					(this_or_next|eq, ":anim", "anim_reload_crossbow"),
					(this_or_next|eq, ":anim", "anim_reload_crossbow_horseback"),
					(this_or_next|eq, ":anim", "anim_reload_pistol"),
					(this_or_next|eq, ":anim", "anim_reload_musket"),
					(this_or_next|eq, ":anim", "anim_reload_musket_full"),
					(this_or_next|eq, ":anim", "anim_reload_musket_two_third"),
					(this_or_next|eq, ":anim", "anim_reload_musket_one_third"),
					(eq, ":anim", "anim_reload_pistol_half"),
					(assign, reg0, 5),#reloading
				(try_end),
			(else_try),
				(agent_get_attack_action, reg0, ":agent_id"),
			(try_end),
		(try_end),
	]),


("calc_attack_penetration",
[
  (store_script_param, ":weapon", 1),
  (store_script_param, ":raw_damage", 2),
  (store_script_param, ":armor", 3),
  (store_script_param, ":dealer_agent", 4),
  # (store_script_param, ":missile", 5),

  # (assign, reg0, ":raw_damage"),
  # (display_message, "@dmg: {reg0}"),

  (try_begin),
    (gt, ":weapon", -1),
    (item_get_type, ":weapon_type", ":weapon"),
  (else_try),
    (assign, ":weapon_type", -1),
  (try_end),

  (assign, ":simple_calc", 1),
  #melee
  (try_begin),
    (is_between, ":weapon_type", itp_type_one_handed_wpn, itp_type_polearm +1),
    
    # (agent_get_bone_position, pos_calc, ":dealer_agent", hb_item_r, 1),
    # (get_distance_between_positions, ":dist", pos_hit, pos_calc),
    # (item_get_weapon_length, ":length", ":weapon"),
    # (val_min, ":dist", ":length"),#fix, dist is sometimes larger than weapon length

    # (assign, reg0, ":dist"),
    # (display_message, "@dist: {reg0}"),

    # (assign, ":cur_slot", slot_agent_cur_item_pos_x),
    # (try_for_range, ":cur_pos", pos2, pos3 +1),
    #   (init_position, ":cur_pos"),
    #   (try_for_range, ":i", 0, 6),
    #     (agent_get_slot, ":val", ":dealer_agent", ":cur_slot"),
    #     (try_begin),
    #       (eq, ":i", 0),
    #       (position_set_x, ":cur_pos", ":val"),
    #     (else_try),
    #       (eq, ":i", 1),
    #       (position_set_y, ":cur_pos", ":val"),
    #     (else_try),
    #       (eq, ":i", 2),
    #       (position_set_z, ":cur_pos", ":val"),
    #     (else_try),
    #       (eq, ":i", 3),
    #       (position_rotate_x, ":cur_pos", ":val"),
    #     (else_try),
    #       (eq, ":i", 4),
    #       (position_rotate_y, ":cur_pos", ":val"),
    #     (else_try),
    #       (position_rotate_z, ":cur_pos", ":val"),
    #     (try_end),
    #     (val_add, ":cur_slot", 1),
    #   (try_end),
    #   (position_move_y, ":cur_pos", ":dist", 0),
    # (try_end),
    
    # (get_distance_between_positions, ":raw_damage", pos2, pos3),

  #   (agent_get_action_dir, ":action_dir", ":dealer_agent"),
  #   (try_begin),
  #     (eq, ":action_dir", atk_thrust),
  #     (assign, ":action_dir", 0),
  #   (else_try),
  #     (eq, ":action_dir", atk_overhead),
  #     (item_has_capability, ":weapon", itc_upper_thrust),
  #     (assign, ":action_dir", 0),
  #   (else_try),
  #     (assign, ":action_dir", 1),
  #   (try_end),

  #   (item_get_max_ammo, ":toughness", ":weapon"),
  #   (try_begin),#thrust
  #     (eq, ":action_dir", 0),
  #     (item_get_leg_armor, ":area", ":weapon"),
  #     (item_get_abundance, ":friction", ":weapon"),
  #     # (set_fixed_point_multiplier, 1000),
  #     # (item_get_weight, ":weight", ":weapon"),
  #     # (set_fixed_point_multiplier, 100),
  #   (else_try),#swing
  #     (item_get_head_armor, ":area", ":weapon"),
  #     (item_get_body_armor, ":friction", ":weapon"),
  #     # #get positional weight
  #     # (store_add, ":slot", slot_item_pos_weight_begin, ":dist"),
  #     # (item_get_slot, ":weight_factor", ":weapon", ":slot"),
  #     # (val_mul, ":raw_damage", ":weight_factor"),
  #     # (val_div, ":raw_damage", 1000),
  #   (try_end),

  #   # #calculate positional speed
  #   # (store_add, ":slot", slot_item_pos_speed_begin, ":dist"),
  #   # (item_get_slot, ":speed_factor", ":weapon", ":slot"),
  #   # (val_mul, ":raw_damage", ":speed_factor"),
  #   # (val_div, ":raw_damage", 1000),

  # #shield bash
  # (else_try),
  #   (eq, ":weapon_type", itp_type_shield),
  #   (item_get_max_ammo, ":toughness", ":weapon"),
  #   (assign, ":area", 255),
  #   (assign, ":friction", 100),
  #   # (set_fixed_point_multiplier, 1000),
  #   # (item_get_weight, ":weight", ":weapon"),
  #   # (set_fixed_point_multiplier, 100),

  #ranged
  (else_try),
    (gt, ":weapon_type", -1),
    # (try_begin),
    #   (eq, ":weapon_type", itp_type_thrown),
    #   (item_get_swing_damage, ":toughness", ":weapon"),
    #   (item_get_leg_armor, ":area", ":weapon"),
    #   (item_get_body_armor, ":friction", ":weapon"),

    #   # (set_fixed_point_multiplier, 1000),
    #   # (item_get_weight, ":weight", ":weapon"),
    #   # (set_fixed_point_multiplier, 100),
    #   # (item_get_max_ammo, ":ammo", ":weapon"),
    # (else_try),
    #   (item_get_swing_damage, ":toughness", ":missile"),
    #   (item_get_leg_armor, ":area", ":missile"),
    #   (item_get_abundance, ":friction", ":missile"),

    #   # (set_fixed_point_multiplier, 1000),
    #   # (item_get_weight, ":weight", ":missile"),
    #   # (set_fixed_point_multiplier, 100),
    #   # (item_get_max_ammo, ":ammo", ":missile"),
    # (try_end),

    # (val_div, ":weight", ":ammo"),

    (try_begin),#damage reverse engineering
      (neq, ":weapon_type", itp_type_crossbow),
      (agent_get_troop_id, ":dealer_troop", ":dealer_agent"),
      (store_attribute_level, ":str_effect", ":dealer_troop", ca_strength),
      (val_div, ":str_effect", 5),
      (val_sub, ":raw_damage", ":str_effect"),# sub str damage
      (val_max, ":raw_damage", 0),
        
      # (assign, reg0, ":raw_damage"),
      # (display_message, "@-str:{reg0}"),
            
      (try_begin),
        (this_or_next|eq, ":weapon_type", itp_type_bow),
        (eq, ":weapon_type", itp_type_thrown),
        (agent_get_horse, ":horse_agent", ":dealer_agent"),
        (agent_is_active, ":horse_agent"),
        (store_skill_level, ":div", "skl_horse_archery", ":dealer_troop"),
        (val_mul, ":div", 19),#0.019
        (val_add, ":div", 800),#0.8
        (val_mul, ":raw_damage", 1000),
        (val_div, ":raw_damage", ":div"),
        # (assign, reg0, ":raw_damage"),
        # (display_message, "@-ha:{reg0}"),
      (try_end),
    
      (try_begin),
        (eq, ":weapon_type", itp_type_bow),
        (store_proficiency_level, ":div", ":dealer_troop", wpt_archery),
      (else_try),
        (eq, ":weapon_type", itp_type_thrown),
        (store_proficiency_level, ":div", ":dealer_troop", wpt_throwing),
      (else_try),
        (store_proficiency_level, ":div", ":dealer_troop", wpt_firearm),
      (try_end),
      (val_mul, ":div", 15),#0.0015
      (val_add, ":div", 8500),#0.85
      (val_mul, ":raw_damage", 10000),
      (val_div, ":raw_damage", ":div"),
            
      # (assign, reg0, ":raw_damage"),
      # (display_message, "@-wpt:{reg0}"),
    (try_end),

  #no weapon
  # (else_try),
  #   (assign, ":simple_calc", 1),
  (try_end),
  
  (try_begin),
    (eq, ":simple_calc", 1),
    (store_sub, ":damage", ":raw_damage", ":armor"),
    (val_max, ":damage", 0),
  # (else_try),
  #   (try_begin),
  #     (eq, ":area", 0),
  #     (assign, ":area", 127),
  #   (try_end),
  #   (try_begin),
  #     (eq, ":friction", 0),
  #     (assign, ":friction", 50),
  #   (try_end),
  #   (try_begin),
  #     (eq, ":toughness", 0),
  #     (assign, ":toughness", 50),
  #   (try_end),

    # (assign, reg0, ":raw_damage"),
    # (assign, reg2, ":area"),
    # (assign, reg3, ":toughness"),
    # (assign, reg4, ":friction"),
    # (assign, reg5, ":armor"),
    # (display_message, "@eff.dmg:{reg0} area:{reg2} thoughness:{reg3} friction:{reg4} armor:{reg5}"),

    # (store_mul, ":area_factor", ":area", 100),
    # (store_sqrt, ":area_factor", ":area"),

    # (store_mul, ":friction_factor", ":friction", 100),
    # (store_sqrt, ":friction_factor", ":friction"), 

    # (store_mul, ":resistance", ":armor", ":friction"),
    # # (val_mul, ":resistance", ":area_factor"),
    # (val_div, ":resistance", ":toughness"),

    # (store_sub, ":depth", ":raw_damage", ":resistance"),
    # (val_max, ":depth", 0),

    # (store_div, ":damage", ":depth", 4),

    # (assign, reg0, ":resistance"),
    # (assign, reg1, ":depth"),
    # (assign, reg2, ":damage"),
    # (display_message, "@resistance:{reg0} depth:{reg1} damage:{reg2}"),
  (try_end),

  (assign, reg0, ":damage"),
]),

#MOD end
]