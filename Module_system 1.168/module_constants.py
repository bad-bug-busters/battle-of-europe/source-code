from ID_items import *
from ID_quests import *
from ID_factions import *
##############################################################
# These constants are used in various files.
# If you need to define a value that will be used in those files,
# just define it here rather than copying it across each file, so
# that it will be easy to change it if you need to.
##############################################################

module_version = 721 #MOD ...IMPORTANT

#MOD INFO max slots 65536

########################################################
##  ITEM SLOTS             #############################
########################################################
#MOD begin
slot_item_pos_weight_begin = 0
slot_item_pos_speed_begin = 1024
slot_item_center_of_percussion = 2048
slot_item_carry_slot = 2049
slot_item_is_alt_mode = 2050
#MOD end

slot_item_multiplayer_availability_linked_list_begin = 2051 #temporary, can be moved to higher values


########################################################
##  AGENT SLOTS            #############################
########################################################
slot_agent_next_action_time       = 19
slot_agent_in_duel_with           = 21
slot_agent_duel_start_time        = 22

#MOD begin
slot_agent_drown_time             = 26
slot_agent_is_walking             = 27
slot_agent_key_walk               = 28
slot_agent_stamina_walk           = 29
slot_agent_limiter_walk           = 30
slot_agent_is_sprinting           = 31
slot_agent_nudge_state            = 32
slot_agent_nudge_time            	= 33
slot_agent_accuracy_modifier      = 36
slot_agent_damage_modifier        = 37
#slot_agent_freezed                = 38
slot_agent_shot_pos_x             = 39
slot_agent_shot_pos_y             = 40
slot_agent_shot_pos_z             = 41
slot_agent_carry_weight           = 42
slot_agent_armor_weight           = 43

slot_agent_cur_stamina            = 44
slot_agent_max_stamina            = 45
slot_agent_lose_stamina           = 46

slot_agent_last_attacker_agent    = 47
slot_agent_bleed_time             = 48
slot_agent_bleed_interval         = 49
#slot_agent_next_bleeding          = 50
slot_agent_bleed_hp_lost          = 51

slot_agent_last_picked_item       = 52
slot_agent_gender       		 		  = 53
slot_agent_interval_damage        = 54
slot_agent_interval_sound         = 55
slot_agent_interval_weapon        = 56

slot_agent_next_action_time       = 57
slot_agent_volley_fire_state      = 58

slot_agent_use_scene_prop         = 59

slot_agent_spawn_ammo_type        = 60
slot_agent_next_shot_is_alternative = 61

slot_agent_sec_since_movement 	  = 62

slot_agent_cur_shield_hp = 63
slot_agent_cur_buckler_hp = 64

slot_agent_time_ms = 65

slot_agent_carry_slot_0_used     = 70
slot_agent_carry_slot_1_used     = 71
slot_agent_carry_slot_2_used     = 72
slot_agent_carry_slot_3_used     = 73
slot_agent_carry_slot_4_used     = 74
slot_agent_carry_slot_5_used     = 75
slot_agent_carry_slot_6_used     = 76
slot_agent_carry_slot_7_used     = 77
slot_agent_carry_slot_8_used     = 78
slot_agent_carry_slot_9_used     = 79
slot_agent_carry_slot_10_used    = 80
slot_agent_carry_slot_11_used    = 81

slot_agent_spawn_item_0 = 82
slot_agent_spawn_item_1 = 83
slot_agent_spawn_item_2 = 84
slot_agent_spawn_item_3 = 85
slot_agent_spawn_item_head = 86
slot_agent_spawn_item_body = 87
slot_agent_spawn_item_foot = 88
slot_agent_spawn_item_gloves = 89
slot_agent_spawn_item_horse = 90
slot_agent_spawn_item_food = 91

slot_agent_spawn_item_0_usage = 92
slot_agent_spawn_item_1_usage = 93
slot_agent_spawn_item_2_usage = 94
slot_agent_spawn_item_3_usage = 95
slot_agent_spawn_item_head_usage = 96
slot_agent_spawn_item_body_usage = 97
slot_agent_spawn_item_foot_usage = 98
slot_agent_spawn_item_gloves_usage = 99
slot_agent_spawn_item_horse_usage = 100
slot_agent_spawn_item_food_usage = 101

# slot_agent_cur_item_pos_x = 102
# slot_agent_cur_item_pos_y = 103
# slot_agent_cur_item_pos_z = 104

# slot_agent_cur_item_rot_x = 105
# slot_agent_cur_item_rot_y = 106
# slot_agent_cur_item_rot_z = 107

# slot_agent_last_item_pos_x = 108
# slot_agent_last_item_pos_y = 109
# slot_agent_last_item_pos_z = 110

# slot_agent_last_item_rot_x = 111
# slot_agent_last_item_rot_y = 112
# slot_agent_last_item_rot_z = 113
#MOD end

########################################################
##  FACTION SLOTS          #############################
########################################################
slot_faction_flag        = 0
slot_faction_banner      = 1

########################################################
##  PARTY SLOTS            #############################
########################################################

########################################################
##  SCENE SLOTS            #############################
########################################################

########################################################
##  TROOP SLOTS            #############################
########################################################
#MOD begin
slot_troop_used_by_player        = 0
slot_troop_max_shot_range        = 1
slot_troop_base_stamina          = 3

slot_troop_online_troop_backup   = 4

slot_troop_army_percentage       = 5
slot_troop_start_percentage      = 6
slot_troop_end_percentage        = 7
#MOD end
											
											
########################################################
##  PLAYER SLOTS           #############################
########################################################

slot_player_spawned_this_round                 = 0
slot_player_last_rounds_used_item_earnings     = 1
slot_player_selected_item_indices_begin        = 2
slot_player_selected_item_indices_end          = 11
slot_player_cur_selected_item_indices_begin    = slot_player_selected_item_indices_end
slot_player_cur_selected_item_indices_end      = slot_player_selected_item_indices_end + 9
slot_player_join_time                          = 21
slot_player_button_index                       = 22 #used for presentations
slot_player_can_answer_poll                    = 23
slot_player_first_spawn                        = 24
slot_player_spawned_at_siege_round             = 25
slot_player_poll_disabled_until_time           = 26
slot_player_total_equipment_value              = 27
slot_player_last_team_select_time              = 28
slot_player_death_pos_x                        = 29
slot_player_death_pos_y                        = 30
slot_player_death_pos_z                        = 31
slot_player_damage_given_to_target_1           = 32 #used only in destroy mod
slot_player_damage_given_to_target_2           = 33 #used only in destroy mod
slot_player_last_bot_count                     = 34
slot_player_bot_type_1_wanted                  = 35
slot_player_bot_type_2_wanted                  = 36
slot_player_bot_type_3_wanted                  = 37
slot_player_bot_type_4_wanted                  = 38
#MOD begin
slot_player_bot_type_5_wanted                  = 39
slot_player_bot_type_6_wanted                  = 40
slot_player_bot_type_7_wanted                  = 41
slot_player_bot_type_8_wanted                  = 42
slot_player_bot_type_9_wanted                  = 43
slot_player_bot_type_10_wanted                  = 44
slot_player_bot_type_11_wanted                  = 45
slot_player_bot_type_12_wanted                  = 46
slot_player_bot_type_13_wanted                  = 47
slot_player_bot_type_14_wanted                  = 48
slot_player_bot_type_15_wanted                  = 49
slot_player_bot_type_16_wanted                  = 50
slot_player_bot_type_17_wanted                  = 51
slot_player_bot_type_18_wanted                  = 52
slot_player_bot_type_19_wanted                  = 53
slot_player_bot_type_20_wanted                  = 54

slot_player_spawn_count                        = 55#MOD edit
slot_player_round_spawn_count                  = 56

slot_player_use_troop                          = 59
slot_player_last_used_troop                    = 60
slot_player_online_status                      = 61
slot_player_online_cur_build                   = 62
slot_player_online_next_build                  = 63
slot_player_online_clan                  			 = 64
slot_player_online_generation                  = 65
slot_player_online_xp                          = 66
slot_player_online_lvl                         = 67

slot_player_show_damage_report                 = 68

slot_player_score                    					 = 70
slot_player_kills                    					 = 71
slot_player_deaths                    				 = 72

slot_player_last_tick_score                    = 73
slot_player_last_tick_kills                    = 74
slot_player_last_tick_deaths                   = 75

slot_player_ticks_since_upkeep           			 = 76
slot_player_gold_earned_since_upkeep           = 77
#MOD end

########################################################
##  TEAM SLOTS             #############################
########################################################

########################################################
##  QUEST SLOTS            #############################
########################################################

########################################################
##  PARTY TEMPLATE SLOTS   #############################
########################################################

########################################################
##  SCENE PROP SLOTS       #############################
########################################################

scene_prop_open_or_close_slot       = 1
scene_prop_smoke_effect_done        = 2

#MOD begin
scene_prop_material                 = 6
scene_prop_is_used                  = 7
scene_prop_spawn_time 				= 8
scene_prop_usage_state              = 9

scene_prop_ammo_type                = 11
scene_prop_item_1                   = 12
scene_prop_item_2                   = 13
scene_prop_item_1_max_ammo          = 14
scene_prop_item_2_max_ammo          = 15
scene_prop_item_1_cur_ammo          = 16
scene_prop_item_2_cur_ammo          = 17

scene_prop_time                    	= 18

scene_prop_pos_x                    = 21
scene_prop_pos_y                    = 22
scene_prop_pos_z                    = 23

scene_prop_rot_x                    = 24
scene_prop_rot_y                    = 25
scene_prop_rot_z                    = 26

scene_prop_scale_x                  = 27
scene_prop_scale_y                  = 28
scene_prop_scale_z                  = 29

scene_prop_default_pos_x              = 30
scene_prop_default_pos_y              = 31
scene_prop_default_pos_z              = 32

scene_prop_default_rot_x              = 33
scene_prop_default_rot_y              = 34
scene_prop_default_rot_z              = 35

scene_prop_move_x              = 36
scene_prop_move_y              = 37
scene_prop_move_z              = 38

scene_prop_weight              = 39
scene_prop_resistance       	 = 40
scene_prop_radius       	 		 = 41
scene_prop_friction       	 	 = 42

scene_prop_prune_time       	 	 = 43

# scene_prop_cur_v0_mm = 44
scene_prop_last_pos_time = 45


#slots that need -1 as default
scene_prop_user_agent               = 60
scene_prop_next_action_time         = 61

scene_prop_parent                   = 100
scene_prop_child_1                  = 101
scene_prop_child_2                  = 102
scene_prop_child_3                  = 103
scene_prop_child_4                  = 104
scene_prop_child_5                  = 105
scene_prop_child_6                  = 106
scene_prop_child_7                  = 107
scene_prop_child_8                  = 108
scene_prop_child_9                  = 109
scene_prop_child_10                 = 110
scene_prop_child_11                 = 111
scene_prop_child_12                 = 112
scene_prop_child_13                 = 113
scene_prop_child_14                 = 114
scene_prop_child_15                 = 115
scene_prop_child_16                 = 116
scene_prop_child_17                 = 117
scene_prop_child_18                 = 118
scene_prop_child_19                 = 119
scene_prop_child_20                 = 120

scene_prop_slots_end                = 200
#MOD end

npc_kingdoms_begin = "fac_kingdom_1"
npc_kingdoms_end = "fac_kingdoms_end"

multiplayer_troops_begin = "trp_kingdom_1_inf_melee_1"
multiplayer_troops_end = "trp_multiplayer_troop_end"

multiplayer_player_troops_begin = "trp_player_troop_0"
multiplayer_player_troops_end = "trp_multiplayer_player_troop_end"

multiplayer_player_troops_online_begin = "trp_player_troop_online_0"
multiplayer_player_troops_online_end = "trp_multiplayer_player_troop_online_end"

multiplayer_ai_troops_begin = "trp_kingdom_1_inf_melee_1"
multiplayer_ai_troops_end = "trp_online_character"

multiplayer_flag_projections_begin = "mesh_flag_project_sw"
multiplayer_flag_projections_end = "mesh_flag_projects_end"

multiplayer_flag_taken_projections_begin = "mesh_flag_project_sw_miss"
multiplayer_flag_taken_projections_end = "mesh_flag_project_misses_end"

multiplayer_game_type_names_begin = "str_multi_game_type_1"
multiplayer_game_type_names_end = "str_multi_game_types_end"

all_items_begin = 0
all_items_end = "itm_items_end"

# Banner constants
banner_meshes_begin = "mesh_banner_clan_001"
banner_meshes_end_minus_one = "mesh_banner_f21"

#NORMAL ACHIEVEMENTS
ACHIEVEMENT_NONE_SHALL_PASS = 1,
ACHIEVEMENT_MAN_EATER = 2,
ACHIEVEMENT_THE_HOLY_HAND_GRENADE = 3,
ACHIEVEMENT_LOOK_AT_THE_BONES = 4,
ACHIEVEMENT_KHAAAN = 5,
ACHIEVEMENT_GET_UP_STAND_UP = 6,
ACHIEVEMENT_BARON_GOT_BACK = 7,
ACHIEVEMENT_BEST_SERVED_COLD = 8,
ACHIEVEMENT_TRICK_SHOT = 9,
ACHIEVEMENT_GAMBIT = 10,
ACHIEVEMENT_OLD_SCHOOL_SNIPER = 11,
ACHIEVEMENT_CALRADIAN_ARMY_KNIFE = 12,
ACHIEVEMENT_MOUNTAIN_BLADE = 13,
ACHIEVEMENT_HOLY_DIVER = 14,
ACHIEVEMENT_FORCE_OF_NATURE = 15,

#SKILL RELATED ACHIEVEMENTS:
ACHIEVEMENT_BRING_OUT_YOUR_DEAD = 16,
ACHIEVEMENT_MIGHT_MAKES_RIGHT = 17,
ACHIEVEMENT_COMMUNITY_SERVICE = 18,
ACHIEVEMENT_AGILE_WARRIOR = 19,
ACHIEVEMENT_MELEE_MASTER = 20,
ACHIEVEMENT_DEXTEROUS_DASTARD = 21,
ACHIEVEMENT_MIND_ON_THE_MONEY = 22,
ACHIEVEMENT_ART_OF_WAR = 23,
ACHIEVEMENT_THE_RANGER = 24,
ACHIEVEMENT_TROJAN_BUNNY_MAKER = 25,

#MAP RELATED ACHIEVEMENTS:
ACHIEVEMENT_MIGRATING_COCONUTS = 26,
ACHIEVEMENT_HELP_HELP_IM_BEING_REPRESSED = 27,
ACHIEVEMENT_SARRANIDIAN_NIGHTS = 28,
ACHIEVEMENT_OLD_DIRTY_SCOUNDREL = 29,
ACHIEVEMENT_THE_BANDIT = 30,
ACHIEVEMENT_GOT_MILK = 31,
ACHIEVEMENT_SOLD_INTO_SLAVERY = 32,
ACHIEVEMENT_MEDIEVAL_TIMES = 33,
ACHIEVEMENT_GOOD_SAMARITAN = 34,
ACHIEVEMENT_MORALE_LEADER = 35,
ACHIEVEMENT_ABUNDANT_FEAST = 36,
ACHIEVEMENT_BOOK_WORM = 37,
ACHIEVEMENT_ROMANTIC_WARRIOR = 38,

#POLITICALLY ORIENTED ACHIEVEMENTS:
ACHIEVEMENT_HAPPILY_EVER_AFTER = 39,
ACHIEVEMENT_HEART_BREAKER = 40,
ACHIEVEMENT_AUTONOMOUS_COLLECTIVE = 41,
ACHIEVEMENT_I_DUB_THEE = 42,
ACHIEVEMENT_SASSY = 43,
ACHIEVEMENT_THE_GOLDEN_THRONE = 44,
ACHIEVEMENT_KNIGHTS_OF_THE_ROUND = 45,
ACHIEVEMENT_TALKING_HELPS = 46,
ACHIEVEMENT_KINGMAKER = 47,
ACHIEVEMENT_PUGNACIOUS_D = 48,
ACHIEVEMENT_GOLD_FARMER = 49,
ACHIEVEMENT_ROYALITY_PAYMENT = 50,
ACHIEVEMENT_MEDIEVAL_EMLAK = 51,
ACHIEVEMENT_CALRADIAN_TEA_PARTY = 52,
ACHIEVEMENT_MANIFEST_DESTINY = 53,
ACHIEVEMENT_CONCILIO_CALRADI = 54,
ACHIEVEMENT_VICTUM_SEQUENS = 55,

#MULTIPLAYER ACHIEVEMENTS:
ACHIEVEMENT_THIS_IS_OUR_LAND = 56,
ACHIEVEMENT_SPOIL_THE_CHARGE = 57,
ACHIEVEMENT_HARASSING_HORSEMAN = 58,
ACHIEVEMENT_THROWING_STAR = 59,
ACHIEVEMENT_SHISH_KEBAB = 60,
ACHIEVEMENT_RUIN_THE_RAID = 61,
ACHIEVEMENT_LAST_MAN_STANDING = 62,
ACHIEVEMENT_EVERY_BREATH_YOU_TAKE = 63,
ACHIEVEMENT_CHOPPY_CHOP_CHOP = 64,
ACHIEVEMENT_MACE_IN_YER_FACE = 65,
ACHIEVEMENT_THE_HUSCARL = 66,
ACHIEVEMENT_GLORIOUS_MOTHER_FACTION = 67,
ACHIEVEMENT_ELITE_WARRIOR = 68,

#COMBINED ACHIEVEMENTS
ACHIEVEMENT_SON_OF_ODIN = 69,
ACHIEVEMENT_KING_ARTHUR = 70,
ACHIEVEMENT_KASSAI_MASTER = 71,
ACHIEVEMENT_IRON_BEAR = 72,
ACHIEVEMENT_LEGENDARY_RASTAM = 73,
ACHIEVEMENT_SVAROG_THE_MIGHTY = 74,

ACHIEVEMENT_MEN_HANDLER = 75,
ACHIEVEMENT_GIRL_POWER = 76,
ACHIEVEMENT_QUEEN = 77,
ACHIEVEMENT_EMPRESS = 78,
ACHIEVEMENT_TALK_OF_THE_TOWN = 79,
ACHIEVEMENT_LADY_OF_THE_LAKE = 80,

#MOD begin
construtible_props_begin = "spr_construct_box_a"
construtible_props_end = "spr_construct_end"

destructible_props_begin = "spr_destructible_props_begin"
destructible_props_end = "spr_scene_props_end"

max_usage_multi_missiles = 2
max_usage_multi_shield = 4
max_usage_multi_horse = 10

drown_time_human = 30
drown_time_horse = 20

drown_stamina_time = 400 #stamina / value = breath time

agent_hp_cant_sprint = 15 #if cur_health < value then decrease stamina recover limit 
agent_hp_cant_run = 5 #if cur_health < value then decrease stamina recover limit 

agent_base_stamina = 9000
agent_sprint_stamina = 7500
agent_run_stamina = 4500

#for movement speed modifier
# weight_divider_run = 50
weight_divider_run_horse = 40
weight_divider_sprint = 10
weight_divider_sprint_horse = 30

stamina_str_divider = 150 #60 points per att
stamina_agi_divider = 100 #90 points per att

#stamina consumption per second
stamina_run = 10 # + carry weight
stamina_run_horse = 5 # + carry weight
stamina_sprint = 600 # + carry weight
stamina_sprint_horse = 450 # + carry weight

#stamina consumption per action
stamina_jump = 200 # + carry weight
stamina_crouch = 80 # + carry weight
stamina_attack = 75 # + weapon weight *10
stamina_attack_shield = stamina_attack
stamina_defend = 50 # + weapon weight *10
stamina_kick = 150 # + carry weight

#stamina recover per second
stamina_recover_walk = 120
stamina_recover_stand = 160

stamina_mounted_weight_divider = 5 #looses less weight-stamina on horseback

wm_points_bonus = 50
pp_archery_bonus = 25
pp_crossbow_bonus = 15
pr_firearm_bonus = 15
# oh_damage_bonus = 5
ps_damage_bonus = 5 #native is 8
resistance_bonus = 5

weight_limit = 10 #if cur_weight > value then decrease wpf
wpt_zero = 100 #displayes wpf - value 

negative_prop_slots_begin = scene_prop_user_agent
negative_prop_slots_end   = scene_prop_child_20 +1

resistance_earth                 = 0
resistance_water                 = 0
resistance_stone                 = 500
resistance_wood                  = 10
resistance_metal                 = 750

resistance_stone_earth           = (resistance_stone + resistance_earth) /2
resistance_wood_earth            = (resistance_wood + resistance_earth) /2
resistance_metal_earth           = (resistance_metal + resistance_earth) /2

resistance_stone_wood            = (resistance_stone + resistance_wood) /2
resistance_wood_metal            = (resistance_wood + resistance_metal) /2
resistance_metal_stone           = (resistance_metal + resistance_stone) /2

cannon_1_barrel_y = 35
cannon_1_barrel_z = 80
cannon_1_wheel_front_y = 65
cannon_1_wheel_front_z = 20
cannon_1_wheel_back_y = -65
cannon_1_wheel_back_z = 20
cannon_1_wheel_left_x = -47
cannon_1_wheel_right_x = 47

cannon_1_aim_platform_y = -100

cannon_1_barrel_min_angle = -2300 #x100
cannon_1_barrel_max_angle = 2300 #x100

cannon_1_wheel_circumference = 129
cannon_1_rotation_circumference = 508

cannon_1_push_distance = 100
cannon_1_push_forward_time = 400
cannon_1_push_back_time = 100


range_arrow_ms = 4
range_bolt_ms = 3
range_bullet_ms = 2

advanced_order_arrays_begin = "trp_team_next_order_time"
advanced_order_arrays_end = "trp_advanced_order_arrays_end"

#volley fire times
#1.0 sec. steps
volley_interval_bow_min = 12
volley_interval_bow_max = 15
volley_interval_crossbow_min = 19
volley_interval_crossbow_max = 22
volley_interval_musket_min = 27
volley_interval_musket_max = 30
#0.001 sec. steps
volley_ready_min = 100
volley_ready_max = 500
volley_release_min = 3500
volley_release_max = 4000

highest_clan_id = 25

online_xp_tick = 1000
online_gold_tick = 10
online_score_gold_divider = 10
online_gen_xp_bonus = 60
online_gen_xp_decrease = 2

online_min_lvl = 0
online_max_lvl = 39
online_retire_lvl = 31
online_lvl_xp_base = 5000
online_lvl_xp_adder = 500

online_round_gold_bonus = 5
online_round_xp_bonus = 500

score_victim_bot_divider = 2
score_dealer_bot_divider = 10
score_prop_divider = 10
score_artillery_divider = 1000
score_bonus_surgeon = 25

loom_1_item = 0 - itm_regular_items_begin + itm_mod_1_items_begin
loom_2_item = 0 - itm_regular_items_begin + itm_mod_2_items_begin
loom_3_item = 0 - itm_regular_items_begin + itm_mod_3_items_begin

item_prune_time = 180

sound_slot_id = 0
sound_slot_time = 1
sound_slot_pos_x = 2
sound_slot_pos_y = 3
sound_slot_pos_z = 4

color_server_message    = 0
color_admin_message     = 1
color_admin_chat        = 2
color_player_warning    = 3
color_player_info       = 4
color_victim_damage     = 5
color_dealer_damage     = 6
color_shot_distance     = 7
color_gained_wage       = 8
color_pay_upkeep        = 9
#MOD end