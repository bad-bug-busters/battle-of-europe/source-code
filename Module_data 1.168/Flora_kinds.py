import string

dword      = 0x8000000000000000
dword_mask = 0xffffffffffffffff

density_bits      = 32
fkf_density_mask  = 0xFFFF #16K

#terain condition flags
fkf_plain             = 0x00000004
fkf_steppe            = 0x00000008
fkf_snow              = 0x00000010
fkf_desert            = 0x00000020
fkf_plain_forest      = 0x00000400
fkf_steppe_forest     = 0x00000800
fkf_snow_forest       = 0x00001000
fkf_desert_forest     = 0x00002000
fkf_terrain_mask      = 0x0000ffff

fkf_realtime_ligting  = 0x00010000 #deprecated
fkf_point_up          = 0x00020000 #uses auto-generated point-up(quad) geometry for the flora kind
fkf_align_with_ground = 0x00040000 #align the flora object with the ground normal
fkf_grass             = 0x00080000 #is grass
fkf_on_green_ground   = 0x00100000 #populate this flora on green ground
fkf_rock              = 0x00200000 #is rock 
fkf_tree              = 0x00400000 #is tree -> note that if you set this parameter, you should pass additional alternative tree definitions
fkf_snowy             = 0x00800000
fkf_guarantee         = 0x01000000

fkf_speedtree         = 0x02000000  #NOT FUNCTIONAL: we have removed speedtree support on M&B Warband

fkf_has_colony_props  = 0x04000000  # if fkf_has_colony_props -> then you can define colony_radius and colony_treshold of the flora kind

#MOD begin
tree_density = 5
tree_bush_density = 10
grass_density = 500
grass_bush_density = 125
rock_density = 2
#MOD end

def density(g):
  if (g > fkf_density_mask):
    g = fkf_density_mask
  return ((dword | g) << density_bits)


fauna_kinds = [
  ("grass",fkf_grass|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_plain|fkf_plain_forest|density(grass_density),
   [["grass_a","0"],["grass_b","0"],["ground_bush","0"]]),
	 
  ("grass_bush",fkf_grass|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_plain|density(grass_bush_density),
   [["grass_bush_c","0"],["grass_bush_e","0"],["grass_bush_i01","0"],["grass_bush_i02","0"]]),
	 
  ("grass_bush_forest",fkf_grass|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_plain_forest|fkf_steppe_forest|density(grass_bush_density),
   [["bushes04_a","0"],["bushes04_b","0"],["bushes04_c","0"],["bushes08_a","0"],["bushes08_b","0"],["grass_bush_a","0"],["grass_bush_b","0"],["grass_bush_h01","0"],["grass_bush_h02","0"]]),

  ("fern",fkf_grass|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_plain_forest|fkf_steppe_forest|density(grass_bush_density),
   [["fern_a","0"],["fern_b","0"]]),
	 
  ("grass_steppe",fkf_grass|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_steppe|fkf_steppe_forest|density(grass_density),
   [["grass_yellow_a","0"],["grass_yellow_b","0"],["grass_bush_k02","0"],["ground_bush","0"]]),
	 
  ("grass_bush_steppe",fkf_grass|fkf_guarantee|fkf_align_with_ground|fkf_point_up|fkf_steppe|fkf_steppe_forest|density(grass_bush_density),
   [["bushes06_a","0"],["bushes06_b","0"],["bushes06_c","0"],["grass_yellow_e","0"]]),



	("grass_bush_l",fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(grass_bush_density),
   [["grass_bush_l01","0"],["grass_bush_l02","0"]]),
  
  ("thorn_a",fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(grass_bush_density),
   [["thorn_a","0"],["thorn_b","0"],["thorn_c","0"],["thorn_d","0"]]),

  ("basak",fkf_align_with_ground|fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(grass_bush_density),
   [["basak","0"]]),
  ("common_plant",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(grass_bush_density),
   [["common_plant","0"],["ground_bush","0"]]),
  ("buddy_plant",fkf_plain|fkf_plain_forest|density(grass_bush_density),
   [["buddy_plant","0"],["buddy_plant_b","0"]]),
  ("yellow_flower",fkf_align_with_ground|fkf_plain|fkf_plain_forest|density(grass_bush_density),
   [["yellow_flower","0"],["yellow_flower_b","0"]]),
  ("spiky_plant",fkf_align_with_ground|fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(grass_bush_density),
   [["spiky_plant","0"]]),
  ("seedy_plant",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(grass_bush_density),
   [["seedy_plant_a","0"]]),
  ("blue_flower",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(grass_bush_density),
   [["blue_flower","0"]]),
  ("big_bush",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(grass_bush_density),
   [["big_bush","0"]]),

  ("bushes02_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["bushes02_a","0"],["bushes02_b","0"],["bushes02_c","0"]]),
  ("bushes03_a",fkf_plain|fkf_plain_forest|fkf_snow|fkf_snow_forest|density(tree_bush_density),
   [["bushes03_a","0"],["bushes03_b","0"],["bushes03_c","0"]]),
  ("bushes05_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["bushes05_a","0"]]),
  ("bushes07_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["bush_a01","0"],["bush_a02","0"]]),
  ("bushes09_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["bush_new_a","0"],["bush_new_c","0"]]),
  ("bushes10_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["dry_leaves","0"],["dry_bush","0"]]),
  ("bushes11_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["bushes11_a","0"],["bushes11_b","0"],["bushes11_c","0"]]),
  ("bushes12_a",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["bushes12_a","0"],["bushes12_b","0"],["bushes12_c","0"]]),

#Mod begin
#fir
  ("tree_fir_1",fkf_plain|fkf_plain_forest|fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   [
     ("tree_fir_1_a","bo_tree_fir_1_a",("0","0")),
     ("tree_fir_1_b","bo_tree_fir_1_b",("0","0")),
		 ("tree_fir_1_c","bo_tree_fir_1_c",("0","0")),
     ("tree_fir_1_d","bo_tree_fir_1_d",("0","0")),
		 ("tree_fir_1_e","bo_tree_fir_1_e",("0","0")),
     ]),
  ("tree_fir_2",fkf_plain|fkf_plain_forest|fkf_tree|density(tree_density),
   [
     ("tree_fir_2_a","bo_tree_fir_2_a",("0","0")),
     ("tree_fir_2_b","bo_tree_fir_2_b",("0","0")),
     ("tree_fir_2_c","bo_tree_fir_2_c",("0","0")),
		 ("tree_fir_2_d","bo_tree_fir_2_d",("0","0")),
     ("tree_fir_2_e","bo_tree_fir_2_e",("0","0")),
     ]),

  ("bushes_tree_fir",fkf_plain|fkf_plain_forest|fkf_snow|fkf_snow_forest|density(tree_bush_density),
   [
     ["bushes_tree_fir_a","0"],
     ["bushes_tree_fir_b","0"],
     ["bushes_tree_fir_c","0"],
     ]),
  
#pine
  ("tree_pine_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   [
     ("tree_pine_1_a","bo_tree_pine_1_a",("0","0")),
     ("tree_pine_1_b","bo_tree_pine_1_b",("0","0")),
		 ("tree_pine_1_c","bo_tree_pine_1_c",("0","0")),
     ("tree_pine_1_d","bo_tree_pine_1_d",("0","0")),
		 ("tree_pine_1_e","bo_tree_pine_1_e",("0","0")),
     ]),
  ("tree_pine_2",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   [
     ("tree_pine_2_a","bo_tree_pine_2_a",("0","0")),
     ("tree_pine_2_b","bo_tree_pine_2_b",("0","0")),
		 ("tree_pine_2_c","bo_tree_pine_2_c",("0","0")),
     ("tree_pine_2_d","bo_tree_pine_2_d",("0","0")),
		 ("tree_pine_2_e","bo_tree_pine_2_e",("0","0")),
     ]),

  ("bushes_tree_pine",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_snow|fkf_snow_forest|density(tree_bush_density),
   [
     ["bushes_tree_pine_a","0"],
     ["bushes_tree_pine_b","0"],
     ["bushes_tree_pine_c","0"],
     ]),

#ash
  ("tree_ash_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),
   [
     ("tree_ash_1_a","bo_tree_ash_1_a",("0","0")),
     ("tree_ash_1_b","bo_tree_ash_1_b",("0","0")),
     ("tree_ash_1_c","bo_tree_ash_1_c",("0","0")),
		 ("tree_ash_1_d","bo_tree_ash_1_d",("0","0")),
     ("tree_ash_1_e","bo_tree_ash_1_e",("0","0")),
     ]),

#aspen
  ("tree_aspen_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),
   [
     ("tree_aspen_1_a","bo_tree_aspen_1_a",("0","0")),
     ("tree_aspen_1_b","bo_tree_aspen_1_b",("0","0")),
     ("tree_aspen_1_c","bo_tree_aspen_1_c",("0","0")),
     ]),

#beech
  ("tree_beech_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),
   [
     ("tree_beech_1_a","bo_tree_beech_1_a",("0","0")),
     ("tree_beech_1_b","bo_tree_beech_1_b",("0","0")),
     ("tree_beech_1_c","bo_tree_beech_1_c",("0","0")),
		 ("tree_beech_1_d","bo_tree_beech_1_d",("0","0")),
     ("tree_beech_1_e","bo_tree_beech_1_e",("0","0")),
     ]),

#birch
  ("tree_birch_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),
   [
     ("tree_birch_1_a","bo_tree_birch_1_a",("0","0")),
     ("tree_birch_1_b","bo_tree_birch_1_b",("0","0")),
     ("tree_birch_1_c","bo_tree_birch_1_c",("0","0")),
     ("tree_birch_1_d","bo_tree_birch_1_d",("0","0")),
     ("tree_birch_1_e","bo_tree_birch_1_e",("0","0")),
     ]),

  ("bushes_tree_birch",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [
     ["bushes_tree_birch_a","0"],
     ["bushes_tree_birch_b","0"],
     ["bushes_tree_birch_c","0"],
     ]),

#elm
  ("tree_elm_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),
   [
     ("tree_elm_1_a","bo_tree_elm_1_a",("0","0")),
     ("tree_elm_1_b","bo_tree_elm_1_b",("0","0")),
     ("tree_elm_1_c","bo_tree_elm_1_c",("0","0")),
		 ("tree_elm_1_d","bo_tree_elm_1_d",("0","0")),
     ("tree_elm_1_e","bo_tree_elm_1_e",("0","0")),
     ]),

#oak
  ("tree_oak_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),
   [
     ("tree_oak_1_a","bo_tree_oak_1_a",("0","0")),
     ("tree_oak_1_b","bo_tree_oak_1_b",("0","0")),
     ("tree_oak_1_c","bo_tree_oak_1_c",("0","0")),
		 ("tree_oak_1_d","bo_tree_oak_1_d",("0","0")),
     ("tree_oak_1_e","bo_tree_oak_1_e",("0","0")),
     ]),

  
#snowy 
  # ("snowy_fir_1",fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   # [
     # ("snowy_fir_1_a","bo_snowy_fir_1_a",("0","0")),
		 # ("snowy_fir_1_b","bo_snowy_fir_1_b",("0","0")),
		 # ("snowy_fir_1_c","bo_snowy_fir_1_c",("0","0")),
		 # ("snowy_fir_1_d","bo_snowy_fir_1_d",("0","0")),
     # ]),
  # ("snowy_fir_2",fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   # [
     # ("snowy_fir_2_a","bo_snowy_fir_2_a",("0","0")),
     # ("snowy_fir_2_b","bo_snowy_fir_2_b",("0","0")),
     # ("snowy_fir_2_c","bo_snowy_fir_2_c",("0","0")),
		 # ("snowy_fir_2_d","bo_snowy_fir_2_d",("0","0")),
     # ("snowy_fir_2_e","bo_snowy_fir_2_e",("0","0")),
     # ]),
  # ("snowy_tree_1",fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   # [
     # ("snowy_tree_1_a","bo_snowy_tree_1_a",("0","0")),
		 # ("snowy_tree_1_b","bo_snowy_tree_1_b",("0","0")),
		 # ("snowy_tree_1_c","bo_snowy_tree_1_c",("0","0")),
		 # ("snowy_tree_1_d","bo_snowy_tree_1_d",("0","0")),
		 # ("snowy_tree_1_e","bo_snowy_tree_1_e",("0","0")),
     # ]),

  # ("bush_snowy_1",fkf_snow|fkf_snow_forest|density(tree_bush_density),
   # [
     # ["bush_snowy_1_a","0"],
     # ["bush_snowy_1_b","0"],
     # ["bush_snowy_1_c","0"],
     # ]),
  # ("bush_snowy_2",fkf_snow|fkf_snow_forest|density(tree_bush_density),
   # [
     # ["bush_snowy_2_a","0"],
     # ]),
  # ("bush_snowy_3",fkf_snow|fkf_snow_forest|fkf_align_with_ground|fkf_point_up|density(tree_bush_density),
   # [
     # ["bush_snowy_3_a","0"],
     # ["bush_snowy_3_b","0"],
     # ["bush_snowy_3_c","0"],
     # ["bush_snowy_3_d","0"],
     # ]),

#dead
  ("dead_tree_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   [
     ("dead_tree_1_a","bo_dead_tree_1_a",("0","0")),
     ("dead_tree_1_b","bo_dead_tree_1_b",("0","0")),
     ("dead_tree_1_c","bo_dead_tree_1_c",("0","0")),
		 ("dead_tree_1_d","bo_dead_tree_1_d",("0","0")),
     ("dead_tree_1_e","bo_dead_tree_1_e",("0","0")),
     ]),
  ("dead_tree_2",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_snow|fkf_snow_forest|fkf_tree|density(tree_density),
   [
     ("dead_tree_2_a","bo_dead_tree_2_a",("0","0")),
     ("dead_tree_2_b","bo_dead_tree_2_b",("0","0")),
     ("dead_tree_2_c","bo_dead_tree_2_c",("0","0")),
		 ("dead_tree_2_d","bo_dead_tree_2_d",("0","0")),
     ("dead_tree_2_e","bo_dead_tree_2_e",("0","0")),
     ]),
#Mod end
  
  ("small_rock",fkf_align_with_ground|fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_desert|fkf_desert_forest|fkf_snow|fkf_snow_forest|fkf_rock|density(rock_density),
   [["rock_c","bo_rock_c"],["rock_d","bo_rock_d"],["rock_e","bo_rock_e"],["rock_f","bo_rock_f"],["rock_g","bo_rock_g"],["rock_h","bo_rock_h"],["rock_i","bo_rock_i"],["rock_k","bo_rock_k"]]),
  # ("rock_snowy",fkf_snow|fkf_snow_forest|fkf_realtime_ligting|fkf_rock|density(rock_density),
   # [["rock_snowy_a","bo_rock_snowy_a"],["rock_snowy_b","bo_rock_snowy_b"],["rock_snowy_c","bo_rock_snowy_c"],]),

  ("rock",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_desert|fkf_desert_forest|fkf_snow|fkf_snow_forest|fkf_rock|density(rock_density),
   [["rock1","bo_rock1"],["rock2","bo_rock2"],["rock3","bo_rock3"],["rock4","bo_rock4"],["rock5","bo_rock5"],["rock6","bo_rock6"],["rock7","bo_rock7"]]),
  # ("rock_snowy2",fkf_snow|fkf_snow_forest|fkf_realtime_ligting|fkf_rock|density(rock_density),
   # [["rock1_snowy","bo_rock1"],["rock2_snowy","bo_rock2"],["rock4_snowy","bo_rock4"],["rock6_snowy","bo_rock6"],]),


  ("palm",fkf_desert|fkf_desert_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),
   [("palm_a","bo_palm_a",("0","0"))]),
	("desert_plant",fkf_desert|fkf_desert_forest|fkf_steppe|fkf_steppe_forest|fkf_tree|density(tree_density),#MOD
   [("desert_plant_a","0",("0","0"))]),

  # ("bush_new_1",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   # [["bush_a01","0"],["bush_a02","0"]]),
  # ("bush_new_2",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   # [["bush_new_a","0"]]),
  # ("bush_new_3",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   # [["bush_a02","0"]]),
  # ("bush_new_4",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   # [["bush_new_c","0"]]),

  ("dry_bush",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|fkf_snow|fkf_snow_forest|density(tree_bush_density),
   [["dry_bush","0"]]),
  ("dry_leaves",fkf_plain|fkf_plain_forest|fkf_steppe|fkf_steppe_forest|density(tree_bush_density),
   [["dry_leaves","0"]]),


  ("grape_vineyard",density(0),
   [("grape_vineyard","bo_grape_vineyard")]),
  ("grape_vineyard_stake",density(0),
   [("grape_vineyard_stake","bo_grape_vineyard_stake")]), 
  
  ("wheat", density(0),
   [["wheat_a","0"],["wheat_b","0"],["wheat_c","0"],["wheat_d","0"]]),
  ("wheat_field", density(0),
   [["wheat_field","0"]]),
  
  ("valleyRock_rounded",fkf_rock|density(rock_density),
   [["valleyRock_rounded_1","bo_valleyRock_rounded_1"],["valleyRock_rounded_2","bo_valleyRock_rounded_2"],["valleyRock_rounded_3","bo_valleyRock_rounded_3"],["valleyRock_rounded_4","bo_valleyRock_rounded_4"]]),
  ("valleyRock_flat",fkf_rock|density(rock_density),
   [["valleyRock_flat_1","bo_valleyRock_flat_1"],["valleyRock_flat_2","bo_valleyRock_flat_2"],["valleyRock_flat_3","bo_valleyRock_flat_3"],["valleyRock_flat_4","bo_valleyRock_flat_4"],["valleyRock_flat_5","bo_valleyRock_flat_5"],["valleyRock_flat_6","bo_valleyRock_flat_6"]]),
  ("valleyRock_flatRounded_small",fkf_rock|density(rock_density),
   [["valleyRock_flatRounded_small_1","bo_valleyRock_flatRounded_small_1"],["valleyRock_flatRounded_small_2","bo_valleyRock_flatRounded_small_2"],["valleyRock_flatRounded_small_3","bo_valleyRock_flatRounded_small_3"]]),
  ("valleyRock_flatRounded_big",fkf_rock|density(rock_density),
   [["valleyRock_flatRounded_big_1","bo_valleyRock_flatRounded_big_1"],["valleyRock_flatRounded_big_2","bo_valleyRock_flatRounded_big_2"]]),
]


def save_fauna_kinds():
  file = open("./flora_kinds.txt","w")
  file.write("%d\n"%len(fauna_kinds))
  for fauna_kind in fauna_kinds:
    meshes_list = fauna_kind[2]
    file.write("%s %d %d\n"%(fauna_kind[0], (dword_mask & fauna_kind[1]), len(meshes_list)))
    for m in meshes_list:
      file.write(" %s "%(m[0]))
      if (len(m) > 1):
        file.write(" %s\n"%(m[1]))
      else:
        file.write(" 0\n")
      if ( fauna_kind[1] & (fkf_tree|fkf_speedtree) ):  #if this fails make sure that you have entered the alternative tree definition (NOT FUNCTIONAL in Warband)
        speedtree_alternative = m[2]
        file.write(" %s %s\n"%(speedtree_alternative[0], speedtree_alternative[1]))
    if ( fauna_kind[1] & fkf_has_colony_props ):
      file.write(" %s %s\n"%(fauna_kind[3], fauna_kind[4]))
  file.close()

def two_to_pow(x):
  result = 1
  for i in xrange(x):
    result = result * 2
  return result

fauna_mask = 0x80000000000000000000000000000000
low_fauna_mask =             0x8000000000000000
def save_python_header():
  file = open("./fauna_codes.py","w")
  for i_fauna_kind in xrange(len(fauna_kinds)):
    file.write("%s_1 = 0x"%(fauna_kinds[i_fauna_kind][0]))
    file.write("%x\n"%(fauna_mask | two_to_pow(i_fauna_kind)))
    file.write("%s_2 = 0x"%(fauna_kinds[i_fauna_kind][0]))
    file.write("%x\n"%(fauna_mask | ((low_fauna_mask|two_to_pow(i_fauna_kind)) << 64)))
    file.write("%s_3 = 0x"%(fauna_kinds[i_fauna_kind][0]))
    file.write("%x\n"%(fauna_mask | ((low_fauna_mask|two_to_pow(i_fauna_kind)) << 64) | two_to_pow(i_fauna_kind)))
  file.close()

print ("Exporting flora data...")
save_fauna_kinds()
